<?php require __DIR__. './__connect_db.php' ?>
<?php 
// session_start();
$page_name='compare';

//取得資料
if(! empty($_SESSION['compare'])){
$detail = array_keys($_SESSION['compare']);
//20190214 10:16 調整 --------Rita
$b_sql = sprintf("SELECT * FROM `lunggage_data` ld JOIN `product_list` pl ON ld.`SID`= pl.`type_sid` JOIN `color_mapping` cm ON cm.`color_sid`=pl.`color_sid`  WHERE pl.`sid`  IN ('%s') ",implode("','", $detail));
$b_stmt = $pdo->query($b_sql);
$detail_data = [];
while($a = $b_stmt->fetch(PDO::FETCH_ASSOC)){

    $detail_data[$a['sid']] = $a;
    }
}

// header('Content-Type: text/plain');
// print_r($_SESSION['compare']);
// print_r($detail_data);
// exit;

?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>比較商品</title>
    <link href="https://fonts.googleapis.com/css?family=Charmonman|Noto+Sans+TC" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style-wawa.css">
    <link rel="stylesheet" href="./css/navigation.css">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');

        html {
            font-size: 16px;
        }

        body {
            /*
            font-family: 'Marko One', serif;
            font-family: 'Mukta Malar', sans-serif;
            font-family: 'Merriweather', serif;
            */
            font-family: 'Noto Sans TC', sans-serif;
            font-size: 1rem;
            color: #4d5258;
        }

        .wrapper {
            flex-direction: column;
        }

        /* ---------------------------Wawa tamp */
        .con-1440 {
            max-width: 75%;
            margin: 0 auto;
        }

        /* ---------special compare tamp */

        .empty{
            font-size: 1.5rem;
            height:10vh;
            padding-top:100px;
            text-align: center;
            color:#cfb06d;
        }
        .non_comparebg{
            width:100%;
            /* background:url("./images/noncompare_bg.jpg") center center; */
            background-size:cover;
            text-align:center;
        }
        .c-li-lineh {
            line-height: 1.2rem;
            list-style-position: outside;
            margin-left: 20px;
            padding-top: 10px;
            list-style-type:disc;
        }
        .compare_data li{
            line-height: 1.2rem;
            list-style-position: outside;
            margin-left: 20px;
            padding-top: 10px;
            list-style-type:disc;
        }
        .delete_btn{
            top: 0;
            right: 0;
        }
        .wish .btn{
            color:#4d5258;
        }

        /* ----------------------section top */
        section{
            font-size:20px;
        }
        .top_img {
            width: 100%;
            height: 350px;
            background: url("./images/compare_bg.jpg") center center no-repeat;
            background-size: cover;
            /* background-attachment: fixed; */
        }

        .compare_top {
            padding: 40px 0;
        }

        .square {
            width: 190px;
            height: 190px;
        }

        .compare_title .inner-text {
            font-size: 4rem;
            border-top: 5px solid #818E9B;
            border-bottom: 5px solid #818E9B;
            /* margin-right: 32px; */
            margin-top: 104px;
            padding: 25px 0;
            font-weight: 700;
            color: #818E9B;
            /* border: 1px solid #f00; */
        }

        .compare_item {
            width: 25%;
            
            margin-left: 48px;
            padding: 32px;
            min-width: 130px;
            flex: 1;
            /* border: 1px solid #f00; */
        }

        .compare_item img {
            width: 70%;
        }

        .compare_item .p1 {
            height:4vw;
            font-size: 2vw;
            margin-bottom: 4px;
        }

        .compare_item .p2 {
            height: 2vw;
        }

        .compare_item .btn {
            width: 75%;
            border: 1px solid #818E9B;
            border-radius: 5px;
            padding: 5px;
            margin: 16px auto;
            cursor: pointer;
        }

        .compare_item .btn1 {
            margin-top: 10px;
            background: #2c3e50;
            color: #fff;
        }
        /* 20190215 14:19更新 */
        .compare_item{
         height:50%;   
        }
        /* ----------------------section bottom */
        .compare_bottom {
            padding: 40px 0;
            flex-direction: column;
        }

        .border-line {
            border: 1px solid #ccc;
            width: 90vw;
            margin: 0 auto;
        }

        .compare_m-l {
            width: 25vw;
            margin-left: 90px;
            padding: 32px;
            min-width: 130px;
        }
        /* 測試背景線 */
        .hover-line {
            width: 100%;
            height: 32px;
            background: #ccc;
            top: 90px;
            z-index: -2;
        }

        /* ----------------------- */
        .line {
            line-height: 3rem;
            flex: 1;
            width: 100%;
        }

        .line:hover {
            background: #ccc;
        }

        .info_pdt {
            min-width: 190px;
            /* border: 1px solid #f00; */
        }

        .blank {
            width: 85%;
        }

        .compare_data {
            width: 25%;
            margin-left: 48px;
            /* padding: 0 32px; */
            min-width: 130px;
            flex: 1;
            /* border: 1px solid #f00; */
        }

        .compare_data ul {
            padding-top: 10px;
        }

        /* ------------------------手機版顯示關閉調整區 */
        .mobile_flex {
            display: none;
        }

        .compare_cate {
            display: none;
        }

        /* 刪除商品alert與他頁同步 2019/2/17 04:01 Rita更新 */
        .cp-delete-bg{
                width: 100%;
                height: 100vh;
                background-color: #000000;
                position: fixed;
                z-index: 97;
                top: 0;
                left: 0;
                opacity: 0.7;
        }
        .cp-alert {
                padding: 20px;
                color: #ffffff;
                background-color: #c67b8a;
                position: fixed;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                z-index: 98;
                font-size: 1.2rem;
                letter-spacing: 2px;
                box-shadow: 1px 3px 10px #414449;
                transition: 0.5s;
                min-width: 450px;
                min-height: 180px;
                justify-content: center;
                align-items: center;
            }
            .cp-alert p{
                text-align:center;
                line-height:150px;
                box-sizing:border-box;
            }

        /* -------------------------------------------------compare RWD */
        @media screen and (max-width:1120px) {
            .compare_title {
                left: -20px;
            }

            .compare_item {
                margin-left: 24px;
            }

            .compare_data {
                margin-left: 24px;
            }
        }

        @media screen and (max-width:975px) {
            .compare_title {
                min-width: 100px;
                left: 0;
                margin: 0 auto;
            }

            .square {
                width: 100px;
                height: 100px;
            }

            .rwd1 {
                max-width: 90%;
            }

            .compare_title .inner-text {
                font-size: 2.5rem;
                margin-top: 70px;
                padding: 15px 0;
            }

            .line {
                min-width: 100px;
                left: 0;
                margin: 0 auto;
            }

            .line:hover {
                background: transparent;
            }

            .info_pdt {
                text-align: left;
                min-width: 100px;
                width: auto;
            }
        }

        @media screen and (max-width:830px) {
            .top_img {
                height: 250px;
            }

            .compare_top {
                flex-direction: column;
            }

            .compare_item {
                width: 33%;
                margin-left: 12px;
            }

            .rwd_item.rwd1 {
                max-width: 100%;
            }

            .square {
                width: 500px;
                height: 100px;
            }

            .compare_title .inner-text {
                font-size: 2.5rem;
                margin-top: 70px;
                padding: 15px 0;
                border-top: none;
                border-bottom: none;
            }
            /* ------商品字體與空間 */
            .compare_item .p1{
                max-height:76px;
                font-size: 1rem;
            }
            .compare_item .p2{
                max-height:30px;
            }
            .compare_item .btn1{
                margin-top:30px;
            }
            /* ------------bottom */
            .compare_bottom {
                padding: 20px 0;
            }

            .compare_cate {
                display: flex;
            }

            .data_area p {
                line-height: 3rem;
                border-bottom: 1px solid #4d5258;
            }

            .data_area p::after {
                content: '       v        ';
                font-size: 18px;
            }

            .line {
                flex-direction: column;
            }

            .info_pdt {
                text-align: center;
                margin: 0 auto;
                width: 100%;
                background: rgb(223, 234, 245);
                /* display: none; */
            }

            .blank {
                width: 100%;
                flex-direction: row;
            }

            .compare_data {
                width: 33%;
                margin-left: 12px;
            }

        }

        @media screen and (max-width:630px) {
            .rwd1 {
                max-width: 100%;
            }

            .mobile_none {
                display: none;
            }

            .mobile_flex {
                display: flex;
            }

            .top_img {
                height: 150px;
            }

            .fixedmenu{
                position: fixed;  
                top: 0px;  
                z-index: 98;  
            }
            .compare_top {
                /* position: relative; */
                /* top: 60px; */
                background: #fff;
                /* z-index: 5; */
                padding: 10px;
                margin-bottom:15px;
            }

            .compare_item {
                margin: 0;
                padding: 8px;
                min-width: 80px;
            }

            .compare_title {
                align-items: flex-start;
                width: 2rem;
            }

            .compare_title .inner-text {
                width: 2rem;
                font-size: 2rem;
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 10px;
                margin-top: 10px;
            }

            .compare_icon img {
                width: 30px;
                height: 30px;
                margin: 0 auto;
            }
            .empty1{
                    padding-top:80%;
                    color:#cfb06d;
                    flex-direction:column;
                    
            }
            .non_comparebg{
                height:100vh;
                background:url("./images/noncompare_bg.jpg") center center;
            }

            /* ------------bottom */
            .compare_data {
                margin: 0;
                padding: 8px;
                min-width: 80px;
                text-align: center;
            }
            @media screen and (max-width:519px){
                .compare_icon img{
                    width: 20px;
                    height: 20px;
                }
                
                .nc-img{
                    margin-top:30px;
                    font-size:1.5rem;
                    color:#cfb06d
                }
            }
    
        }
            @media screen and (max-width: 630px) {
                .info-alert {
                    min-width: 80vw;
                    min-height: 80vw;
                }

            }
    </style>
</head>

<body>
<?php include __DIR__. './__navbar.php' ?>
<?php include __DIR__. './__html__label.php' ?>
    <?php //--------------------start----------------?>
    <?php if(empty($detail_data)): ?>
    <?php $url = "./index_commodity.php"; ?> 
    <meta http-equiv="refresh" content="3;url=<?php echo $url; ?>">
    <div class="non_comparebg">
    <div class="top_img relative mobile_none"></div>
        <div class="empty con-1440 mobile_none">
            尚未加入商品進入比較清單，3秒後將前往商品列表頁。
        </div>
        
        <div class="empty1 con-1440 mobile_flex">
            尚未加入商品進入比較清單，3秒後將前往商品列表頁。
            <i class="nc-img fas fa-undo"></i>
        </div>
        
        </div>
    <?php else: ?>
    <div id="cp-delete" class="d-none">
        <div class="cp-delete-bg"></div>
        <div id="cp-info" class="cp-alert">
        <p>已移除商品</p>
        </div>
    </div>
    <div class="top_img relative"></div>
    <section>
        <div class="wrapper relative">
            <div class="compare_top d-flex con-1440 justify-center rwd1">
                <div class="compare_title text-center relative mobile_none justify-center rwd1">
                    <div class="square">
                        <p class="inner-text">商品比較</p>
                    </div>
                </div>
                <!--比較商品上方三項 手機版會顯示上面兩個compare_item-->
                <!-- php撈資料 -->
                
                <div class="rwd_item d-flex relative justify-center rwd1">

                <?php //$row1 = $b_stmt->fetch(PDO::FETCH_ASSOC) ?>
                <?php foreach ($detail as $a){
                            $compare_item = $detail_data[$a] ?>
                    <div class="compare_item text-center relative product<?= $compare_item['sid'] ?>">
                    
                        <img src="./images/product/<?= $compare_item['pic_nu'] ?>" alt="">
                    <a class="nonstyle-a delete_btn absolute" style="color:black;" href="javascript:void(0); remove_citem(<?= $compare_item['sid'] ?>);">&times;</a>
                        <p class="p1 product<?= $compare_item['sid'] ?>"><?= $compare_item['brand'] ?></p>
                        
                        <p class="p2 product<?= $compare_item['sid'] ?>"><?= $compare_item['size_text']?></p>
                        <button class="btn btn1 text-center  add_to_cart_btn" data-sid="<?= $compare_item['sid'] ?>">加入購物車</button>
                        <a href="javascript:void(0)" class="wish heartShape nonstyle-a" data-sid="<?= $compare_item['sid'] ?>"><div class="btn">加入願望清單</div></a>
                        <!--
                        <div class="mobile_flex compare_icon">
                            <a class="nonstyle-a add_to_cart_btn" href=""><img src="./images/icon_sm_shopbag.svg" alt=""></a>
                            <a class="nonstyle-a" href="javascript:void(0)"><img class="wish heartShape" data-sid="<?= $compare_item['sid'] ?>" src="./images/icon-love-1.svg" alt=""></a>
                        </div>
                        -->
                    </div>
                    <?php } ?>
                </div>
            </div>

            
            <div class="border-line"></div>
            <div class="compare_bottom d-flex relative con-1440 rwd1">
                <div class="data_area justify-center">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">基礎資訊</p>
                    <div class="line d-flex relative">
                        
                        <div class="info_pdt text-center">價格</div>
                        
                        <div class="blank d-flex">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data text-center product<?= $compare_item['sid'] ?>"mairgin-0auto><?= $compare_item['price'] ?></div>
                            
                            <?php } ?>
                        </div>
                        
                    </div>

                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">尺寸</div>
                        
                        <div class="blank d-flex">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data text-center product<?= $compare_item['sid'] ?>"><?= $compare_item['size_text'] ?></div>
                            
                            <?php } ?>
                        </div>
                        
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">款式</div>
                        
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['border']?></div>
                            
                            <?php } ?>
                        </div>

                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">容量</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['insideL']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="data_area">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">詳細規格</p>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">重量</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['kg']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">箱體</div>

                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['box']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">材質</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['texture']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">長寬高</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['cm']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">海關鎖</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['tsa']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">輪子</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['roll']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    </div>
                <div class="data_area">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">特色功能</p>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">擴充</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?= $compare_item['expan']?></div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">煞車</div>
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>"><?= $compare_item['brake']?></div>
                           
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative">
                            <div class="info_pdt text-center">保固</div>
                            <div class="blank d-flex text-center">
                            <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                                <div class="compare_data product<?= $compare_item['sid'] ?>"><?=$compare_item['warranty']?></div>
                                
                                <?php } ?>
                            </div>
                        </div>
                    <div class="line d-flex relative">
                        <div class="info_pdt text-center">其他細節</div>
                        
                        <div class="blank d-flex text-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['sid'] ?>">
                                <ul>
                                <?=$compare_item['anno']?>
                                </ul>
                            </div>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php endif; ?>
    <?php //----------------------end--------------?>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script>
        
        var deItem = function(){
        var cpDelete = $('#cp-delete');
            cpDelete.removeClass('d-none');

            setTimeout(function () {
                cpDelete.addClass('d-none');
            }, 1000);
            };
        //加入購物車 比較 願望清單開始

        //點擊加入購物車
        $('.add_to_cart_btn').click(function(){
            var card = $(this);
            var qty = 1;
            var sid = card.attr('data-sid');
            $.get('add_to_cart_api.php', {sid:sid, qty:qty,add:true}, function(data){
                // alert('商品加入購物車');
                // alert(sid + "::" + qty);
                cart_count(data);//泡泡選單
                addClick();
            },'json');
        });

        //加入願望清單
        $('.heartShape').click(function(){
            var card = $(this);
            var sid = card.attr('data-sid');
            var add = ($(".heartShape").attr("src") == "./images/icon-love-1.svg")? true: false;
            $.get('add_to_wishlist_api.php', {sid:sid,add:true}, function(data){
                if(data.success) {
                    addClick();
                };
            },'json');
        });
        
        //navigation 手機板menu展開滑動右向左
        $('.nav_menu').click(function(){
            $('#click_menu').css('display', 'block');
            $('.menu_page').animate({
                width: '100%'
            }, 300);
        });
        //navigation 手機板menu關閉
        $('#close_menu').click(function(){
            $('.menu_page').animate({
                width: '0'
            }, 300);
            setTimeout(function(){
                $('#click_menu').css('display','none');
            },400,);
        });

        // 比較項目滑開關閉
        $('.slide-toggle').click(function () {
            $(this).siblings().slideToggle(500);
        });

        //移除項目
        var remove_citem = function(sid){
            deItem();
            $('.product'+sid).remove();
            $.get('add_to_compare_api.php', {sid:sid}, function(data){
                // location.reload();
                cart_count(data);
                $('.product'+sid).remove();
                setTimeout(function () {
                    location.reload();
                }, 500);
            }, 'json');
        };
        //2019/02/15 16:30 加入標籤
        function addClick(){
            // window.label.show();
                $('#label').css("border-radius", "25px 0 0 25px");//變成圓形
                $('#label').css("right", "0");

            setTimeout(function (){
                $('#label').attr("disabled",false);
                $('#label').css("border-radius", "5px 0 0 5px");
                $('#label').css("right", "-100px");
            },900);
        };

    </script>
<!--0214:先裝上footer-->
<?php include __DIR__. '/__html__footer.php' ?>