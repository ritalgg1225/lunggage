<?php
require __DIR__ . '/__connect_db.php';
$page_name = 'wishlist';
$page_title = '願望清單';

if (!isset($_SESSION['user'])) {
    header('Location: ./');
    exit;
}
$wish_sql = "SELECT * FROM `wishlist`
JOIN `product_list`
ON `product_list`.`sid` = `wishlist`.`list_id`
JOIN `lunggage_data`
ON `product_list`.`type_sid` = `lunggage_data`.`SID`
JOIN `color_mapping`
ON `product_list`.`color_sid` = `color_mapping`.`color_sid`
WHERE `member_id`=?";
$wish_stmt = $pdo->prepare($wish_sql);
$wish_stmt->execute([
    $_SESSION['user']['id']
]);

$wish_detial = $wish_stmt->fetchAll(PDO::FETCH_ASSOC);


?>
    <!DOCTYPE html>
    <html lang="zh">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/style-wawa.css">
        <link rel="stylesheet" href="css/member-layout.css">
        <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
        <title>會員中心 - 願望清單</title>
        <style>

            /* ----------------------------------------------------- member RWD */
            @media screen and (max-width: 1240px) {

            }

            @media screen and (max-width: 1024px) {
                .member-wrap {
                    max-width: 80vw;
                }

            }

            @media screen and (max-width: 930px) {
                .wishlist-item-nameNsize {
                    flex-direction: column;
                    align-items: flex-start;
                }

                .wishlist-sizeNcolor > div {
                    padding-right: 20px;
                }

            }

            @media screen and (max-width: 768px) {
                .member-wrap {
                    max-width: 90vw;
                }

                .member-sub {
                    transform: rotate(0deg);
                    top: -45px;
                    left: 0;
                    font-size: 45px;
                    max-width: 90vw;
                }

                .member-nav {
                    justify-content: center;
                }

                .member-row .mb-1-6 > div {
                    margin: 0 20px;
                }

                .member-row .mb-1-6 > div span {
                    display: none;
                }

                .member-row .mb-1-6 > div a {
                    border: 1px solid #4d5258;
                    padding: 5px 20px;
                    border-radius: 21px;
                }

                .m-y-10 p {
                    padding-left: 0;
                }

                .member-wishlist {
                    width: 90%;
                }

                .member-scroll {
                    width: 100%;
                }

                .member-row {
                    height: auto;
                }

            }

            @media screen and (max-width: 520px) {
                .member-row {
                    min-height: 0;
                    margin-bottom: -50px;
                }

                .member-sub {
                    font-size: 35px;
                    top: -40px;
                }

                .member-nav {
                    margin-top: 130px;
                }

                .member-nav ul li {
                    padding: 0 10px;
                }

                .m-y-10 p {
                    font-size: 1.15rem;
                }

                .wishlist-item {
                    padding: 0;
                }

                .wishlist-item figure {
                    width: 50%;
                    margin: 15px 0;
                }

                .wishlist-item-detial {
                    width: 50%;
                    flex-direction: column;
                    align-items: flex-start;
                }

                .wishlist-btn {
                    display: flex;
                }

                .wishlist-move i,
                .wishlist-dele i {
                    display: none;
                }

                .wishlist-move,
                .wishlist-dele {
                    display: flex;
                    width: 80px;
                    margin-right: 8px;
                    padding: 0 5px;
                    align-items: center;
                    justify-content: center;
                }

                .wishlist-move a,
                .wishlist-dele a {
                    padding: 5px 10px;
                }

                .wishlist-nothing-pic {
                    display: none;
                }

                .wishlist-nothing {
                    min-height: 30vh;
                }


            }

            @media screen and (max-width: 400px) {

                .member-sub {
                    font-size: 30px;
                    top: -35px;
                }

                .member-nav ul li {
                    padding: 0 10px;
                    letter-spacing: 4px;
                }

                .m-y-10 p {
                    font-size: 1.1rem;
                }

                .wishlist-move,
                .wishlist-dele {
                    width: 70px;
                    margin-right: 4px;
                }

                .wishlist-move a,
                .wishlist-dele a {
                    font-size: 0.8rem;
                }
            }

            /*--------------------------info通知-----*/

            .info-wrap-bg {
                width: 100%;
                height: 100vh;
                background-color: #000000;
                position: fixed;
                z-index: 97;
                top: 0;
                left: 0;
                opacity: 0.7;
            }

            .info-alert {
                padding: 20px;
                color: #ffffff;
                background-color: #c67b8a;
                position: fixed;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                z-index: 98;
                font-size: 1.2rem;
                letter-spacing: 2px;
                box-shadow: 1px 3px 10px #414449;
                transition: 0.5s;
                min-width: 450px;
                min-height: 180px;
                justify-content: center;
                align-items: center;
            }

            @media screen and (max-width: 630px) {
                .info-alert {
                    min-width: 80vw;
                    min-height: 80vw;
                }

            }
        </style>
    </head>

<body>
<?php include __DIR__ . '/__navbar.php' ?>
<?php include __DIR__ . './__html__label.php' ?>
    <div id="info-wrap" class="d-none">
        <div class="info-wrap-bg"></div>
        <div id="info" class="info-alert d-flex"></div>
    </div>
    <section>
        <div class="member-bg"></div>
        <div class="con-1440 member-wrap">
            <div class="ff-marko member-sub">
                MEMBER CENTER
            </div>
            <div class="member-nav d-flex">
                <ul class="d-flex nonstyle-ul">
                    <li><a class="" href="member.php">我的帳戶</a></li>
                    <li><a class="" href="member_order.php">訂單查詢</a></li>
                    <li class="active"><a class="active" href="#">願望清單</a></li>
                </ul>
            </div>
            <div class="member-row">
                <h2 class="fw-300">願望清單</h2>
                <?php if (empty($wish_detial)): ?>
                    <div class="mb-5-6 wishlist-nothing d-flex">
                        <h2 class="fw-400">目前沒有喜愛商品</h2>
                        <a href="./index_commodity.php"><i class="fas fa-arrow-circle-right"></i> 去購物吧</a>
                        <div class="wishlist-nothing-pic mb-4-5">
                            <figure class="mb-1-5">
                                <img src="images/product/AAA0004_01.jpg" alt="">
                                <ul>
                                    <p class="ff-mukta fw-700">AWAY</p>
                                    <li class="ff-mukta">The Carry-On</li>
                                    <li class="ff-mukta">$8900</li>
                                </ul>
                            </figure>
                            <figure class="mb-1-5">
                                <img src="images/product/AAA0003_01.jpg" alt="">
                                <ul>
                                    <p class="ff-mukta fw-700">AWAY</p>
                                    <li class="ff-mukta">The Carry-On</li>
                                    <li class="ff-mukta">$8900</li>
                                </ul>
                            </figure>
                            <figure class="mb-1-5">
                                <img src="images/product/AAA0004_01.jpg" alt="">
                                <ul>
                                    <p class="ff-mukta fw-700">AWAY</p>
                                    <li class="ff-mukta">The Carry-On</li>
                                    <li class="ff-mukta">$8900</li>
                                </ul>
                            </figure>
                            <figure class="mb-1-5">
                                <img src="images/product/AAA0003_01.jpg" alt="">
                                <ul>
                                    <p class="ff-mukta fw-700">AWAY</p>
                                    <li class="ff-mukta">The Carry-On</li>
                                    <li class="ff-mukta">$8900</li>
                                </ul>
                            </figure>
                            <figure class="mb-1-5">
                                <img src="images/product/AAA0006_01.jpg" alt="">
                                <ul>
                                    <p class="ff-mukta fw-700">AWAY</p>
                                    <li class="ff-mukta">The Carry-On</li>
                                    <li class="ff-mukta">$8900</li>
                                </ul>
                            </figure>
                        </div>
                    </div>
                <?php else: ?>
                    <!-- ------重複區塊 start ----- -->
                    <?php
                    foreach ($wish_detial as $wk) {

                        ?>
                        <div class="mb-5-6 member-wishlist wish-item" data-sid="<?= $wk['list_id'] ?>">
                            <div class="wishlist-item d-flex">
                                <figure class="">
                                    <a href="./product.php?sid=<?= $wk['SID'] ?>">
                                    <img src="./images/product/<?= $wk['pic_nu'] ?>" alt="">
                                    </a>
                                </figure>

                                <div class="d-flex wishlist-item-detial flex-1">
                                    <div class="d-flex wishlist-item-nameNsize flex-1">
                                        <ul class="flex-1">
                                            <li class="wishlist-brand"><?= $wk['brand'] ?></li>
                                            <li class="wishlist-type"><?= $wk['type'] ?></li>
                                            <li class="wishlist-price">NT$ <span><?= $wk['price'] ?></span></li>
                                        </ul>
                                        <div class="wishlist-sizeNcolor d-flex flex-1">
                                            <div class="flex-1">
                                                <p><?= $wk['size'] ?> 吋</p>
                                            </div>
                                            <div class="flex-1">
                                                <select name="" id="" class="wishlist-color">
                                                    <option value=""><?= $wk['color'] ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wishlist-btn ">
                                        <div class="wishlist-move">
                                            <a class="nonstyle-a move-to-cart-btn" href="javascript:">移至購物車 <i class="fas fa-shopping-cart"></i></a>
                                        </div>
                                        <div class="wishlist-dele">
                                            <a class="nonstyle-a delete-wish-btn" href="javascript:">刪除 <i
                                                        class="far fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ------重複區塊 end ----- -->
                    <?php } ?>
                <?php endif; ?>
                <div class="m-b-50"></div>
            </div>
        </div>
    </section>

    <script>
        var info = $('#info');
        var infoWrap = $('#info-wrap');


        $('.delete-wish-btn').click(function () {
            var wishlist = $(this).closest('.wish-item');
            var sid = wishlist.attr('data-sid');

            $.get('delete_wishlist.php', {sid: sid}, function (data) {
                if (data.success) {
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }

                if (data.info) {
                    infoWrap.removeClass('d-none');
                    info.html(data.info);

                    setTimeout(function () {
                        infoWrap.addClass('d-none');
                    }, 1000);
                }

            }, 'json');
        });

        $('.move-to-cart-btn').click(function () {
            var wishlist = $(this).closest('.wish-item');
            var sid = wishlist.attr('data-sid');
            var qty = 1;

            $.get('move_to_cart_api.php', {sid: sid,qty:qty,add:true}, function (data) {
                cart_count(data);
            }, 'json');

            $.get('delete_wishlist.php', {sid: sid}, function (data) {
                addClick();
                if (data.success) {
                    setTimeout(function () {
                        location.reload();
                    }, 900);
                }

            }, 'json');
        });

        function addClick(){
            $('#label').css("border-radius", "25px 0 0 25px");//變成圓形
            $('#label').css("right", "0");

            setTimeout(function (){
                $('#label').attr("disabled",false);
                $('#label').css("border-radius", "5px 0 0 5px");
                $('#label').css("right", "-100px");
            },900);
        };
    </script>
<?php include __DIR__ . '/__html__footer.php' ?>