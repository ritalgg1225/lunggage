<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        .d-flex{
            display: flex;
        }
        .con{
            flex-wrap: wrap;
            width: 100%;
            /*height: 50vh;*/
        }
        .w-25{
            width: 25%;
            height: 50vh;
        }
        .c-text{
            color: #ffffff;
            font-size: 2rem;
            justify-content: center;
            align-items: center;
            text-align: center;
        }
        .cc{
            background-color: #dddddd;
        }
        .c-1{
            background-color: #243B55;

        }
        .c-2{
            background-color: #426983;
        }
        .c-3{
            background-color: #666e74;
        }
        .c-4{
            background-color: #1f7696;
        }
        .c-5{
            background-color: #2c3e50;
        }
        .c-6{
            background-color: #818E9B;
        }
        .c-7{
            background: rgba(129, 142, 155,0.9);
        }
        .c-8{
            background-color: #2f4255;

        }
        .c-9{
            background-color: #c2a883;

        }
        .c-10{
            background-color: #c2aa71;

        }
    </style>
</head>
<body>
<div class="con d-flex">
    <div class="w-25 c-1 d-flex c-text">#243B55<br>(原定深藍背景)</div>
    <div class="w-25 c-5 d-flex c-text">#2c3e50<br>(商品頁按鈕)</div>
    <div class="w-25 c-6 d-flex c-text">#818E9B<br>(商品頁按鈕)</div>
    <div class="w-25 c-2 d-flex c-text">#426983</div>
    <div class="w-25 c-3 d-flex c-text">#666e74</div>
    <div class="w-25 c-4 d-flex c-text">#1f7696<br>(登入按鈕)</div>

    <div class="w-25 cc c-7 d-flex c-text">rgba(129, 142, 155,0.9);</div>
    <div class="w-25 cc c-8 d-flex c-text">#2f4255<br>(隨便選)</div>
    <div class="w-25 cc c-9 d-flex c-text">副標的金</div>
    <div class="w-25 cc c-10 d-flex c-text">#c2aa71</div>
    <div class="w-25 cc c-11 d-flex c-text">#0000</div>
    <div class="w-25 cc c-12 d-flex c-text">#0000</div>
</div>
</body>
</html>