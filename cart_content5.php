<!-- wrapper -->
<div class="w75vwDC  textAlignCenterDC marginAutoDC">
    <div class="h10vhDC w30vwDC marginAutoDC"></div>
    <div class="marginBottom10vhDC">
        <img class="logoDC" src="images/logo_680x335_black.svg" alt="">
    </div>  
            <!-- 購物車肚皮 -->
            <main class="ATMbelly backGreyBlueDC">
                <div class="p1DC">訂單完成</div>
                <div class="p2DC">感謝你的購買</div>
                <div class="p3DC">商品預計 2019/02/22 寄出</div>
            </main>
</div>
<script type="text/javascript">
    function pageRedirect() {
        window.location.replace("index.php");
    }
    setTimeout("pageRedirect()", 5000);
</script>