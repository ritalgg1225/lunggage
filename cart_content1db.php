<style>
/*--------------------------info通知-----*/

.info-wrap-bg{
width: 100%;
height: 100vh;
background-color: #000000;
position: fixed;
z-index: 97;
top: 0;
left: 0;
opacity: 0.7;
}
.info-alert{
padding: 20px;
color: #ffffff;
background-color: #c67b8a;
position: fixed;
left: 50%;
top: 50%;
transform: translate(-50%,-50%);
z-index: 98;
font-size: 1.2rem;
letter-spacing: 2px;
box-shadow: 1px 3px 10px #414449;
transition: 0.5s;
min-width: 450px;
min-height: 180px;
justify-content: center;
align-items: center;
}

@media screen and (max-width:630px){
.info-alert{
min-width: 80vw;
min-height: 80vw;
}

}
</style>


<!-- wrapper -->
<div class="wrapper">
    <!-- 購物車肚皮 -->
    <main id="mainDC">
        <!-- 分類標籤 -->
        <div class="h5vhDC backMainDC positionStickyDC top0DC zIndexDC">
            <div class="boxHeadDC flex">
                <div class="w14vwDC labelDC">照片</div>
                <div class="w14vwDC labelDC">商品</div>
                <div class="w7vwDC labelDC">尺寸</div>
                <div class="w7vwDC labelDC">顏色</div>
                <div class="w7vwDC labelDC">數量</div>
                <div class="w7vwDC labelDC">金額</div>
                <div class="w7vwDC labelDC">刪除</div>
            </div>
        </div>
        <!-- 存貨不足提示 -->
        <div class="notEnoughGoodsDC d-none">
            存貨不足
        </div>
        <!-- 購物車肚子 -->
        <?php if (empty($cart_data)): ?>
        <div id="info-wrap" class="">
            <div class="info-wrap-bg"></div>
            <div id="info" class="info-alert d-flex">購物車目前沒有商品</div>
        </div>
    </main>
</div>
<!-- DC大使館結帳區 -->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a href="">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題-->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC">
                    <span id="total-price" class="price">o 元</span>
                </div>
            </div>
            <?php if(empty($cart_data)): ?>
                <button class="failButtonDC pWhiteDC">結帳</button>
            <?php else: ?>
                <a id="emptyCartDC" href="cart_DC_checkList.php"><button class="checkOutButtonDC backTransparentDC pWhiteDC">結帳</button></a>
            <?php endif; ?>
        </div>
    </div>
</div>
<!---->
        <?php else: ?>
            <div class="cartEmptyDC d-none" role="alert">
                購物車裡沒有商品
            </div>
        <?php
        $total = 0;

        foreach ($keys as $k){
        $i = $cart_data[$k];
        // var_dump($i);
        $total += $i['qty'] * $i['price'];
        ?>

        <li class="flex borderBottomGreyDC goodsDC product-item" data-sid="<?= $i['sid'] ?>" id="product<?= $i['sid'] ?>">
            <figure class="figureDC">
                <img class="cartImageDC" src="./images/product/<?= $i['pic_nu'] ?>" alt="">
            </figure>
            <!-- RWD分層 -->
            <div class="flex rwd1">
                <div class="forMobile1DC"><?= $i['brand'] ?><br>
                    <span><?= $i['type'] ?></span></div>
                <!-- rwd -->
                <div class="flex jhinDC">
                    <div class="goodsModule2DC"><?= $i['size'] ?>吋</div>
                    <div class="goodsModule2DC"><?= $i['color'] ?></div>
                </div>
                <!-- rwd -->
                <div class="flex jhinDC">
                    <div id="notEnoughgoodsDC" class="quantity goodsModule2DC" data-qty="<?= $i['qty'] ?>">
                        <select class="mobileSelectDC form-control item-qty custom-select select" style="display: inline-block;">
                            <?php for($k=1;$k<=9;$k++){ ?>
                                <option value="<?=$k?>" <?= $k==$i['qty'] ? 'selected="selected"' : '' ?>>
                                    <?=$k?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="goodsModule2DC product-price" data-price="<?= $i['price'] ?>">$ <?= $i['price'] ?>元</div>

                </div>
                <!-- 刪除和願望轉換方向 -->
                <div class="rwd2">
                    <a href="javascript: remove_item(<?= $i['sid'] ?>)" class="goodsModule3DC">
                        <div class=""><i class="fas fa-trash-alt"></i></div>
                    </a>
                </div>
            </div>
        </li>


        <?php } ?>
    </main>
    <!-- WA特區右邊字 -->
    <div class="ff-marko register-sub">
            SHOPPING CART
        </div>
</div>


<!-- DC大使館結帳區 -->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a class="pWhiteDC" href="javascript:history.go(-1)">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題-->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC">
                    <span id="total-price" class="price" data-price="<?= $total ?>"><?= $total ?></span>
                </div>
            </div>
            <?php if(empty($cart_data)): ?>
                <button class="failButtonDC pWhiteDC">結帳</button>
            <?php else: ?>
                    <a class="nonstyle-a white-a open-popup-link" href="cart_DC_checkList.php"><button class="checkOutButtonDC backTransparentDC pWhiteDC">結帳</button></a>
                    <button class="failButtonDC pWhiteDC d-none">結帳</button>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php endif; ?>

<script>
    var dallorCommas = function(n){
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    };

    var remove_item = function(sid){
            $.get('add_to_cart.php', {sid:sid}, function(data){
                //location.reload();

                cart_count(data); // 更新選單上泡泡裡的數字
                $('#product'+sid).remove(); // 移除該列 <tr>
                calTotalPrice(); // 計算總共多少錢
                updateAllPrice(); // 依據資料變更呈現
            }, 'json');
    };

    var updateAllPrice = function() {
        $('.price').each(function () {
            var p = $(this).attr('data-price');
            p = dallorCommas(p);
            $(this).text('$ ' + p + '元');
        });
    };
    updateAllPrice();

    var calTotalPrice = function(){
        var t_price = $('#total-price');
        var total = 0;

        $('.product-item').each(function(){
            var price = $(this).find('.product-price').attr('data-price');
            var qty = $(this).find('select').val();
            console.log(qty);
            total += price*qty;

            console.log('price:'+ price, qty);

        });

        console.log('total:'+ total);
        t_price.attr('data-price', total);
        t_price.html('$' + dallorCommas(total) + '元');
    };

    $('.item-qty').on('change', function(){
        var item_tr = $(this).closest('li.borderBottomGreyDC');
        var sid = item_tr.attr('data-sid');
        var qty = $(this).val();
        var price = item_tr.find('.product-price').attr('data-price');
        console.log(sid);

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            //location.reload(); // 刷新頁面

            cart_count(data); // 更新選單上泡泡裡的數字

            item_tr.find('.sub-total').attr('data-price', price*qty); // 變更該項目的小計
            calTotalPrice(); // 計算總共多少錢

            updateAllPrice(); // 依據資料變更呈現
        }, 'json');

    });

    $(".goodsModule3DC").click(function(){
        var productNumber = $(".goodsDC").length;
        console.log(productNumber)
        if(productNumber == 1){
            //location.reload();
            $('div.cartEmptyDC').removeClass('d-none');
            $('.checkOutButtonDC').remove();
            $('button.failButtonDC').removeClass('d-none');


        }
    });
</script>

<!--以上DC大使館-->

        

       