<?php
require __DIR__. '/__connect_db.php';
$page_name = 'edit_me';
$page_title = '編輯個人資料';

//如果不是登入狀態就跳轉登入頁
if (! isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}

?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="stylesheet" href="css/member-layout.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <title>會員中心 - 會員資料</title>
    <style>
        /* ----------------------------------------------------- member RWD */
        @media screen and (max-width:1240px){

        }
        @media screen and (max-width:1024px){
            .member-wrap{
                max-width: 80vw;
            }

        }
        @media screen and (max-width:768px){
            .member-wrap{
                max-width: 90vw;
            }
            .member-sub{
                transform: rotate(0deg);
                top: -45px;
                left: 0;
                font-size: 45px;
                max-width: 90vw;
            }
            .member-nav{
                justify-content: center;
            }
            .member-row>.d-flex{
                flex-direction: column;
            }
            .member-row .mb-1-6{
                width: 100%;
                flex-direction: row;
                justify-content: center;
                margin-bottom: 20px;
            }
            .member-row .mb-1-6>div{
                margin: 0 20px;
            }
            .member-row .mb-1-6>div span{
                display: none;
            }
            .member-row .mb-1-6>div a{
                border: 1px solid #4d5258;
                padding: 10px 15px;
                border-radius: 21px;
                display: block;
            }
            .member-row .member-account{
                width: 100%;
            }
            .m-y-10 p{
                padding-left: 0;
            }
            .member-scroll{
                width: 100%;
            }
            .member-row{
                height: auto;
            }

        }
        @media screen and (max-width:520px){
            .member-row{
                margin-bottom: -50px;
            }
            .member-sub{
                font-size: 35px;
                top: -40px;
            }
            .member-nav{
                margin-top: 130px;
            }
            .member-nav ul li {
                padding: 0 10px;
            }
            .m-y-10 p{
                font-size: 1.15rem;
            }
            .member-account>div{
                width: 88%;
            }

        }
        @media screen and (max-width:400px){
            
            .member-sub{
                font-size: 30px;
                top: -35px;
            }

            .member-nav ul li {
                padding: 0 10px;
                letter-spacing: 4px;
            }
            .m-y-10 p{
                font-size: 1.1rem;
            }
            .member-row .mb-1-6 a{
                font-size: 16px;
            }

        }

        /*--------------------------info通知-----*/

        .info-wrap-bg{
            width: 100%;
            height: 100vh;
            background-color: #000000;
            position: fixed;
            z-index: 97;
            top: 0;
            left: 0;
            opacity: 0.7;
        }
        .info-alert{
            padding: 20px;
            color: #ffffff;
            background-color: #c67b8a;
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            z-index: 98;
            font-size: 1.2rem;
            letter-spacing: 2px;
            box-shadow: 1px 3px 10px #414449;
            transition: 0.5s;
            min-width: 450px;
            min-height: 180px;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width:630px){
            .info-alert{
                min-width: 80vw;
                min-height: 80vw;
            }

        }

    </style>
</head>

<body>
<?php include __DIR__ . '/__navbar.php'?>
    <div id="info-wrap" class="d-none">
        <div class="info-wrap-bg"></div>
        <div id="info" class="info-alert d-flex"></div>
    </div>
    <section>
        <div class="member-bg"></div>
        <div class="con-1440 member-wrap">
            <div class="ff-marko member-sub">
                MEMBER CENTER
            </div>
            <div class="member-nav d-flex">
                <ul class="d-flex nonstyle-ul">
                    <li class="active"><a class="active" href="#">我的帳戶</a></li>
                    <li><a class="" href="member_order.php">訂單查詢</a></li>
                    <li><a class="" href="member_wishlist.php">願望清單</a></li>
                </ul>
            </div>
            <div class="member-row">
                <h2 class="fw-300">會員資料</h2>
                <div class="d-flex">
                    <div class="mb-1-6 d-flex">
                        <div>
                            <a class="nonstyle-a active" href="#">會員資料<br><span>Account Information</span></a>
                        </div>
                        <div>
                            <a class="nonstyle-a" href="member_payment.php">付款帳戶<br><span>Payment & Crdits</span></a>
                        </div>
                    </div>
                    <div class="mb-4-6 d-flex member-account">
                        <div class="mb-4-5">
                            <!-- 註冊表單樣式-->
                            <form action="" method="post" name="editForm" onsubmit="return editformCheck()">
                                <div class="p-b-40">
                                    <label for="email" class="fw-400">會 員 信 箱</label>
                                    <div class="member-detial m-y-10">
                                        <p class=""><?= $_SESSION['user']['email'] ?></p>
                                        <hr>
                                    </div>
                                    <div class="member-detial-input">
                                        <input type="text" class="input-style fc-blue" name="email" id="emailEdit" disabled
                                               value="<?= $_SESSION['user']['email'] ?>">
                                        <small id="emailHelp"></small><br>
                                        <span class="span-note"> <i> 會員信箱即是會員帳號，無法更改</i></span>
                                    </div>
                                </div>

                                <div class="p-b-40">
                                    <label for="name" class="fw-400">會 員 姓 名</label>
                                    <div class="member-detial m-y-10">
                                        <p class=""><?= $_SESSION['user']['name'] ?></p>
                                        <hr>
                                    </div>
                                    <div class="member-detial-input">
                                        <input type="text" class="input-style fc-blue" name="name" id="nameEdit"
                                               value="<?= $_SESSION['user']['name'] ?>">
                                        <small id="nameHelp"></small><br>
                                        <span class="span-note"> <i> 會員姓名即為預設的收件人姓名</i></span>
                                    </div>
                                </div>
                                <div class="p-b-40">
                                    <label for="mobile" class="fw-400">手 機 號 碼</label>
                                    <div class="member-detial m-y-10">
                                        <p class=""><?= $_SESSION['user']['mobile'] ?></p>
                                        <hr>
                                    </div>
                                    <div class="member-detial-input">
                                        <input type="text" class="input-style fc-blue" name="mobile" id="mobileEdit"
                                               value="<?= $_SESSION['user']['mobile'] ?>">
                                        <small id="mobileHelp"></small><br>
                                    </div>
                                </div>
                                <div class="p-b-40">
                                    <label for="address" class="fw-400">收 件 地 址</label>
                                    <div class="member-detial m-y-10">
                                        <p class=""><?= $_SESSION['user']['address'] ?></p>
                                        <hr>
                                    </div>
                                    <div class="member-detial-input">
                                        <input type="text" class="input-style fc-blue" name="address" id="addressEdit"
                                               value="<?= $_SESSION['user']['address'] ?>">
                                        <small id="addressHelp"></small><br>
                                    </div>
                                </div>
                                <div class="p-b-40">
                                    <label class="fw-400">生 日</label><br>
                                    <div class="member-detial m-y-10">
                                        <p class=""><?= $_SESSION['user']['birthday'] ?></p>
                                        <hr>
                                    </div>
                                    <div class="member-detial-input">
                                        <input type="date" class="input-style fc-blue" name="birthday" id="birthdayEdit"
                                               value="<?= $_SESSION['user']['birthday'] ?>">

                                    </div>
                                    <div class="p-b-40 m-t-40 member-detial-input">
                                        <label for="password" class="fw-400">請輸入原始密碼</label>
                                        <input type="password" class="input-style fc-blue" name="password" id="passwordEdit">
                                        <small id="passwordHelp"></small><br>
                                    </div>
                                    <!--
                                    <div class="p-b-40 member-detial-input">
                                        <label for="passwordCheck" class="fw-300">密 碼 確 認</label>
                                        <input type="password" class="input-style" name="passwordCheck" id="passwordCheck">
                                        <small id="passwordCheckHelp"></small><br>
                                    </div>
                                    -->
                                </div>
                                <div class="text-center btn-edit-div">
                                    <button type="button" class="btn-submit btn-edit-info">資 料 修 改</button>
<!--                                    <button type="button" class="btn-submit btn-edit-password">變 更 密 碼</button>-->
                                </div>
                                <div class="text-center btn-cancel-div">
                                    <button type="submit" class="btn-submit">送 出</button>
                                    <button type="button" class="btn-submit btn-cancel">取 消</button>
                                </div>
                            </form>
                            <div class="m-b-50"></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--------- script ----------->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        var fields = ['password', 'name', 'mobile'];
        var s;
        var info = $('#info');
        var infoWrap = $('#info-wrap');
        var emailHelp = $('#emailHelp');
        var email = $('#email');


        function editformCheck() {
            var isPass = true;
            var mobile_pattern = /^09\d{2}\-?\d{3}\-?\d{3}$/;

            for (s in fields) {
                cancelAlert(fields[s]);
            }


            if (document.editForm.name.value.length < 2) {
                setAlert('name', '請輸入正確姓名');
                isPass = false;
            }

            if (document.editForm.password.value.length < 6) {
                setAlert('password', '密碼欄位需6字元以上');
                isPass = false;
            }

            if (!mobile_pattern.test(document.editForm.mobile.value)) {
                setAlert('mobile', '手機號碼格式或長度錯誤');
                isPass = false;
            }

            if(isPass){
                $.post('member_api.php', $(document.editForm).serialize(), function (data) {


                    if (data.success){
                        setTimeout(function(){
                            // location.reload();
                            location.href = './member.php';
                        }, 1500);
                    }

                    if (data.info){
                        infoWrap.removeClass('d-none');
                        info.html(data.info);

                        setTimeout(function(){
                            infoWrap.addClass('d-none');
                        }, 1300);
                    }

                },'json');
            }

            return false;
        }


        // 輸入錯誤通知
        function setAlert(fieldName, message) {
            $('#' + fieldName).css('border-bottom', '1px solid #9e3c3c');
            $('#' + fieldName + 'Help').text(message);
        }

        // 取消輸入錯誤通知
        function cancelAlert(fieldName) {
            $('#' + fieldName).css('border-bottom', '1px solid #858a8f');
            $('#' + fieldName + 'Help').text('');
        }


        $(".btn-cancel-div").hide();
        $(".member-detial-input").hide();

        $(".btn-edit-info").click(function () {
            $(".member-detial").slideToggle(800);
            $(".btn-cancel-div").slideToggle(800);
            $(".btn-edit-div").slideToggle(800);
            $(".member-detial-input").slideToggle(800);
        });

        $(".btn-cancel").click(function () {
            $(".member-detial").slideToggle(800);
            $(".btn-cancel-div").slideToggle(800);
            $(".btn-edit-div").slideToggle(800);
            $(".member-detial-input").slideToggle(800);
            for (s in fields) {
                cancelAlert(fields[s]);
            }
        });



    </script>

<?php include __DIR__ . '/__html__footer.php'?>