

<!-- wrapper -->
<div class="wrapper">
            <!-- 購物車肚皮 -->
            <main id="mainDC">
                <!-- 訂購資料 -->
                <div class="backMainDC positionStickyDC top0DC zIndexDC">
                    <div class="boxHeadDC flex justifyContentCenterDC alignItemsCenterDC pWhiteDC positionStickyDC top0DC zIndexDC">
                        資料內容
                    </div>
                </div>
                <!-- 購物車肚子 -->
                <div class="w65vwDC marginCenterDC textAlignCenterDC pGreyDC">
                    <div class="formBoxDC">
                            <form name="registerform" method="post">

                                <div class="flex">
                                    <span class="field1DC">收件人姓名</span>
                                    <label class="w100PercentDC" for="name">
                                        <input id="recipient" type="text" name="name" placeholder="<?= $_SESSION['user']['name'] ?>" />
                                    </label>
                                </div>
                                <div class="flex">
                                    <span class="field1DC">手機</span>
                                    <label class="w100PercentDC" for="mobile">
                                        <input id="r_cellphone" type="text" name="mobile" placeholder="<?= $_SESSION['user']['mobile'] ?>" />
                                    </label>
                                </div>
                                <div class="flex">
                                    <span class="field1DC">收件地址</span>
                                    <label class="w100PercentDC" for="address">
                                        <input id="r_address" type="text" name="address" placeholder="<?= $_SESSION['user']['address'] ?>" />
                                    </label>
                                </div>

                                <!-- 點選配送時間 -->
                                <div class="sendTimeBoxDC">
                                    <div class="textAlignLeftDC">
                                        <p>配送時段</p>
                                    </div>
                                    
                                    <div class="sendTimeDC">
                                        <!--  -->
                                            <div class="pretty p-default p-round p-smooth">
                                                <input type="radio" name="selectTime" checked/>
                                                <div class="state p-primary">
                                                        <label>上午8:00-12:00</label>
                                                </div>
                                            </div>
                                            <div class="pretty p-default p-round p-smooth mobile1DC">
                                                <input type="radio" name="selectTime" />
                                                    <div class="state p-primary">
                                                        <label>下午12:00-18:00</label>
                                                    </div>
                                            </div>

                                        <!--  -->
                                            <div class="pretty p-default p-round p-smooth">
                                                <input type="radio" name="selectTime" />
                                                    <div class="state p-primary">
                                                        <label>不指定</label>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                                <!-- 發票資訊 -->
                                <div class="flex justifyContentCenterDC h4vhDC marginTB1vhDC borderBottomGreyDC">
                                    <p>發票資訊</p>
                                </div>
                                <!-- checkbox -->

                                <div class="receiptChoicesDC">
                                    <!-- for rwd -->
                                    <div class="flex mobile2DC">
                                        <div class="pretty p-default p-curve p-thick p-smooth">
                                            <input type="radio" name="rec_type" onClick="showA()"/>
                                            <div class="state p-primary">
                                                <label>電子發票 </label>
                                            </div>
                                        </div> 
                                        <div class="pretty p-default p-curve p-thick p-smooth">
                                            <input type="radio" name="rec_type" onClick="showB()" />
                                            <div class="state p-primary">
                                                <label>電子發票(手機載具)</label>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- for rwd -->
                                    <div class="flex mobile2DC">
                                        <div class="pretty p-default p-curve p-thick p-smooth">
                                            <input type="radio" name="rec_type" onClick="showC()" />
                                            <div class="state p-primary">
                                                <label>發票捐贈</label>
                                            </div>
                                        </div> 
                                        <div class="pretty p-default p-curve p-thick p-smooth">
                                            <input type="radio" name="rec_type" onClick="showD()" />
                                            <div class="state p-primary">
                                                <label>統編(公司戶)發票</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- 輸入手機載具 -->
                                <div class="flex marginTop5vhDC" id="B">
                                <p class="w15vwDC mobile3DC">請輸入手機載具號碼</p>
                                <input type="text">
                                </div>
                                <!-- 捐贈單位 -->
                                <div class="center-on-page marginTop5vhDC h10vhDC" id="C">
                                    <div class="">
                                        <p class="w10vwDC textAlignLeftDC mobile3DC" style="margin-bottom: 1vh;">捐贈單位</p>
                                        <select class="pWhiteDC mobile3DC select" name="slct" id="slct" style="width: 30vw;">
                                            <option>請選擇單位</option>
                                            <option value="1">財團法人創世基金會</option>
                                            <option value="2">財團法人台灣兒童暨家庭扶助基金會</option>
                                        </select>
                                    </div>                            
                                </div>
                                <!-- 統編 -->
                                <div class="marginTop5vhDC" id="D">
                                    <div class="flex"><p class="w10vwDC textAlignLeftDC mobile3DC">統一編號</p><input type="text"></div>
                                    <div class="flex"><p class="w10vwDC textAlignLeftDC mobile3DC">發票抬頭</p><input type="text"></div>
                                    <div class="flex"><p class="w10vwDC textAlignLeftDC mobile3DC">統編發票寄送地址</p><input type="text"></div>
                                    <!-- 單獨選項 -->
                                    <div class="pretty p-default p-curve p-thick p-smooth mobile3DC">
                                        <input type="checkbox" />
                                        <div class="state p-primary">
                                            <label>發票寄送地址同商品配送地址</label>
                                        </div>
                                    </div>
                                    <div class="pTallPoppyDC font12pxDC marginTop5vhDC">*紙本發票待鑑賞期過後，隔週統一時間寄出，請務必確認收件地址填寫正確，以免延誤寄送時間</div>
                                </div>
                            </form>
                        </div>
                </div>
            </main>
                        <!-- WA特區右邊字 -->
                        <div class="ff-marko register-sub">
                            SHOPPING CART
                        </div>
        </div>
<!-- DC大使館結帳區 -->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a href="cart_DC_checkList.php">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題 -->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC" >NT$ <?= $price ?>元</div>
            </div>
            <a href="cart_DC_pay.php?price=<?= $price ?>">
                <button class="checkOutButtonDC backTransparentDC pWhiteDC" >
                    <span class="inlineToNoneDC">填寫</span>付款方式
                </button>
            </a>

        </div>
    </div>
</div>
