<!------------------------------------------- detail php-->
<?php 
require __DIR__. '/__connect_db.php';
$perams =[];


 
//取得分类资料 -> 品牌
$brand_sql = "SELECT * FROM container_list_brand WHERE PARENT_SID=0";
$brand_stmt = $pdo->query($brand_sql);
$brand_cates = $brand_stmt->fetchAll(PDO::FETCH_ASSOC);

//取得分类资料 -> 尺寸
$cosi_sql = "SELECT * FROM container_list_cosi WHERE PARENT_SID=0";
$cosi_stmt = $pdo->query($cosi_sql);
$cosi_cates = $cosi_stmt->fetchAll(PDO::FETCH_ASSOC);

//取得分类资料 -> 天数
$day_sql = "SELECT * FROM container_list_day WHERE PARENT_SID=0";
$day_stmt = $pdo->query($day_sql);
$day_cates = $day_stmt->fetchAll(PDO::FETCH_ASSOC);

//取得分类资料 -> 类型
$box_sql = "SELECT * FROM container_list_box WHERE PARENT_SID=0";
$box_stmt = $pdo->query($box_sql);
$box_cates = $box_stmt->fetchAll(PDO::FETCH_ASSOC);

//取得分类资料 -> 价格范围
$price_sql = "SELECT * FROM container_list_price WHERE PARENT_SID=0";
$price_stmt = $pdo->query($price_sql);
$price_cates = $price_stmt->fetchAll(PDO::FETCH_ASSOC);

//取得分类资料 -> 材质
$texture_sql = "SELECT * FROM container_list_texture WHERE PARENT_SID=0";
$texture_stmt = $pdo->query($texture_sql);
$texture_cates = $texture_stmt->fetchAll(PDO::FETCH_ASSOC);


 

?>
<!------------------------------------------- detail php end-->

 
<!---------------------------------------------- detail html-->
<div class="detail transition-6s"><!--篩選更多-->
    <button class="ff-noto-wi flex">
        <p>篩選更多</p>
        <img src="img/icon_sm_filter.svg" alt="">
    </button>
</div>

<div class="detail_2 flex " id="detail_use">
    <div class="detail_2-1 transition-6s flex ">
        
        <form action="" method="post" name="filter_form" class="form_shop">
            <!----------------------------------------品牌-->
            <div class="detail_title_mobile"><h6>品牌</h6></div>
            <ul class="transition-3s">
                <li class="detail_title_pc ">
                    <h6>品牌</h6>
                </li>

                    <?php foreach($brand_cates as $brand_item): ?>

                        <li class="relative detail_li_use">
                            <input class=" input_use brand_items checkbox_i" type="checkbox"  value="<?= $brand_item['name_brand'] ?>">
                            <p><?= $brand_item['name_brand'] ?></p>
                        </li>

                    <?php endforeach; ?>
                
            </ul>
            <!----------------------------------------尺寸-->
            <div class="detail_title_mobile"><h6>尺寸/吋</h6></div>
            <ul class="transition-3s">
                <li class="detail_title_pc">
                    <h6>尺寸/吋</h6>
                </li>
                <?php foreach($cosi_cates as $cosi_item): ?>

                    <li class="relative detail_li_use">
                            <input class=" input_use cosi_items checkbox_i" type="checkbox"  value="<?= $cosi_item['name_cosi'] ?>">
                            <p><?= $cosi_item['name_cosi'] ?></p>
                    </li>
 
                <?php endforeach; ?>
            </ul>
            <!----------------------------------------天數/容量-->
            <div class="detail_title_mobile"><h6>容量</h6></div>
            <ul class="transition-3s">
                <li class="detail_title_pc">
                    <h6>容量</h6>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use day_items radio_day" type="radio"  value="30,50" name="day_name">
                    <p>30L - 50L</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use day_items radio_day" type="radio"  value="50,70" name="day_name">
                    <p>50L - 70L</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use day_items radio_day" type="radio"  value="70,100" name="day_name">
                    <p>70L - 100L</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use day_items radio_day" type="radio"  value="100,200" name="day_name">
                    <p>100L以上</p>
                </li>
            </ul>
            <!----------------------------------------類型-->
            <div class="detail_title_mobile"><h6>類型</h6></div>
            <ul class="transition-3s">
                <li class="detail_title_pc">
                    <h6>類型</h6>
                </li>
                <?php foreach($box_cates as $box_item): ?>

                    <li class="relative detail_li_use">
                            <input class=" input_use box_items checkbox_i" type="checkbox"  value="<?= $box_item['name_box'] ?>">
                            <p><?= $box_item['name_box'] ?></p>
                    </li>

                <?php endforeach; ?>
            </ul>
            <!----------------------------------------價格範圍-->
            <div class="detail_title_mobile"><h6>價格範圍</h6></div>
            <ul class="transition-3s">
                <li class="detail_title_pc">
                    <h6>價格範圍</h6>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use price_items radio_price" type="radio" value="5000,6000" name="price_name">
                    <p>$5000 - $6000</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use price_items radio_price" type="radio" value="6000,7000" name="price_name">
                    <p>$6000 - $7000</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use price_items radio_price" type="radio" value="7000,8000" name="price_name">
                    <p>$7000 - $8000</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use price_items radio_price" type="radio" value="8000,9000" name="price_name">
                    <p>$8000 - $9000</p>
                </li>
                <li class="relative detail_li_use">
                    <input class=" input_use price_items radio_price" type="radio" value="9000,10000" name="price_name">
                    <p>$9000 - $10000</p>
                </li>
            </ul>
            <!----------------------------------------材質-->
            <div class="detail_title_mobile"><h6>材質</h6></div>
            <ul style="border-right: none" class="transition-3s">
                <li class="detail_title_pc">
                    <h6>材質</h6>
                </li>
                <?php foreach($texture_cates as $texture_item): ?>

                    <li class="relative detail_li_use">
                            <input class=" input_use texture_items checkbox_i" type="checkbox" value="<?= $texture_item['name_texture'] ?>">
                            <p><?= $texture_item['name_texture'] ?></p>
                    </li>

                <?php endforeach; ?>
                
            </ul>

        </form>
    </div>
</div>
<!---------------------------------------------- detail html end-->