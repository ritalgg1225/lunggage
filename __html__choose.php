    <div class="choose_how ff-merri-wi">HOW TO CHOOSE</div>
    <div class="choose">
        <div class="choose_b">
            <h2 class="ff-merri-wi p_50px_wii fw-400">HOW TO CHOOSE</h2>
            <P>您知道不同材質的行李箱適合帶去旅行惡國家不同嗎？<br>
                您知道輪子的流暢度是行李箱的靈魂嗎？<br>
                您知道怎樣選擇行李箱尺寸大小嗎？<br>
                購買行李箱之前讓我們來告訴您，<br>
                如何選擇適合您的箱子與一些基本挑選常識喔！<br>
            </P>
            <button>
            <!-- 2019/2/16 21:41 Rita 更新 -->
                <a href="./pick.php">
                    <p style="line-height: 36px;">去看看</p>
                </a>
            </button>
        </div>
    </div>
    <div class="mobile_p p_16px_wii">
       <p> 您知道不同材質的行李箱適合帶去旅行惡國家不同嗎？</p>
       <p>您知道輪子的流暢度是行李箱的靈魂嗎？您知道怎樣選擇行李箱尺寸大小嗎？購買行李箱之前讓我們來告訴您，如何選擇適合您的箱子與一些基本挑選常識喔！</p>
    </div>
    
    <div class="button">
    <!-- 2019/2/16 21:41 Rita 更新 -->
        <a href="./pick.php">
            <p class="ff-noto-wi">去看看</p>
        </a>
    </div>    