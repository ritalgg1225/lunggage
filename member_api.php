<?php
require __DIR__. '/__connect_db.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];

if(isset($_POST['name']) and isset($_POST['password']) and isset($_SESSION['user'])){
    $result['postData'] = $_POST;


    //sha1把內容編碼  trim(去除頭尾空白)
    $password = sha1(trim($_POST['password']));


    $sql = "UPDATE `members` SET `mobile`=?,`address`=?,`birthday`=?,`name`=? WHERE `id`=? AND `password`=?";

    $stmt = $pdo->prepare($sql);

    $stmt->execute([
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $_POST['name'],
        $_SESSION['user']['id'],
        $password,
    ]);

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '資料修改完成';

        $_SESSION['user']['mobile'] = $_POST['mobile'];
        $_SESSION['user']['address'] = $_POST['address'];
        $_SESSION['user']['birthday'] = $_POST['birthday'];
        $_SESSION['user']['name'] = $_POST['name'];
    } else {
        $result['code'] = 410;
        $result['info'] = '密碼輸入錯誤 或 資料沒有更新';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);