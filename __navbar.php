<?php
require __DIR__ . '/__connect_db.php';
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
<link rel="stylesheet" href="./css/login.css">
<link rel="stylesheet" href="./css/navigation.css">

<nav class="">
    <div class="nav_container relative d-flex align-item-center mairgin-0auto">
        <div class="nav_left relative">
            <a href="./"><img class="pad-hide" src="./images/logo_190_black.svg" alt=""></a>
            <a class="nonstyle-a d-none pad-show" href="./cart_DCdb.php"><img src="./images/icon_sm_shopbag.svg" alt="" title="購物清單">
                <div class="d-flex cart-count-circle"><span class="badge_pill_cart">0</span></div></a>
            <a class="nonstyle-a d-none mobile-show" href="./cart_DCdb.php"><img src="./images/icon_sm_shopbag_2.svg" alt="" title="購物清單">
            <div class="d-flex cart-count-circle"><span class="badge_pill_cart">0</span></div>
            </a>
        </div>
        <div class="flex-right d-flex">
            <div class="nav_mid relative">
                <ul class="nonstyle-ul d-flex justify-center align-item-center p-0 m-0">
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="./hotnews.php">最新消息</a></li>
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="./pick.php">挑選秘訣</a></li>
                    <div class="product_bar d-flex align-item-center">
                        <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color menu_product_R" href="./index_commodity.php">商品選購</a></li>
                    </div>
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="./warranty.php">保固維修</a></li>
                    <div class="member_bar d-flex align-item-center">
                        <?php if (isset($_SESSION['user'])): ?>
                        <li><a class="nonstyle-a nav_p-x-25 menu_member_R maintext-color" href="./member.php">會員中心</a></li>
                        <?php else: ?>
                        <li><a class="nonstyle-a nav_p-x-25 menu_member_R maintext-color open-popup-link" href="#test-popup">會員中心</a></li>
                        <?php endif; ?>
                    </div>
                </ul>
            </div>
            <div class="nav_mid_mobile d-none">
                <a href="./"><img class="disn-519" src="./images/logo_190_black.svg" alt=""></a>
                <a href="./"><img class="dis-519" src="./images/logo_190_white.svg" alt=""></a>
            </div>
            <div class="nav_right relative d-flex">
                <ul class="nonstyle-ul d-flex p-0 m-0">
<!--                    <li class="nav_right_icon search relative"><img src="./images/icon_sm_search.svg"-->
<!--                                alt="" title="搜尋">-->
                        <div class="search_slide absolute"></div></li>
                    <?php if (isset($_SESSION['user'])): ?>
                        <li class="nav_right_icon"><a class="nonstyle-a " href="member-logout.php" ><img
                                        src="./images/icon_sm_logout.svg" alt="" title="登出"></a></li>
                    <?php else: ?>

                        <li class="nav_right_icon"><a class="nonstyle-a open-popup-link" href="#test-popup"><img
                                        src="./images/icon_sm_login.svg" alt="" title="登入"></a></li>
                    <?php endif; ?>
                    <li class="nav_right_icon"><a class="nonstyle-a" href="./cart_DCdb.php"><img src="./images/icon_sm_shopbag.svg"
                                alt="" title="購物清單">
                            <div class="d-flex cart-count-circle-web"><span class="badge_pill_cart">0</span></div>
                        </a></li>
                    <li class="nav_right_icon"><a class="nonstyle-a" href="./compare.php"><img src="./images/icon_sm_compare.svg"
                            alt="" title="商品比較">
                        <div class="d-flex cart-count-circle-web compare-pill"><span class="badge_pill_compare">0</span></div>
                    </a></li>
                </ul>
            </div>
        </div>
        <div class="nav_menu d-none">
            <img class="disn-519" src="./images/icon-menu-d.svg" alt="">
            <img class="dis-519" src="./images/icon-menu-w.svg" alt="">
        </div>
    </div>

    <!-- login popup start-->
    <div id="test-popup" class="con-960-px d-flex login-con mfp-hide">
        <div class="login-row d-flex">
            <div class="mb-3-5 login-picarea d-flex">
                <div>
                    <h2>會員登入</h2>
                    <h3 class="ff-marko">Member Login</h3>
                    <p>還不是會員嗎?</p>
                    <a href="./member_register.php">立即註冊</span></a>
                </div>
            </div>
            <div class="mb-2-5 login-formarea d-flex">
                <form method="post" name="loginform" class="d-flex loginform mb-4-5" onsubmit="return formCheck()">
                    <div class="m-b-30 login-input-outline">
                        <label for="email" class="fw-300">會員帳號<span style="color:#9e3c3c">*</span></label><br>
                        <input type="text" class="login-input-style" name="email" id="email"><br>
                        <small id="emailHelp"></small>
                        <br>
                    </div>
                    <div class="m-b-30 login-input-outline">
                        <label for="password" class="fw-300">登入密碼<span style="color:#9e3c3c">*</span></label><br>
                        <input type="password" class="login-input-style" name="password" id="password"><br>
                        <small id="passwordHelp"></small>
                        <br>
                    </div>
                    <div class="d-flex login-link m-y-20">
                        <a href="javascript:">忘記密碼</a>
                    </div>
                    <div>
                        <button type="submit" class="btn-login">登 入</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login popup end-->

    <!-- dropdown start-->
    <div class="product_menuframe_R d-flex absolute menu_bg justify-center d-none">
    <!--  原次選單區(拿掉了)  -->
    </div>
    <div class="member_menuframe_R absolute menu_bg d-none justify-center">
        <div class="member_menu_R absolute transition">
            <div class="member_list_R relative">
                <ul class="nonstyle-ul d-flex p-0 m-0">
                    <?php if (isset($_SESSION['user'])): ?>
                        <li><a class="nonstyle-a white-a" href="./member.php">修改個人資料</a></li>
                        <li><a class="nonstyle-a white-a" href="./member_order.php">訂單資訊</a></li>
                        <li><a class="nonstyle-a white-a" href="./member_wishlist.php">願望清單</a></li>
                    <?php else: ?>
                        <li><a class="nonstyle-a white-a open-popup-link" href="#test-popup">會員登入</a></li>
                        <li><a class="nonstyle-a white-a" href="./member_register.php">會員註冊</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- dropdown end-->

    <!-- mobile menu start-->
    <div id="click_menu">
        <div class="menu_page">
            <div class="close_icon d-flex">
                <?php if (isset($_SESSION['user'])): ?>
                    <a href="./member_wishlist.php"><img class="pointer" src="./images/icon-love-4.svg" alt=""></a>
                <?php else: ?>
                    <a class="open-popup-link" href="#test-popup"><img class="pointer" src="./images/icon-love-4.svg" alt=""></a>
                <?php endif; ?>
                <a class="nonstyle-a" id="close_menu">&times;</a>
            </div>
            <div class="menu_select d-flex">
                <ul class="nonstyle-ul mid_select">
                    <?php if (isset($_SESSION['user'])): ?>
                        <li><a class="nonstyle-a white-a" href="./member-logout.php">登出</a></li>
                        <li class="white-a ff-noto"><span><?= $_SESSION['user']['name'] ?>  歡迎登入</span></li>
                        <li class="white-a ff-merri"><span><?= $_SESSION['user']['email'] ?></span></li>
                    <?php else: ?>
                        <li><a class="nonstyle-a white-a open-popup-link" href="#test-popup">登入</a></li>
                    <?php endif; ?>
                </ul>
                <ul class="nonstyle-ul last_select">
                    <li><a href="./compare.php">商品比較</a></li>
                    <li><a href="./hotnews.php">最新消息</a></li>
                    <li><a href="./pick.php">挑選秘訣</a></li>
                    <li><a href="./index_commodity.php">商品選購</a></li>
                    <li><a href="./warranty.php">保固維修</a></li>
                    <?php if (isset($_SESSION['user'])): ?>
                        <li><a href="./member.php">會員中心</a></li>
                    <?php else: ?>
                        <li><a class="open-popup-link" href="#test-popup">會員中心</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- mobile menu end-->
</nav>

    <script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="./js/__nav.js"></script>

    <!-- js for login popup-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
    <script src="./js/__login.js"></script>