<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>維修保固</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
</head>
<?php include __DIR__. '/__navbar.php' ?>
<style>
/* -------------------------------------- entire---------------------------------------------------*/
@import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
body{
    font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif;
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
/* --------------------------------------group style--------------------------------------------------*/
.relative{
    position: relative;
}
.absolute{
    position: absolute;
}
.non_bullet{
    list-style-type: none;
}
.pt_40{
    padding-top: 40px;
}
.pt_20{
    padding-top: 20px;
}
.t_bold{
    font-weight: bold;
}
.t_center{
    text-align: center;
}
/* --------------------------------------body--------------------------------------------------*/
.container{
    width: 75%;
    margin:  0 auto;
}
.banner{
    width: 100%;
    height: 30vh;
    background: url(images/scenario-w-01-yc.jpg)  top center no-repeat fixed;
    background-position: 0 -200px ;/* 調整fixed圖片位置 */
    background-size: cover;
    /* background: url(images/scenario-w-01-yc.jpg)  no-repeat fixed;
    background-position: 0 -100px ;
    background-size: cover; */
}
.banner h1 {
  color :#c2b088;
  top: 20vh;
  /* left: 2vw; */
  font-size: 2.5rem;
  text-shadow: #22313F 10 10;
}
.content span{
    color: #be1b1b;
}
.content h2{
    padding:  10px 0 10px 0;
}
.content li{
    padding: 10px;
}
@media only screen and (max-width : 480px) {
    .container{
        width: 80%;
    }
    .banner h1{
        font-size: 1.5rem;
    }
    .content li{
        padding: 5px;
    }
}
</style>
<body>
    <div class="banner relative">
        <h1 class="relative t_bold t_center">保固維修與售後服務</h1>
    </div>
    <div class="container pt_40">
        <ul class="content non_bullet pt_20"><h2>注意事項</h2>
            <li>1.行李箱售出一年內均享有免費保固維修服務：<span>（.Container所銷售的各項產品也享有終身保修，商品維修酌收工本費。）</span><br>
                    啟用維修服務前，請出示售後服務卡，方能認定商品是否仍在保固期間內，若已超過保固期將酌收零件費用與運費。
                    </li>
            <li>2.維修之定義是指恢復正常功能使用為主(不保證與原貌相同)。</li>
            <li>3.不負責維修之範圍：不適當使用及人為破壞/物料磨損、燒傷、刮傷、刮痕/主體之更新如：割裂、摔裂、摔破/拉鍊齒縫裂開脫落/整條邊條、管條、框條、拉鍊的更換/無損壞要求零件換新/清潔/肩帶/鑰匙/衣架/內襯布汙損/隔層版/配件/經航空、海陸運途中，因人為處裡不當所導致之任何損毀。</li>
            <li>4.一般維修約14~21工作天(不含假日及運送時間)，上述時間不適用於零件缺件狀況。因考量零件取得，在正常維修品下，若為特殊零件，時效上會超過三個星期，還請消費者見諒。(若超過三個月待件商品將通知顧客領回或無限期延長待料)</li>
            <li>5.外殼部份〈含軟硬箱〉均不在保固項目內；因外力因素造成損壞，如機場託運、貨運寄送等因素，請逕向航空(貨運)公司提出索賠要求，本公司不列於保固範圍內，還請協助留意！</li>
        </ul>
        <ul class="content non_bullet pt_20"><h2>發票事宜</h2>
            <li>維修費用可以提供發票。發票統一由總公司開立，若是需要發票者請於商品送修時註明或申請，發票將於維修完成後60天內統一郵寄!</li>
        </ul>
        <ul class="content non_bullet pt_20"><h2>維修送修方式</h2>
            <li>與客服部門聯絡完畢後，請自寄維修部(運費自付)</li>
            <li>地址：</li>
            <li>電話：02-3313-6666</li>
            <li>備註：請用A4大的紙張寫上聯絡人、地址、電話，以便收到維修品後我方可以與您連絡。</li>
            <li>維修完成後，會直接寄出給您。<br>※維修時間約21-28天左右(不含例假日)</li>
            <li>不確定有沒有零件可以維修，都可以先與客服部門洽詢。</li>
        </ul>
        <ul class="content non_bullet pt_20"><h2>航空賠償</h2>
            <li>依國際航空協會規定，行李於運輸過程中受到損害，應於損害七日內以書面向運送人提出申訴！建議您於行李提領後立即檢查行李，若有異狀立即向您所搭乘的航空/運送人提出申訴。</li>
        </ul>
        <p>※以上所有規定，本司.Containe 保有最終解釋之權利。</p>
    </div>
    
<?php include __DIR__. '/__html__footer.php' ?>