<!-- wrapper -->
<div class="wrapper">
    <!-- 購物車肚皮 -->
    <main id="mainDC">
        <!-- 分類標籤 -->
        <div class="h5vhDC backMainDC positionStickyDC top0DC zIndexDC">
            <div class="boxHeadDC flex justifyContentCenterDC">
                <div class="w14vwDC labelDC textAlignCenterDC">照片</div>
                <div class="w14vwDC labelDC textAlignCenterDC">商品</div>
                <div class="w7vwDC labelDC textAlignCenterDC">尺寸</div>
                <div class="w7vwDC labelDC textAlignCenterDC">顏色</div>
                <div class="w7vwDC labelDC textAlignRightDC">數量</div>
                <div class="w7vwDC labelDC textAlignRightDC">金額</div>
            </div>
        </div>
        <!-- 存貨不足提示 -->
        <div class="notEnoughGoodsDC d-none">
            存貨不足
        </div>
        <!-- 購物車肚子 -->
        <?php if (empty($cart_data)): ?>

    </main>
</div>


<!-- DC大使館結帳區 -->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a href="">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題-->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC">
                    <span id="total-price" class="price">o 元</span>
                </div>
            </div>
            <?php if(empty($_SESSION['user'])): ?>
            <!-- Rita 2019/02/16 測試成功 DC大使請加入判斷 -->
             <a id="emptyCartDC" class="open-popup-link" href="#test-popup2"><button class="checkOutButtonDC backTransparentDC pWhiteDC">結帳</button></a>
<!--                <button class="failButtonDC pWhiteDC">結帳</button>-->
            <?php else: ?>
                <a id="emptyCartDC" href="cart_DC_checkList.php"><button class="checkOutButtonDC backTransparentDC pWhiteDC">結帳</button></a>
            <?php endif; ?>
        </div>
    </div>
</div>
<!---->
        <?php else: ?>

        <?php
        $total = 0;

        foreach ($keys as $k){
        $i = $cart_data[$k];
        // var_dump($i);
        $total += $i['qty'] * $i['price'];
        ?>

        <div class="flex borderBottomGreyDC goodsDC product-item" data-sid="<?= $i['sid'] ?>" id="product<?= $i['sid'] ?>">
            <figure class="figure1DC w15PercentDC">
                <img class="cartStep2Img" src="./images/product/<?= $i['pic_nu'] ?>" alt="">
            </figure>
            <!-- RWD分層 -->
            <div class="flex rwd1">
                <div class="goodsModule1DC"><?= $i['brand'] ?><br>
                    <span><?= $i['type'] ?></span></div>
                <!-- rwd -->
                <div class="flex viDC">
                    <div class="goodsModule2DC"><?= $i['size'] ?>吋</div>
                    <div class="goodsModule2DC"><?= $i['color'] ?></div>
                </div>
                <!-- rwd -->
                <div class="flex viDC">
                    <div id="notEnoughgoodsDC" class="quantity goodsModule2DC">

                        <div><?= $i['qty'] ?></div>
                    </div>
                    <div class="goodsModule2DC product-price" data-price="<?= $i['price'] ?>">$ <?= $i['price'] ?>元</div>

                </div>

            </div>
        </div>


        <?php } ?>
    </main>
    <!-- WA特區右邊字 -->
    <div class="ff-marko register-sub">
            SHOPPING CART
        </div>
</div>


<!-- DC大使館結帳區 -->
            <!-- Rita 2019/02/16 12:28 測試成功 -->
            <!-- login popup start-->
            <div id="test-popup2" class="con-960-px d-flex login-con mfp-hide">
                <div class="login-row d-flex">
                    <div class="mb-3-5 login-picarea d-flex">
                        <div>
                            <h2>會員登入</h2>
                            <h3 class="ff-marko">Member Login</h3>
                            <p>還不是會員嗎?</p>
                            <a href="./member_register.php">立即註冊</span></a>
                        </div>
                    </div>
                    <div class="mb-2-5 login-formarea d-flex">
                        <form method="post" name="loginform2" class="d-flex loginform mb-4-5" onsubmit="return formCheck2()">
                            <div class="m-b-30 login-input-outline">
                                <label for="email" class="fw-300">會員帳號<span style="color:#9e3c3c">*</span></label><br>
                                <input type="text" class="login-input-style" name="email" id="email2"><br>
                                <small id="emailHelp2"></small>
                                <br>
                            </div>
                            <div class="m-b-30 login-input-outline">
                                <label for="password" class="fw-300">登入密碼<span style="color:#9e3c3c">*</span></label><br>
                                <input type="password" class="login-input-style" name="password" id="password2"><br>
                                <small id="passwordHelp2"></small>
                                <br>
                            </div>
                            <div class="d-flex login-link m-y-20">
                                <a href="javascript:">忘記密碼</a>
                            </div>
                            <div>
                                <button type="submit" class="btn-login">登 入</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- login popup end-->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a href="cart_DCdb.php">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題-->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC">
                    <span id="total-price" class="price" data-price="<?= $total ?>"><?= $total ?></span>
                </div>
            </div>
            <?php if(empty($_SESSION['user'])): ?>
<!--                <a id="emptyCartDC" class="open-popup-link" href="#test-popup2"><button class="failButtonDC pWhiteDC">請登入</button></a>-->
                <a id="emptyCartDC" class="open-popup-link" href="#test-popup2"><button class="checkOutButtonDC backTransparentDC pWhiteDC">請登入</button></a>
            <?php else: ?>
                <a id="emptyCartDC" href="cart_DC_sendInfor.php?price=<?= $total ?>"><button class="checkOutButtonDC backTransparentDC pWhiteDC">結帳</button></a>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php endif; ?>

<script>
    var dallorCommas = function(n){
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    };

    var remove_item = function(sid){
        $.get('add_to_cart.php', {sid:sid}, function(data){
            //location.reload();

            cart_count(data); // 更新選單上泡泡裡的數字
            $('#product'+sid).remove(); // 移除該列 <tr>
            calTotalPrice(); // 計算總共多少錢
            updateAllPrice(); // 依據資料變更呈現
        }, 'json');
    };

    var updateAllPrice = function() {
        $('.price').each(function () {
            var p = $(this).attr('data-price');
            p = dallorCommas(p);
            $(this).text('$ ' + p + '元');
        });
    };
    updateAllPrice();

    var calTotalPrice = function(){
        var t_price = $('#total-price');
        var total = 0;

        $('.product-item').each(function(){
            var price = $(this).find('.product-price').attr('data-price');
            var qty = $(this).find('select').val();

            total += price*qty;

            console.log('price:'+ price, qty);

        });

        console.log('total:'+ total);
        t_price.attr('data-price', total);
        t_price.html('$' + dallorCommas(total) + '元');
    };

    $('.item-qty').on('change', function(){
        var item_tr = $(this).closest('div.borderBottomGreyDC');
        var sid = item_tr.attr('data-sid');
        var qty = $(this).val();
        var price = item_tr.find('.product-price').attr('data-price');

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            //location.reload(); // 刷新頁面

            //cart_count(data); // 更新選單上泡泡裡的數字

            // item_tr.find('.sub-total').attr('data-price', price*qty); // 變更該項目的小計
            calTotalPrice(); // 計算總共多少錢

            //updateAllPrice(); // 依據資料變更呈現
        }, 'json');

    });

    // $("#emptyCartDC").on("click",function(e){
    //     e.preventDefault()
    //     var price = $("#total-price").attr("data-price")
    //     console.log(price)
    //     $.get("cart_DC_sendInfor.php",{price:price},function(data){
    //             window.location.href("cart_DC_sendInfor.php")
    //
    //     },"json")
    // })

//<!--以上DC大使館-->
    //<!-- Rita 2019/02/16 12:28 測試成功 延長script -->

    // 登入視窗彈出
    // Example 1: From an element in DOM
    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
    // 表單
    var fields = ['email', 'password'];
    var s;
    var info = $('#info');
    var infoWrap = $('#info-wrap');
    var emailHelp = $('#emailHelp2');
    var passwordHelp = $('#passwordHelp2');
    var email = $('#email2');
    var password = $('#password2');


    function formCheck2() {
        var isPass = true;
        var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        for (s in fields) {
            cancelAlert(fields[s]);
        }


        if (!email_pattern.test(document.loginform2.email.value)) {
            setAlert('email', 'email格式錯誤');
            isPass = false;
        }

        if (document.loginform2.password.value.length < 6) {
            setAlert('password', '密碼欄位需6字元以上');
            isPass = false;
        }

        if(isPass){
            $.post('member_login_api.php', $(document.loginform2).serialize(), function (data) {


                if (data.success){
                    location.reload();
                    // setTimeout(function(){
                    //     location.href = './';
                    // }, 1000);
                }else{
                    emailHelp.html(data.info);
                    passwordHelp.html(data.info);
                    email.css('border', '2px solid #9e3c3c');
                    password.css('border', '2px solid #9e3c3c');
                }

                if (data.info){
                    infoWrap.removeClass('d-none');
                    info.html(data.info);

                    setTimeout(function(){
                        infoWrap.addClass('d-none');
                    }, 1500);
                }

            },'json');
        }

        return false;
    }


    // 輸入錯誤通知
    function setAlert2(fieldName, message) {
        $('#' + fieldName).css('border', '1px solid #9e3c3c');
        $('#' + fieldName + 'Help').text(message);
    }

    // 取消輸入錯誤通知
    function cancelAlert2(fieldName) {
        $('#' + fieldName).css('border', '1px solid #858a8f');
        $('#' + fieldName + 'Help').text('');
    }
</script>