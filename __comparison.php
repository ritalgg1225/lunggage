<style>
/* ----------------------css group style-------------------- */
  .relative{
    position: relative;
  }
  .non_bullet{
    list-style-type: none;
  }
  .padding-0{
    padding: 0;
  }
  .bold{
    font-weight: 700;
  }
  .lit_text{
    color: #e1e7ee;
  }
  .t_center{
    text-align: center;
    margin:  20px auto;
}

/* ----------------------body-------------------- */
.container{
  width: 100%;
  /* height: 50vh; */
  margin: 0 auto ;
  overflow: hidden;
}
.ba-slider {
    position: relative;
    overflow: hidden;
}
.ba-slider li{
    font-size: 1.2rem;
    line-height: 2.6rem;
    letter-spacing: 0.2rem;
    text-shadow: 2px 2px 6px #000000;
}
.ba-slider img{
    width: 100%;
    display:block;
}
.resize {
    position: absolute;
    top:0;
    left: 0;
    height: 100%;
    width: 50%;
    overflow: hidden;
}
.handle { /* Thin line seperator */
  position:absolute; 
  left:50%;
  top:0;
  bottom:0;
  width:0;
  /* margin-left:-2px; */
  background: rgba(0,0,0,.5);
  cursor: ew-resize;
} 
.handle:after {  /* Big orange knob  */
    position: absolute;
    top: 40%;
    width: 64px;
    height: 64px;
    margin: -32px 0 0 -32px; 
    content:'\21d4';
    color:#e9e7e8;
    /* font-weight:bold; */
    font-size:36px;
    text-align:center;
    line-height:64px;
    background: #243b55; 
    /* border:1px solid #243b55;  */
    border-radius: 50%;
    transition:all 0.3s ease;
    /* box-shadow:
      0 2px 6px rgba(0,0,0,.3), 
      inset 0 2px 0 rgba(255,255,255,.5),
      inset 0 60px 50px -30px #15365b;  */
}
.draggable:after {
    width: 48px;
    height: 48px;
    margin: -24px 0 0 -24px;
    line-height:48px;
    font-size:30px;
}
.text1{
  position: absolute;
  top: 10%;
  left: 150px;
  box-sizing: border-box;
  /* padding: 20px; */
  text-align: left;
}
.text2{
  /* width: 75%; */
  display:block;
  position: absolute;
  top: 10%;
  /* right:5%; */
  box-sizing: border-box;
  padding:0 150px  0 0;
  text-align: right;
}
.text-left{
  position: absolute;
  top: 10%;
  box-sizing: border-box;
  padding: 0 0 0 150px;
  text-align: left;
}
.text-right{
  text-align: right;
  display:block;
  position: absolute;
  top: 10%;
  right: 150px;
}
/* --------------------------------btn--------------------------------- */
.read_more {
    width: 200px;
    padding: 8px 15px ;
    /* padding-left: 15px;
    padding-top: 5px;
    padding-bottom: 5px; */
    border-radius: 5px;
    color: #fefefe;
    background: linear-gradient(to right, #bea17f 50%, #fff 50%);
    background-size: 200% 100%;
    background-position: right bottom;
    color: #bea17f;
    transition: .3s ease-in;
    border: #bea17f solid 1px;
    cursor: pointer;
}
/* btn文字後方的箭頭 */
.read_more span {
  font-size: 18px;
  font-weight: 600;
}
.read_more a{
    /* color: #fefefe; */
    color: #bea17f;
}
.news_card:hover .read_more{
  background-position: left bottom;
  color: #fff;
}
.news_card:hover .read_more a{
    color: #fff;
}
@media screen and (max-width:830px){
    .text-left{
        padding: 0 0 0 80px;
    }
    .text-right{
        right: 80px;
    }
    .mobile_n{
      display:none;
    }
}
@media screen and (max-width: 670px) {
  .ba-slider li{
  font-size: 1rem;
  line-height: 2rem;
  }
  .text-left{
    padding:0 0 0 20px;
  }
  .text-right{
    right: 20px;
  }
}
@media screen and (max-width: 500px) {
  .ba-slider li{
  font-size: .85rem;
  line-height: 1.5rem;
  }
}
</style>

<!-- <section class="container">
    <div class="ba-slider">
        <img class="" src="./images/compare01.jpg" alt="">
        <div class="text1"> 
          <ul class="non_bullet padding-0">
            <li class="bold">Calpak</li>
            <li class="bold">Vis-À-Vis,55Cm</li>
            <li>霧面PC箱</li>
            <li>拉鍊及海關鎖</li>
            <li>四個360度飛機輪</li>
            <li>創新的衣物壓縮系統</li>
          </ul>
        </div>
        <div class="resize">
          <img class="relative" src="./images/compare02.jpg" alt="">
          <div class="text2">
            <ul class="non_bullet padding-0">
              <li class="bold">Briggs & Riley</li>
              <li class="bold">baseline, 22吋</li>
              <li>1680D尼龍布箱</li>
              <li>萬向飛機輪</li>
              <li>YKK拉鍊</li>
              <li>可擴充</li>
              <li>海關鎖</li>
            </ul>  
          </div> 
          
        </div>
        <span class="handle"></span>
      </div>
</section> -->


<div class="compare_add ff-merri-wi">ADD TO COMPARISON</div>
   <div class="">
        <div class="compare_b">
            <div class="compare_t ff-merri-wi p_50px_wii">ADD TO COMPARISON</div>
        </div>
  </div>
</div>
<section class="container">
    <div class="ba-slider">
        <img class="" src="./images/luggage_compare_04.jpg" alt="">
        <div class="text-right lit_text">
            <ul class="non_bullet padding-0">
              <ul class="non_bullet padding-0">
                  <li class="bold">Briggs & Riley</li>
                  <li class="bold">baseline, 22吋</li>
                  <li>1680丹尼龍布箱</li>
                  <li>可擴充</li>
                  <li>四個飛機輪</li>
                  <li>YKK拉鍊</li>
                  <li>TSA海關鎖</li>
              </ul>
          </ul>  
          </div> 
        <div class="resize">
          <img class="relative" src="./images/luggage_compare_05.jpg" alt="">
          
          <div class="text-left lit_text"> 
            <ul class="non_bullet padding-0">
              <li class="bold">Calpak</li>
              <li class="bold">Vis-À-Vis,55Cm</li>
              <li>霧面PC箱</li>
              <li>360度萬向飛機輪</li>
              <li>衣物壓縮系統</li>
              <li>拉鍊對開</li>
              <li>海關鎖</li>
            </ul>
        </div>
        </div>
        <span class="handle"></span>
      </div>
      <div class="mobile_n news_card container">
      <a  class="" href="./compare.php"><p class="read_more t_center">進入比較</p></a>
      </div>
</section>      
<div class="mobile_p p_16px_wii">
       <p> 面對網路上各式各樣的行李箱，覺得挑選到眼花撩亂了嗎？</p>
       <p>【.CONTAINER】特別將各款行李箱細節拉出來比較，只要將你有興趣的商品點選「加入比較清單」，即可輕鬆方便選出符合您需求的行李箱，無論是出國旅遊還是出差商旅都能滿足.</p>
    </div>
    <div class="news_card container mobile_p">
      <a  class="" href="./compare.php"><p class="read_more t_center">進入比較</p></a>
      </div>
</div>

<script>
// Call & init
$(document).ready(function(){
  $('.ba-slider').each(function(){
    var cur = $(this);
    // Adjust the slider
    var width = cur.width()+'px';
    cur.find('.resize img').css('width', width);//調整照片寬
    cur.find('.text-left').css('width', width);    
    cur.find('.text2').css('width', width);//調整照片裡字寬
    // 拖曳事件 Bind dragging events
    drags(cur.find('.handle'), cur.find('.resize'), cur);
  });
});

// Update sliders on resize. 
// Because we all do this: i.imgur.com/YkbaV.gif
$(window).resize(function(){
  $('.ba-slider').each(function(){
    var cur = $(this);
    var width = cur.width()+'px';
    cur.find('.resize img').css('width', width);
  });
});

function drags(dragElement, resizeElement, container) {
	
  // Initialize the dragging event on mousedown.
  dragElement.on('mousedown touchstart', function(e) {
    
    dragElement.addClass('draggable');
    resizeElement.addClass('resizable');
    
    // Check if it's a mouse or touch event and pass along the correct value
    var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
    
    // Get the initial position
    var dragWidth = dragElement.outerWidth(),
        posX = dragElement.offset().left + dragWidth - startX,
        containerOffset = container.offset().left,
        containerWidth = container.outerWidth();
 
    // 限制了左右最大寬度 Set limits 
    minLeft = containerOffset + 5;
    maxLeft = containerOffset + containerWidth - dragWidth - 5;
    
    // 計算滑鼠拖曳距離 Calculate the dragging distance on mousemove.
    dragElement.parents().on("mousemove touchmove", function(e) {
    	
      // Check if it's a mouse or touch event and pass along the correct value
      var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
      
      leftValue = moveX + posX - dragWidth;
      
      // 如果超過限制的左右寬 就等於 Prevent going off limits
      if ( leftValue < minLeft) {
        leftValue = minLeft;
      } else if (leftValue > maxLeft) {
        leftValue = maxLeft;
      }
      
      // Translate the handle's left value to masked divs width.
      widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
			
      // Set the new values for the slider and the handle. 
      // Bind mouseup events to stop dragging.
      $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
        $(this).removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
      $('.resizable').css('width', widthValue);
    }).on('mouseup touchend touchcancel', function(){
      dragElement.removeClass('draggable');
      resizeElement.removeClass('resizable');
    });
    e.preventDefault();
  }).on('mouseup touchend touchcancel', function(e){
    dragElement.removeClass('draggable');
    resizeElement.removeClass('resizable');
  });
}

</script>