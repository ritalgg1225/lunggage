<!-- wrapper -->
<div class="wrapper">
            <!-- 購物車肚皮 -->
            <main id="mainDC" class="backGreyBlueDC">
                <!-- 分類標籤 -->
                <div class="backMainDC positionStickyDC top0DC zIndexDC">
                    <div class="boxHeadDC flex">
                        <div class="w65vwDC marginCenterDC flex">
                            <div class="marginCenterDC flex pWhiteDC alignItemsCenterDC">請選擇付款方式</div>
                        </div>
                    </div>
                </div>
                <!-- 購物車肚子 -->
                <form>
                    <div id="flip1" class="w65vwDC marginCenterDC">信用卡付款</div>
                    <div id="panel1">
                        <div class="flex marginTop1vhDC justifyContentSpaceAroundDC pretty p-default p-curve thick">
                            <div class="pretty p-default p-curve p-thick p-smooth marginLeft1vwDC">
                                <input id="creditCard" type="radio" name="paygroup" checked/>
                                    <div class="state p-primary">
                                        <label>信用卡付款</label>
                                    </div>
                                    
                            </div>
                        </div>
                        <div class="creditCardInfor">
                            <div class="payModule1DC">
                                <p class="textAlignLeftDC">持卡人姓名</p>
                                <input type="text" class="w82PercentDC">
                            </div>
                            <div class="payModule1DC">
                                <p class="textAlignLeftDC">信用卡號</p>
                                <input type="text" class="w25PercentDC">
                                <input type="text" class="w25PercentDC marginLeft1vwDC">
                                <input type="text" class="w25PercentDC marginLeft1vwDC">
                                <input type="text" class="w25PercentDC marginLeft1vwDC">
                            </div>
                            <div class="payModule1DC rwd4">
                                <p class="textAlignLeftDC sosDC">信用卡到期日</p>
                                <div class="form-group" id="expiration-date">
                                    <select class=" helpDC" >
                                        <option value="01">January</option>
                                        <option value="02">February </option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <p class="marginLeft1vwDC">月</p>
                                    <select  class="marginLeft1vwDC helpDC" >
                                        <option value="16"> 2016</option>
                                        <option value="17"> 2017</option>
                                        <option value="18"> 2018</option>
                                        <option value="19"> 2019</option>
                                        <option value="20"> 2020</option>
                                        <option value="21"> 2021</option>
                                    </select>
                                    <p class="marginLeft1vwDC">年</p>
                                    </div>
                            </div> 
                            <div class="payModule3DC rwd4">
                                <p class="safeCodeDC">檢核碼</p>
                                <input type="text">
                                <p class="marginLeft1vwDC">(卡片背面共三碼)</p>
                            </div> 
                            <div class="rwd3"></div>
                        </div>
                    </div>
                    <div class="ATMTransferDC">
                    <div id="flip2" class="w65vwDC marginCenterDC">ATM轉帳</div>
                        <div id="panel2">
                            <div class="flex marginTop1vhDC justifyContentSpaceAroundDC pretty p-default p-curve thick">
                                <div class="pretty p-default p-curve p-thick p-smooth marginLeft1vwDC">
                                    <input id="transfer" type="radio" name="paygroup"/>
                                        <div class="state p-primary">
                                            <label>ATM轉帳</label>
                                        </div>
                                </div>
                            </div>
                            <p class="marginTop1vhDC textAlignLeftDC">確認使用ATM轉帳付款後，系統會產生一組虛擬序號，請於截止日前完成轉帳</p>
                            <div class="textAlignLeftDC marginTop5vhDC" style="line-height: 1.6">
                            <strong>購物條款</strong>
                            <p class="p_12px_wil marginTop1vhDC">
                            *完成訂單後預計4-6個工作天送達<br>
                            *會員完成線上訂購程序以後，本公司會自動經由電子郵件或其他方式寄發通知，但是該項通知只是代表已經收到會員訂購訊息，不代表雙方交易已經完成或契約已經成立，本公司仍保留是否接受該訂單的權利；但會員已付款者，視為契約成立。<br>
                            *本公司提供多種付款方式供會員選擇，若會員選擇以信用卡支付，其在線上輸入信用卡相關資訊，目的是為了向發 卡機構確認信用卡之有效性及取得交易授權，不表示會員已經付款、也不代表交易已經完成或契約已經成立，本公司保留是否接受該訂單的權利；在本公司接受該訂單以前，會員無需付款，本公司也不會請領信用卡交易款項，該筆交易金額也不會出現在會員的信用卡帳單中。<br>
                            * 會員完成訂購程序後，如確有無法接單之情事，本公司將於會員完成訂單後二個工作天內，通知有無法接單之情事，會員如於收受通知後二日內無異議者，為免有損會員權益，本公司將先行取消請款作業， 再行取消訂單。如果本公司確認交易條件無誤、會員所訂購之商品仍有存貨且無其他本公司無法接受訂單之情形，此時本公司將另行寄發會員出貨通知。<br>
                            *會員所訂購的商品，若經配送兩次無法送達且經無法聯繫超過三天者，本公司將取消該筆訂單並全額退款。
                            </p>
                            </div>
                            <!-- 視覺調整墊高 -->
                            <div class="payModule2DC"></div>
                        </div>
                    </div>
                    <div class="CashOnDeliveryDC">
                        <div id="flip3" class="w65vwDC marginCenterDC">貨到付款</div>
                        <div id="panel3" style="color:rgba(255, 255, 255, 0.7)">
                            <div class="flex marginTop1vhDC justifyContentSpaceAroundDC pretty p-default p-curve thick">
                                <div class="pretty p-default p-curve p-thick p-smooth marginLeft1vwDC">
                                    <input ="delivery" type="radio" name="paygroup"/>
                                        <div class="state p-primary">
                                            <label>貨到付款</label>
                                        </div>
                                </div>
                            </div>
                            <div class="textAlignLeftDC marginTop5vhDC" style="line-height: 1.6">
                            <strong>購物條款</strong>
                            <p class="p_12px_wil marginTop1vhDC">
                            *完成訂單後預計4-6個工作天送達<br>
                            *會員完成線上訂購程序以後，本公司會自動經由電子郵件或其他方式寄發通知，但是該項通知只是代表已經收到會員訂購訊息，不代表雙方交易已經完成或契約已經成立，本公司仍保留是否接受該訂單的權利；但會員已付款者，視為契約成立。<br>
                            *本公司提供多種付款方式供會員選擇，若會員選擇以信用卡支付，其在線上輸入信用卡相關資訊，目的是為了向發 卡機構確認信用卡之有效性及取得交易授權，不表示會員已經付款、也不代表交易已經完成或契約已經成立，本公司保留是否接受該訂單的權利；在本公司接受該訂單以前，會員無需付款，本公司也不會請領信用卡交易款項，該筆交易金額也不會出現在會員的信用卡帳單中。<br>
                            * 會員完成訂購程序後，如確有無法接單之情事，本公司將於會員完成訂單後二個工作天內，通知有無法接單之情事，會員如於收受通知後二日內無異議者，為免有損會員權益，本公司將先行取消請款作業， 再行取消訂單。如果本公司確認交易條件無誤、會員所訂購之商品仍有存貨且無其他本公司無法接受訂單之情形，此時本公司將另行寄發會員出貨通知。<br>
                            *會員所訂購的商品，若經配送兩次無法送達且經無法聯繫超過三天者，本公司將取消該筆訂單並全額退款。
                            </p>
                            </div>
                            <!-- 視覺墊高 -->
                            <div class="payModule4DC"></div>
                        </div>
                    </div>
                </form>
            </main>
            <!-- WA特區右邊字 -->
            <div class="ff-marko register-sub">
                SHOPPING CART
            </div>
        </div>
<!-- DC大使館結帳區 -->
<div class="nextStepBoxDC">
    <div class="w65vwDC h15vhDC marginCenterDC flex justifyContentBetweenDC alignItemsCenterDC pWhiteDC">
        <!-- 回上一頁 -->
        <div class="flex alignItemsCenterDC rwdp1DC">
            <a href="cart_DC_sendInfor.php?price=<?= $price ?>">
                <img class="lastPageDC" src="images/arrow-return.svg" alt="">
                <p class="inlineToNoneDC">回上一頁</p>
            </a>
        </div>
        <!-- 結帳 -->
        <div class="rwdStyle1DC">
            <!-- 解決樣式問題 -->
            <div class="flex alignItemsFlexStartDC">
                <div class="rwdp1DC">小計</div>
                <img class="w10PercentDC borderBoxDC" src="images/slash.svg" alt="">
                <div class="totalPriceDC">NT$ <?= $price ?>元</div>
            </div>
            <button id="redirect" class="checkOutButtonDC backTransparentDC pWhiteDC">購物完成</button>
        </div>
    </div>
</div>
<!--以上DC大使館-->
<script>


    $('#redirect').click(function() {

        var radioChecked = $('#transfer:checked').val();

        if (radioChecked){
            window.location.href = "cart_DC_ATM.php?price=<?= $price ?>";
        } else {
            window.location.href = "cart_DC_endPurchase.php";
        }

    });



</script>