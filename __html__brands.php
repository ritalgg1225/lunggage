<div class="brands">
    <div class="brands_t ff-merri-wi p_50px_wii">BRANDS</div>
    <div class="brands_b flex">
        <div class="brands_left mb-1-3">
            <ul class="mb-2-3 flex">
                <li id="tvbox-1">
                    <img src="./images/brandLOGO-calpak.png" alt="">
                    <div class="brands_hoverline"></div>
                </li>
                <li id="tvbox-2">
                    <img src="./images/brandLOGO-Antler.png" alt="">
                    <div class="brands_hoverline">
                </li>
                <li id="tvbox-3"><img src="./images/brandLOGO-delsey.png" alt="">
                    <div class="brands_hoverline">
                </li>
                <li id="tvbox-4"><img src="./images/brandLOGO-away.png" alt="">
                    <div class="brands_hoverline">
                </li>
                <li id="tvbox-5"><img src="./images/brandLOGO-hideo.png" alt="">
                    <div class="brands_hoverline">
                </li>
                <li id="tvbox-6"><img src="./images/brandLOGO-proteca.png" alt="">
                    <div class="brands_hoverline">
                </li>
            </ul>
        </div>
        <div class="brands_right mb-2-3">
            <div class="brandsBox tvBox-1" data-tvtag="tvBox-1"></div>
            <div class="brandsBox tvBox-2 d-none" data-tvtag="tvBox-2"></div>
            <div class="brandsBox tvBox-3 d-none" data-tvtag="tvBox-3"></div>
            <div class="brandsBox tvBox-4 d-none" data-tvtag="tvBox-4"></div>
            <div class="brandsBox tvBox-5 d-none" data-tvtag="tvBox-5"></div>
            <div class="brandsBox tvBox-6 d-none" data-tvtag="tvBox-6"></div>
        </div>
    </div>
</div>
<script src="./js/vegas.min.js"></script>