<!-- 流程進度圖 -->
<section id="forHonor">
    <div class="flex step-line-outbox">
        <div class="step-1 d-flex">
                    <a class="step-1-inner d-flex nonstyle-a" href="cart_DCdb.php?price=<?= $price ?>">
                        <h3>01</h3>
                        <p>購物車</p>
                    </a>
                </div>
                <div class="step-2 d-flex">
                    <a class="step-2-inner d-flex nonstyle-a" href="cart_DC_checklist.php?price=<?= $price ?>">
                        <h3>02</h3>
                        <p>確認訂單</p>
                    </a>
                </div>
                <div class="step-3 d-flex">
                    <a class="step-3-inner d-flex nonstyle-a" href="cart_DC_sendInfor.php?price=<?= $price ?>">
                        <h3>03</h3>
                        <p>配送資料</p>
                    </a>
                </div>
                <div class="step-4 d-flex">
                    <a class="step-4-inner d-flex nonstyle-a active" href="">
                        <h3>04</h3>
                        <p>付款方式</p>
                    </a>
                </div>
            </div>
        </section>