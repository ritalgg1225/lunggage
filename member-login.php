<?php
require __DIR__. '/__connect_db.php';
$page_name = 'login';
$page_title = '登入';

if(empty($_SERVER['HTTP_REFERER'])){
    $come_from = './';
} else {
    $come_from = $_SERVER['HTTP_REFERER'];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar|Noto+Sans+TC" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <title>會員中心 - 會員資料</title>
    <style>
        /* ----------------------------------------------------- 背景底圖區 */

        /* ------表格樣式------ */
        .login-input-style {
            background: transparent;
            border: 1px solid #858a8f;
            outline: none;
            padding-left: 10px;
            margin-bottom: 5px;
            -webkit-box-shadow: 000px 1000px white inset;
            font-size: 1rem;
            width: 300px;
        }
        /* ------註冊表格 ----- */

        form label {
            padding-bottom: 15px;
            color: #4d5258;
            font-size: 1.25rem;
        }

        form input {
            padding: 8px 0;
            color: #4d5258;
            font-size: 1.3rem;
            font-family: 'Noto Sans TC', sans-serif;
            font-weight: 300;
        }

        select,
        option {
            color: #4d5258;
            font-family: 'Noto Sans TC', sans-serif;
            font-weight: 300;
        }
        small {
            color: #be1b1b;
            font-size: 1rem;
        }

        .btn-submit {
            background: #243B55;
            color: #ffffff;
            font-family: 'Noto Sans TC', sans-serif;
            border: none;
            padding: 10px 20px;
            cursor: pointer;
        }

        .btn-submit:hover {
            background: #4a6583;
            color: #ffffff;
        }

        /* ------登入範圍區 ----- */
        .login-bg-con {
            width: 100%;
            height: 100vh;
            background: rgba(36, 59, 85, 0.8);
        }

        .login-con {
            min-height: 80vh;
            align-items: center;
        }

        .login-row {
            padding: 40px 30px;
            width: 100%;
            height: 600px;
            background: url(images/login_bg_03.jpg) no-repeat center center;
            background-size: cover;
        }

        .login-row h2 {
            text-align: center;
            letter-spacing: 10px;
            font-size: 2.2rem;
            color: #243B55;
            font-weight: 300;
        }
        
        
        .login-row a:hover{
           text-decoration: none;
        }

        /* ------左邊右邊登入 ----- */
        .login-left form{
            flex-direction: column;
            align-items: center;
        }
        .login-left a{
           color: #4d5258;
           padding: 0 25px;
        }
        .login-left form div{
            width: 100%;
        }
        .login-left{
            border-right: 1px solid #858a8f;
        }
        .login-left,
        .login-right {
            padding: 30px;
            flex-direction: column;
            justify-content: space-between;
            align-items: center;
        }
        .login-right p{
           line-height: 1.8;
           color: #4d5258;
           font-size: 1.1rem;
        }
        .login-link{
            justify-content: space-evenly;
        }

        .login-input-outline, .login-input-outline input{
            width: 100%;
            margin-top: 5px;
        }
/* ---登入按鈕寬度--- */       
        .btn-login{
            width: 100%;
            font-size: 1.1rem;
            font-weight: 600;
            
        }
/* ---首次登入--- */
        .first-shop{
            text-decoration: none;
            border: 1px solid #243B55;
            display: block;
            line-height: 1.15;
            width: 100%;
            text-align: center;
            font-size: 1.1rem;
            font-weight: 600;
            padding-top: 10px;
            padding-bottom: 10px;
            color: #4d5258;
           
        }
        .first-shop:hover{
            border: 1px solid #4a6583;
            background-color: #4a6583;
            color: #ffffff;
        }
 /* ----------------------------------------------------- login RWD */
        @media screen and (max-width:768px){
            .login-bg-con{
                height: auto;
            }
            .con-1100-px{
                width: 100%;
                height: auto;
            }

            .login-row{
                flex-direction: column;
                height: auto;
                width: 95%;
                margin: auto;
            }
            .login-row h2{
                font-size: 7vmin;
                font-weight: 500;
            }
            .login-left{
                border: none;
                border-bottom: 1px solid #858a8f;
                padding-bottom: 60px;
            }
            .login-left, .login-right{
                width: 100%;
                padding-left: 0;
                padding-right: 0;
            }
            .login-right p{
                margin-bottom: 40px;
            }
        }
        @media screen and (max-width:520px){
            .login-input-outline{
                margin-bottom: 10px;
            }
            .login-link{
                margin: 10px 0 25px 0;
            }
            .login-right p{
                font-size: 0.9rem;
            }
        }

        /*--------------------------info通知-----*/

        .info-wrap-bg{
            width: 100%;
            height: 100vh;
            background-color: #000000;
            position: fixed;
            z-index: 97;
            top: 0;
            left: 0;
            opacity: 0.7;
        }
        .info-alert{
            padding: 20px;
            color: #ffffff;
            background-color: #c67b8a;
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            z-index: 98;
            font-size: 1.2rem;
            letter-spacing: 2px;
            box-shadow: 1px 3px 10px #414449;
            transition: 0.5s;
            min-width: 450px;
            min-height: 180px;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width:630px){
            .info-alert{
                min-width: 80vw;
                min-height: 80vw;
            }

        }
    </style>
</head>

<body>
<?php //include __DIR__ . '/__navbar.php'?>
    <section>
<!--       ↓ 中間彈跳提示視窗 info通知 ↓        -->
        <div id="info-wrap" class="d-none">
            <div class="info-wrap-bg"></div>
            <div id="info" class="info-alert d-flex"></div>
        </div>
<!--       ↑ 中間彈跳提示視窗 info通知 ↑        -->
        <!-- <div class="member-bg"></div> -->
<!--        <div class="login-bg-con">-->
            <div class="con-1100-px d-flex login-con">
                <div class="login-row d-flex">
                    <div class="mb-1-2 login-left d-flex">
                        <h2 class="m-b-50">會 員 登 入</h2>
                            <form action="" method="post" name="registerform" class="d-flex mb-4-5" onsubmit="return formCheck()">
                                <div class="m-b-30 login-input-outline">
                                    <label for="email" class="fw-300">帳 號<span style="color:#9e3c3c">*</span></label><br>
                                    <input type="text" class="login-input-style" name="email" id="email"><br>
                                    <small id="emailHelp"></small><br>
                                </div>
                                <div class="m-b-30 login-input-outline">
                                    <label for="password" class="fw-300">密 碼<span style="color:#9e3c3c">*</span></label><br>
                                    <input type="password" class="login-input-style" name="password" id="password"><br>
                                    <small id="passwordHelp" ></small><br>
                                </div>
                                <div class="d-flex login-link m-y-35">
                                    <a href="#javascript">忘記密碼</a>
                                    <a href="member_register.html">會員註冊</a>
                                </div>
                                <div>
                                    <button type="submit" class="btn-submit btn-login">登 入</button>
                                </div>
                            </form>
                    </div>
                    <div class="mb-1-2 login-right d-flex">
                        <h2 class="m-b-50">訪客首次購物</h2>
                        <p>將喜愛的商品放入購物車完成訂購步驟，<br> 
                            最後留下個人資料，系統將自動為您升級為會員。<br>
                            立即享受，如此輕鬆的快速線上購物</p>
                        <div class="mb-4-5">
                            <a href="" class="first-shop">繼 續 首 次 購 物</a>
                        </div>
                    </div>
                </div>
            </div>
<!--        </div>-->
    </section>

    <!--------- script ----------->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        var fields = ['email', 'password'];
        var s;
        var info = $('#info');
        var infoWrap = $('#info-wrap');
        var emailHelp = $('#emailHelp');
        var email = $('#email');
        var come_from = '<?= $come_from ?>';


        function formCheck() {
            var isPass = true;
            var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            for (s in fields) {
                cancelAlert(fields[s]);
            }


            if (!email_pattern.test(document.registerform.email.value)) {
                setAlert('email', 'email格式錯誤');
                isPass = false;
            }

            if (document.registerform.password.value.length < 6) {
                setAlert('password', '密碼欄位需6字元以上');
                isPass = false;
            }

            if(isPass){
                $.post('member_login_api.php', $(document.registerform).serialize(), function (data) {


                    if (data.success){
                        setTimeout(function(){
                            location.href = come_from;
                        }, 1000);
                    }else{
                        emailHelp.html(data.info);
                        email.css('border', '1px solid #9e3c3c');
                    }

                    if (data.info){
                        infoWrap.removeClass('d-none');
                        info.html(data.info);

                        setTimeout(function(){
                            infoWrap.addClass('d-none');
                        }, 1500);
                    }

                },'json');
            }

            return false;
        }


        // 輸入錯誤通知
        function setAlert(fieldName, message) {
            $('#' + fieldName).css('border', '1px solid #9e3c3c');
            $('#' + fieldName + 'Help').text(message);
        }

        // 取消輸入錯誤通知
        function cancelAlert(fieldName) {
            $('#' + fieldName).css('border', '1px solid #858a8f');
            $('#' + fieldName + 'Help').text('');
        }




    </script>

<?php include __DIR__ . '/__html__footer.php'?>