
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>最新消息</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

</head>
<?php require __DIR__. '/__connect_db.php'?>
<?php include __DIR__. '/__navbar.php' ?>

<style>
/* -------------------------------------- body---------------------------------------------------*/
@import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
body {
  /* background-color: #aab9c0; */
  width: 100%;
  font-family: 'Open Sans', sans-serif;
  list-style: none;
  box-sizing: border-box;
}
a{
    text-decoration: none;
}
/* -------------------------------------- group style---------------------------------------------------*/
.relative{
    position: relative;
}
.absolute{
    position: absolute;
}
.t_center{
    text-align: center;
    margin:  0 auto;
}
.flex{
    display: flex;
}
.f_container{
    flex-wrap: wrap;
    flex-direction: row;
    /* flex-flow: row wrap; */
}
.f_colum{
    flex-direction: column;
}
.border-box{
    box-sizing: border-box;
}
/*-------*/
.hotnews-bg {
    background: url(./images/hotnews-bg-02.jpg) no-repeat center center;
    background-size: cover;
    width: 100%;
    height: 100vh;
    padding-top: 1px;
    color: #4d5258;
    position: fixed;
    top: 0;
    opacity: 0.3;
}
/*-------*/
/* ------------------------------------------------- card---------------------------------------------------*/
.card_content h2 {
  font-size: 2rem;
  font-weight: 700;
}
.card_content h2, .sub_title, p, a{
    /* padding: 10px 0; */
    margin: 10px 0;
}
/* 活動標題下方的橫線 */
.card_content h2:after {
    content: '';
    width: 6vw;
    background-color: #bea17f;
    height: 3px;
    display: block;
    margin-top: 5px;
}
.news_card{
    width: 75vw;
    /*border: #aab9c0 1px solid;*/
    border-radius: 5px;
    margin: 50px auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    background: #ffffffb8;
}
.card_img{
    width: 44%;
    height: 100%;
    right: 0;
}
.card_img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
}
/* 圖片hover的動畫效果 */
.card_img:hover{
    transform: scale(1.1);
    transition: all .5s ease-in-out;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.card_content {
    width: 50%;
    padding: 20px;
    /* box-sizing: border-box; */
}
.card_content h5{
    margin: 0;
}
/* --------------------------------btn--------------------------------- */
.read_more {
    width: 30%;
    padding: 8px 15px ;
    /* padding-left: 15px;
    padding-top: 5px;
    padding-bottom: 5px; */
    border-radius: 5px;
    color: #fefefe;
    background: linear-gradient(to right, #bea17f 50%, #fff 50%);
    background-size: 200% 100%;
    background-position: right bottom;
    color: #bea17f;
    transition: .3s ease-in;
    border: #bea17f solid 1px;
    cursor: pointer;
}
/* btn文字後方的箭頭 */
.read_more span {
  font-size: 18px;
  font-weight: 600;
}
.read_more a{
    /* color: #fefefe; */
    color: #bea17f;
}
.news_card:hover .read_more{
  background-position: left bottom;
  color: #fff;
}
.news_card:hover .read_more a{
    color: #fff;
}

@media only screen and (min-width: 885px) and (max-width : 1295px){ 
    .card_content .hot_title h2{
        font-size: 2rem;
    }
    .card_content .description p{
    overflow: hidden;
    /* text-overflow: ellipsis; */
    display: -webkit-box;
    /* white-space: nowrap; */
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 4;
    }
    .read_more{
        width: 50%;
    }
}
@media only screen and (min-width : 768px) and (max-width : 1024px) {
    /* .card_content{
        flex-grow: 2;
    }
    .card_img{
        flex-grow: 1;
    } */
}
@media only screen and (max-width : 884px) {
    .card_img{
        position: relative;
    }
    .card_content .hot_title h2{
        font-size: 1.5rem;
    }
    .card_content .description p{
        /* display: none; */
    font-size: 1rem;
    line-height: 1.7rem;
    overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    }
    .news_card{
        flex-direction: column;
    }
    .card_content{
        width: 100%;
        /* padding-top: 50%; */
    }
    .card_img {
        width: 100%;
        height: 40%;
    }
}
@media only screen and (max-width : 519px) {
    .news_card{
        width: 90%;
    }
    .card_content .title h2{
        font-size: 1.5rem;
    }
    .card_content .description p{
        display: none;
    }
    .read_more span{
        display: none;
    }
    .read_more{
        width: 100%;
    }
}
</style>
<body>
<div class="hotnews-bg"></div>
    <div class="news_card flex f_container relative">
        <div class="card_content flex f_colum">
            <div class="hot_title"><h2>你喝咖啡 我送折價券</h2></div>
            <div class="sub_title"><h4>2019/02/01 - 2019/02/28，四週共28天</h4></div>
            <div class="description"><h5>【週週抽機票，送雙人來回機票，喝咖啡暢遊四大城市】</h5><br>
                <p>每天都想飛出國？cama ꇑ .Container帶你暢遊香港、沖繩、曼谷、東京！
                當週消費就抽雙人來回機票、行李箱、飛行里程、半年份咖啡～
                還有超多好康大獎，等著你來抽！
                </p>
                </div>
                <a  class="read_more" href=""><p class="t_center">更多細節<span>  →</span></p></a>
        </div>          
        <div class="card_img absolute">
            <img src="images/scenario_event_01.jpg" alt="">    
        </div>
    </div>
    <div class="news_card flex f_container relative">
        <div class="card_content flex f_colum">
            <div class="hot_title"><h2>2019 VIP會員回饋方案</h2></div>
            <div class="sub_title"><h4>一整年</h4></div>
            <div class="description"><h5>【全館結帳再享95折優惠】</h5><br>
                <p>2019年度消費累積滿20000元，即可成為VIP會員，一年期間可享結帳95折優惠!
                </p></div>
                <a  class="read_more" href=""><p class="t_center">更多細節<span>  →</span></p></a>
        </div>          
        <div class="card_img absolute">
            <img src="images/2019.jpg" alt="">    
        </div>
    </div>
    <div class="news_card flex f_container relative">
        <div class="card_content flex f_colum">
            <div class="hot_title"><h2>.Container 會員生日好禮</h2></div>
            <div class="sub_title"><h4>即日起～</h4></div>
            <div class="description"><h5>【立刻加入會員來領去專屬的生日好禮吧！】</h5><br>
                <p>歡慶.Container平台上線，即日起註冊成為本站會員，並完成email與手機驗證，至會員中心填寫生日並修改成功，即可獲得限定一份的會員生日好禮！
                2019/2/15前完成還能加碼獲得.Container 500元折價券，強檔好禮馬上來申請！
                </p></div>
                <a  class="read_more" href=""><p class="t_center">更多細節<span>  →</span></p></a>
        </div>          
        <div class="card_img absolute">
            <img src="images/scenario_event_03.jpg" alt="">    
        </div>
    </div>
    <div class="news_card flex f_container relative">
        <div class="card_content flex f_colum">
            <div class="hot_title"><h2>BRIC's x UBER，好禮免費拿</h2></div>
            <div class="sub_title"><h4>2018/08/31 - 2018/10/31</h4></div>
            <div class="description"><h5>【BRIC'S x UBER，萬元行李箱抽獎活動】</h5><br>
                <!-- <p>歡慶.Container平台上線，即日起註冊成為本站會員，並完成email與手機驗證，至會員中心填寫生日並修改成功，即可獲得限定一份的會員生日好禮！
                2019/2/15前完成還能加碼獲得.Container 500元折價券，強檔好禮馬上來申請！
                </p> -->
                <ul>
                    <li>第一重好禮: UBER首乘2趟 100元免費送</li>
                    <li>第一重好禮: BRIC'行李箱免費抽</li>
                    <li>第一重好禮: BRIC'真皮護照免費換</li>
                </ul>
            </div>
                <a  class="read_more" href=""><p class="t_center">更多細節<span>  →</span></p></a>
        </div>          
        <div class="card_img absolute">
            <img src="images/scenario_event_05.jpg" alt="">    
        </div>
    </div>
    <!-- <div class="news_card flex relative f_container">
 
        <div class="card_content">
            <div class="title"><h2>你喝咖啡 我送折價券</h2></div>
            <div class="sub_title"><h4>2018/11/28 - 2018/12/25，四週共28天</h4></div>
            <div class="description"><h5>【週週抽機票，週週抽雙人來回機票，喝咖啡暢遊四大城市】</h5><br>
                <p>每天都想飛出國？cama ꇑ .Container帶你暢遊香港、沖繩、曼谷、東京！
                當週消費就抽雙人來回機票、行李箱、飛行里程、半年份咖啡～
                還有超多好康大獎，等著你來抽！
                </p></div>
            <div class="read_more"><p class="t_center">更多細節<span>  →</span></p></div>    
        </div>          
        <div class="card_img absolute">
            <img src="images/scenario_event_01.jpg" alt="">    
        </div>
    </div> -->
    <!-- <div class="news_card flex relative f_container">
        <div class="card_content">
            <div class="title"><h2>.Container 會員生日好禮</h2></div>
            <div class="sub_title"><h4>即日起～</h4></div>
            <div class="description"><h5>【立刻加入會員來領去專屬的生日好禮吧！】</h5><br>
                <p>歡慶.Container平台上線，即日起註冊成為本站會員，並完成email與手機驗證，至會員中心填寫生日並修改成功，即可獲得限定一份的會員生日好禮！
                2019/2/15前完成還能加碼獲得.Container 500元折價券，強檔好禮馬上來申請！
                </p></div>
            <div class="read_more"><p class="t_center">更多細節<span>  →</span></p></div>
        </div>
        <div class="card_img absolute">
            <img src="images/scenario_event_03.jpg" alt="">    
        </div>    
    </div> -->
    
    <?php include __DIR__. '/__html__footer.php' ?>