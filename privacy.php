<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>隱私權</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
</head>
<?php require __DIR__. '/__connect_db.php'?>
<?php include __DIR__. '/__navbar.php' ?>
<style>
/* -------------------------------------- entire---------------------------------------------------*/
 @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
*{
  margin: 0;
  padding: 0;
}
body{
    color: #4d5258;
    font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif;
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
/* --------------------------------------group style--------------------------------------------------*/
.relative{
    position: relative;
}
.non_bullet{
    list-style-type: none;
}
.pt_40{
    padding-top: 40px;
}
.pt_20{
    padding-top: 20px;
}
.t_bold{
    font-weight: bold;
}
.t_center{
    text-align: center;
}
/* --------------------------------------body--------------------------------------------------*/
.container{
    width: 75vw;
    margin:  0 auto;
}
.banner{
    width: 100%;
    height: 30vh;
    background: url(images/scenario-pri-01-yc.jpg) no-repeat fixed;
    background-size: cover;
}
.banner h1 {
    color :#c2b088;
    top: 20vh;
    font-size: 2.5rem;
    text-shadow: #22313F 10 10;
}
.pri_content span{
    color: #be1b1b;
}
.pri_content h2{
    padding:  10px 0 10px 0;
}
/* --------------------------------------rwd--------------------------------------------------*/
@media only screen and (max-width : 480px) {
    .container{
        width: 80%;
    }
    .banner h1{
        font-size: 1.5rem;
    }
}
</style>
<body>
    <div class="banner relative">
        <h1 class="relative t_bold t_center">隱私權保護</h1>
    </div>
    <div class="container">
        <ul class="pri_content pt_20"><h2>使用者隱私權保護聲明</h2>
            <p>.Container 極為重視個人資料之安全與隱私。<br>
                並將依據適用於所提交資訊目的之數據保護法處理您使用網站提交之任何個人資料。<br>
                依中華民國「個人資料保護法」及本隱私權保護聲明，
                在未獲得您許可之情況下，我們不會將您的姓名、住址、電子郵件信箱或任何其他個人資料轉交給第三方，
                但也提醒您；請勿在網路上公開透露您的個人資料，因該資料有可能會被他人蒐集和使用，特別是在網路上公開的發言場合。
            </p>
        </ul>
        <ul class="pri_content pt_20"><h2>(一)個人資料蒐集及使用</h2>
            <p>當您瀏覽.Container網站時，我們即會自動收集並保存該資訊。例如，您每次造訪本網站時，我們會收集您的IP位置、電腦主機名稱、瀏覽器資訊及轉來網站之網域名稱。<br>
                除事先說明或依台灣法律規定外，.Container不會擅自將您的個人資料提供給第三者或作為其他目的使用。<br>
                當您完成購物流程或參加其他活動時，.Container會要求您登綠個人資料，以便完成交易與相關服務。<br>
                .Container將自動記錄您在網站內的瀏覽作為流量分析型態與網站使用之資訊。但該資訊中不含有任何可識別您個人身分之內容。我們使用該資訊分析並改善本網站服務，同時向客戶提供滿意的網際網路體驗。<br>
                </P>
                <P>使用者資料:<br>
                <span>為了保障各位客戶權益，第一次光臨.Container網站消費的顧客，只要確認完成交易後，即會自動升級為會員。</span><br>
                註冊過程中您可以為自己設定一組帳號及密碼，經由該帳號，您可依照網站說明使用相關會員服務。<br>
                請妥善保管您的會員帳號及密碼，不要將上述資料提供給任何人，在使用完畢後，請登出您的帳號。<br>
                如果您與他人共用一台電腦或使用公共電腦，請記得關閉您的瀏覽器，以防止他人看到上述資料或取得進入您帳號的方法。<br>
                </p>
        </ul>
        <ul class="pri_content pt_20"><h2>(二)蒐集之目的</h2>
            <p>基於商品配送、消費者客戶管理與服務（包括但不限於會員相關權益通知、服務滿意度調查、為提供更適合會員之商品或優惠
                所作之統計分析等）、行銷資料、其他合於營業登記項目或章程所定之業務。
                </p>
        </ul>
        <ul class="pri_content pt_20"><h2>(三)蒐集之個人資料類別</h2>
            <p>本公司於網站內蒐集的個人資料包括
                個人資料類別：識別類(姓名、地址、電話、行動電話、電子郵件) 特徵類(出生年月日)。
                </p>
        </ul>
        <ul class="pri_content non_bullet pt_20"><h2>(四)利用期間、地區、對象及方式</h2>
            <li>(1) 期間：本公司營運期間。</li>
            <li>(2) 地區：消費者之個人資料將用於台灣地區。</li>
            <li>(3) 利用對象及方式：消費者之個人資料蒐集除用於本公司之會員管理、客戶管理之檢索查詢等功能外，將有以下利用：<br>
            <p>物品寄送：於交寄相關商品時，將消費者個人資料利用於交付給相關物流、郵寄廠商用於物品寄送之目的。
                金融交易及授權：消費者所提供之財務相關資訊，將於金融交易過程(如信用卡授權、轉帳)時提交給金融機構以完成金融交易並且(如信用卡授權、帳號)不會留在本公司做保存。
                行銷：本公司將利用消費者之地址及電子郵件、電話號碼進行本公司之宣傳行銷。                    
            </p></li>
        </ul>
        <ul class="pri_content non_bullet pt_20"><h2>(五)消費者個人資料之權利</h2>
            <p>消費者交付本公司個人資料者，依個資法得行使以下權利：<br>
                查詢或請求閱覽。<br>
                請求製給複製本。<br>
                請求補充或更正。<br>
                請求停止蒐集、處理或利用。<br>
                請求刪除。<br>
                </p>
            <p>消費者可以透過訂單線上問答或寄送電子郵件至lungagge000@case-iii.com.tw（為避免電子郵件系統漏信或其他原因無法收悉，
                以本公司回覆收悉為準)方式及來電洽詢客服02-3313-6666進行申請刪除取消、更正。
                </p>
        </ul>
        <ul class="pri_content non_bullet pt_20"><h2>(六)以上個資填寫不正確，將會影響會員權益。</h2>
            <p>隱私權保護政策之修訂
                .Container保留更改本聲明各項內容之權利，我們將於網站同一位置公告更改聲明之外，不另外對會員進行個別通知。
                若您對本隱私權保護政策有任何問題，可利用電子郵件container000@iii.com.tw直接與本公司請直接與本公司聯繫或停止使用本網站服務。
            </p>
        </ul>
        <p>修訂版本2018年6月19日</p>
    </div>
    
<?php include __DIR__. '/__html__footer.php' ?>