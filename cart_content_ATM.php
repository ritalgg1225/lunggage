<!-- wrapper -->
<div class="w75vwDC  textAlignCenterDC marginAutoDC">
    <div class="h10vhDC w30vwDC marginAutoDC"></div>
    <div class="marginBottom10vhDC">
        <img class="logoDC" src="images/logo_680x335_black.svg" alt="">
    </div>  
            <!-- 購物車肚皮 -->
            <main class="ATMbellyDC backWhiteDC">
                <span class="ATM_title">你專屬帳號及應付金額</span>
                <form class="ATM" name="registerform" method="post">
                    <div class="flex mobile4DC">
                        <span class="fieldDC flex">收款銀行</span>
                        <label class="w100PercentDC" for="name">
                            <input class="cursorDefault" type="text" name="name" placeholder="台灣銀行" readonly="readonly"/>
                        </label>
                    </div>
                    <div class="flex mobile4DC">
                        <span class="fieldDC fieldDC">銀行代碼</span>
                        <label class="w100PercentDC marginTop1vhDC" for="name">
                            <input class="cursorDefault" type="text" name="name" placeholder="004" readonly="readonly"/>
                        </label>
                    </div>
                    <div class="flex mobile4DC">
                        <span class="fieldDC fieldDC">收款帳號</span>
                        <label class="w100PercentDC" for="name">
                            <input class="cursorDefault" id="ATMaccount" type="text" name="name" placeholder="123456-666787" readonly="readonly"/>
                        </label>
                    </div>
                    <div class="flex mobile4DC">
                        <span class="fieldDC fieldDC">應付款項</span>
                        <label class="w100PercentDC" for="name">
                            <input class="cursorDefault" type="text" name="name" placeholder="$ <?= $price ?>元" readonly="readonly"/>
                        </label>
                    </div>
                    <div class="flex mobile4DC">
                        <span class="fieldDC">截止日期</span>
                        <label class="w100PercentDC" for="name">
                            <input class="cursorDefault" type="text" name="name" placeholder="108/02/20" readonly="readonly"/>
                        </label>
                    </div>
                    <div class="">
                        <button type="button" class="checkOutButtonDC backGreyBlueDC pWhiteDC">確認</button>
                    </div>
                </form>
            </main>
</div>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

    function pageRedirect() {
        window.location.replace("index.php");
    }
    // setTimeout("pageRedirect()", 10000);
    $(".checkOutButtonDC").click(function() {
        pageRedirect();
    });
</script>