// 選單下滑
$(document).ready(function () {
    $("#flip1").click(function () {
        $("#panel1").slideToggle("slow");
        $("#panel2").slideUp("slow");
        $("#panel3").slideUp("slow");
    });
});

$(document).ready(function () {
    $("#flip2").click(function () {
        $("#panel2").slideToggle("slow");
        $("#panel1").slideUp("slow");
        $("#panel3").slideUp("slow");
    });
});

$(document).ready(function () {
    $("#flip3").click(function () {
        $("#panel3").slideToggle("slow");
        $("#panel1").slideUp("slow");
        $("#panel2").slideUp("slow");
    });
});