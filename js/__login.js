// 登入視窗彈出
// Example 1: From an element in DOM
$('.open-popup-link').magnificPopup({
    type:'inline',
    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});

// 表單
var fields = ['email', 'password'];
var s;
var info = $('#info');
var infoWrap = $('#info-wrap');
var emailHelp = $('#emailHelp');
var passwordHelp = $('#passwordHelp');
var email = $('#email');
var password = $('#password');


function formCheck() {
    var isPass = true;
    var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    for (s in fields) {
        cancelAlert(fields[s]);
    }


    if (!email_pattern.test(document.loginform.email.value)) {
        setAlert('email', 'email格式錯誤');
        isPass = false;
    }

    if (document.loginform.password.value.length < 6) {
        setAlert('password', '密碼欄位需6字元以上');
        isPass = false;
    }

    if(isPass){
        $.post('member_login_api.php', $(document.loginform).serialize(), function (data) {


            if (data.success){
                setTimeout(function(){
                    location.href = './';
                }, 1000);
            }else{
                emailHelp.html(data.info);
                passwordHelp.html(data.info);
                email.css('border', '2px solid #9e3c3c');
                password.css('border', '2px solid #9e3c3c');
            }

            if (data.info){
                infoWrap.removeClass('d-none');
                info.html(data.info);

                setTimeout(function(){
                    infoWrap.addClass('d-none');
                }, 1500);
            }

        },'json');
    }

    return false;
}


// 輸入錯誤通知
function setAlert(fieldName, message) {
    $('#' + fieldName).css('border', '1px solid #9e3c3c');
    $('#' + fieldName + 'Help').text(message);
}

// 取消輸入錯誤通知
function cancelAlert(fieldName) {
    $('#' + fieldName).css('border', '1px solid #858a8f');
    $('#' + fieldName + 'Help').text('');
}