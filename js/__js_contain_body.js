    //“jQuery(function($)” 可改成JQ写法
$(document).ready(function($) { 
        
        //將“動畫”類添加到視圖中的任何一个“.animatable”的函數
        var doAnimations = function() {
          
          
        //計算當前偏移並獲取所有動畫        
          var offset = $(window).scrollTop() + $(window).height(),
              $animatables = $('.animatable');
          
          //如果我們沒有動畫，則取消綁定滾動處理程序
          if ($animatables.length == 0) {
            $(window).off('scroll', doAnimations);
          }
          
          
        //檢查所有動畫，並在必要時為其設置動畫
        //offset()：是获得top或者left的位置，与盒子设置的margin或者padding或者位置有关
              $animatables.each(function(i) {
             var $animatable = $(this);
                  if (($animatable.offset().top + $animatable.height() - 20) < offset) {
              $animatable.removeClass('animatable').addClass('animated');
                  }
          });
      
          };
        
        //滾動時绑定动作（"滚动"，doAnimations)，並觸發滾動
          $(window).on('scroll', doAnimations);//scroll是一个动作，和click一样
        $(window).trigger('scroll');
      
}); 




