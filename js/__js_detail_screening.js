//下拉式选单
// $(".detail").click(function(){
//     $(".detail_2-1").toggleClass('active');
// //     // $(".detail_2-1").slideToggle("slow");
// });

$(function(){
    $(".detail").click(function(){
        $("#detail_use").fadeToggle('slow');
        // $(".detail_2-1 ul p ").fadeToggle('slow');
        // alert("ssssss");
    });
});



//筛选更多：多選checkbox/是否被点击背景变色
$(function(){
    $(".checkbox_i").click(function(){
        if($(this).prop("checked")){
            // $(this).next().css('background', '#243b55');
            $(this).next().css('border-bottom', '#243B55 solid 2px');
            $(this).next().css('color', '#243B55');
        }else{ 
            // $(this).next().css('background', 'rgba(225, 225, 225, 0)');
            $(this).next().css('border-bottom', 'rgba(225, 225, 225, 0)');
            $(this).next().css('color', '#9fa6ad');
        }
    });   
});

//筛选更多：單選radio/是否被点击背景变色 容量
$(function(){
    $(".radio_day").click(function(){
            $(this).next().css('border-bottom', '#243B55 solid 2px');
            $(this).next().css('color', '#243B55');
            $(".radio_day").not(this).next().css('border-bottom', 'rgba(225, 225, 225, 0)');
            $(".radio_day").not(this).next().css('color', '#9fa6ad');
    });  
});

// //筛选更多：單選radio/是否被点击背景变色 價格
$(function(){
    $(".radio_price").click(function(){
            $(this).next().css('border-bottom', '#243B55 solid 2px');
            $(this).next().css('color', '#243B55');
            $(".radio_price").not(this).next().css('border-bottom', 'rgba(225, 225, 225, 0)');
            $(".radio_price").not(this).next().css('color', '#9fa6ad');
    });  
});

//------------------筛选--------------------//

var products_data;

$(function(){
        // var brand_items = $('.brand_items');//品牌
        // var cosi_items = $('.cosi_items');//尺寸
        // var day_items = $('.day_items');//天数
        // var box_items = $('.box_items');//类型
        // var price_items = $('.price_items');//价格范围
        // var texture_items = $('.texture_items');//材质
  

        window.p_filter = function(page){
            page = page || 1;

            //品牌选区
            var brandAll =[];
            $(".form_shop ul .brand_items:checked").each(function(){
                brandAll.push($(this).val());
            });

            //尺寸选区
            var cosiAll =[];
            $(".form_shop ul .cosi_items:checked").each(function(){
                cosiAll.push($(this).val());
            });

            //天数:容量选区
            var dayAll =filter_form.day_name.value;
            var dayAll_a = [];
            $(".form_shop ul .day_items:checked").each(function(){
                dayAll_a = dayAll.split(",");
            });

            //价格范围选区
            var priceAll =filter_form.price_name.value;
            var priceAll_a = [];
            $(".form_shop ul .price_items:checked").each(function(){
                priceAll_a = priceAll.split(",");

            });
            // console.log(priceAll_a);

            // var aaa = priceAll.spilt(0);
            // var ccc = priceAll.splice(1);
            // $(".form_shop ul .price_items:checked").each(function(){
            //     priceAll.push($(this).val());
            // });

            //类型选区
             var boxAll =[];
             $(".form_shop ul .box_items:checked").each(function(){
                 boxAll.push($(this).val());
             });
            
            //材质选区
            var textureAll =[];
            $(".form_shop ul .texture_items:checked").each(function(){
                textureAll.push($(this).val());
            });


            var product_list =$("#product_list");
            var pagination_ul = $('#pagination_ul');

            var send_obj = {
                page :page,
                brand: JSON.stringify(brandAll),
                size: JSON.stringify(cosiAll),
                day_0: JSON.stringify(dayAll_a[0]),//容量最小值
                day_1: JSON.stringify(dayAll_a[1]),//容量最大值
                box: JSON.stringify(boxAll),
                price_0: JSON.stringify(priceAll_a[0]),//價錢最小值
                price_1: JSON.stringify(priceAll_a[1]),//價錢最大值
                texture: JSON.stringify(textureAll),
                
                
            };   

            // console.log(send_obj);

            $.post('__html_com_detail_api.php',send_obj, function (data) {
                products_data = data;
                 console.log(data)//data是api所回传的东西可自己取名

                var k,item;
                product_list.empty();
                    //顯示商品
                    for(k in products_data.data){
                        item = products_data.data[k];//for in 回圈：是将obj内容(value)k分别取出
                        
                        // console.log(item);

                        product_list.append(shop(item)); //append();结尾插入内容
                    }

                    //顯示商品顏色



                    // for(s in products_data.colors){
                        

                    //     for(var a=s; a<= 12;a++ ){
                    //         obj = products_data.colors[a];//for in 回圈：是将obj内容(value)k分别取出
                        
                    //         console.log(obj);
                    //     }

                    //     product_list.append(shop(obj)); //append();结尾插入内容
                    // }


            //---------------------------------页数 start
                pagination_ul.empty();//最外层ul

                var tpl = pagin_prev;//按键 左
                var page_active = '';
                for(var i=1; i<=products_data.totalPages; i++){
                    if(data.page==i){
                        page_active = 'page_active';
                    } else {
                        page_active = '';
                    }
                    tpl += pagin({
                        i:i,
                        page_active: page_active
                    });
                }
                // console.log(tpl);
                tpl += pagin_next;//pagin_next：按键右
                // console.log(tpl);

                pagination_ul.html(tpl);
            //---------------------------------页数 end

            //---------------------------------color start
                // color spots settings
                $('#product_list > li').each(function(){
                    var SID = $(this).attr('data-sid');
                    var colors = products_data.colors[SID]; // array
                    // console.log('colors:', colors);
                    $(this).find('.li-color').each(function(index){ // index:当前选择哪一项的位置
                        // console.log('this:', this);
                        // console.log(index);
                        if(colors[index]){
                            $(this).css('background-color', colors[index]);
                            $(this).show();
                        }
                    });

                });

                
                $('#product_list > li').each(function(){
                    var SID = $(this).attr('data-sid');
                    // console.log(SID);
                    var colors_c = products_data.colors_text[SID]; // array
                    console.log('colors:', colors_c);
                    $(this).find('.list_dt_color').each(function(index){ // index:当前选择哪一项的位置
                        // console.log('this:', this);
                        console.log(index);
                        if(colors_c[index]){
                            $(this).html(colors_c[index]);
                            $(this).show();
                        }
                    });

                });



                //<p class="list_dt_color">月光白</p>
                //<p class="list_dt_color">月光白</p>
                //<p class="list_dt_color">月光白</p>
            //---------------------------------color end


            //---------------------------------加入成功 start
                $(".add_click").on("click",$(".add_click"),function(){

                     $('#label').css("border-radius", "25px 0 0 25px");//變成圓形
                     $('#label').css("right", "0");
             
                     setTimeout(function (){
                        $('#label').attr("disabled",false);
                        $('#label').css("border-radius", "5px 0 0 5px");
                        $('#label').css("right", "-100px");
                     },900);
             
                 });

                 $(".add").on("click",$(".add"),function(){

                    $('#label').css("border-radius", "25px 0 0 25px");//變成圓形
                    $('#label').css("right", "0");
            
                    setTimeout(function (){
                       $('#label').attr("disabled",false);
                       $('#label').css("border-radius", "5px 0 0 5px");
                       $('#label').css("right", "-100px");
                    },900);
            
                });
            //---------------------------------加入成功 end       
            }, 
            "json"); 

         

        };

        var shop_data = `
    <li id="prod<%= SID %>" data-sid="<%= SID %>">
        <a class="nonstyle-a" style="color:#243B55" href="./product.php?sid=<%= SID %>">
            <!-- 商品點擊區塊 -->
            <div class="list_ani  absolute">
                        <ul class="list_ani_ul d-flex">

                            <a href="./product.php?sid=<%= SID %>">

                                <li class="list_ani_hover">
                                        <!--<div class="flex list_ani_search">-->
                                            
                                            <!--<img class="wish" src="./images/icon_sm_search-wi.png" alt="搜尋">-->
                                        <!--</div>-->
                                        <p class="ff-mukta list_dt_brand"><%= brand %></p>
                                        <p class="list_dt_price ff-mukta">NT.<span><%= price %></span></p>
                                        <p class="ff-mukta-noto list_dt_size"><span><%= size %></span> 吋</p>
                                        <p class="list_dt_color" data-sid="<%= SID %> ></p>
                                        <p class="list_dt_color">月光白</p>
                                        <p class="list_dt_color">月光白</p>
                                        <p class="list_dt_color">月光白</p>
                                        
                                </li>

                            </a>

                            <!------ 2019/2/16 13:01 Rita更新 start------->

                            
                            <!-- 2019/2/16 13:01 Rita更新刪除加入願望-->
                            <!-- 2019/2/17 14:37 Rita更新刪除加入比較-->

                            <!--<li  class="flex add_click">-->
                            <!--<p><%= size_text %></p>-->
                            <!--</li>-->

                            <!------ 2019/2/16 13:01 Rita更新 end------->


                        </ul>
            </div>
                <!-- 商品图片 -->
                <a href="./product.php?sid=<%= SID %>"><!-- mobile需要不要删除 -->
                    <img class="relative" src="./images/product/<%= pic_nu %>">
                </a>
                 <!-- 商品名字 -->
                <h6 class="ff-mukta"><%= brand %></h6>
    
                 <!-- 商品系列 -->
                <p class="p_12px_wil ff-mukta-noto"><%= type %></p>
    
                 <!-- 商品颜色 -->
                <div class="ul-color flex">
                    <div class="li-color" style="" ></div>
                    <div class="li-color"></div>
                    <div class="li-color"></div>
                </div>
                
                 <!-- 商品价钱 -->
                <div class="price" style="color:#961900">
                <p class="fc-red">NT. <%= price %> </p>
                </div>
    
                <!-- 商品最爱和比较 -->
    
                <!--底線-->
                <div class="buy_border"></div>
        </a>
    </li>
            `;
var shop = _.template(shop_data);
        
// --------------------------------------------------------頁數 老師修改部分


//left
var pagin_prev = `
    <li class="page-item-wi_prev page_left">
        <a class="page-link" href="javascript:goPrev()">
            <i class="fas fa-angle-left"></i>
        </a>
    </li>
`;
//next
var pagin_next = `
    <li class="page-item-wi_next page_right">
        <a class="page-link" href="javascript:goNext()">
            <i class="fas fa-angle-right"></i>
        </a>
    </li>
`;
//页码 1 ~ 11 页
var pagin_data = `
    <li class="page-item-wi <%= page_active %>">
            <a class="page-link" id="ppp" href="javascript:p_filter(<%= i %>)"><%= i %></a>
    </li>
`;

var pagin = _.template(pagin_data);

    $(".form_shop ul input").click(function(){
        p_filter(1);
    });



p_filter(1);

});

//页码往后
var goPrev = function(){
    if(products_data.page==1){
        return;
    }
    p_filter(products_data.page-1);
}

//页码往前
var goNext = function(){
    if(products_data.page==products_data.totalPages){
        return;
    }
    p_filter(products_data.page+1);
}