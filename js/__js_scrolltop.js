
$(document).ready(function () {

    var scrollTop = $(".scrollTop");

    $(window).scroll(function () {

        var topPos = $(this).scrollTop();

        if (topPos > 500) {
            $(scrollTop).css("opacity", "1");

        } else {
            $(scrollTop).css("opacity", "0");
        }

    });


    $(scrollTop).click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    });

    //-----------------------------------------音乐效果
    window.onload = function () {
        var elevator = new Elevator({
            element: document.querySelector('.scrollTop'),
            // mainAudio: './music/elevator.mp3',
            endAudio: './music/ding.mp3',
            duration: 1500,
        });
    };

    elevator.elevate();
});

