$(".contain").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./img/page_banner_1.jpg" },
        { src: "./img/page_banner_2.jpg" },
        { src: "./img/page_banner_3.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});