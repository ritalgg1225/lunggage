$(".tvBox-1").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-calpak-01.jpg" },
        { src: "./images/pic-calpak-02.jpg" },
        { src: "./images/pic-calpak-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});

$(".tvBox-2").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-antler-01.jpg" },
        { src: "./images/pic-antler-02.jpg" },
        { src: "./images/pic-antler-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});


$(".tvBox-3").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-delsey-01.jpg" },
        { src: "./images/pic-delsey-02.jpg" },
        { src: "./images/pic-delsey-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});

$(".tvBox-4").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-away-01.jpg" },
        { src: "./images/pic-away-02.jpg" },
        { src: "./images/pic-away-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});

$(".tvBox-5").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-hideo-01.jpg" },
        { src: "./images/pic-hideo-02.jpg" },
        { src: "./images/pic-hideo-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});

$(".tvBox-6").vegas({
    timer: false,
    delay: 7000,
    slides: [
        { src: "./images/pic-proteca-01.jpg" },
        { src: "./images/pic-proteca-02.jpg" },
        { src: "./images/pic-proteca-03.jpg" }
    ],
    animation: 'kenburns',
    animationDuration: 7000,
    overlay: 'overlays/01.png'
});


$("#tvbox-1").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-1'])").each(function () {
        $(this).addClass("d-none");
    });
});

$("#tvbox-2").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-2'])").each(function () {
        $(this).addClass("d-none");
    });
});

$("#tvbox-3").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-3'])").each(function () {
        $(this).addClass("d-none");
    });
});

$("#tvbox-4").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-4'])").each(function () {
        $(this).addClass("d-none");
    });
});

$("#tvbox-5").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-5'])").each(function () {
        $(this).addClass("d-none");
    });
});

$("#tvbox-6").click(function () {
    $(".brandsBox").removeClass("d-none");
    $(".brandsBox:not(.brandsBox[data-tvtag='tvBox-6'])").each(function () {
        $(this).addClass("d-none");
    });
});