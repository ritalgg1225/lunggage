<?php 
require __DIR__. '/__connect_db.php';

//----------------品牌
$brand = isset($_POST['brand']) ? $_POST['brand'] : '';
if(!empty($brand)){
    $brand = json_decode($brand);
}

//----------------尺寸
$size = isset($_POST['size']) ? $_POST['size'] : '';
if(!empty($size)){
    $size = json_decode($size);
}

//----------------容量 :$day_0 $day_1 範圍
$day_0 = isset($_POST['day_0']) ? $_POST['day_0'] : '';
if(!empty($day_0)){
    $day_0 = json_decode($day_0);
}

$day_1 = isset($_POST['day_1']) ? $_POST['day_1'] : '';
if(!empty($day_1)){
    $day_1 = json_decode($day_1);
}

//----------------類型
$box = isset($_POST['box']) ? $_POST['box'] : '';
if(!empty($box)){
    $box = json_decode($box);
}

//----------------價格範圍 :$price_0 $price_1 範圍
$price_0 = isset($_POST['price_0']) ? $_POST['price_0'] : '';
if(!empty($price_0)){
    $price_0 = json_decode($price_0);
}

$price_1 = isset($_POST['price_1']) ? $_POST['price_1'] : '';
if(!empty($price_1)){
    $price_1 = json_decode($price_1);
    
}

//----------------材質
$texture = isset($_POST['texture']) ? $_POST['texture'] : '';
if(!empty($texture)){
    $texture = json_decode($texture);
}



$where = ' WHERE 1 ';

if(!empty($brand) ){
    $where .= sprintf(
        " AND `brand` IN ('%s')",
         implode("','", $brand)
    );
}

//".="：串接赋值


if(!empty($size) ){
    $where .= sprintf(
        // " AND (`size`>='%s' AND `size`<='%s')" ,
        " AND `size`IN ('%s') " ,
         implode("','", $size)
    );
}


if(!empty($day_0) ){
    $where .= sprintf(
        " AND (`insideL` >='%s' AND `insideL` <= '%s')",
         $day_0,$day_1
    );
}


if(!empty($box) ){
    $where .= sprintf(
        " AND `box` IN ('%s')",
         implode("','", $box)
    );
}


if(!empty($price_0) ){
    $where .= sprintf(
        " AND (`price` >= %s AND `price` <=%s)",// 价格是数值，不要'%s'
         intval($price_0), intval($price_1)
    );
}


if(!empty($texture) ){
    $where .= sprintf(
        " AND `texture` IN ('%s')",
         implode("','", $texture)
    );
}

//$page -> 用户要看第几页
$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
if(!empty($page)){
    $page = json_decode($page);
}

$per_page = 16;//一页16笔
$start_index = ($page-1)*$per_page;


$sql = "SELECT lu.*, pr.* FROM `lunggage_data` lu 
JOIN `product_list` pr ON lu.`SID` = pr.`type_sid`
 $where
 LIMIT $start_index , $per_page
 ";
// echo $price_0;

// echo $price_1;

//  echo $sql;
// echo $sql; exit;
$sql_i =  $pdo->query($sql);
$product = $sql_i->fetchAll(PDO::FETCH_ASSOC);//选中项目的数量和内容
//   echo $product;
//-------------------------------顏色
$type_size_ids = [];
foreach($product as $p){
    $type_size_ids[] = $p['type_sid'];
}


$color_sql = sprintf("SELECT cm.color_code, cm.color, pl.type_sid FROM `product_list` pl 
JOIN color_mapping cm ON pl.color_sid=cm.color_sid
WHERE pl.type_sid IN (%s)", implode(',', $type_size_ids) );


// colors data
$colors = $pdo->query($color_sql)->fetchAll(PDO::FETCH_ASSOC);
$colors1 = [];
foreach($colors as $c){
    $colors1[$c['type_sid']][] = $c['color_code'];//將$c['color_code'] push 到 [] 中
    // echo $colors;
}
$colors1_text = [];
foreach($colors as $c){
    $colors1_text[$c['type_sid']][] = $c['color'];//將$c['color_code'] push 到 [] 中
    // echo $colors;
}


//-------------------------------頁數
$per_page = 16;//一页16笔
$t_sql = " SELECT COUNT(1)  FROM `lunggage_data` lu 
JOIN `product_list` pr ON lu.`SID` = pr.`type_sid`
$where
";//选中选项有几笔

// echo $t_sql;

$total_rows = ceil($pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0]);//一共有几个项目

$total_page = ceil($total_rows/$per_page); //算整数


echo json_encode([
    'page' => $page,
    'totalItems' => $total_rows,
    'totalPages' => $total_page,
    'colors' => $colors1,
    'colors_text' => $colors1_text,
    'data' => $product,
    

], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); 


 