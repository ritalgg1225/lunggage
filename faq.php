<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FAQs</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
</head>

<?php include __DIR__. '/__navbar.php' ?>
<style>
/* -------------------------------------- entire---------------------------------------------------*/
@import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
*{
  margin: 0;
  padding: 0;
}
body{
    font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif;
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
/* ----------------------css group style-------------------- */
.relative{
  display: relative;
}
.flex{
  display: flex;
}
.t_center{
    text-align: center;
}
/* ----------------------- banner ---------------------------- */
header{
    width: 100vw;
    background-color: #ffffff;
    position: fixed;
    z-index: 9999;
}
.banner{
    width: 100%;
    height: 30vh;
    background: url(images/scenario-qa-02-yc.jpg)  no-repeat fixed;
    background-position: 0 -200px ;
    background-size: cover;
}
.banner h1 {
  color :#c2b088;
  font-weight: bold;
  position: relative;
  top: 20vh;
  /* left: 2vw; */
  font-size: 2.5rem;
  text-align: center;
  text-shadow: #22313F 10 10;
  vertical-align:middle;
}
.banner p {
  color :#c2b088;
  font-weight: bold;
  position: relative;
  top: 15vh;
  /* left: 2vw; */
  font-size: 2.5rem;
  text-align: center;
  text-shadow: #22313F 10 10;
  vertical-align:middle;
  /* color :#c2b088;
  font-weight: bold;
  position: absolute;
  top: 15vh;
  left: 30vw;
  font-size: 1rem;
  padding: 1rem;
  text-align: center;
  text-shadow: #22313F 10 10; */
}
.flex{
  display: flex;
}
.categories li{
  align-content: space-between;
}
.categories li:active{
  border-bottom: 2px solid #22313f;
}
.categories li a{
  /* padding-bottom: .5rem; */
  text-decoration: none;
  color:#22313F;
}
.categories a:hover li{
  color:#34495e;
  border: 2px solid #22313f;
}
.categories a:active li{
  /* color:#22313F; */
  border: 2px solid #22313f;
}
.accordion dl {
}

.accordion dt a {
  /* font-weight: 700; */
  padding: 1rem;
  display: block;
  text-decoration: none;
  color: #fff;
  -webkit-transition: background-color 0.5s ease-in-out;
}
.accordion dd {
  background-color: #637b92;
  color:#fafafa;
  padding-left: 2rem;
  font-size: 1em;
  line-height: 1.5em;
}
.accordion dd > p {
  padding: 1rem 2rem 1rem 2rem;
}
.accordion {
  position: relative;
  background-color: #c2b088;
}
.accordion h2{
  letter-spacing: .2rem;
}
.faq {
  max-width: 75vw;
  margin: 0 auto;
  padding: 2rem 0 2rem 0;
}
.faq-title{
  font-size: 1rem;
  padding: 1rem 0 1rem 0 ;
}
.faq li{
  list-style: none;
  padding:  0 2rem 1rem 2rem ;
}
.accordionTitle {
  background-color: #22313F;
  border-bottom: 1px solid #2c3e50;
  letter-spacing: .2rem;
  font-size: 1rem;
  font-weight: 200;
}
.accordionTitle:before {
  content: "+";
  font-size: 1.5rem;
  line-height: 1.5rem;
  float: left;
  -moz-transition: -moz-transform 0.3s ease-in-out;
  -o-transition: -o-transform 0.3s ease-in-out;
  -webkit-transition: -webkit-transform 0.3s ease-in-out;
  transition: transform 0.3s ease-in-out;
  /* height: 5rem; */
}
.accordionTitle:hover {
  background-color: #2c3e50;
}

.accordionTitleActive {
  background-color:#34495e;
}
.accordionTitleActive:before {
  -webkit-transform: rotate(-225deg);
  -moz-transform: rotate(-225deg);
  transform: rotate(-225deg);
}
.accordionItem {
  height: auto;
  overflow: hidden;
}
.accordionItem li{
  padding: 1rem 2rem 1rem 2rem;
}
@media all {
  .accordionItem {
    max-height: 50em;
    -moz-transition: max-height 1s;
    -o-transition: max-height 1s;
    -webkit-transition: max-height 1s;
    transition: max-height 1s;
  }
}
@media screen and (min-width: 48em) {
  .accordionItem {
    max-height: 15em;
    -moz-transition: max-height 0.5s;
    -o-transition: max-height 0.5s;
    -webkit-transition: max-height 0.5s;
    transition: max-height 0.5s;
  }
}
.accordionItemCollapsed {
  max-height: 0;
}
.animateIn {
  -webkit-animation-name: accordionIn;
  -webkit-animation-duration: 0.65s;
  -webkit-animation-iteration-count: 1;
  -webkit-animation-direction: normal;
  -webkit-animation-timing-function: ease-in-out;
  -webkit-animation-fill-mode: both;
  -webkit-animation-delay: 0s;
  -moz-animation-name: normal;
  -moz-animation-duration: 0.65s;
  -moz-animation-iteration-count: 1;
  -moz-animation-direction: alternate;
  -moz-animation-timing-function: ease-in-out;
  -moz-animation-fill-mode: both;
  -moz-animation-delay: 0s;
  animation-name: accordionIn;
  animation-duration: 0.65s;
  animation-iteration-count: 1;
  animation-direction: normal;
  animation-timing-function: ease-in-out;
  animation-fill-mode: both;
  animation-delay: 0s;
}
.animateOut {
  -webkit-animation-name: accordionOut;
  -webkit-animation-duration: 0.75s;
  -webkit-animation-iteration-count: 1;
  -webkit-animation-direction: alternate;
  -webkit-animation-timing-function: ease-in-out;
  -webkit-animation-fill-mode: both;
  -webkit-animation-delay: 0s;
  -moz-animation-name: accordionOut;
  -moz-animation-duration: 0.75s;
  -moz-animation-iteration-count: 1;
  -moz-animation-direction: alternate;
  -moz-animation-timing-function: ease-in-out;
  -moz-animation-fill-mode: both;
  -moz-animation-delay: 0s;
  animation-name: accordionOut;
  animation-duration: 0.75s;
  animation-iteration-count: 1;
  animation-direction: alternate;
  animation-timing-function: ease-in-out;
  animation-fill-mode: both;
  animation-delay: 0s;
}
@-webkit-keyframes accordionIn {
  0% {
    opacity: 0;
    -webkit-transform: scale(0.8);
  }
  100% {
    opacity: 1;
    -webkit-transform: scale(1);
  }
}
@-moz-keyframes accordionIn {
  0% {
    opacity: 0;
    -moz-transform: scale(0.8);
  }
  100% {
    opacity: 1;
    -moz-transform: scale(1);
  }
}
@keyframes accordionIn {
  0% {
    opacity: 0;
    transform: scale(0.8);
  }
  100% {
    opacity: 1;
    transform: scale(1);
  }
}
@-webkit-keyframes accordionOut {
  0% {
    opacity: 1;
    -webkit-transform: scale(1);
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(0.8);
  }
}
@-moz-keyframes accordionOut {
  0% {
    opacity: 1;
    -moz-transform: scale(1);
  }
  100% {
    opacity: 0;
    -moz-transform: scale(0.8);
  }
}
@keyframes accordionOut {
  0% {
    opacity: 1;
    transform: scale(1);
  }
  100% {
    opacity: 0;
    transform: scale(0.8);
  }
}
@media only screen and (min-width: 1440px) { 

}
@media only screen and (max-width : 1200px){
  .faq li{
    padding: 1rem 2rem 1rem 2rem;
  }
}
@media only screen and (min-width : 920px) and (max-width : 1024px) {
  .accordion dt a{
    font-size: 1rem;
    font-weight: 400;
    text-align: center;
  }
}
@media only screen and (min-width : 768px) and (max-width : 919px) {
  .categories{
      display: none;
    }
    .option{
        width: 50%;
    }
}
@media only screen and (max-width : 767px) {
    .categories{
      display: none;
    }
    .option{
        width: 100%;
        padding: 30px 20px;
    }
    .t_content{
        width: 85%;
    }
    .accordionTitle {
      letter-spacing: .2rem;
      font-size: .8rem;
      font-weight: 200;
    }
    .accordionItem ul, .accordionItem p{
        font-size: .9rem;
    }
}
@media only screen and (max-width : 585px){
    .accordion dt a{
      text-align: left;
    }
    .accordionTitle:before{
      content: " ";
    }
}
</style>
<body>
    <header>
        <!-- <nav><h1>. Container</h1></nav> -->
    </header>
    <div class="banner relative">
      <p>常見問題</p>
    </div>
    <div class="faq">
        <ul class="categories flex">
          <li><a class="selected" href="#account">會員常見問題</a></li>
          <li><a href="#shopping">購物常見問題</a></li>
          <li><a href="#receipt">發票常見問題</a></li>
          <li><a href="#delivery">台灣本島配送取貨問題</a></li>
          <li><a href="#refund">退貨及退款</a></li>
        </ul> <!-- categories -->
        <div class="accordion">
            <dl id="account" class="faq-group">
                <h2 class="faq-title t_center">會員常見問題</h2>
            <dt><a href="#" class="accordionTitle t_center"><span></span>Q1. 如何快速加入會員？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <ul>
                  <li>A. 可於會員中心選單選擇會員註冊，註冊完後即可安心購物，此為免費服務無
                    須付任何費用。</li>
                  <li>B. 可先選購商品並進入購買結帳系統，訂單成立後會直接建立您的會員帳號。</li>
                </ul>
            </dd>
            <dt><a href="#" class="accordionTitle t_center"><span></span>Q2. 忘記密碼？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>你可於登入時點選忘記密碼，系統將會給予電子信箱或手機的驗證密碼，輸入後
                  即可登入並請更改新密碼。
                </p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center"><span></span>Q3. 忘記第一次購物時填的電子信箱？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>煩請來信或於上班時間內來電直接與客服中心聯繫。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#"><span></span>Q4. 如何修改個人資料及密碼？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>登入會員中心，由此變更您登入的姓名、生日、手機號碼、地址或變更您的登入密碼。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#"><span></span>Q5. 如何確認訂購完成？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>登入會員後可至「訂單管理」查看，確認訂單是否成功送出，以及訂單進度。</p>
            </dd>
            </dl>
            <!-- </ul> -->
            <dl id="shopping" class="faq-group">
                <h2 class="faq-title t_center">購物常見問題</h2>
            <dt><a href="#" class="accordionTitle t_center">Q1. 購物流程說明?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
              <p>我們希望提供您親切易懂的指引式購物畫面，讓您充分享受便利的購物體驗。<br>
                註冊會員→加入購物車→立即購買→去結帳→選擇收貨地址/配送方式/付款方式→提交訂單→訂單成立→完成</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q2. 目前提供那些付款方式?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>本平台目前提供信用卡與貨到付款兩種。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q3. 如何查詢目前訂單處理進度？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>登入會員，輸入您的E-Mail及登入密碼後，點選訂單管理即可查詢訂單的處理狀態。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q4. 訂單成立後是否可以加訂，修改商品尺寸與數量？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>訂單成立後不能修改，若有相關需求請來信或於上班時間內來電客服。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q5. 訂單成立後是否可以取消訂單？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>在您尚未完成付款前可以至會員中心自行取消訂單，若已付款後請來信或於上班時間內來電告知客服人員協助取消。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q6. 我想購買的的商品已經缺貨，甚麼時候會進貨呢？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>進貨的款式及尺寸,都是依照各品牌官方分配的發貨公告,故選項款式的更新時間,配合各品牌出貨時程而定。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q7. 如何計算七天鑑賞期？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>根據消費者保護法之規定，.container提供您享有商品到貨的七日鑑賞期權益，是由消費者完成簽收取件的隔日開始算起至第7天止（如您的收件地址有管理員代收，則以代收的隔日起算喔，請留意送件通知），為七日鑑賞期限。
                    例如，完成簽收的時間是12/20，其七天鑑賞期起訖日即為12/21~12/27。您如欲辦理退貨需於「12/27前」提出退貨申請，並填寫相關資料以利商品退貨處理。
                    </p>
            </dd>
            </dl>
            <!--  -->
            <dl id="receipt" class="faq-group">
                <h2 class="faq-title t_center">發票常見問題</h2>
            <dt><a href="#" class="accordionTitle t_center">Q1.甚麼是「電子發票」?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
              <p>根據財政部令「電子發票實施作業要點」，於本網站消費開立之統一發票，將於期限內上傳至財政部電子發票整合服務平台留存，消費者可利用財政部電子發票整合服務平台查詢消費紀錄。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q2.我會收到實體發票嗎?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>於本網站消費開立之二聯式、三聯式發票將以託管方式，一律不提供紙本發票，將於收到商品/預購品後七個工作天內以E-mail寄送發票開立通知信給您。<br>
                  貼心提醒:為維護您的權益，請務必填寫正確的E-mail，公司行號若需報帳可自行下載列印電子發票即可報帳。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q3.電子發票中獎如何接到通知?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>若發票中獎將E-mail發票證明聯給您，以利兌獎。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q4.商品於鑑賞期間內辦理退貨，電子發票如何處理?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>退貨無需附回發票，原消費產生之電子發票將於退貨完成後自動作廢。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q5.發票需要統一編號可以填寫在哪裡?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>若您的電子發票需要統編，請務必於提交訂單頁面留言處填寫。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q6. 二聯式三聯式發票若仍需要紙本發票如何取得?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>可至您E-mail提供之連結查詢電子發票明細，自行列印。提醒您！列印後的發票電子檔僅供參考之用，不具兌獎功能。其他有關領獎事項依「統一發票給獎辦法」規定辦理。(有任何兌獎疑義，可至財政部電子發票整合服務平台常見問答服務項目查詢，或撥打電子發票24小時免付費專線0800-521-988。)
                    </p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q7.收到的電子發票內容有誤怎麼辦?</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>請來信lungagge000@case-iii.com.tw，與我們聯繫，感謝您。</p>
            </dd>
          </dl>
            <!--  -->
            <!--  -->
            <dl id="delivery" class="faq-group">
                <h2 class="faq-title t_center">台灣配送取貨問題</h2>
            <dt><a href="#" class="accordionTitle t_center">Q1.可選擇的配送方式有哪些？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
              <ul>
                <li>A.線上刷卡→商品將以宅配方式寄出，台灣地區及海外地區皆可寄送。</li>
                <li>B.貨到付款→商品將以宅配方式送達，並於交付您貨物時收取貨款，貨物送達時會與您電話連絡，需請您務必保持電話暢通。</li>
              </ul>
              <p>※宅配人員僅提供送件及代收款項的服務，若會員對收到的商品有任何問題，請直接與.container客服連絡，配送人員無法為您辦理退換貨，目前貨到付款服務，僅接受現金付款</p>
              <p>※「貨到付款」服務僅限台灣本島，外島地區沒有提供「貨到付款」服務，不便之處敬請見諒。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q2.請問運費如何計算?多久會收到商品？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>因商品屬大宗包裹，物流宅配運送費用旅行箱單件皆為200元；配送時間約2~3個工作天(不含週六日及國定假日)。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q3.如何更改訂購內容，送貨地址或取消訂單？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>訂單送出後即無法修改訂單內容，如需修改，可登入會員中心發送站內信給客服人員，由客服人員協助更該送貨資訊。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q4.台灣外島地區可以寄送嗎？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>可以，但台灣外島配送時間需較本島長，約5個工作天送達(不含六日及國定假日，若遇休假日則順延一天)，不便之處敬請見諒。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q5.可配送海外嗎？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>很抱歉，本平台目前尚未提供海外訂購，未來將盡快為您服務。</p>
            </dd>
            <!--  -->
            <dl id="refund" class="faq-group">
                <h2 class="faq-title t_center">退貨及退款</h2>
            <dt><a href="#" class="accordionTitle t_center">Q1.可選擇的配送方式有哪些？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
              <p>請先來信至lungagge000@case-iii.com.tw，我們將有專人與您聯絡退貨事宜，或是請於上班時間周一至周五 09:00-18:00撥打+886-2-3313-6666，亦有專員盡快為您處理。</p>
              <p>❤貼心提醒：因應法規，為維護您的權益，退款資料的銀行戶名需與購買者同名，請務必特別留意。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q2.退款需要多久時間？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>退貨商品完成收貨寄回後，若商品檢查無狀況，款項將會在7-14個工作天完成退款作業。</p>
            </dd>
            <dt><a href="#" class="accordionTitle t_center">Q3. 發票遺失是否能辦理退換貨？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>因本網站採用電子發票，故原消費產生之電子發票將於退貨完成後自動作廢。</p>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q4.什麼情況無法辦理退貨？</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
              <ul>
                <li>A.退回時，商品配件不全或已在商品上加工，例如：塗鴉、黏貼物品……等非出貨前商品原樣。</li>
                <li>B.超過七天鑑賞期。</li>
                <li>C.商品已使用過。</li>
              </ul>
            </dd>
            <dt><a class="accordionTitle t_center" href="#">Q5.*特別注意-退/換貨運費說明</a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p>凡於本平台.Container購買行李箱的貴賓，都可依消費者保護法的規定，享有商品貨到日起七天猶豫期的權益。<br>

                    但猶豫期並非試用期，請留意！您所退的商品必須保持原狀。<br>
                    大型貨物運費昂貴，退貨需請貴賓自行負擔運送費用。<br>
                    ＊退貨負擔運費：寄送費200元<br>
                    </p>
            </dd>
          </dl>
            <!--  -->
        </div>
  </div>
  <script>
  ( function( window ) {
'use strict';
function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
} else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}
var classie = {
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

if ( typeof define === 'function' && define.amd ) {
  define( classie );
} else {
  window.classie = classie;
}
})( window );
var $ = function(selector){
  return document.querySelector(selector);
}
var accordion = $('.accordion');
accordion.addEventListener("click",function(e) {
  e.stopPropagation();
  e.preventDefault();
  if(e.target && e.target.nodeName == "A") {
    var classes = e.target.className.split(" ");
    if(classes) {
      for(var x = 0; x < classes.length; x++) {
        if(classes[x] == "accordionTitle") {
          var title = e.target;
          var content = e.target.parentNode.nextElementSibling;
          classie.toggle(title, 'accordionTitleActive');
          if(classie.has(content, 'accordionItemCollapsed')) {
            if(classie.has(content, 'animateOut')){
              classie.remove(content, 'animateOut');
            }
            classie.add(content, 'animateIn');
          }else{
             classie.remove(content, 'animateIn');
             classie.add(content, 'animateOut');
          }
          classie.toggle(content, 'accordionItemCollapsed');      
        }
      }
    }  
  }
});
  </script>
<?php include __DIR__. '/__html__footer.php' ?>