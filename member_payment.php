<?php
require __DIR__. '/__connect_db.php';
$page_name = 'payment';
$page_title = '信用卡帳戶資料';

//如果不是登入狀態就跳轉登入頁
if (! isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}
?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar|Noto+Sans+TC" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="stylesheet" href="css/member-layout.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <title>會員中心 - 付款帳戶</title>
    <style>
         /* ----------------------------------------------------- member RWD */
         @media screen and (max-width:1240px){

        }
        @media screen and (max-width:1024px){
            .member-wrap{
                max-width: 80vw;
            }

        }
        @media screen and (max-width:768px){
            .member-wrap{
                max-width: 90vw;
            }
            .member-sub{
                transform: rotate(0deg);
                top: -45px;
                left: 0;
                font-size: 45px;
                max-width: 90vw;
            }
            .member-nav{
                justify-content: center;
            }
            .member-row>.d-flex{
                flex-direction: column;
            }
            .member-row .mb-1-6{
                width: 100%;
                flex-direction: row;
                justify-content: center;
                margin-bottom: 20px;
            }
            .member-row .mb-1-6>div{
                margin: 0 20px;
            }
            .member-row .mb-1-6>div span{
                display: none;
            }
            .member-row .mb-1-6>div a{
                border: 1px solid #4d5258;
                padding: 10px 15px;
                border-radius: 21px;
            }
            .member-row .member-account{
                width: 100%;
            }
            .m-y-10 p{
                padding-left: 0;
            }
            .member-scroll{
                width: 100%;
            }
            .member-row{
                height: auto;
            }

        }
        @media screen and (max-width:600px){
            .payment-card, .payment-detial{
                margin-left: 20px;
                margin-right: 20px; 
            }
        }
        @media screen and (max-width:520px){
            .member-sub{
                font-size: 35px;
                top: -40px;
            }
            .member-nav{
                margin-top: 130px;
            }
            .member-nav ul li {
                padding: 0 10px;
            }
            .m-y-10 p{
                font-size: 1.15rem;
            }
            .member-payment>div{
                flex-direction: column;
            }
            .payment-card{
                margin-bottom: 0; 
            }
            .payment-detial{
                margin-top: 0;
            }

        }
        @media screen and (max-width:400px){
            
            .member-sub{
                font-size: 30px;
                top: -35px;
            }
            .member-nav ul li {
            padding: 0 10px;
            letter-spacing: 4px;
            }
            .m-y-10 p{
                font-size: 1.1rem;
            }
            .member-row .mb-1-6>div a{
                font-size: 16px;
            }

        }
    </style>
</head>

<body>
<?php include __DIR__ . '/__navbar.php'?>
    <section>
        <div class="member-bg"></div>
        <div class="con-1440 member-wrap">
            <div class="ff-marko member-sub">
                MEMBER CENTER
            </div>
            <div class="member-nav d-flex">
                <ul class="d-flex nonstyle-ul">
                    <li class="active"><a class="active" href="#">我的帳戶</a></li>
                    <li><a class="" href="member_order.php">訂單查詢</a></li>
                    <li><a class="" href="member_wishlist.php">願望清單</a></li>
                </ul>
            </div>
            <div class="member-row">
                <h2 class="fw-300">信用卡付款帳號</h2>
                <div class="d-flex  ">
                    <div class="mb-1-6 d-flex">
                        <div>
                            <a class="nonstyle-a" href="member.php">會員資料<br><span>Account Information</span></a>
                        </div>
                        <div>
                            <a class="nonstyle-a active" href="#">付款帳戶<br><span>Payment & Crdits</span></a>
                        </div>
                    </div>
                    <div class="mb-5-6 d-flex member-account">
                        <div class="mb-4-5">
                            <!-- ------重複區塊 start ----- -->
                            <div class="member-payment d-flex">
                                <div class="d-flex">
                                    <div class="payment-card">
                                        <img src="images/visa_icon.svg" alt="">
                                    </div>
                                    <div class="payment-detial">
                                        <p class="fw-700 ff-mukta">WANG DA MING</p>
                                        <p class="ff-mukta">1122-3344-5566-7788</p>
                                        <p class="ff-mukta">Exp 01/2020</p>
                                    </div>
                                </div>
                                <div class="payment-edit-bg d-flex">
                                    <div class="payment-edit">
                                        <a class="nonstyle-a" href=""><i class="fas fa-pencil-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- ------重複區塊 end ----- -->
                            <div class="padding-area"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include __DIR__ . '/__html__footer.php'?>