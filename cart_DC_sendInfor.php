<?php require __DIR__. '/__connect_db.php';?>
<?php
$price = isset($_GET['price']) ? intval($_GET['price']) : 0;
?>
<?php include __DIR__. '/cart_head.php' ?>

    <div class="bgImgDC">
        <!-- 小0特區HEADER -->
        <?php include __DIR__. '/__navbar.php' ?>
        <!-- 流程進度圖 -->
        <?php include __DIR__. '/cart_step3.php' ?>
        <!-- wrapper -->
        <?php include __DIR__. '/cart_content3.php' ?>
            <!-- WA特區FOOTER -->
            <?php include __DIR__. '/__html__footer_DC.php' ?>
    </div>
    <script src="./js/receiptDC.js"></script>
