<?php
require __DIR__ . '/__connect_db.php';
$page_name = 'order';
$page_title = '歷史訂單';

if (!isset($_SESSION['user'])) {
    header('Location: ./');
    exit;
}

$o_sql = "SELECT * FROM `orders` 
          WHERE `member_id`=? AND `order_date`>'2018-07-01'";

$o_stmt = $pdo->prepare($o_sql);
$o_stmt->execute([
    $_SESSION['user']['id']
]);

$orders = $o_stmt->fetchAll(PDO::FETCH_ASSOC);

$order_sids = [];
foreach($orders as $order){
    $order_sids[] = $order['sid'];
}

$od_sql = sprintf("SELECT * FROM `order_details` 
          JOIN `product_list` ON order_details.list_id=product_list.sid
          JOIN `lunggage_data` ON `product_list`.`type_sid` = `lunggage_data`.`SID`
          JOIN `color_mapping` ON `product_list`.`color_sid` = `color_mapping`.`color_sid`
          WHERE order_details.order_sid in (%s)", implode(',', $order_sids));


$od_stmt = $pdo->query($od_sql);

$details = $od_stmt->fetchAll(PDO::FETCH_ASSOC);



?>
    <!DOCTYPE html>
    <html lang="zh">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/normalize.css">
        <!-- <link href="https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar|Noto+Sans+TC" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/style-wawa.css">
        <link rel="stylesheet" href="css/member-layout.css">
        <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
        <title>會員中心 - 訂單資訊</title>
        <style>
            /* ----------------------------------------------------- member RWD */
            @media screen and (max-width: 1240px) {

            }

            @media screen and (max-width: 1024px) {
                .member-wrap {
                    max-width: 80vw;
                }

            }

            @media screen and (max-width: 930px) {
            }

            @media screen and (max-width: 835px) {
                .order-number {
                    flex-direction: column;
                }

                .order-number ul {
                    margin-bottom: 8px;
                }

            }

            @media screen and (max-width: 768px) {
                .member-wrap {
                    max-width: 90vw;
                }

                .member-sub {
                    transform: rotate(0deg);
                    top: -45px;
                    left: 0;
                    font-size: 45px;
                    max-width: 90vw;
                }

                .member-nav {
                    justify-content: center;
                }


                .member-row .mb-1-6 > div {
                    margin: 0 20px;
                }

                .member-row .mb-1-6 > div span {
                    display: none;
                }

                .member-row .mb-1-6 > div a {
                    border: 1px solid #4d5258;
                    padding: 5px 20px;
                    border-radius: 21px;
                }

                .m-y-10 p {
                    padding-left: 0;
                }

                /* ------------------- */
                .member-order {
                    width: 90%;
                }

                .member-scroll {
                    width: 100%;
                }

                .member-row {
                    height: auto;
                }

            }

            @media screen and (max-width: 635px) {
                .order-item-detial {
                    width: 50%;
                    flex-direction: column;
                    align-items: flex-start;
                    margin: 15px 0;
                }

                .order-item-detial .d-none {
                    display: inline-block;
                    color: #4d5258;
                    padding: 3px 0;
                }

                .order-item figure {
                    width: 50%;
                    margin: 15px 0;
                }

            }

            @media screen and (max-width: 520px) {
                .member-row{
                    min-height: 0;
                    margin-bottom: -50px;
                }
                .member-sub {
                    font-size: 35px;
                    top: -40px;
                }
                .member-nav{
                    margin-top: 130px;
                }
                .member-nav ul li {
                    padding: 0 10px;
                }

                .m-y-10 p {
                    font-size: 1.15rem;
                }

                /* ----------------------------- */
                .order-number .order-date {
                    flex-direction: column;
                }

                .order-date li {
                    padding: 5px 0;
                }

                .shipping-schedule {
                    flex-direction: column;
                    align-items: center;
                }

                .shipping-schedule li {
                    width: 100%;
                    background: #818e9b;
                    color: #ffffff;
                    padding: 8px;
                    text-align: center;
                    margin: 4px 0;
                }

                .shipping-schedule li a {
                    color: #ffffff;
                }

                .order-item-name span {
                    line-height: 1;
                }


            }

            @media screen and (max-width: 400px) {

                .member-sub {
                    font-size: 30px;
                    top: -35px;
                }

                .member-nav ul li {
                    padding: 0 10px;
                    letter-spacing: 4px;
                }

                .m-y-10 p {
                    font-size: 1.1rem;
                }

            }
        </style>
    </head>

<body>
<?php include __DIR__ . '/__navbar.php' ?>
    <section>
        <div class="member-bg"></div>
        <div class="con-1440 member-wrap">
            <div class="ff-marko member-sub">
                MEMBER CENTER
            </div>
            <div class="member-nav d-flex">
                <ul class="d-flex nonstyle-ul">
                    <li><a class="" href="member.php">我的帳戶</a></li>
                    <li class="active"><a class="active" href="#">訂單查詢</a></li>
                    <li><a class="" href="member_wishlist.php">願望清單</a></li>
                </ul>
            </div>
            <div class="member-row">
                <h2 class="fw-300">訂單資訊</h2>
<!--                <pre>-->
<!--                --><?php
//                // echo $d_sql;
//
//                print_r($orders);
//                print_r($details);
//                ?>
<!--                </pre>-->
                <!-- ------1 重複區塊 start ----- -->
                <?php
                foreach ($orders as $o){

                    ?>
                <div class="mb-5-6 member-order">
                    <div class="d-flex order-number">
                        <ul class="nonstyle-ul d-flex order-date">
                            <li>訂單編號 12345678</li>
                            <li>訂單日期 <?= $o['order_date'] ?></li>
                            <li>訂單金額 <span class="fw-700">$<?= $o['amount'] ?></span></li>
                        </ul>
                        <ul class="nonstyle-ul d-flex shipping-schedule">
                            <li><a class="nonstyle-a " href="">物流進度</a></li>
                            <li class="">待配送</li>
                        </ul>
                    </div>
                    <!-- ----2 重複品項 start ---- -->
                    <?php
                    foreach ($details as $od){
                        if($od['order_sid']==$o['sid']){
                        ?>
                    <div class="order-item d-flex">
                        <figure>
                            <a href="./product.php?sid=<?= $od['SID'] ?>">
                            <img src="./images/product/<?= $od['pic_nu'] ?>" alt="">
                            </a>
                        </figure>
                        <div class="order-item-detial d-flex">
                            <ul class="flex-4 ">
                                <li class="order-name"><?= $od['brand'] ?></li>
                                <li class="order-type"><?= $od['type'] ?></li>
                            </ul>
                            <div class="flex-1"><span class="d-none">尺吋：</span><?= $od['size_text'] ?></div>
                            <div class="flex-1"><span class="d-none">顏色：</span><?= $od['color'] ?></div>
                            <div class="flex-1"><span class="d-none">數量：</span><?= $od['quantity'] ?></div>
                            <div class="flex-1 "><span class="d-none">價格： </span>NT$ <span
                                        class="order-item-price"><?= $od['price'] ?></span></div>
                        </div>
                    </div>
                    <?php }} ?>
                    <!-- ----2 重複品項 end ---- -->
                </div>
                <?php } ?>
                <!-- ------1 重複區塊 end ----- -->
                <div class="m-b-50"></div>
            </div>
        </div>
    </section>
<?php include __DIR__ . '/__html__footer.php' ?>