<div class="choose_bgc">
    <div class="d-flex choose">
        <div class="choose_info mb-1-2 d-flex">
            <ul>
                <h2 class="ff-merri fw-400">HOW TO CHOOSE</h2>
                <li>您知道不同材質的行李箱適合帶去旅行的國家不同嗎？</li>
                <li>您知道輪子的流暢度是行李箱的靈魂嗎？</li>
                <li>您知道怎樣選擇行李箱尺寸大小嗎？</li>
                <li>購買行李箱之前讓我們來告訴您，</li>
                <li>如何選擇適合您的箱子與一些基本挑選常識喔！</li>
                <li><a href="./pick.php">去看看</a></li>
            </ul>
        </div>
        <div class="mb-1-2 choose-rwd">
            <figure class="picmove">
                <img src="./images/luggage_compare_03.jpg" alt="">
            </figure>
            <img class="choose-text choose-text-1 ophide" src="./images/choose-text-1.svg" alt="">
            <img class="choose-text choose-text-2 ophide-2" src="./images/choose-text-2.svg" alt="">
            <img class="choose-text choose-text-3 ophide-3" src="./images/choose-text-3.svg" alt="">
        </div>
    </div>
</div>


<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
