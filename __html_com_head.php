<!doctype html>
<html lang="en">

<head>
    <title>commodity</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Charmonman|Noto+Sans+TC" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="icon" href="./images/favicon.png" type="image/png">

    <link rel="stylesheet" href="./css/navigation.css">
    <link rel="stylesheet" href="./css/__css_label.css">
    <link rel="stylesheet" href="./css/__css_head.css">
    <!-- <link rel="stylesheet" href="./css/__css_scrolltop.css"> --><!-- 暂时不用 -->
    <link rel="stylesheet" href="./css/__css_contain.css">
    <link rel="stylesheet" href="./css/__css_detail.css">
    <link rel="stylesheet" href="./css/__css_bread.css">
    <link rel="stylesheet" href="./css/__css_contain_body.css">
    <!-- <link rel="stylesheet" href="./css/__css_footer.css"> --><!-- footer CSS在PHP中 -->
    
    <script src="./js/jquery.min.js"></script>
    <script src="./js/lodash.js"></script>
    <link rel="stylesheet" href="css/vegas.min.css">
    
</head>
<body>
