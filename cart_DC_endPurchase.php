
<?php
require __DIR__. '/__connect_db.php';



// 如果沒登入會員, 或者購物車內是空的
if(! isset($_SESSION['user']) or empty($_SESSION['cart'])){
     header('Location: ./');
    exit;
}


$price = isset($_GET['price']) ? intval($_GET['price']) : 0;

// <--- begin 同購物車列表頁前處理
$keys = array_keys($_SESSION['cart']);
//$sql = "SELECT * FROM `products` WHERE `sid` IN ('18', '20', '7')";
$sql = sprintf("SELECT `product_list`.*, `lunggage_data`.* FROM `product_list` JOIN `lunggage_data` ON `product_list`.`type_sid` = `lunggage_data`.`SID`  WHERE `product_list`.`sid` IN ('%s')",
    implode("','", $keys)
);

$stmt = $pdo->query($sql);

    $cart_data = []; // 存放商品資料和數量
    $total_price = 0; // 總計
    while($r = $stmt->fetch(PDO::FETCH_ASSOC)){
    
        // 把商品的數量, 設定給該項目的 qty 屬性
        $r['qty'] = $_SESSION['cart'][$r['sid']];
    
        $cart_data[$r['sid']] = $r;
    
        $total_price += $r['qty']*$r['price'];
    }
    // <--- end 同購物車列表頁前處理
    
    
    
    $o_sql = "INSERT INTO `orders`(`member_id`, `amount`, `order_date`) VALUES (?, ?, NOW())";
    $o_stmt = $pdo->prepare($o_sql);
    
    $o_stmt->execute([
        $_SESSION['user']['id'],
        $total_price
    ]);
    
    $o_sid = $pdo->lastInsertId(); // 取得新增資料後, 該筆的主鍵
    
    
    $d_sql = "INSERT INTO `order_details`(`order_sid`, `list_id`, `price`, `quantity`) VALUES (?,?,?,?)";
    
    $d_stmt = $pdo->prepare($d_sql);
    // print_r($cart_data);
    // exit;
    foreach($keys as $p_sid){
        $d_stmt->execute([
            $o_sid,
            $p_sid,
           $cart_data[$p_sid]['price'],
            $cart_data[$p_sid]['qty']
        ]);
    }
    
unset($_SESSION['cart']); // 清除購物車內容

?>
<?php include __DIR__. '/cart_head.php' ?>
<body>
    <div class="bgImgDC">
    <?php include __DIR__. '/cart_content5.php' ?>
    </div>
    <!-- WA特區FOOTER -->

<?php
require __DIR__. '/__connect_db.php';
//---------------------------------------------------old
// 如果沒登入會員, 或者購物車內是空的
if(! isset($_SESSION['user']) or empty($_SESSION['cart'])){
        header('Location: ./');
        exit;
    }
    
    // <--- begin 同購物車列表頁前處理
    $keys = array_keys($_SESSION['cart']);
    //$sql = "SELECT * FROM `products` WHERE `sid` IN ('18', '20', '7')";
    $sql = sprintf("SELECT * FROM `lunggage_data` WHERE `SID` IN ('%s')",
        implode("','", $keys)
    );
    
    $stmt = $pdo->query($sql);
    
    $cart_data = []; // 存放商品資料和數量
    $total_price = 0; // 總計
    while($r = $stmt->fetch(PDO::FETCH_ASSOC)){
    
        // 把商品的數量, 設定給該項目的 qty 屬性
        $r['qty'] = $_SESSION['cart'][$r['SID']];
    
        $cart_data[$r['SID']] = $r;
    
        $total_price += $r['qty']*$r['price'];
    }
    // <--- end 同購物車列表頁前處理
    
    
    
    $o_sql = "INSERT INTO `orders`(`member_id`, `amount`, `order_date`) VALUES (?, ?, NOW())";
    $o_stmt = $pdo->prepare($o_sql);
    
    $o_stmt->execute([
        $_SESSION['user']['id'],
        $total_price
    ]);
    
    $o_sid = $pdo->lastInsertId(); // 取得新增資料後, 該筆的主鍵
    
    
    $d_sql = "INSERT INTO `order_details`(`order_sid`, `list_id`, `price`, `quantity`) VALUES (?,?,?,?)";
    
    $d_stmt = $pdo->prepare($d_sql);
    
    foreach($keys as $p_sid){
        $d_stmt->execute([
            $o_sid,
            $p_sid,
            $cart_data[$p_sid]['price'],
            $cart_data[$p_sid]['qty']
        ]);
    }
 //---------------------------------------------------old end

    
unset($_SESSION['cart']); // 清除購物車內容

?>
<?php
$price = isset($_GET['price']) ? intval($_GET['price']) : 0;
?>
<?php include __DIR__. '/cart_head.php' ?>
<body>
    <div class="bgImgDC">
    <?php include __DIR__. '/cart_content5.php' ?>
    </div>
    <!-- WA特區FOOTER -->

<?php include __DIR__. '/__html__footer_DC.php' ?>