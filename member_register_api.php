<?php
require __DIR__. '/__connect_db.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];

if(isset($_POST['name']) and isset($_POST['email']) and isset($_POST['password'])){
    $result['postData'] = $_POST;


    $s_sql = "SELECT 1 FROM `members` WHERE `email`=?";
    $s_stmt = $pdo->prepare($s_sql);
    $s_stmt->execute([ $_POST['email'] ]);
    if($s_stmt->rowCount() >= 1){
        $result['code'] = 420;
        $result['info'] = 'Email已被註冊，請重新輸入';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    //sha1把內容編碼(影片01-07-10-29)
    $hash = sha1($_POST['email']. uniqid());

    //strtolower(英文轉小寫) trim(去除頭尾空白)
    $email = strtolower(trim($_POST['email']));

    //sha1把內容編碼  trim(去除頭尾空白)
    $password = sha1(trim($_POST['password']));


    $sql = "INSERT INTO `members`(
`email`, `password`, `mobile`, `address`,
 `birthday`, `hash`, `name`, `create_at`
  ) VALUES (
  ?, ?, ?, ?,
  ?, ?, ?, NOW()
  )";

    $stmt = $pdo->prepare($sql);

    $stmt->execute([
        $email,
        $password,
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $hash,
        $_POST['name']
    ]);

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '註冊完成 即將轉跳首頁';
    } else {
        $result['code'] = 410;
        $result['info'] = '註冊失敗';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);