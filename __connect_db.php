<?php
$db_host = 'localhost';
$db_name = 'container';
$db_user = 'container';
$db_pass = 'container';

$dsn = sprintf('mysql:host=%s;dbname=%s',$db_host,$db_name);

//$pdo = new PDO($dsn,$db_user,$db_pass);


try {
    $pdo = new PDO($dsn, $db_user, $db_pass);

    // 連線使用的編碼設定
    $pdo->query("SET NAMES utf8");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $ex) {
    echo 'Connection failed:'. $ex->getMessage();
}


 if(! isset($_SESSION)){
     session_start();
 }