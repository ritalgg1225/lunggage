<header>
    <!-----------------------------------白色-->
    <div class="home_wrap">
        <div class="home relative flex">
            <div class="logo">
                <figure>
                    <img src="./img/logo_680x335_black.svg" alt="">
                </figure>
                <div class="arrow">
                    <i class="fas fa-angle-double-down p_50px_wii animated infinite fadeOutDown delay-1s"></i>
                </div>
            </div>
        </div>
    </div>

    <!-----------------------------------黑色-->
    <div class="home_2_wrap">

        <div class="home_2 flex">
            <div class="logo">
                <figure>
                    <img src="./img/logo_680x335_white.svg" alt="">
                </figure>
                <div class="button_a">
                    <button class="b_1">
                        <p class="ff-noto-wi" style="line-height: 36px;">繼續</p>
                    </button>
                    <button class="b_2">
                    <a class="nontyle-a go-to-list" href="./index_commodity.php"><p class="ff-noto-wi" style="line-height: 36px;">商品選購</p></a>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-----------------------------------立刻尋找-->
<!--
    <button class="p_12px_wil ff-noto-wi b_3">
        <p>立刻尋找</p>
    </button>
-->
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>



</header>

