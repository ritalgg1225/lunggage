<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <!-- 昱蘭CSS -->
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="stylesheet" href="css/cartSub.css">
    <link rel="stylesheet" href="css/cartStepDC.css">
    <!-- dc -->
    <link rel="stylesheet" href="css/cartDC.css">
    <link rel="stylesheet" href="css/formDC.css">
    <link rel="stylesheet" href="css/cartPayDC.css">
    <link rel="stylesheet" href="css/fixDC.css">
    <link rel="stylesheet" href="css/cartSelectDC.css">
    <link rel="stylesheet" href="css/content1_rwdDC.css">
    <link rel="stylesheet" href="css/quantity.css">
    <!-- pretty checkbox -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <!-- 表單CSS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <!-- 小0CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
          crossorigin="anonymous">
    <!-- 雅筑   -->
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <title>購物車DC</title>
    <style>

    </style>
</head>
<body>