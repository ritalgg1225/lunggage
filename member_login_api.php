<?php
require __DIR__. '/__connect_db.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];

if(isset($_POST['email']) and isset($_POST['password'])){
    $result['postData'] = $_POST;

    $email = strtolower(trim($_POST['email']));
    $password = sha1(trim($_POST['password']));

    $sql = "SELECT `id`, `email`, `mobile`, `address`, `birthday`, `name` FROM `members` WHERE `email`=? AND `password`=?";

    $stmt = $pdo->prepare($sql);

    $stmt->execute([
        $email,
        $password
    ]);

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '登入成功';

    // (影片01-07-11-07 設定SESSION 把$stmt ->fetch出來存放)
        $_SESSION['user'] = $stmt->fetch(PDO::FETCH_ASSOC);

    } else {
        $result['code'] = 410;
        $result['info'] = '帳號或密碼錯誤';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);