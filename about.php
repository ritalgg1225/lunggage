<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>平台故事</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
</head>
<?php require __DIR__. '/__connect_db.php'?>
<?php include __DIR__. '/__navbar.php' ?>

<style>
/* -------------------------------------- body ---------------------------------------------------*/
@import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
*{
  margin: 0;
  padding: 0;
}
body{
    /* font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif; */
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
/* -------------------------------------- group style---------------------------------------------------*/
.relative{
    position: relative;
}
.absolute{
    position: absolute;
}
.container{
    width: 75vw;
    margin:  0 auto;
}
.f_container{
    flex-flow: row wrap;
}
.flex{
    display: flex;
}
.t_center{
    text-align: center;
    margin:  0 auto;
}
.t_left{
    text-align: left;
}
.no_bullet{
    list-style-type: none;
}
.ff-merri{
    font-family: 'Merriweather', serif;
}
/* -------------------------------------- hero section---------------------------------------------------*/
.a_hero{
    height: 85vh;
    /* margin: 0 auto ;
    width: 75vw; */
    text-align: center;
    /* background-color: #aab9c0; */
}
.a_hero .logo{
    /* text-align: center; */
    vertical-align: middle;
    /* width: 50vw; */
    /* height: 40vh; */
}
.a_hero .logo img{
    width: 50%;
    /* height: 100%; */
    top: 25vh;
    left: 50%;
    transform: translateX(-50%);
}
.logo_bk{
    z-index: 3;
}
.a_hero .logo .logo_fk{
    z-index: 4;
}
/* scroll */
.logo a {
  /* bottom: 5vh; */
  top: 70vh;
  /* left: 50%; */
  z-index: 2;
  /* display: block; */
  /* -webkit-transform: translate(0, -50%); */
  /* transform: translate(0, -50%); */
  color: #bea17f;
  letter-spacing: .1em;
  text-decoration: none;
  transition: opacity .3s;
  /* padding-top: 80vh; */
}
.logo a:hover {
  opacity: .5;
}
/* arrow */
.logo a span {
  /* bottom: -20px; */
  top: -10vh;
  left: 50%;
  width: 1.5rem;
  height: 1.5rem;
  margin-left: -12px;
  border-left: 1.5px solid #bea17f;
  border-bottom: 1.5px solid #bea17f;
  -webkit-transform: rotateZ(-45deg);
  transform: rotateZ(-45deg);
  -webkit-animation: sdb06 1.5s infinite;
  animation: sdb06 1.5s infinite;
  box-sizing: border-box;
}
/* -------------------------------------- mission section---------------------------------------------------*/
#us_vv{
    background: #243B55;
    color: #bea17f;   
}
.option{
    box-sizing: border-box;
    padding: 60px 40px;
    flex-grow: 0;
    width: 45%;
}
.option h2{
    font-weight: 700;
    font-size: 2.5rem;
    line-height: 2.5rem;
    padding: 15px 10px;
    /* letter-spacing: .5rem; */
}
.option dd{
    font-weight: 300;
    font-size: 1.3rem;
    line-height: 1.7;
    padding: 40px 10px;
    letter-spacing: .2rem;
}
.hr{
    width: 30%;
    height: 2px;
    background-color: #bea17f;
}
/* -------------------------------------- intro section---------------------------------------------------*/
.a_content .a_intro{
    width: 60%;
}
.a_content .a_intro ul{
    padding-top: 10vh;
}
.a_content li{
    padding: 10px 0;
}
/* animated arrow */
@keyframes sdb06 {
  0% {
    transform: rotateY(0) rotateZ(-45deg) translate(0, 0);
    opacity: 0;
  }
  50% {
    opacity: 1;
  }
  100% {
    transform: rotateY(720deg) rotateZ(-45deg) translate(-20px, 20px);
    opacity: 0;
  }
}
@media only screen and (min-width: 1440px) { 
    .option{
        width: 50%;
        flex-grow: 1;
    }
}
@media only screen and (min-width : 768px) and (max-width : 1024px) {
    .option{
        width: 50%;
        padding: 10px 15px;
        /* flex-grow: 1; */
    }
    .option h2{
        font-size: 1.8rem;
    }
    .option h4{
        font-size: 1.5rem;
    }

}
@media only screen and (min-width : 481px) and (max-width : 767px) {
    .option{
        width: 50%;
        padding: 10px;
    }
    .mission{
        /* flex-direction: column; */
    }
    .container, .a_content .a_intro{
        width: 90%;
    }
    .logo a{
        top: 70vh;
    }
    .logo{
        text-align: center;
    }
    .option h2{
        font-size: 1.5rem;
    }
    .option h4{
        font-size: 1.25rem;       
    .option dd{
        padding: 10px;
    }
}
@media only screen and (min-width : 320px) and (max-width : 480px) {
    .option{
        width: 100%;
        padding: 20px 10px;
    }
    .mission{
        /* flex-direction: column; */
    }
    .a_hero .logo img{
        top: 30vh;
    }
    .container, .a_content .a_intro{
        width: 90%;
    }
    .logo a{
        top: 70vh;
    } 
    .logo{
        text-align: center;
    }
    .option h2{
        font-size: 1rem;
        line-height: .8rem;        
    }
    .option h4{
        font-size: .8rem;
    }
    .option dd,.option h2{
        padding: 10px;
    }
}

</style>
<body>
    <div class="a_hero container relative">
        <div class="logo">
            <img class="logo_bk absolute" src="./images/logo_about_bk.svg" alt="">
            <img class="logo_fk absolute" src="./images/logo_about_fg.svg" alt="">
            <a class="relative t_center" href="#us_vv"><span class="absolute">
            </span>Scroll</a>
        </div>
    </div>
    <div class="a_content">
        <div id="us_vv" class="">
            <dl class="flex container t_center f_container mission">
                <div class="option">
                    <dt><h2 class="ff-merri">Our Mission</h2></dt>
                    <div class="hr t_center"></div>
                    <dd><h4>陪伴客戶輕鬆地找到符合需求的好伴旅-行李箱</h4></dd>
                </div>
                <div class="option">
                    <dt><h2 class="ff-merri">Our Vision</h2></dt>
                    <div class="hr t_center"></div>
                    <dd><h4>成為最貼近使用者的行李箱整合性平台</h4></dd>
                </div>
                <div class="option">
                    <dt><h2 class="ff-merri">Our Promise</h2></dt>
                    <div class="hr t_center"></div>
                    <dd><h4>值得追求的高品質行李箱加上完善的網路購物體驗</h4></dd>
                </div>
                <div class="option">
                    <dt><h2 class="ff-merri">Our Vibe</h2></dt>
                    <div class="hr t_center"></div>
                    <dd><h4>透過理性的角度把枯燥的日常變成驚喜的瞬間</h4></dd>
                </div>

            </dl>

        </div>
        <div class="a_intro relative t_center">
            <ul class="t_left">
                <li class="no_bullet t_center"><h3>. Container不只是販售物件的網站，更是滿足需求的確幸</h3></li>
                <li class="no_bullet">追求質感和具有優良品質的行李箱，讓每一位使用者能在任何環境與需求底下更輕鬆的行動，因為這樣的行李箱而讓生活更幸福。人生中的小確幸，大概就是透過這樣一個生活中的物件，從而點綴自己的小幸福。</li>
                <li class="no_bullet">過去的選物網站都以外型與功能為主。除了找尋那些願意「專注做好產品」的品牌，我們則再加入一個新標準——「需求」，也就是思考對客戶來說: 什麼是真正的優化生活？更細膩發掘還有哪些需求沒有被照顧到。</li>
                <li class="no_bullet">品牌LOGO從Container精萃轉化而來，顧名思義就是期許這些物件能成為帶著許多生活痕跡的容器，甚至是裝有世界各地的氣息箱子。每一個箱子包含了各種幸福或是生活的細節，藉由這些經歷和過程不斷的成長並堆疊出一個世界。</li>
                <li class="no_bullet">本平台配色使用帶著沉穩氣息的普魯士藍色與微微金屬光澤的黃銅色，達成具有質感且細膩的現代都會的簡約風格。在一眼望去的行李箱中，我們致力於讓使用的人去挖掘其中的細節產生出無限的想像。</li>
            </ul>
        </div>
    </div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script>
// window.onload = function(){
//     var logo = $(".logo"),
//     layerLogoB = $(".logo_bk"),
//     layerLogoF = $(".logo_fk");
// }

// var layerLogoF = $(".logo_fk");
// var logo = $(".logo");

// mouse event for container @ logo(hero section)
$('.logo').on('mousemove',function(e){
    var pageX = e.clientX/100-50,
        pageY = e.clientY/100;
    $('.logo_fk').css('transform', 'translateX('+ pageX + '% ) ');
    // ,'translateY(' + pageY/100 + '%) '
});

// scrolldown btn
// $(function() {
//   $('a[href*=#]').on('click', function(e) {
//     e.preventDefault();
//     $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
//   });
// });

</script>
    <?php include __DIR__. '/__html__footer.php' ?>