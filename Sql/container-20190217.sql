-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2019-02-17 16:15:21
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `container`
--

-- --------------------------------------------------------

--
-- 資料表結構 `color_mapping`
--

CREATE TABLE `color_mapping` (
  `color_sid` int(11) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `color_mapping`
--

INSERT INTO `color_mapping` (`color_sid`, `color`, `color_code`) VALUES
(1, '沉穩黑', '#000000'),
(2, '皇家藍', '#243B55'),
(3, '自然棕', '#AC8669'),
(4, '月光白', '#eeeaff'),
(5, '星光灰', '#D5D5D5'),
(6, '橄欖綠', '#7A7A4D'),
(7, '酒紅色', '#BE1B1B'),
(8, '活力黃', '#FFDD00'),
(9, '鐵深灰', '#646464'),
(10, '活潑橘', '#FF7700'),
(11, '紅框黑', '#ff0000'),
(12, '藍框黑', '#0000ff'),
(13, '紫羅蘭', '#CE8ACE'),
(14, '天空藍', '#99d9ea');

-- --------------------------------------------------------

--
-- 資料表結構 `lunggage_data`
--

CREATE TABLE `lunggage_data` (
  `SID` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL COMMENT '品牌',
  `type` varchar(255) NOT NULL COMMENT '系列',
  `price` varchar(255) NOT NULL COMMENT '價錢',
  `border` varchar(255) NOT NULL COMMENT '款式',
  `box` varchar(11) NOT NULL COMMENT '硬/軟',
  `texture` varchar(255) NOT NULL COMMENT '材質',
  `cm` varchar(255) NOT NULL COMMENT '長寬高 cm',
  `kg` varchar(10) NOT NULL COMMENT '重量kg',
  `insideL` varchar(10) NOT NULL COMMENT '容量 L',
  `tsa` varchar(255) NOT NULL COMMENT 'TSA海關鎖',
  `roll` varchar(255) NOT NULL COMMENT '輪子',
  `expan` varchar(10) NOT NULL COMMENT '擴充',
  `brake` varchar(10) NOT NULL COMMENT '煞車',
  `warranty` varchar(10) NOT NULL COMMENT '保固年限',
  `intro` varchar(1000) NOT NULL COMMENT '詳細說明',
  `anno` varchar(255) NOT NULL COMMENT '詳細說明內容',
  `detail_img` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `lunggage_data`
--

INSERT INTO `lunggage_data` (`SID`, `brand`, `type`, `price`, `border`, `box`, `texture`, `cm`, `kg`, `insideL`, `tsa`, `roll`, `expan`, `brake`, `warranty`, `intro`, `anno`, `detail_img`) VALUES
(1, 'AWAY', 'The Carry-On', '6877', '鋁框', '硬箱', 'PC', 'W55 H35 D23 cm', '3.4 kg', '40 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li>內建電池可充電</li>\r\n<li>360°Hinomoto輪</li>\r\n<li>隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(2, 'AWAY', 'The Bigger Carry-On', '7489', '鋁框', '硬箱', 'PC', 'W58 H37 D 24 cm', '3.5 kg', '48 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li class=\"c-li-lineh\">內建電池可充電</li>\r\n<li class=\"c-li-lineh\">360°Hinomoto輪</li>\r\n<li class=\"c-li-lineh\">隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(3, 'AWAY', 'The Medium', '8406', '鋁框', '硬箱', 'PC', 'W66 H47 D28 cm', '4.4 kg', '68 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li class=\"c-li-lineh\">內建電池可充電</li>\r\n<li class=\"c-li-lineh\">360°Hinomoto輪</li>\r\n<li class=\"c-li-lineh\">隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(4, 'AWAY', 'The Large', '9017', '鋁框', '硬箱', 'PC', 'W74 H52 D32 cm', '5 kg', '99 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li class=\"c-li-lineh\">內建電池可充電</li>\r\n<li class=\"c-li-lineh\">360°Hinomoto輪</li>\r\n<li class=\"c-li-lineh\">隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(5, 'AWAY', 'The Carry-On with Pocket', '8406', '鋁框', '硬箱', 'PC', 'W55 H35 D23 cm', '3.8 kg', '38 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li class=\"c-li-lineh\">多層口袋</li>\r\n<li class=\"c-li-lineh\">防水尼龍</li>\r\n<li class=\"c-li-lineh\">可放置15吋筆記型電腦</li>\r\n<li class=\"c-li-lineh\">內建電池可充電</li>\r\n<li class=\"c-li-lineh\">360°Hinomoto輪</li>\r\n<li class=\"c-li-lineh\">隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(6, 'AWAY', 'The Bigger Carry-On with Pocket', '9017', '鋁框', '硬箱', 'PC', 'W58 H37 D24 cm', '4 kg', '46 L', 'Y', '4輪', 'N', 'N', '終身', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '<li class=\"c-li-lineh\">多層口袋</li>\r\n<li class=\"c-li-lineh\">防水尼龍</li>\r\n<li class=\"c-li-lineh\">可放置15吋筆記型電腦</li>\r\n<li class=\"c-li-lineh\">內建電池可充電</li>\r\n<li class=\"c-li-lineh\">360°Hinomoto輪</li>\r\n<li class=\"c-li-lineh\">隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(7, 'AWAY', 'The Kid\'s Carry-On', '5960', '鋁框', '硬箱', 'PC', 'W46 H30 D23 cm', '2.7 kg', '31 L', 'Y', '4輪', 'N', 'N', '十年', '外部採用高品質的德國聚碳酸酯，具有極佳的耐用性、富有彈性又輕盈，內部選用可拆卸的防水面料，並附有高容量電池、USB充電頭，解決旅行中常有的電壓插頭不合或手機沒電的困擾，底部則為日本360°旋轉滾輪，能安靜平穩地拖拉於磚瓦地面。', '\r\n<li>內建電池可充電</li>\r\n<li>360°Hinomoto輪</li>\r\n<li>隱藏式洗衣袋</li>', 'AWAY-detail1.jpg;;AWAY-detail2.jpg;;AWAY-detail3.jpg;;AWAY-detail4.jpg;;AWAY-detail5.jpg;;AWAY-detail6.jpg'),
(8, 'DELSEY', 'TURENNE', '6487', '鋁框', '硬箱', 'PC', 'W55 H40 D20 cm', '2 kg', '35 L', 'Y', '4輪(飛機輪)', 'N', 'N', '', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'DELSEY-detail1.jpg;;DELSEY-detail2.jpg;;DELSEY-detail3.jpg;;DELSEY-detail4.jpg;;DELSEY-detail5.jpg;;DELSEY-detail6.jpg'),
(9, 'DELSEY', 'TURENNE', '7035', '鋁框', '硬箱', 'PC', 'W78 H50 D30 cm', '2.1 kg', '37 L', 'Y', '4輪(飛機輪)', 'N', 'N', '十年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'DELSEY-detail1.jpg;;DELSEY-detail2.jpg;;DELSEY-detail3.jpg;;DELSEY-detail4.jpg;;DELSEY-detail5.jpg;;DELSEY-detail6.jpg'),
(10, 'DELSEY', 'TURENNE', '8040', '鋁框', '硬箱', 'PC', 'W65 H44.5 D26 cm', '3 kg', '62 L', 'Y', '4輪(飛機輪)', 'N', 'N', '十年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'DELSEY-detail1.jpg;;DELSEY-detail2.jpg;;DELSEY-detail3.jpg;;DELSEY-detail4.jpg;;DELSEY-detail5.jpg;;DELSEY-detail6.jpg'),
(11, 'DELSEY', 'TURENNE', '8743', '鋁框', '硬箱', 'PC', 'W70 H47 D30 cm', '3.2 kg', '81 L', 'Y', '4輪(飛機輪)', 'N', 'N', '十年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'DELSEY-detail1.jpg;;DELSEY-detail2.jpg;;DELSEY-detail3.jpg;;DELSEY-detail4.jpg;;DELSEY-detail5.jpg;;DELSEY-detail6.jpg'),
(12, 'DELSEY', 'TURENNE', '9445', '鋁框', '硬箱', 'PC', 'W75 H48.5 D30 cm', '3.3 kg', '94 L', 'Y', '4輪(飛機輪)', 'N', 'N', '十年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'DELSEY-detail1.jpg;;DELSEY-detail2.jpg;;DELSEY-detail3.jpg;;DELSEY-detail4.jpg;;DELSEY-detail5.jpg;;DELSEY-detail6.jpg'),
(13, 'DELSEY', 'ENVOL', '5267', '鋁框', '硬箱', 'PC', 'W55 H35 D25 cm', '3 kg', '41 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '', 'ENVOL-detail1.jpg;;ENVOL-detail2.jpg;;ENVOL-detail3.jpg;;ENVOL-detail4.jpg'),
(14, 'DELSEY', 'ENVOL', '5480', '鋁框', '硬箱', 'PC', 'W68 H44 D28 cm', '3.9 kg', '65 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '', 'ENVOL-detail1.jpg;;ENVOL-detail2.jpg;;ENVOL-detail3.jpg;;ENVOL-detail4.jpg'),
(15, 'DELSEY', 'ENVOL', '7035', '鋁框', '硬箱', 'PC', 'W78 H50 D30 cm', '4.9 kg', '95 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '', 'ENVOL-detail1.jpg;;ENVOL-detail2.jpg;;ENVOL-detail3.jpg;;ENVOL-detail4.jpg'),
(16, 'DELSEY', 'CHATELET AIR', '8803', '鋁框', '硬箱', 'PC', 'W55 H35 D25 cm', '3.3 kg', '39 L', 'Y', '4輪(飛機輪)', 'N', 'N', '十年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'chatelet-detail1.jpg;;chatelet-detail2.jpg;;chatelet-detail3.jpg;;chatelet-detail4.jpg;;chatelet-detail5.jpg;;chatelet-detail6.jpg'),
(17, 'DELSEY', 'CHATELET AIR', '8803', '鋁框', '硬箱', 'PC', 'W55 D40 D20 cm', '3.2 kg', '38 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'chatelet-detail1.jpg;;chatelet-detail2.jpg;;chatelet-detail3.jpg;;chatelet-detail4.jpg;;chatelet-detail5.jpg;;chatelet-detail6.jpg'),
(18, 'DELSEY', 'MONTMARTRE AIR', '5104', '拉鍊', '軟箱', '聚脂纖維', 'W55 H35 D20 cm', '2.3 kg', '40 L', 'N', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>獨家專利-ST保安拉鍊</li>', 'montmartre-detail1.jpg;;montmartre-detail2.jpg'),
(19, 'DELSEY', 'MONTMARTRE AIR', '5245', '拉鍊', '軟箱', '聚脂纖維', 'W55 H40 D20 cm', '2.2 kg', '37 L', 'N', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>', 'montmartre-detail1.jpg;;montmartre-detail2.jpg'),
(20, 'DELSEY', 'MONTMARTRE AIR', '5245', '拉鍊', '軟箱', '聚脂纖維', 'W55 H35 D25 cm', '2.3 kg', '42 L', 'N', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'montmartre-detail1.jpg;;montmartre-detail2.jpg'),
(21, 'DELSEY', 'MONTMARTRE AIR', '5456', '拉鍊', '軟箱', '聚脂纖維', 'W55 H40 D20 cm', '2.2 kg', '37 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'montmartre-detail1.jpg;;montmartre-detail2.jpg'),
(22, 'DELSEY', 'SEGUR', '5104', '鋁框', '硬箱', 'PC', 'W55 H35 D25 cm', '2.9 kg', '43 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'SEGUR-detail1.jpg;;SEGUR-detail2.jpg;;SEGUR-detail3.jpg;;SEGUR-detail4.jpg;;SEGUR-detail5.jpg;;SEGUR-detail6.jpg'),
(23, 'DELSEY', 'SEGUR', '5245', '鋁框', '硬箱', 'PC', 'W55 H40 D20 cm', '2.7 kg', '40 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'SEGUR-detail1.jpg;;SEGUR-detail2.jpg;;SEGUR-detail3.jpg;;SEGUR-detail4.jpg;;SEGUR-detail5.jpg;;SEGUR-detail6.jpg'),
(24, 'DELSEY', 'SEGUR', '5949', '鋁框', '硬箱', 'PC', 'W70 H47 D28 cm', '3.9 kg', '82 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'SEGUR-detail1.jpg;;SEGUR-detail2.jpg;;SEGUR-detail3.jpg;;SEGUR-detail4.jpg;;SEGUR-detail5.jpg;;SEGUR-detail6.jpg'),
(25, 'DELSEY', 'SEGUR', '5949', '鋁框', '硬箱', 'PC', 'W78 H50 D30 cm', '4.7 kg', '105 L', 'Y', '4輪(飛機輪)', 'N', 'N', '五年', '歐洲知名品牌DELSEY是全世界最安全的旅行箱，集合6大頂尖設計師團隊，打造出世界專利ST包安拉鍊，融合流行設計與堅固耐用的性能，讓全球旅行者帶來更多的安全與保障。', '<li>專利設計內裡可拆卸清洗</li>\r\n<li>可放置筆記型電腦襯墊</li>\r\n<li>多口袋設計</li>', 'SEGUR-detail1.jpg;;SEGUR-detail2.jpg;;SEGUR-detail3.jpg;;SEGUR-detail4.jpg;;SEGUR-detail5.jpg;;SEGUR-detail6.jpg'),
(26, 'HIDEO WAKAMATSU', 'VEIL 2-WAY CARRY-ON', '6870', '拉鍊', '軟箱', '尼龍', 'W51 H36 D23 cm', '2.7 kg', '32 L', 'Y', '2輪單向輪', 'N', 'N', '一年', 'Hideo Wakamatsu創造出具有創新工程和非一般設計的行李箱。VEIL可能看起來像一個非常神秘的名字，但仔細看，它的塑料塗層尼龍外觀輕巧，堅韌，防水；靈活的外觀意味著您永遠不必在包裝清單上妥協。', '<li class=\"c-li-lineh\">伸縮手柄</li>\r\n<li class=\"c-li-lineh\">2WAY用法，可拉可背</li>', 'hideo-detail1.jpg;;hideo-detail2.jpg;;hideo-detail3.jpg;;hideo-detail4.jpg'),
(27, 'HIDEO WAKAMATSU', 'VEIL CARRY-ON', '6870', '拉鍊', '軟箱', '尼龍', 'W51 H36 D23 cm', '2.7 kg', '32 L', 'Y', '4小輪', 'N', 'N', '一年', 'Hideo Wakamatsu創造出具有創新工程和非一般設計的行李箱。VEIL可能看起來像一個非常神秘的名字，但仔細看，它的塑料塗層尼龍外觀輕巧，堅韌，防水；靈活的外觀意味著您永遠不必在包裝清單上妥協。', '<li class=\"c-li-lineh\">伸縮手柄</li>', 'hideo-detail1.jpg;;hideo-detail2.jpg;;hideo-detail3.jpg;;hideo-detail4.jpg'),
(28, 'HIDEO WAKAMATSU', 'VEIL', '7770', '拉鍊', '軟箱', '尼龍', 'W66 H48 D33 cm', '3.6 kg', '65 L', 'Y', '4小輪', 'N', 'N', '一年', 'Hideo Wakamatsu創造出具有創新工程和非一般設計的行李箱。VEIL可能看起來像一個非常神秘的名字，但仔細看，它的塑料塗層尼龍外觀輕巧，堅韌，防水；靈活的外觀意味著您永遠不必在包裝清單上妥協。', '<li class=\"c-li-lineh\">優質YKK拉鍊</li><li class=\"c-li-lineh\">多儲物袋設計鍊</li><li class=\"c-li-lineh\">伸縮手柄</li>', 'hideo-detail1.jpg;;hideo-detail2.jpg;;hideo-detail3.jpg;;hideo-detail4.jpg'),
(29, 'HIDEO WAKAMATSU', 'TARPAULIN CARRY-ON', '6270', '拉鍊', '軟箱', '防水油布', 'W51 H38 D25 cm', '2.9 kg', '32 L', 'N', '2輪 單向輪', 'N', 'N', '一年', '這款輕巧的滾動行李箱既實用又耐用，外觀美觀，採用防水油布製成。防水，抗撕裂，耐用的材料配有防水拉鍊和靜音，光滑的滾輪。這個手提箱沒有典型的金屬框架來衡量它或占用空間。當你輕裝上陣或擴大服裝更換時，請收緊它。內部綁帶和口袋使您可以輕鬆保持物品的整潔。', '<li class=\"c-li-lineh\">多儲物袋設計鍊</li><li class=\"c-li-lineh\">靛藍色內襯</li>', 'Tarpaulin-detail1.jpg;;Tarpaulin-detail2.jpg'),
(30, 'HIDEO WAKAMATSU', 'TARPAULIN', '7170', '拉鍊', '軟箱', '防水油布', 'W64 H48 D25 cm', '3.6 kg', '65 L', 'N', '2輪 單向輪', 'N', 'N', '一年', '這款輕巧的滾動行李箱既實用又耐用，外觀美觀，採用防水油布製成。防水，抗撕裂，耐用的材料配有防水拉鍊和靜音，光滑的滾輪。這個手提箱沒有典型的金屬框架來衡量它或占用空間。當你輕裝上陣或擴大服裝更換時，請收緊它。內部綁帶和口袋使您可以輕鬆保持物品的整潔。', '<li class=\"c-li-lineh\">多儲物袋設計鍊</li><li class=\"c-li-lineh\">靛藍色內襯</li>', 'Tarpaulin-detail1.jpg;;Tarpaulin-detail2.jpg'),
(31, 'HIDEO WAKAMATSU', 'TAOPEN TOP CARRY-ON', '7170', '拉鍊', '硬箱', 'PC', 'W55 H36 D23 cm', '3.4 kg', '33 L', 'Y', '4小輪', 'N', 'N', '一年', 'Hideo Wakamatsu通過Open Top提升您的旅行和商務體驗，Open Top是我們設計創新，工程，功能和性能的巔峰之作。這款出色的國際隨身攜帶設備採用創新的前蓋，讓您可以自由地將特定物品與其他物品分開，以便快速地拿取您的旅行物品。這款行李箱滿足大部分歐洲和亞洲旅行的隨身攜帶尺寸要求。它也非常適合1-2天的國內旅行。', '<li class=\"c-li-lineh\">雙TSA安裝鎖</li><li class=\"c-li-lineh\">1格拉鍊袋</li><li class=\"c-li-lineh\">伸縮手柄</li>', '31-hideo-opentop-detial-01.jpg;;31-hideo-opentop-detial-02.jpg;;31-hideo-opentop-detial-03.jpg;;31-hideo-opentop-detial-04.jpg;;31-hideo-opentop-detial-05.jpg;;31-hideo-opentop-detial-06.jpg'),
(32, 'HIDEO WAKAMATSU', 'TAFLASH CARRY-ON ', '6570', '拉鍊', '硬箱', 'PC', 'W55 H36 D23 cm', '2.9 kg', '32 L', 'Y', '4輪 飛機輪', 'N', 'N', '一年', 'Flash系列是我們在硬殼箱中最輕的行李箱。它由超輕但非常堅固的聚碳酸酯製成，並通過四個360度旋轉輪提供最大的機動性。前面板拉鍊隔層可以輕鬆拿取筆記本電腦，並有足夠的空間存放旅行證件，衣物和其他裝備。符合大部分國際和國內隨身攜帶尺寸要求。', '<li class=\"c-li-lineh\">符合大部分國際隨身攜帶尺寸規範</li>', '32-hideo-flash-detial-01.jpg;;32-hideo-flash-detial-02.jpg;;32-hideo-flash-detial-03.jpg;;32-hideo-flash-detial-04.jpg;;32-hideo-flash-detial-05.jpg;;32-hideo-flash-detial-06.jpg'),
(33, 'HIDEO WAKAMATSU', 'FLASH CARRY-ON WIDE', '6570', '拉鍊', '硬箱', 'PC', 'W43 H41 D23 cm', '2.9 kg', '36 L', 'Y', '4輪 飛機輪', 'N', 'N', '一年', 'Flash系列是我們在硬殼箱中最輕的行李箱。它由超輕但非常堅固的聚碳酸酯製成，並通過四個360度旋轉輪提供最大的機動性。前面板拉鍊隔層可以輕鬆拿取筆記本電腦，並有足夠的空間存放旅行證件，衣物和其他裝備。符合大部分國際和國內隨身攜帶尺寸要求。', '<li class=\"c-li-lineh\">符合大部分國際隨身攜帶尺寸規範</li>', '33-hideo-flashwide-detial-01.jpg;;33-hideo-flashwide-detial-02.jpg;;33-hideo-flashwide-detial-03.jpg;;33-hideo-flashwide-detial-04.jpg'),
(34, 'HIDEO WAKAMATSU', 'DOLPHIN 2-WAY CARRY-ON', '6270', '拉鍊', '軟箱', '尼龍', 'W51 H33 D17 cm', '2.7 kg', '28 L', 'N', '2輪 單向輪', 'N', 'N', '一年', '當您將每次旅行視為冒險之旅時，您需要的就是一個可以長途使用直到最後一刻的行李箱。我們承諾的Dolphin系列終身耐用。無論使用拖拉滾動或是後背狀態皆得心應手，可擴展的防水油布車身使其更易於打包，堅固耐用，防水，適合最大膽的旅客。', '<li class=\"c-li-lineh\">2用包</li><li class=\"c-li-lineh\">符合大部分國際隨身攜帶尺寸規範</li><li class=\"c-li-lineh\">多儲物袋設計</li>', '34-hideo-dolphin-detial-01.jpg;;34-hideo-dolphin-detial-02.jpg;;34-hideo-dolphin-detial-03.jpg;;34-hideo-dolphin-detial-04.jpg'),
(35, 'HIDEO WAKAMATSU', 'NARROW CARRY-ON ', '6870', '鋁框', '硬箱', 'PC', 'W55 H36 D23 cm', '2.7 kg', '32 L', 'Y', '4輪 飛機輪', 'N', 'N', '一年', 'Hideo的窄版系列在有趣，時尚的美學和對功能性的嚴肅強調之間實現了完美平衡。輕巧而寬敞，行李箱的車身採用先進的聚碳酸酯構造，採用高強度香檳色鋁製框架結構，並採用雙TSA表面安裝鎖固定。', '<li class=\"c-li-lineh\">符合大部分國際隨身攜帶尺寸規範</li><li class=\"c-li-lineh\">360度靜音輪</li>', '35-hideo-narrow-detial-01.jpg;;35-hideo-narrow-detial-02.jpg;;35-hideo-narrow-detial-03.jpg;;35-hideo-narrow-detial-04.jpg'),
(36, 'HIDEO WAKAMATSU', 'NARROW', '7770', '鋁框', '硬箱', 'PC', 'W69 H43 D27 cm', '3.6 kg', '94 L', 'Y', '4輪 飛機輪', 'N', 'N', '一年', 'Hideo的窄版系列在有趣，時尚的美學和對功能性的嚴肅強調之間實現了完美平衡。輕巧而寬敞，行李箱的車身採用先進的聚碳酸酯構造，採用高強度香檳色鋁製框架結構，並採用雙TSA表面安裝鎖固定。', '<li class=\"c-li-lineh\">360度靜音輪</li><li class=\"c-li-lineh\">雙海關鎖</li>', '36-hideo-narrow-24-detial-01.jpg;;36-hideo-narrow-24-detial-02.jpg;;36-hideo-narrow-24-detial-03.jpg;;36-hideo-narrow-24-detial-04.jpg'),
(37, 'HIDEO WAKAMATSU', 'NARROW2', '8370', '鋁框', '硬箱', 'PC', 'W76 H51 D29 cm', '4.2 kg', '', 'Y', '4輪 飛機輪', 'N', 'N', '一年', 'Hideo的窄版系列在有趣，時尚的美學和對功能性的嚴肅強調之間實現了完美平衡。輕巧而寬敞，行李箱的車身採用先進的聚碳酸酯構造，採用高強度香檳色鋁製框架結構，並採用雙TSA表面安裝鎖固定。', '<li class=\"c-li-lineh\">360度靜音輪</li><li class=\"c-li-lineh\">雙海關鎖</li>', '36-hideo-narrow-24-detial-01.jpg;;36-hideo-narrow-24-detial-02.jpg;;36-hideo-narrow-24-detial-03.jpg;;36-hideo-narrow-24-detial-04.jpg'),
(38, 'HIDEO WAKAMATSU', 'STEALTH', '8530', '鋁框', '硬箱', 'PC', 'W71 H46 D28 cm', '3.8 kg', '82 L', 'Y', '4輪', 'N', 'N', '一年', '堅韌是一種奢侈直到遇見Stealth。外殼採用輕質聚碳酸酯製成，可根據周圍環境彎曲和彎曲。這款中型包裝箱採用流線型設計，採用全新的輪系統，可實現平穩的操控，讓世界環遊世界。', '<li class=\"c-li-lineh\">360度靜音輪</li><li class=\"c-li-lineh\">雙海關鎖</li>', '36-hideo-narrow-24-detial-01.jpg;;36-hideo-narrow-24-detial-02.jpg;;36-hideo-narrow-24-detial-03.jpg;;36-hideo-narrow-24-detial-04.jpg'),
(39, 'HARTMANN', 'METROPOLITAN2 GLOBAL', '8530', '拉鍊', '軟箱', '尼龍', 'W57 H45 D27 cm', '4 kg', '53 L', 'N', '4輪', 'Y', 'N', '一年', 'Metropolitan 2延續其精緻的風格和設計，為商務旅客量身定制。更新了Hartmann DucordTM條紋等檔案元素和USB端口等現代功能，現代設計採用時尚尼龍製成，使用輕質蜂窩框架。經過精心考慮的內飾提供多種收納選擇，以及帶襯墊的三折式西裝。', '<li class=\"c-li-lineh\">帶襯墊的三折式西裝套</li> <li class=\"c-li-lineh\"> USB接口</li> <li class=\"c-li-lineh\">最佳的輪式系統，帶有硬化鋼製軸承</li> <li class=\"c-li-lineh\">多個內部口袋方便規劃收納</li>', '39-hartmann-met2Global-20-detial-01.jpg;;39-hartmann-met2Global-20-detial-02.jpg;;39-hartmann-met2Global-20-detial-03.jpg;;39-hartmann-met2Global-20-detial-04.jpg;;39-hartmann-met2Global-20-detial-05.jpg;;39-hartmann-met2Global-20-detial-06.jpg'),
(40, 'HARTMANN', 'METROPOLITAN2 DOMESTIC', '8930', '拉鍊', '軟箱', '尼龍', 'W58 H42 D27 cm', '4.2 kg', '53 L', 'N', '4輪', 'Y', 'N', '一年', 'Metropolitan 2延續其精緻的風格和設計，為商務旅客量身定制。更新了Hartmann DucordTM條紋等檔案元素和USB端口等現代功能，現代設計採用時尚尼龍製成，使用輕質蜂窩框架。經過精心考慮的內飾提供多種收納選擇，以及帶襯墊的三折式西裝。', '<li class=\"c-li-lineh\">帶襯墊的三折式西裝套</li> <li class=\"c-li-lineh\"> USB接口</li> <li class=\"c-li-lineh\">最佳的輪式系統，帶有硬化鋼製軸承</li> <li class=\"c-li-lineh\">多個內部口袋方便規劃收納</li>', '39-hartmann-met2Global-20-detial-01.jpg;;39-hartmann-met2Global-20-detial-02.jpg;;39-hartmann-met2Global-20-detial-03.jpg;;39-hartmann-met2Global-20-detial-04.jpg;;39-hartmann-met2Global-20-detial-05.jpg;;39-hartmann-met2Global-20-detial-06.jpg'),
(41, 'HARTMANN', 'METROPOLITAN2 UNDERSEAT', '9000', '拉鍊', '軟箱', '尼龍', 'W46 H37 D24 cm', '3 kg', '40 L', 'N', '4輪', 'N', 'N', '一年', 'Metropolitan 2延續其精緻的風格和設計，為商務旅客量身定制。更新了Hartmann DucordTM條紋等檔案元素和USB端口等現代功能，現代設計採用時尚尼龍製成，使用輕質蜂窩框架。Underseater完善了這款系列，為商旅常使用的筆記本電腦等物品提供了最方便的使用體驗。', '<li class=\"c-li-lineh\"> USB接口</li> <li class=\"c-li-lineh\">備有筆記型電腦襯墊</li><li class=\"c-li-lineh\">多個內部口袋方便規劃收納</li>', '39-hartmann-met2Global-20-detial-01.jpg;;39-hartmann-met2Global-20-detial-02.jpg;;39-hartmann-met2Global-20-detial-03.jpg;;39-hartmann-met2Global-20-detial-04.jpg;;39-hartmann-met2Global-20-detial-05.jpg;;39-hartmann-met2Global-20-detial-06.jpg'),
(42, 'HARTMANN', 'TWEED LEGEND UNDERSEAT', '9100', '拉鍊', '軟箱', '尼龍', 'W46 H36 D23 cm', '3.2 kg', '40 L', 'N', '4輪', 'N', 'N', '一年', '憑藉傳統工藝和風格，Tweed Legend伴隨著最難忘的旅程。', '<li class=\"c-li-lineh\"> USB端口和拉鍊電池袋，可隨時隨地充電</li><li class=\"c-li-lineh\">備有筆記型電腦襯墊</li>', '42-hartmann-tweed-17-detial-01.jpg;;42-hartmann-tweed-17-detial-02.jpg'),
(43, 'HARTMANN', 'CENTURY HARDSIDE', '9000', '拉鍊', '硬箱', 'PC', 'W56 H37 D24 cm', '3.4 kg', '32 L', 'Y', '4輪', 'Y', 'N', '一年', 'Century Hardside是一款搭配4大輪的可擴充式行李箱，非常適合國內旅行和一兩天的短途旅行。因為您的隨身攜帶物品將是您最常用的行李箱，所以您需要一件能夠反映您精緻風格的作品，其質量水平每次都能完美搭配您的衣著。可拆卸的三件套西裝，便於將掛衣服從家裡運送到飛機到酒店衣櫃，無論您到哪裡旅行，都能享受奢華的包裝和旅行體驗。', '<li class=\"c-li-lineh\">可拆卸的三折襯墊西裝</li><li class=\"c-li-lineh\">拉鍊式擴充</li>', '43-hartmann-century-21-detial-01.jpg;;43-hartmann-century-21-detial-02.jpg;;43-hartmann-century-21-detial-03.jpg;;43-hartmann-century-21-detial-04.jpg;;43-hartmann-century-21-detial-05.jpg;;43-hartmann-century-21-detial-06.jpg'),
(44, 'HARTMANN', 'CENTURY HARDSIDE M', '9110', '拉鍊', '硬箱', 'PC', 'W70 H44 D30 cm', '4.3 kg', '94 L', 'Y', '4輪', 'Y', 'N', '一年', '對於商務或休閒旅行的幾天到一周的旅行，Century Hardside提供您所需的所有空間和優雅。一個可拆卸的三角形西裝，經典的內部DucordTM條紋和可擴展的空間。由於行李箱可以與您的旅程的每一段無縫連接，您將以輕鬆，永恆的風格旅行。經過TSA認證的鎖具將安全性和安心性與您對Hartmann行李箱所期望的高端功能相結合。', '<li class=\"c-li-lineh\">可拆卸的三折襯墊西裝</li><li class=\"c-li-lineh\">拉鍊式擴充</li>', '43-hartmann-century-21-detial-01.jpg;;43-hartmann-century-21-detial-02.jpg;;43-hartmann-century-21-detial-03.jpg;;43-hartmann-century-21-detial-04.jpg;;43-hartmann-century-21-detial-05.jpg;;43-hartmann-century-21-detial-06.jpg'),
(45, 'HARTMANN', 'CENTURY HARDSIDE E', '9150', '拉鍊', '硬箱', 'PC', 'W80 H55 D36 cm', '5.4 kg', '142 L', 'Y', '4輪', 'Y', 'N', '一年', '這款是Century Hardside最寬敞的選擇，非常適合長途旅行或與旅行伴侶共用行李。可拆卸的三折西裝，經典的DucordTM條紋，繫帶，以及光滑的網狀拉鍊面板，即使您在世界各地航行，也能保持您的隨身物品整齊包裝和保護。在為一次盛大的旅行打包時，選擇優質的行李箱至關重要，這是您所有難忘旅程的明智投資。', '<li class=\"c-li-lineh\">可拆卸的三折襯墊西裝</li><li class=\"c-li-lineh\">拉鍊式擴充</li>', '43-hartmann-century-21-detial-01.jpg;;43-hartmann-century-21-detial-02.jpg;;43-hartmann-century-21-detial-03.jpg;;43-hartmann-century-21-detial-04.jpg;;43-hartmann-century-21-detial-05.jpg;;43-hartmann-century-21-detial-06.jpg'),
(46, 'HARTMANN', 'CENTURY DOMESTIC', '8100', '拉鍊', '軟箱', '聚酯纖維', 'W56 H41 D23 cm', '3.5 kg', '53 L', 'N', '4輪', 'Y', 'N', '一年', '對於短短幾天的旅行，Hartmann Century Domestic提供極致的高端風格。純淨的鋁製手柄和滾珠軸承輪可輕鬆拉行，而耐用的滌綸斜紋織物則在美觀和功能方面表現出色。拉鍊口袋和領帶保持一切井井有條，三件套西裝確保您的服裝從家到平面旅行到您酒店的衣櫃，整潔無皺，就像您打包時一樣。您的隨身攜帶物品可能是您最常用的行李箱之一，因此投資您喜愛的Hartmann行李箱是最棒的。', '<li class=\"c-li-lineh\">一體式三折式西裝，可輕鬆運送懸掛式服裝</li><li class=\"c-li-lineh\">滾珠軸承輪在狹小空間內提供卓越的機動性</li><li class=\"c-li-lineh\">拉鍊式擴充</li>', '46-hartmann-domestic-21-detial-01.jpg;;46-hartmann-domestic-21-detial-02.jpg;;46-hartmann-domestic-21-detial-03.jpg;;46-hartmann-domestic-21-detial-04.jpg;;46-hartmann-domestic-21-detial-05.jpg;;46-hartmann-domestic-21-detial-06.jpg'),
(51, 'CROWN', 'F1615', '8350', '鋁框', '硬箱', 'PC', 'W49.4 H70 D30 cm', '5.3 kg', '95 L', 'Y', '2輪', 'N', 'Y', '一年', '新型輕量化PC料，兼顧輕量與塑料彈性，安全保護您的行李。多段式拉桿設計，可依每個人身高不同，調整拉桿高度。備有TSA海關安全鎖及行李箱煞車開關TSA安全密碼鎖具，讓您海外旅行也能安心輕鬆。彈力手把可讓您提取行李箱時，更加方便且不占空間。HINOMOTO輪具前後大小輪設計，小錢輪增加靈活度，大後輪增加載重能力，拖拉行李箱時用兩顆大輪拖行，較穩定順暢且不易損壞。', '<li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(52, 'CROWN', 'LINNER', '7680', '鋁框', '硬箱', 'PC', 'W45 H60 D29.5 cm', '4.3 kg', '70 L', 'Y', '4輪', 'N', 'N', '一年', '新型輕量化PC料，兼顧輕量與塑料彈性，安全保護您的行李。多段式拉桿設計，可依每個人身高不同，調整拉桿高度。備有TSA海關安全鎖及行李箱煞車開關TSA安全密碼鎖具，讓您海外旅行也能安心輕鬆。彈力手把可讓您提取行李箱時，更加方便且不占空間。HINOMOTO輪具前後大小輪設計，小錢輪增加靈活度，大後輪增加載重能力，拖拉行李箱時用兩顆大輪拖行，較穩定順暢且不易損壞。', '<li class=\"c-li-lineh\">行李箱底部箱面手把</li><li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(53, 'CROWN', 'F2501 黑色彩框十字箱', '5980', '鋁框', '硬箱', 'PC+ABS', 'W34.5 H48 D26 cm', '3.7 kg', '35 L', 'Y', '4輪', 'N', 'Y', '一年', '多段式拉桿設計，配備TSA海關安全鎖，讓您海外旅行也能安心輕鬆過關，行李箱煞車開關，讓您在斜坡路面也能放開雙手，不怕行李箱滑走。防撞增加護角設計可抵禦強大撞擊，能給予箱子增加多一層的防護。使用日本HINOMOTO高品質輪具，後輪55釐米、前輪50釐米，使用更輕鬆，行走更穩定。本商品使用製作飛機窗口的塑料聚碳酸脂樹脂和堅固的AB塑料混合製作以達到堅固、輕便的效果。', '<li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(54, 'CROWN', 'F2501 黑色彩框十字箱', '6980', '鋁框', '硬箱', 'PC+ABS', 'W40 H59 D27 cm', '4.9 kg', '56 L', 'Y', '4輪 前後大小輪', 'N', 'Y', '一年', '多段式拉桿設計，配備TSA海關安全鎖，讓您海外旅行也能安心輕鬆過關，行李箱煞車開關，讓您在斜坡路面也能放開雙手，不怕行李箱滑走。防撞增加護角設計可抵禦強大撞擊，能給予箱子增加多一層的防護。使用日本HINOMOTO高品質輪具，後輪55釐米、前輪50釐米，使用更輕鬆，行走更穩定。本商品使用製作飛機窗口的塑料聚碳酸脂樹脂和堅固的AB塑料混合製作以達到堅固、輕便的效果。', '<li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(55, 'CROWN', 'F2501 黑色彩框十字箱', '7680', '鋁框', '硬箱', 'PC+ABS', 'W44 H65 D31 cm', '5.4 kg', '68 L', 'Y', '4輪 前後大小輪', 'N', 'Y', '一年', '多段式拉桿設計，配備TSA海關安全鎖，讓您海外旅行也能安心輕鬆過關，行李箱煞車開關，讓您在斜坡路面也能放開雙手，不怕行李箱滑走。防撞增加護角設計可抵禦強大撞擊，能給予箱子增加多一層的防護。使用日本HINOMOTO高品質輪具，後輪55釐米、前輪50釐米，使用更輕鬆，行走更穩定。本商品使用製作飛機窗口的塑料聚碳酸脂樹脂和堅固的AB塑料混合製作以達到堅固、輕便的效果。', '<li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(56, 'CROWN', 'F2501 黑色彩框十字箱', '8280', '鋁框', '硬箱', 'PC+ABS', 'W48 H71 D31.5 cm', '6.4 kg', '84 L', 'Y', '4輪 前後大小輪', 'N', 'Y', '一年', '多段式拉桿設計，配備TSA海關安全鎖，讓您海外旅行也能安心輕鬆過關，行李箱煞車開關，讓您在斜坡路面也能放開雙手，不怕行李箱滑走。防撞增加護角設計可抵禦強大撞擊，能給予箱子增加多一層的防護。使用日本HINOMOTO高品質輪具，後輪55釐米、前輪50釐米，使用更輕鬆，行走更穩定。本商品使用製作飛機窗口的塑料聚碳酸脂樹脂和堅固的AB塑料混合製作以達到堅固、輕便的效果。', '<li class=\"c-li-lineh\">多隔層內裝設計</li>', ''),
(57, 'BRIC\'S', 'Travel', '6864', '拉鍊', '軟箱', '尼龍', 'W53 H35 D22.8 cm', '2.4 kg', '32 L', 'N', '2輪', 'N', 'N', '一年', '特殊加工尼龍布，使採用高性能尼龍材質配上PVC襯底，既輕巧又防水，耐用又好清潔。加上特殊支撐結構，讓箱子不再軟塌好整理。另外使用義大利小牛皮，名牌、手把、拉片全部使用Bric\'s精心挑選的義大利小牛皮，讓產品經典高雅。採用高品質360度旋轉雙輪，多段式鋁製拉桿，可根據身高與喜好調整高度，拉桿可收納外觀更簡潔，清亮又堅固。', '<li class=\"c-li-lineh\">特殊支撐結構</li> <li class=\"c-li-lineh\">使用義大利小牛皮</li> <li class=\"c-li-lineh\">360高品質輪具</li><li class=\"c-li-lineh\">使用義大利小牛皮</li>', ''),
(58, 'BRIC\'S', 'Travel', '7904', '拉鍊', '軟箱', '尼龍', 'W65 H40 D24 cm', '2.8 kg', '60 L', 'N', '2輪', 'N', 'N', '一年', '特殊加工尼龍布，使採用高性能尼龍材質配上PVC襯底，既輕巧又防水，耐用又好清潔。加上特殊支撐結構，讓箱子不再軟塌好整理。另外使用義大利小牛皮，名牌、手把、拉片全部使用Bric\'s精心挑選的義大利小牛皮，讓產品經典高雅。採用高品質360度旋轉雙輪，多段式鋁製拉桿，可根據身高與喜好調整高度，拉桿可收納外觀更簡潔，清亮又堅固。', '<li class=\"c-li-lineh\">特殊支撐結構</li> <li class=\"c-li-lineh\">使用義大利小牛皮</li> <li class=\"c-li-lineh\">360高品質輪具</li><li class=\"c-li-lineh\">使用義大利小牛皮</li>', ''),
(59, 'BRIC\'S', 'Travel', '9440', '拉鍊', '軟箱', '尼龍', 'W76 H48 D25.4 cm', '4 kg', '105 L', 'N', '2輪', 'N', 'N', '一年', '特殊加工尼龍布，使採用高性能尼龍材質配上PVC襯底，既輕巧又防水，耐用又好清潔。加上特殊支撐結構，讓箱子不再軟塌好整理。另外使用義大利小牛皮，名牌、手把、拉片全部使用Bric\'s精心挑選的義大利小牛皮，讓產品經典高雅。採用高品質360度旋轉雙輪，多段式鋁製拉桿，可根據身高與喜好調整高度，拉桿可收納外觀更簡潔，清亮又堅固。', '<li class=\"c-li-lineh\">特殊支撐結構</li><li class=\"c-li-lineh\">使用義大利小牛皮</li><li class=\"c-li-lineh\">360高品質輪具</li><li class=\"c-li-lineh\">使用義大利小牛皮</li>', ''),
(60, 'HORIZN STUDIOS', 'M5', '8350', '拉鍊', '硬箱', 'PC', 'W55 H40 D20 cm', '3.4 kg', '33 L', 'Y', '4輪', 'N', 'N', '一年', '德國設計的Horizn標誌性航空航天級聚碳酸酯，是我們所有智能行李箱的輕質保護外殼，使每件產品都非常堅韌和富有彈性。 聚碳酸酯的材料特性使得行李箱能夠完美地反應其內容物並彈性吸收任何外部壓力。 前袋採用高端尼龍製成，超級耐用，防水，搭配義大利Vacchetta皮革，隨著時間的推移逐漸形成美麗的光澤，成為您旅行的最佳見證。', '<li class=\"c-li-lineh\">一鍵式可拆卸智能充電器</li><li class=\"c-li-lineh\">內置壓縮墊和優質洗衣袋</li><li class=\"c-li-lineh\">防水前袋，可容納15吋筆記型電腦</li>', ''),
(61, 'HORIZN STUDIOS', 'H5', '8600', '拉鍊', '硬箱', 'PC', 'W55 H40 D20 cm', '3 kg', '35 L', 'Y', '4輪', 'N', 'N', '一年', '德國設計的Horizn標誌性航空航天級聚碳酸酯，是我們所有智能行李箱的輕質保護外殼，它使每件產品都非常堅韌和富有彈性。 聚碳酸酯的材料特性使得行李箱能夠完美地反應其內容物並彈性吸收任何外部壓力，使其堅固又耐用。', '<li class=\"c-li-lineh\">一鍵式可拆卸智能充電器</li><li class=\"c-li-lineh\">內置壓縮墊和優質洗衣袋</li>', ''),
(62, 'HORIZN STUDIOS', 'H6', '9100', '拉鍊', '硬箱', 'PC', 'W64 H46 D24 cm', '3.9 kg', '65 L', 'Y', '4輪', 'N', 'N', '一年', '德國設計的Horizn標誌性航空航天級聚碳酸酯，是我們所有智能行李箱的輕質保護外殼，它使每件產品都非常堅韌和富有彈性。 聚碳酸酯的材料特性使得行李箱能夠完美地反應其內容物並彈性吸收任何外部壓力，使其堅固又耐用。', '<li class=\"c-li-lineh\">一鍵式可拆卸智能充電器</li><li class=\"c-li-lineh\">內置壓縮墊和優質洗衣袋</li>', ''),
(63, 'PROTECA', '360s', '8900', '拉鍊', '硬箱', 'PC', 'W54 H36 D25 cm', '2.8 kg', '32 L', 'Y', '4輪', 'N', 'N', '三年', '可以從任何地方打開，360°開放式。 不論是直式或橫式，皆可對應各種場景的壓倒性易用性。為了減少死角，內部採用單面存儲方式。輪子採用了“SilentCaster(R)輪“，大大減少了輪子滾動音量。 此外，通過裝設可以平滑滑動的“Bearon輪“，除了靜音之外，還可以輕鬆地行駛。 箱身上具備360°系列經典十字花紋。 簡約的設計帶來經典的美感和堅實的力量。', '<li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">360°開放式系統，可從任何方向打開</li><li class=\"c-li-lineh\">革命性收納方式，只需將行李放在一邊，另一邊蓋上蓋子</li>', 'proteca_360s_85L_white_detal1.jpg;;proteca_360s_85L_white_detal2.jpg;;proteca_360s_85L_white_detal3.jpg;;proteca_360s_85L_white_detal4.jpg;;proteca_360s_85L_white_detal5.jpg;;proteca_360s_85L_white_detal6.jpg'),
(64, 'PROTECA', '360s', '9200', '拉鍊', '硬箱', 'PC', 'W59 H43 D26 cm', '3.4 kg', '44 L', 'Y', '4輪', 'N', 'N', '三年', '可以從任何地方打開，360°開放式。 不論是直式或橫式，皆可對應各種場景的壓倒性易用性。為了減少死角，內部採用單面存儲方式。輪子採用了“SilentCaster(R)輪“，大大減少了輪子滾動音量。 此外，通過裝設可以平滑滑動的“Bearon輪“，除了靜音之外，還可以輕鬆地行駛。 箱身上具備360°系列經典十字花紋。 簡約的設計帶來經典的美感和堅實的力量。', '<li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">360°開放式系統，可從任何方向打開</li><li class=\"c-li-lineh\">革命性收納方式，只需將行李放在一邊，另一邊蓋上蓋子</li>', 'proteca_360s_85L_white_detal1.jpg;;proteca_360s_85L_white_detal2.jpg;;proteca_360s_85L_white_detal3.jpg;;proteca_360s_85L_white_detal4.jpg;;proteca_360s_85L_white_detal5.jpg;;proteca_360s_85L_white_detal6.jpg'),
(65, 'PROTECA', '360s', '9300', '拉鍊', '硬箱', 'PC', 'W65 H48 D27cm', '3.8 kg', '61 L', 'Y', '4輪', 'N', 'N', '三年', '可以從任何地方打開，360°開放式。 不論是直式或橫式，皆可對應各種場景的壓倒性易用性。為了減少死角，內部採用單面存儲方式。輪子採用了“SilentCaster(R)輪“，大大減少了輪子滾動音量。 此外，通過裝設可以平滑滑動的“Bearon輪“，除了靜音之外，還可以輕鬆地行駛。 箱身上具備360°系列經典十字花紋。 簡約的設計帶來經典的美感和堅實的力量。', '<li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">360°開放式系統，可從任何方向打開</li><li class=\"c-li-lineh\">革命性收納方式，只需將行李放在一邊，另一邊蓋上蓋子</li>', 'proteca_360s_85L_white_detal1.jpg;;proteca_360s_85L_white_detal2.jpg;;proteca_360s_85L_white_detal3.jpg;;proteca_360s_85L_white_detal4.jpg;;proteca_360s_85L_white_detal5.jpg;;proteca_360s_85L_white_detal6.jpg'),
(66, 'PROTECA', '360s', '9500', '拉鍊', '硬箱', 'PC', 'W76 H54 D27cm', '4.4 kg', '85 L', 'Y', '4輪', 'N', 'N', '三年', '可以從任何地方打開，360°開放式。 不論是直式或橫式，皆可對應各種場景的壓倒性易用性。為了減少死角，內部採用單面存儲方式。輪子採用了“SilentCaster(R)輪“，大大減少了輪子滾動音量。 此外，通過裝設可以平滑滑動的“Bearon輪“，除了靜音之外，還可以輕鬆地行駛。 箱身上具備360°系列經典十字花紋。 簡約的設計帶來經典的美感和堅實的力量。', '<li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">360°開放式系統，可從任何方向打開</li><li class=\"c-li-lineh\">革命性收納方式，只需將行李放在一邊，另一邊蓋上蓋子</li>', 'proteca_360s_85L_white_detal1.jpg;;proteca_360s_85L_white_detal2.jpg;;proteca_360s_85L_white_detal3.jpg;;proteca_360s_85L_white_detal4.jpg;;proteca_360s_85L_white_detal5.jpg;;proteca_360s_85L_white_detal6.jpg'),
(67, 'PROTECA', 'Step Walker', '8100', '拉鍊', '硬箱', 'PC', 'W51 H39 D25 cm', '2.9 kg', '36 L', 'Y', '4輪', 'N', 'N', '三年', '檢票口，人群，自動扶梯，旅行都充滿了壓力。Step Walker以安全行李箱的概念改進了：走動、轉彎、停止等運行性能，三用的輪子可以使用4輪行走，亦能使用直向或橫向的2輪行走。 輪子材質使用了我們獨有的“SilentCaster(R)“輪，大大減少了滾動的音量。 我們不僅追求順暢，而且追求舒適。 箱身使用了不易被刮傷的防刮材質，這是一個不論任何的旅行風格都適合使用的行李箱。', '<li class=\"c-li-lineh\">輕量設計</li><li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">3WAY手柄設計</li>', 'proteca_stepwalker_135L_lightblue_detail_1.jpg;;proteca_stepwalker_135L_lightblue_detail_2.jpg;;proteca_stepwalker_135L_lightblue_detail_3.jpg;;proteca_stepwalker_135L_lightblue_detail_4.jpg;;proteca_stepwalker_135L_lightblue_detail_5.jpg;;proteca_stepwalker_135L_lightblue_detail_6.jpg'),
(68, 'PROTECA', 'Step Walker', '8600', '拉鍊', '硬箱', 'PC', 'W66 H51 D29 cm', '3.8 kg', '75 L', 'Y', '4輪', 'N', 'N', '三年', '檢票口，人群，自動扶梯，旅行都充滿了壓力。Step Walker以安全行李箱的概念改進了：走動、轉彎、停止等運行性能，三用的輪子可以使用4輪行走，亦能使用直向或橫向的2輪行走。 輪子材質使用了我們獨有的“SilentCaster(R)“輪，大大減少了滾動的音量。 我們不僅追求順暢，而且追求舒適。 箱身使用了不易被刮傷的防刮材質，這是一個不論任何的旅行風格都適合使用的行李箱。', '<li class=\"c-li-lineh\">輕量設計</li><li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">3WAY手柄設計</li>', 'proteca_stepwalker_135L_lightblue_detail_1.jpg;;proteca_stepwalker_135L_lightblue_detail_2.jpg;;proteca_stepwalker_135L_lightblue_detail_3.jpg;;proteca_stepwalker_135L_lightblue_detail_4.jpg;;proteca_stepwalker_135L_lightblue_detail_5.jpg;;proteca_stepwalker_135L_lightblue_detail_6.jpg'),
(69, 'PROTECA', 'Step Walker', '8700', '拉鍊', '硬箱', 'PC', 'W69 H53 D35 cm', '4.5 kg', '100 L', 'Y', '4輪', 'N', 'N', '三年', '檢票口，人群，自動扶梯，旅行都充滿了壓力。Step Walker以安全行李箱的概念改進了：走動、轉彎、停止等運行性能，三用的輪子可以使用4輪行走，亦能使用直向或橫向的2輪行走。 輪子材質使用了我們獨有的“SilentCaster(R)“輪，大大減少了滾動的音量。 我們不僅追求順暢，而且追求舒適。 箱身使用了不易被刮傷的防刮材質，這是一個不論任何的旅行風格都適合使用的行李箱。', '<li class=\"c-li-lineh\">輕量設計</li><li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">3WAY手柄設計</li>', 'proteca_stepwalker_135L_lightblue_detail_1.jpg;;proteca_stepwalker_135L_lightblue_detail_2.jpg;;proteca_stepwalker_135L_lightblue_detail_3.jpg;;proteca_stepwalker_135L_lightblue_detail_4.jpg;;proteca_stepwalker_135L_lightblue_detail_5.jpg;;proteca_stepwalker_135L_lightblue_detail_6.jpg'),
(70, 'PROTECA', 'Step Walker', '8800', '拉鍊', '硬箱', 'PC', 'W81 H60 D34 cm', '5.3 kg', '135 L', 'Y', '4輪', 'N', 'N', '三年', '檢票口，人群，自動扶梯，旅行都充滿了壓力。Step Walker以安全行李箱的概念改進了：走動、轉彎、停止等運行性能，三用的輪子可以使用4輪行走，亦能使用直向或橫向的2輪行走。 輪子材質使用了我們獨有的“SilentCaster(R)“輪，大大減少了滾動的音量。 我們不僅追求順暢，而且追求舒適。 箱身使用了不易被刮傷的防刮材質，這是一個不論任何的旅行風格都適合使用的行李箱。', '<li class=\"c-li-lineh\">輕量設計</li><li class=\"c-li-lineh\">4輪靜音輪，可將音量減少30％</li><li class=\"c-li-lineh\">3WAY手柄設計</li>', 'proteca_stepwalker_135L_lightblue_detail_1.jpg;;proteca_stepwalker_135L_lightblue_detail_2.jpg;;proteca_stepwalker_135L_lightblue_detail_3.jpg;;proteca_stepwalker_135L_lightblue_detail_4.jpg;;proteca_stepwalker_135L_lightblue_detail_5.jpg;;proteca_stepwalker_135L_lightblue_detail_6.jpg'),
(71, 'PROTECA', 'GENTRY', '8600', '拉鍊', '硬箱', '尼龍', 'W55 H35 D25 cm', '3.1 kg', '29 L', 'Y', '4輪', 'N', 'N', '三年', '這是一款像是跑步一樣順暢，又輕又穩定的行李箱。推薦喜歡旅行的人使用它，高品質的精美行李箱。 採用70毫米的大輪子，即使在國外有著歷史痕跡的石頭路面上甚至是傳統的酒店地毯上也可以強勁而輕盈地行駛。使用高級皮革材料製作手柄和腰帶給一個成年旅客最適合的迷人行李箱。', '<li class=\"c-li-lineh\">4輪靜音輪SilentCaster(R)70，音量減少50％</li><li class=\"c-li-lineh\">前袋設計</li>', 'proteca_gentry_72L_black_detail_1.jpg;;proteca_gentry_72L_black_detail_2.jpg;;proteca_gentry_72L_black_detail_3.jpg;;proteca_gentry_72L_black_detail_4.jpg'),
(72, 'PROTECA', 'GENTRY', '8900', '拉鍊', '硬箱', '尼龍', 'W64 H42 D28 cm', '4.3 kg', '49 L', 'Y', '4輪', 'N', 'N', '三年', '這是一款像是跑步一樣順暢，又輕又穩定的行李箱。推薦喜歡旅行的人使用它，高品質的精美行李箱。 採用70毫米的大輪子，即使在國外有著歷史痕跡的石頭路面上甚至是傳統的酒店地毯上也可以強勁而輕盈地行駛。使用高級皮革材料製作手柄和腰帶給一個成年旅客最適合的迷人行李箱。', '<li class=\"c-li-lineh\">4輪靜音輪SilentCaster(R)70，音量減少50％</li><li class=\"c-li-lineh\">前袋設計</li>', 'proteca_gentry_72L_black_detail_1.jpg;;proteca_gentry_72L_black_detail_2.jpg;;proteca_gentry_72L_black_detail_3.jpg;;proteca_gentry_72L_black_detail_4.jpg'),
(73, 'PROTECA', 'GENTRY', '9200', '拉鍊', '硬箱', '尼龍', 'W74 H48 D30 cm', '5.2 kg', '73 L', 'Y', '4輪', 'N', 'N', '三年', '這是一款像是跑步一樣順暢，又輕又穩定的行李箱。推薦喜歡旅行的人使用它，高品質的精美行李箱。 採用70毫米的大輪子，即使在國外有著歷史痕跡的石頭路面上甚至是傳統的酒店地毯上也可以強勁而輕盈地行駛。使用高級皮革材料製作手柄和腰帶給一個成年旅客最適合的迷人行李箱。', '<li class=\"c-li-lineh\">4輪靜音輪SilentCaster(R)70，音量減少50％</li><li class=\"c-li-lineh\">前袋設計</li>', 'proteca_gentry_72L_black_detail_1.jpg;;proteca_gentry_72L_black_detail_2.jpg;;proteca_gentry_72L_black_detail_3.jpg;;proteca_gentry_72L_black_detail_4.jpg'),
(74, 'PROTECA', 'Feena ST', '8130', '拉鍊', '軟箱', '尼龍', 'W55 H35 D25 cm', '2 kg', '29 L', 'Y', '4輪', 'N', 'Y', '三年', '這是一款能夠讓行動變得輕鬆的行李箱。採用Ace獨立開發的無面板設計，具備輪胎煞車功能同時實現了1.8 kg的驚人輕盈。 身體重量減輕，重量輕，功能性美，以易用性為最優先。 從航空旅行到購物，隨時隨地與您同行。 如果使用Feena ST，每一天的旅程都能享受輕鬆自由。', '<li class=\"c-li-lineh\">煞車功能，開關輕鬆固定</li><li class=\"c-li-lineh\">採用新型輕質結構Air Rim II </li><li class=\"c-li-lineh\">前口袋方便置入護照</li>', 'proteca_feenast_29L_blue_detail_1.jpg;;proteca_feenast_29L_blue_detail_2.jpg;;proteca_feenast_29L_blue_detail_3.jpg;;proteca_feenast_29L_blue_detail_4.jpg;;proteca_feenast_29L_blue_detail_5.jpg;;proteca_feenast_29L_blue_detail_6.jpg'),
(75, 'crumpler', 'Vis-`A-Vis Trunk Matte', '8900', '拉鍊', '硬箱', 'PC', 'W50 H78 D26 cm', '5 kg', '90 L', 'Y', '4輪', 'N', 'N', '一年', '採用啞光外殼，將Vis-`A-Vis系列帶到了另一個層次。堅固，輕便，耐用的外殼可保護您的裝備，同時加固的拉鍊和內置TSA認證的鎖具為您提供額外的安全性，讓您高枕無憂。創新的衣物壓縮系統可以保持您的衣服平整，而超平滑的滑動輪提供最舒適的行動感受。適合那些經常旅行並喜歡時尚旅行的人士的完美行李箱。', '<li class=\"c-li-lineh\">啞光外殼 </li><li class=\"c-li-lineh\">創新的衣物壓縮系統</li>', 'AAA0076detaii1.jpg;;AAA0076detaii2.jpg'),
(76, 'crumpler', 'Vis-`A-Vis Trunk Matte', '7900', '拉鍊', '硬箱', 'PC', 'W37 H55 D22cm', '3.3 kg', '47 L', 'Y', '4輪', 'N', 'N', '一年', '採用啞光外殼，將Vis-`A-Vis系列帶到了另一個層次。堅固，輕便，耐用的外殼可保護您的裝備，同時加固的拉鍊和內置TSA認證的鎖具為您提供額外的安全性，讓您高枕無憂。創新的衣物壓縮系統可以保持您的衣服平整，而超平滑的滑動輪提供最舒適的行動感受。適合那些經常旅行並喜歡時尚旅行的人士的完美行李箱。', '<li class=\"c-li-lineh\">啞光外殼 </li><li class=\"c-li-lineh\">創新的衣物壓縮系統</li>', 'AAA0076detaii1.jpg;;AAA0076detaii2.jpg'),
(77, 'calpak', 'jen-atkin', '8530', '鋁框', '硬箱', 'PC', 'W78 H40 D36cm', '5.6 kg', '111 L', 'Y', '4輪', 'N', 'N', '一年', '高級豪華的功能：Hinomoto 360度靜音運行旋轉輪，鋁製框架和一鍵式TSA海關安全組合鎖，讓您順利，安全地旅行。別忘了用透明的大衣防塵罩和附帶的貼紙製作你自己的！', '<li class=\"c-li-lineh\">360度靜音旋轉輪</li>', 'AAA078-24-4-1-1.jpg;;AAA078-24-4-1-2.jpg;;AAA078-24-4-1-3.jpg;;AAA078-24-4-1-4.jpg;;proteca_feenast_29L_blue_detail_5.jpg'),
(78, 'calpak', 'jen-atkin', '8350', '鋁框', '硬箱', 'PC', 'W62 H43 D28 cm', '4.7 kg', '72 L', 'Y', '4輪', 'N', 'N', '一年', '高級豪華的功能：Hinomoto 360度靜音運行旋轉輪，鋁製框架和一鍵式TSA海關安全組合鎖，讓您順利，安全地旅行。別忘了用透明的大衣防塵罩和附帶的貼紙製作你自己的！', '<li class=\"c-li-lineh\">360度靜音旋轉輪</li>', 'AAA078-24-4-1-1.jpg;;AAA078-24-4-1-2.jpg;;AAA078-24-4-1-3.jpg;;AAA078-24-4-1-4.jpg;;proteca_feenast_29L_blue_detail_5.jpg'),
(79, 'calpak', 'jen-atkin', '7850', '鋁框', '硬箱', 'PC', 'W56 H35 D25 cm', '4 kg', '42 L', 'Y', '4輪', 'N', 'N', '一年', '高級豪華的功能：Hinomoto 360度靜音運行旋轉輪，鋁製框架和一鍵式TSA海關安全組合鎖，讓您順利，安全地旅行。別忘了用透明的大衣防塵罩和附帶的貼紙製作你自己的！', '<li class=\"c-li-lineh\">360度靜音旋轉輪</li>', 'AAA078-24-4-1-1.jpg;;AAA078-24-4-1-2.jpg;;AAA078-24-4-1-3.jpg;;AAA078-24-4-1-4.jpg;;proteca_feenast_29L_blue_detail_5.jpg'),
(80, 'calpak', 'medora-L', '6673', '拉鍊', '硬箱', 'PC+ABS', 'W78 H40 D36 cm', '5.1 kg', '111 L', 'Y', '4輪', 'Y', 'N', '一年', '閃亮迷人的Medora Luggage Collection，帶著它絕對讓您在任何人群中脫穎而出。 Medora擁有無可挑剔的內部設計和閃亮的表面，是最漂亮的行李箱，也讓旅行變得輕而易舉。', '<li class=\"c-li-lineh\">360內部拉鍊分隔袋、拉鍊網眼袋和兩個彈性口袋</li>', ''),
(81, 'calpak', 'medora-M', '6052', '拉鍊', '硬箱', 'PC+ABS', 'W64 H45 D28 cm', '4.1 kg', '80 L', 'Y', '4輪', 'Y', 'N', '一年', '閃亮迷人的Medora Luggage Collection，帶著它絕對讓您在任何人群中脫穎而出。 Medora擁有無可挑剔的內部設計和閃亮的表面，是最漂亮的行李箱，也讓旅行變得輕而易舉。', '<li class=\"c-li-lineh\">360內部拉鍊分隔袋、拉鍊網眼袋和兩個彈性口袋</li>', ''),
(82, 'calpak', 'medora-carryon', '5276', '拉鍊', '硬箱', 'PC+ABS', 'W54 H39 D36 cm', '3.2 kg', '45 L', 'Y', '4輪', 'Y', 'N', '一年', '閃亮迷人的Medora Luggage Collection，帶著它絕對讓您在任何人群中脫穎而出。 Medora擁有無可挑剔的內部設計和閃亮的表面，是最漂亮的行李箱，也讓旅行變得輕而易舉。', '<li class=\"c-li-lineh\">360內部拉鍊分隔袋、拉鍊網眼袋和兩個彈性口袋</li>', ''),
(83, 'Antler', 'Atmosphere L', '7849', '拉鍊', '軟箱', '尼龍', 'W82 H48 D32 cm', '2.7 kg', '138 L', 'Y', '4輪', 'Y', 'N', '十年', 'Antler最新的超輕系列，最大尺寸僅重2.7公斤。 Atmosphere提供經典設計與輕便手提箱的完美結合，箱體可擴充及內含衣架環。', '<li class=\"c-li-lineh\">可擴充及好拿取的前袋設計</li><li class=\"c-li-lineh\">內部隔間有衣架環</li><li class=\"c-li-lineh\">配有拆卸化妝品袋、筆記型電腦袋</li><li class=\"c-li-lineh\">優化的護腳保護和新型踢板</li>', 'AAA084-19-3-1-1.jpg;;AAA084-19-3-1-2.jpg;;AAA084-19-3-1-3.jpg;;AAA084-19-3-1-4.jpg'),
(84, 'Antler', 'Atmosphere M', '7205', '拉鍊', '軟箱', '尼龍', 'W70 H44 D28 cm', '2.3 kg', '94 L', 'Y', '4輪', 'Y', 'N', '十年', 'Antler最新的超輕系列，最大尺寸僅重2.7公斤。 Atmosphere提供經典設計與輕便手提箱的完美結合，箱體可擴充及內含衣架環。', '<li class=\"c-li-lineh\">可擴充及好拿取的前袋設計</li><li class=\"c-li-lineh\">內部隔間有衣架環</li><li class=\"c-li-lineh\">配有拆卸化妝品袋、筆記型電腦袋</li><li class=\"c-li-lineh\">優化的護腳保護和新型踢板</li>', 'AAA084-19-3-1-1.jpg;;AAA084-19-3-1-2.jpg;;AAA084-19-3-1-3.jpg;;AAA084-19-3-1-4.jpg'),
(85, 'Antler', 'Atmosphere carryon', '6400', '拉鍊', '軟箱', '尼龍', 'W55 H40 D20 cm', '1.9 kg', '44 L', 'Y', '4輪', 'Y', 'N', '十年', 'Antler最新的超輕系列，最大尺寸僅重2.7公斤。 Atmosphere提供經典設計與輕便手提箱的完美結合，箱體可擴充及內含衣架環。', '<li class=\"c-li-lineh\">可擴充及好拿取的前袋設計</li><li class=\"c-li-lineh\">內部隔間有衣架環</li><li class=\"c-li-lineh\">配有拆卸化妝品袋、筆記型電腦袋</li><li class=\"c-li-lineh\">優化的護腳保護和新型踢板</li>', 'AAA084-19-3-1-1.jpg;;AAA084-19-3-1-2.jpg;;AAA084-19-3-1-3.jpg;;AAA084-19-3-1-4.jpg'),
(86, 'Antler', 'Business Trolley Wardrobe', '5434', '拉鍊', '軟箱', '聚酯纖維', 'W58 H61 D23 cm', '3.8 kg', '56 L', 'N', '2輪', 'N', 'N', '十年', '經典的專業商務造型，具有智能的現代風格。', '<li class=\"c-li-lineh\">前袋方便拿取物品與四個衣架</li><li class=\"c-li-lineh\">多個內部口袋和隔層</li><li class=\"c-li-lineh\">襯墊提手和背帶</li>', 'AAA087-19-1-1-1.jpg;;AAA087-19-1-1-2.jpg;;AAA087-19-1-1-3.jpg;;AAA087-19-1-1-4.jpg'),
(87, 'incase', 'ProConnected', '8900', '拉鍊', '硬箱', '聚酯纖維+PC', 'W56 H36 D23 cm', '3.4 kg', '32 L', 'Y', '4輪', 'N', 'N', '終身', '這款隨身攜帶的智能行李箱經過TSA認證，具有足夠的備用電源（20,100 mAh / 74.37 Wh），可通過USB-C供電為MacBook充電，並包含兩個額外的USB-A端口。當需要登機時，使用無需工具的快速釋放裝置將電池彈出。', '<li class=\"c-li-lineh\">可拆卸式滾動輪</li><li class=\"c-li-lineh\">內置電池並且可迅速拆除，方便登機時操作</li><li class=\"c-li-lineh\">兩個USB-A充電端口</li>', 'AAA088-1-1-1-1.jpg;;AAA088-1-1-1-2.jpg;;AAA088-1-1-1-3.jpg;;AAA088-1-1-1-4.jpg'),
(88, 'incase', 'Novi', '7136', '拉鍊', '硬箱', 'PC', 'W76 H45 D30 cm', '5 kg', '100 L', 'Y', '4輪', 'N', 'N', '終身', '與Novi一起旅行，這是硬殼滾筒的未來。由A級馬克隆聚碳酸酯外殼，聚酯提花內襯和可拆卸，光滑的滑動輪構成，專為性能而設計。它有一個寬敞的主隔間和一個TSA認證鎖，並配有內部網袋，包括洗衣袋和保護尼龍覆蓋。', '<li class=\"c-li-lineh\"> TPE輪/TPU手柄</li><li class=\"c-li-lineh\">內部網袋</li>', 'AAA088-1-1-1-1.jpg;;AAA088-1-1-1-2.jpg;;AAA088-1-1-1-3.jpg;;AAA088-1-1-1-4.jpg'),
(97, 'Briggs & Riley', 'baseline, U128CX', '9710', '拉鍊', '軟箱', '尼龍', 'W69 H51 D24 cm', '4.9 kg', '106 L', 'Y', '2輪', 'Y', 'N', '終身', '當需要最大的包裝空間時，這款行李箱是您最佳選擇。可收納4-7天的衣服和必需品。使用我們的CX技術，可將您的包裝容量提高26％。只需將外殼兩側拉起，然後將外殼壓縮至原始尺寸即可。', '<li class=\"c-li-lineh\">內置掛衣環與隔間</li><li class=\"c-li-lineh\">前置上開式儲物區</li>', ''),
(99, 'RICARDO', 'MALIBU BAY 2.0', '8690', '拉鍊', '軟箱', '聚酯纖維', 'W60 H45 D28 cm', '2.9 kg', '78 L', 'N', '4輪', 'N', 'N', '一年', '來自Malibu Bay 2.0的中型行李箱 - 我們最輕便的行李箱系列非常適合中長途旅行以及任何喜歡打包和檢查行李的人。這款多功能行李箱外殼採用耐用的dreamfel(R)面料外觀，以及適合您配件的大量口袋（洗漱用品，充電器，旅行證件，筆記本電腦等）。這款多功能旅行箱採用休閒而精緻的造型和人造皮革裝飾，是各種旅行的絕佳選擇。\r\n', '<li class=\"c-li-lineh\">內含大量收納分隔</li><li class=\"c-li-lineh\">人造皮革裝飾</li>', '100-detial-01.jpg;;100-detial-02.jpg'),
(100, 'RICARDO', 'SAUSALITO', '9100', '拉鍊', '軟箱', '聚酯纖維', 'W60 H45 D27 cm', '4.2 kg', '56 L', 'N', '4輪', 'N', 'N', '一年', '來自Malibu Bay 2.0的中型行李箱 - 我們最輕便的行李箱系列非常適合中長途旅行以及任何喜歡打包和檢查行李的人。這款多功能行李箱外殼採用耐用的dreamfel(R)面料外觀，以及適合您配件的大量口袋（洗漱用品，充電器，旅行證件，筆記本電腦等）。這款多功能旅行箱採用休閒而精緻的造型和人造皮革裝飾，是各種旅行的絕佳選擇。\r\n', '<li class=\"c-li-lineh\">內含大量收納分隔</li><li class=\"c-li-lineh\">人造皮革裝飾</li>', '100-detial-01.jpg;;100-detial-02.jpg'),
(101, 'RICARDO', 'SAN CLEMENTE 2.0', '8931', '拉鍊', '硬箱', 'PC', 'W64 H45 D29 cm', '3.9 kg', '90 L', 'Y', '4輪', 'Y', 'N', '一年', '這款可擴展的行李箱為您提供更多的包裝容量，由100％聚碳酸酯製成。配有符合人體工程學的伸縮式手柄和嵌入式輪子，可幫助您輕鬆拖拉，還具有TSA海關安全鎖，增加了安全性。在內部，寬敞的二級隔間提供了一個更加簡單的衣櫃系統，可以將衣服從衣櫃轉移到衣櫃。這款中型硬殼行李箱是長達五天（或更長時間，具體取決於季節和包裝習慣）旅行的理想選擇。', '<li class=\"c-li-lineh\">內置掛衣環</li>', '104-detial-01.jpg;;104-detial-02.jpg;;104-detial-03.jpg;;104-detial-04.jpg'),
(102, 'RICARDO', 'MONTEREY 2.0', '8690', '拉鍊', '硬箱', 'PC', 'W60 H45 D28 cm', '4.1 kg', '84 L', 'N', '4輪', 'N', 'N', '一年', '這款中型托運行李箱非常適合中長途旅行，或者如果您喜歡可裝載更多容量的行李箱。這款行李箱提供多樣的分隔，並配有一個用於繫上小行李箱的附加包帶，以及一個寬敞的內部空間和一個獨立的分層包裝區域，方便輕鬆地從主隔間外部拿取物品。', '<li class=\"c-li-lineh\">前置分層區域方便拿取物品</li>', '109-detial-01.jpg;;109-detial-02.jpg;;109-detial-03.jpg;;109-detial-04.jpg'),
(103, 'RICARDO', 'SAN MARCOS', '8900', '拉鍊', '軟箱', '尼龍', 'W60 H45 D28 cm', '4.1 kg', '84 L', 'Y', '4輪', 'Y', 'N', '一年', '這款中型托運行李箱非常適合中長途旅行，或者如果您喜歡可裝載更多容量的行李箱。這款行李箱提供多樣的分隔，並配有一個用於繫上小行李箱的附加包帶，以及一個寬敞的內部空間和一個獨立的分層包裝區域，方便輕鬆地從主隔間外部拿取物品。', '<li class=\"c-li-lineh\">內置掛衣環</li><li class=\"c-li-lineh\">另有可拆取式TSA小袋</li>', '110-detial-01.jpg;;110-detial-02.jpg'),
(104, 'RICARDO', 'SAN MARCOS', '8920', '拉鍊', '軟箱', '尼龍', 'W60 H45 D28 cm', '4.1 kg', '84 L', 'Y', '4輪', 'Y', 'N', '一年', '這款時尚的旋轉行李箱是San Marcos行李箱系列的一部分，讓您享受每次旅行。配備符合TSA海關安全，它還有一個方便的地方，卡入式TSA的小袋，可以取下放至隨身攜帶的行李箱中。配有多個儲物空間，而擴展功能使您旅途中有超出預想的物品時都能輕鬆裝下。', '<li class=\"c-li-lineh\">內置掛衣環</li><li class=\"c-li-lineh\">另有可拆取式TSA小袋</li>', '110-detial-01.jpg;;110-detial-02.jpg'),
(106, 'RICARDO', 'CUPERTINO', '8650', '拉鍊', '硬箱', 'PC合金', 'W60 H45 D28', '4.1 kg', '56L', 'Y', '4輪', 'N', 'N', '一年', 'Cupertino硬殼行李箱系列具有TSA海關安全鎖和專有的聚碳酸酯合金保護您的旅行物品。這款行李箱非常適合中長途旅行以及喜歡打包的旅客。它寬敞的內部空間和許多功能，能完美保護您的旅行包裝物品。對於那些想要耐用的硬殼行李箱以及喜歡在旅途中輕鬆處理行李收納的人來說，這是最佳的選擇。', '<li class=\"c-li-lineh\">適合中長途旅行</li>', '114-detial-01.jpg;;114-detial-02.jpg;;114-detial-03.jpg;;114-detial-04.jpg'),
(107, 'RICARDO', 'SAN CLEMENTE', '8811', '拉鍊', '硬箱', 'PC合金', 'W60 H45 D28 cm', '4.2 kg', '84 L', 'Y', '4輪', 'N', 'N', '一年', 'Cupertino硬殼行李箱系列具有TSA海關安全鎖和專有的聚碳酸酯合金保護您的旅行物品。這款行李箱非常適合中長途旅行以及喜歡打包的旅客。它寬敞的內部空間和許多功能，能完美保護您的旅行包裝物品。對於那些想要耐用的硬殼行李箱以及喜歡在旅途中輕鬆處理行李收納的人來說，這是最佳的選擇。', '<li class=\"c-li-lineh\">適合中長途旅行</li>', '116-detial-01.jpg;;116-detial-02.jpg'),
(108, 'RICARDO', 'YOSEMITE', '8069', '拉鍊', '硬箱', 'PC', 'W60 H45 D28 cm', '3.8 kg', '84 L', 'Y', '4輪', 'Y', 'N', '一年', '堅固與耐用是Yosemite系列靈感的基石。聚碳酸酯的堅硬外殼為您的行李物品提供加強保護。符合TSA海關安全鎖可確保旅行期間的安全。寬敞的內部空間和可擴充拉鍊意味著您可以在緊湊的空間內打包。', '', '117-detial-01.jpg;;117-detial-02.jpg'),
(109, 'RICARDO', 'ARRIS', '8710', '拉鍊', '硬箱', 'PC', 'W60 H45 D28 cm', '4.3 kg', '84 L', 'Y', '4輪', 'Y', 'N', '一年', '憑藉其流線型的外形和雅緻的細節，這款優雅的隨身式旋轉行李箱營造出微妙而時尚的風格。作為Arris系列的一部分，光滑的外殼採用超耐用的模克隆(R)聚碳酸酯製成，可承受很大的壓力。側面採用堅固卻又輕巧的材質，平滑的車輪和可伸縮的手柄，可輕鬆操控，亦可擴充，以及配備TSA海關安全鎖，可保護您的貴重物品。它非常適合商務旅行或週末度假，擁有智能存儲功能，可隨時隨地保持井井有條。', '<li class=\"c-li-lineh\">流線型外型</li>', '118-detial-01.jpg;;118-detial-02.jpg;;118-detial-03.jpg;;118-detial-04.jpg;;118-detial-05.jpg;;118-detial-06.jpg'),
(110, 'RICARDO', 'AILERON', '9480', '拉鍊', '硬箱', '飛機級鋁外殼', 'W60 H45 D28 cm', '6.3 kg', '84 L', 'Y', '4輪', 'N', 'N', '一年', '這款中型行李箱以奢華為設計理念，為自信的旅行品味師提供無與倫比的安全性和收納性。由飛機級鋁製成，並加有防護角護板，光滑的外殼可以保護您的物品免受行走的壓力，而可伸縮的手柄和頂級的旋轉輪可以幫助您輕鬆駕馭您的旅程。主隔層襯有奢華的金屬編織紡織品，並配有搭扣繫帶和分區分隔，讓您的旅行衣櫃井井有條。我們的邊框配有兩個TravelSentry(R)認證鎖，可確保您的個人物品安全。', '<li class=\"c-li-lineh\">防撞角護板</li><li class=\"c-li-lineh\">兩個TravelSentry(R)認證鎖</li>', ''),
(111, 'RICARDO', 'SPECTRUM', '8690', '拉鍊', '硬箱', 'PC', 'W60 H45 D28 cm', '3.9 kg', '84 L', 'Y', '4輪', 'N', 'N', '一年', '無論您的旅行在哪裡，您都會喜歡Spectrum系列中這款時尚的中型箱。耐衝擊的聚碳酸酯外殼可以保護內容物。卓越的機動性採用可伸縮，符合人體工程學的手推車手柄和平滑滑動旋轉輪的形式。可擴充的主要包裝空間具有方便的收納功能，如帶拉鍊環的拉鍊隔層和防皺條，可讓您的衣服保持最佳狀態。', '<li class=\"c-li-lineh\">符合人體工學手柄</li><li class=\"c-li-lineh\">拉鍊環的拉鍊隔層和防皺條</li>', '122-detial-01.jpg;;122-detial-02.jpg'),
(112, 'RICARDO', 'MAR VISTA', '8690', '拉鍊', '硬箱', 'PC', 'W60 H45 D28 cm', '4 kg', '84 L', 'N', '4輪', 'N', 'N', '一年', '無論您的旅行在哪裡，您都會喜歡Spectrum系列中這款時尚的中型箱。耐衝擊的聚碳酸酯外殼可以保護內容物。卓越的機動性採用可伸縮，符合人體工程學的手推車手柄和平滑滑動旋轉輪的形式。可擴充的主要包裝空間具有方便的收納功能，如帶拉鍊環的拉鍊隔層和防皺條，可讓您的衣服保持最佳狀態。', '<li class=\"c-li-lineh\">符合人體工學手柄</li><li class=\"c-li-lineh\">拉鍊環的拉鍊隔層和防皺條</li>', '124-detial-01.jpg;;124-detial-02.jpg');

-- --------------------------------------------------------

--
-- 資料表結構 `product_list`
--

CREATE TABLE `product_list` (
  `sid` int(11) NOT NULL,
  `type_sid` int(11) NOT NULL,
  `color_sid` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `size_text` varchar(5) NOT NULL,
  `uniq_name` varchar(25) NOT NULL COMMENT '款式sid 顏色sid 尺寸',
  `pic_nu` varchar(20) NOT NULL,
  `imgs` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `product_list`
--

INSERT INTO `product_list` (`sid`, `type_sid`, `color_sid`, `size`, `size_text`, `uniq_name`, `pic_nu`, `imgs`) VALUES
(1, 1, 1, 20, '20吋', '1-1-20', 'AAA0001_01.jpg', 'AAA0001_01.jpg;;AAA0001_02.jpg;;AAA0001_03.jpg;;AAA0001_04.jpg'),
(2, 2, 2, 22, '22吋', '2-2-22', 'AAA0002_01.jpg', 'AAA0002_01.jpg;;AAA0002_02.jpg;;AAA0002_03.jpg;;AAA0002_04.jpg'),
(3, 3, 3, 27, '27吋', '3-3-27', 'AAA0003_01.jpg', 'AAA0003_01.jpg;;AAA0003_02.jpg;;AAA0003_03.jpg'),
(4, 4, 2, 28, '28吋', '4-2-28', 'AAA0004_01.jpg', 'AAA0004_01.jpg;;AAA0004_02.jpg;;AAA0004_03.jpg'),
(5, 5, 1, 20, '20吋', '5-1-20', 'AAA0005_01.jpg', 'AAA0005_01.jpg;;AAA0005_02.jpg;;AAA0005_03.jpg;;AAA0005_04.jpg'),
(6, 6, 1, 22, '22吋', '6-1-22', 'AAA0006_01.jpg', 'AAA0006_01.jpg;;AAA0006_02.jpg;;AAA0006_03.jpg;;AAA0006_04.jpg;;AAA0006_05.jpg'),
(7, 7, 1, 19, '19吋', '7-1-19', 'AAA0007_01.jpg', 'AAA0007_01.jpg;;AAA0007_02.jpg;;AAA0007_03.jpg;;AAA0007_04.jpg'),
(8, 8, 4, 21, '21吋', '8-4-21', 'AAA0008-02_01.jpg', 'AAA0008-02_01.jpg;;AAA0008-02_02.jpg;;AAA0008-02_03.jpg;;AAA0008-02_04.jpg;;AAA0008-02_05.jpg;;AAA0008-02_06.jpg'),
(9, 8, 1, 21, '21吋', '8-1-21', 'AAA0008-01_01.jpg', 'AAA0008-01_01.jpg;;AAA0008-01_02.jpg;;AAA0008-01_03.jpg;;AAA0008-01_04.jpg;;AAA0008-01_05.jpg;;AAA0008-01_06.jpg;;AAA0008-01_07.jpg'),
(10, 9, 4, 24, '24吋', '9-4-24', 'AAA0011_01.jpg', 'AAA0011_01.jpg;;AAA0011_02.jpg;;AAA0011_03.jpg;;AAA0011_04.jpg;;AAA0011_05.jpg;;AAA0011_06.jpg;;AAA0011_07.jpg'),
(11, 9, 1, 24, '24吋', '9-1-24', 'AAA0010_01.jpg', 'AAA0010_01.jpg;;AAA0010_02.jpg;;AAA0010_03.jpg;;AAA0010_04.jpg;;AAA0010_05.jpg;;AAA0010_06.jpg;;AAA0010_07.jpg'),
(12, 10, 4, 26, '26吋', '10-4-26', 'AAA0013_01.jpg', 'AAA0013_01.jpg;;AAA0013_02.jpg;;AAA0013_03.jpg;;AAA0013_04.jpg;;AAA0013_05.jpg;;AAA0013_06.jpg;;AAA0013_07.jpg'),
(13, 10, 1, 26, '26吋', '10-1-26', 'AAA0012_01.jpg', 'AAA0012_01.jpg;;AAA0012_02.jpg;;AAA0012_03.jpg;;AAA0012_04.jpg;;AAA0012_05.jpg;;AAA0012_06.jpg;;AAA0012_07.jpg'),
(14, 11, 4, 27, '27吋', '11-4-27', 'AAA0015_01.jpg', 'AAA0015_01.jpg;;AAA0015_02.jpg;;AAA0015_03.jpg;;AAA0015_04.jpg;;AAA0015_05.jpg;;AAA0015_06.jpg;;AAA0015_07.jpg'),
(15, 11, 1, 27, '27吋', '11-1-27', 'AAA0014_01.jpg', 'AAA0014_01.jpg;;AAA0014_02.jpg;;AAA0014_03.jpg;;AAA0014_04.jpg;;AAA0014_05.jpg;;AAA0014_06.jpg;;AAA0014_07.jpg'),
(16, 12, 4, 28, '28吋', '12-4-28', 'AAA0015_01.jpg', 'AAA0015_01.jpg;;AAA0015_02.jpg;;AAA0015_03.jpg;;AAA0015_04.jpg;;AAA0015_05.jpg;;AAA0015_06.jpg;;AAA0015_07.jpg'),
(17, 12, 1, 28, '28吋', '12-1-28', 'AAA0016_01.jpg', 'AAA0016_01.jpg;;AAA0016_02.jpg;;AAA0016_03.jpg;;AAA0016_04.jpg;;AAA0016_05.jpg;;AAA0016_06.jpg;;AAA0016_07.jpg'),
(18, 13, 5, 21, '21吋', '13-5-21', 'AAA1013_01.jpg', 'AAA1013_01.jpg;;AAA1013_02.jpg'),
(19, 14, 5, 26, '26吋', '14-5-26', 'AAA1014_01.jpg', 'AAA1014_01.jpg;;AAA1014_02.jpg'),
(20, 15, 5, 28, '28吋', '15-5-28', 'AAA1016_01.jpg', 'AAA1016_01.jpg;;AAA1016_02.jpg'),
(21, 16, 4, 21, '21吋', '16-4-21', 'AAA1015-01_01.jpg', 'AAA1015-01_01.jpg;;AAA1015-01_02.jpg;;AAA1015-01_03.jpg;;AAA1015-01_04.jpg;;AAA1015-01_05.jpg'),
(22, 17, 4, 21, '21吋', '17-4-21', 'AAA0017-01_01.jpg', 'AAA0017-01_01.jpg;;AAA0017-01_02.jpg;;AAA0017-01_03.jpg;;AAA0017-01_04.jpg;;AAA0017-01_05.jpg'),
(23, 17, 5, 21, '21吋', '17-5-21', 'AAA0017-02_01.jpg', 'AAA0017-02_01.jpg;;AAA0017-02_02.jpg'),
(24, 18, 1, 21, '21吋', '18-1-21', 'AAA0018_01.jpg', 'AAA0018_01.jpg;;AAA0018_02.jpg'),
(25, 19, 1, 21, '21吋', '19-1-21', 'AAA0019_01.jpg', 'AAA0019_01.jpg;;AAA0019_02.jpg;;AAA0019_03.jpg;;AAA0019_04.jpg;;AAA0019_05.jpg;;AAA0019_06.jpg;;AAA0019_07.jpg'),
(26, 20, 1, 22, '22吋', '20-1-22', 'AAA0020_01.jpg', 'AAA0020_01.jpg;;AAA0020_02.jpg;;AAA0020_03.jpg;;AAA0020_04.jpg;;AAA0020_05.jpg;;AAA0020_06.jpg;;AAA0020_07.jpg'),
(27, 21, 1, 23, '23吋', '21-1-23', 'AAA0021_01.jpg', 'AAA0001_01.jpg;;AAA0001_02.jpg;;AAA0001_03.jpg;;AAA0001_04.jpg'),
(28, 22, 1, 21, '21吋', '22-1-21', 'AAA0022_01.jpg', 'AAA0022_01.jpg;;AAA0022_02.jpg;;AAA0022_03.jpg;;AAA0022_04.jpg;;AAA0022_05.jpg;;AAA0022_06.jpg'),
(29, 23, 1, 22, '22吋', '23-1-22', 'AAA0023_01.jpg', 'AAA0023_01.jpg;;AAA0023_02.jpg'),
(30, 24, 1, 26, '26吋', '24-1-26', 'AAA0024_01.jpg', 'AAA0024_01.jpg;;AAA0024_02.jpg'),
(31, 25, 1, 27, '27吋', '25-1-27', 'AAA0025_01.jpg', 'AAA0025_01.jpg;;AAA0025_02.jpg;;AAA0025_03.jpg;;AAA0025_04.jpg;;AAA0025_05.jpg;;AAA0025_06.jpg;;AAA0025_07.jpg'),
(32, 26, 6, 20, '20吋', '26-6-20', 'AAA0026_01.jpg', 'AAA0026_01.jpg;;AAA0026_02.jpg;;AAA0026_03.jpg;;AAA0026_04.jpg;;AAA0026_05.jpg;;AAA0026_06.jpg'),
(33, 27, 1, 20, '20吋', '27-1-20', 'AAA0027-01_01.jpg', 'AAA0027-01_01.jpg;;AAA0027-01_02.jpg;;AAA0027-01_03.jpg;;AAA0027-01_04.jpg;;AAA0027-01_05.jpg;;AAA0027-01_06.jpg'),
(34, 27, 6, 20, '20吋', '27-6-20', 'AAA0027-02_01.jpg', 'AAA0027-02_01.jpg;;AAA0027-02_02.jpg;;AAA0027-02_03.jpg;;AAA0027-02_04.jpg'),
(35, 28, 1, 26, '26吋', '28-1-26', 'AAA0028-01_01.jpg', 'AAA0028-01_01.jpg;;AAA0028-01_02.jpg;;AAA0028-01_03.jpg;;AAA0028-01_04.jpg;;AAA0028-01_05.jpg;;AAA0028-01_06.jpg'),
(36, 29, 1, 20, '20吋', '29-1-20', 'AAA0029-01_01.jpg', 'AAA0029-01_01.jpg;;AAA0029-01_02.jpg;;AAA0029-01_03.jpg;;AAA0029-01_04.jpg;;AAA0029-01_05.jpg'),
(37, 29, 7, 20, '20吋', '29-7-20', 'AAA0029-02_01.jpg', 'AAA0029-02_01.jpg;;AAA0029-02_02.jpg;;AAA0029-02_03.jpg;;AAA0029-02_04.jpg;;AAA0029-02_05.jpg'),
(38, 30, 1, 25, '25吋', '30-1-25', 'AAA0030_01.jpg', 'AAA0030_01.jpg;;AAA0030_02.jpg;;AAA0030_03.jpg;;AAA0030_04.jpg;;AAA0030_05.jpg'),
(39, 31, 2, 22, '22吋', '31-2-22', 'AAA0031-01_01.jpg', 'AAA0031-01_01.jpg;;AAA0031-01_02.jpg;;AAA0031-01_03.jpg'),
(40, 31, 5, 22, '22吋', '31-5-22', 'AAA0031-02_01.jpg', 'AAA0031-02_01.jpg;;AAA0031-02_02.jpg;;AAA0032-01_03.jpg;;AAA0032-01_04.jpg'),
(41, 32, 9, 19, '19吋', '32-9-19', 'AAA0032_01.jpg', 'AAA0032_01.jpg;;AAA0032_02.jpg'),
(42, 33, 9, 19, '19吋', '33-9-19', 'AAA0033_01.jpg', 'AAA0033_01.jpg;;AAA0033_02.jpg;;AAA0033_03.jpg'),
(43, 34, 1, 20, '20吋', '34-1-20', 'AAA0034_01.jpg', 'AAA0034_01.jpg;;AAA0034_02.jpg;;AAA0034_03.jpg;;AAA0034_04.jpg'),
(44, 35, 4, 19, '19吋', '35-4-19', 'AAA0035_01.jpg', 'AAA0035_01.jpg;;AAA0035_02.jpg;;AAA0035_03.jpg;;AAA0035_04.jpg'),
(45, 36, 4, 24, '24吋', '36-4-24', 'AAA0036-02_01.jpg', 'AAA0036-02_01.jpg;;AAA0036-02_02.jpg;;AAA0036-02_03.jpg'),
(46, 36, 1, 24, '24吋', '36-1-24', 'AAA0036-01_01.jpg', 'AAA0036-01_01.jpg;;AAA0036-01_02.jpg;;AAA0036-01_03.jpg;;AAA0036-01_04.jpg'),
(47, 37, 4, 27, '27吋', '37-4-27', 'AAA0037-01_01.jpg', 'AAA0037-01_01.jpg;;AAA0037-01_02.jpg;;AAA0037-01_03.jpg;;AAA0037-01_04.jpg'),
(48, 37, 7, 27, '27吋', '37-7-27', 'AAA0037-02_01.jpg', 'AAA0037-02_01.jpg;;AAA0037-02_02.jpg;;AAA0037-02_03.jpg;;AAA0037-02_04.jpg'),
(49, 38, 5, 25, '25吋', '38-5-25', 'AAA0038_01.jpg', 'AAA0038_01.jpg;;AAA0038_02.jpg;;AAA0038_03.jpg;;AAA0038_04.jpg'),
(50, 39, 1, 20, '20吋', '39-1-20', 'AAA0039-01_01.jpg', 'AAA0039-01_01.jpg;;AAA0039-01_02.jpg;;AAA0039-01_03.jpg;;AAA0039-01_04.jpg;;AAA0039-01_05.jpg'),
(51, 39, 8, 20, '20吋', '39-8-20', 'AAA0039-02_01.jpg', 'AAA0039-02_01.jpg;;AAA0039-02_02.jpg;;AAA0039-02_03.jpg;;AAA0039-02_04.jpg;;AAA0039-02_05.jpg'),
(52, 40, 1, 22, '22吋', '40-1-22', 'AAA0040-01_01.jpg', 'AAA0040-01_01.jpg;;AAA0040-01_02.jpg;;AAA0040-01_03.jpg;;AAA0040-01_04.jpg;;AAA0040-01_05.jpg'),
(53, 40, 8, 22, '22吋', '40-8-22', 'AAA0040-02_01.jpg', 'AAA0040-02_01.jpg;;AAA0040-02_02.jpg;;AAA0040-02_03.jpg;;AAA0040-02_04.jpg;;AAA0040-02_05.jpg'),
(54, 41, 1, 17, '17吋', '41-1-17', 'AAA0041-01_01.jpg', 'AAA0041-01_01.jpg;;AAA0041-01_02.jpg;;AAA0041-01_03.jpg;;AAA0041-01_04.jpg'),
(55, 41, 8, 17, '17吋', '41-8-17', 'AAA0041-02_01.jpg', 'AAA0041-02_01.jpg;;AAA0041-02_02.jpg;;AAA0041-02_03.jpg;;AAA0041-02_04.jpg'),
(56, 42, 3, 17, '17吋', '42-3-17', 'AAA0042_01.jpg', 'AAA0042_01.jpg;;AAA0042_02.jpg;;AAA0042_03.jpg;;AAA0042_04.jpg'),
(57, 43, 3, 21, '21吋', '43-3-21', 'AAA0043_01.jpg', 'AAA0043_01.jpg;;AAA0043_02.jpg;;AAA0043_03.jpg;;AAA0043_04.jpg'),
(58, 44, 3, 26, '26吋', '44-3-26', 'AAA0044-01_01.jpg', 'AAA0044-01_01.jpg;;AAA0044-01_02.jpg;;AAA0044-01_03.jpg;;AAA0044-01_04.jpg'),
(59, 44, 9, 26, '26吋', '44-9-26', 'AAA0044-02_01.jpg', 'AAA0044-02_01.jpg;;AAA0044-02_02.jpg;;AAA0044-02_03.jpg;;AAA0044-02_04.jpg'),
(60, 45, 3, 30, '30吋', '45-3-30', 'AAA0045-01_01.jpg', 'AAA0045-01_01.jpg;;AAA0045-01_02.jpg;;AAA0045-01_03.jpg'),
(61, 45, 9, 30, '30吋', '45-9-30', 'AAA0045-02_01.jpg', 'AAA0045-02_01.jpg;;AAA0045-02_02.jpg;;AAA0045-02_03.jpg'),
(62, 46, 1, 21, '21吋', '46-1-21', 'AAA0046-01_01.jpg', 'AAA0046-01_01.jpg;;AAA0046-01_02.jpg;;AAA0046-01_03.jpg'),
(63, 46, 3, 21, '21吋', '46-3-21', 'AAA0046-02_01.jpg', 'AAA0046-02_01.jpg;;AAA0046-02_02.jpg;;AAA0046-02_03.jpg'),
(64, 51, 9, 28, '28吋', '51-9-28', 'AAA0051-01_01.jpg', 'AAA0051-01_01.jpg'),
(65, 51, 10, 28, '28吋', '51-10-28', 'AAA0051-02_01.jpg', 'AAA0051-02_01.jpg'),
(66, 52, 2, 26, '26吋', '52-2-26', 'AAA0053-01_01.jpg', 'AAA0053-01_01.jpg'),
(67, 52, 7, 26, '26吋', '52-7-26', 'AAA0053-02_01.jpg', 'AAA0053-02_01.jpg'),
(68, 53, 12, 19, '19吋', '53-12-19', 'AAA0054-01_01.jpg', 'AAA0054-01_01.jpg'),
(69, 53, 11, 19, '19吋', '53-11-19', 'AAA0054-02_01.jpg', 'AAA0054-02_01.jpg'),
(70, 54, 12, 24, '24吋', '54-12-24', 'AAA0055-01_01.jpg', 'AAA0055-01_01.jpg'),
(71, 54, 10, 24, '24吋', '54-10-24', 'AAA0055-02_01.jpg', 'AAA0055-02_01.jpg'),
(72, 55, 12, 27, '27吋', '55-12-27', 'AAA0056-01_01.jpg', 'AAA0056-01_01.jpg'),
(73, 55, 10, 27, '27吋', '55-10-27', 'AAA0056-02_01.jpg', 'AAA0056-02_01.jpg'),
(74, 56, 12, 29, '29吋', '56-12-29', 'AAA0057-01_01.jpg', 'AAA0057-01_01.jpg'),
(75, 56, 10, 29, '29吋', '56-10-29', 'AAA0057-02_01.jpg', 'AAA0057-02_01.jpg'),
(76, 57, 1, 20, '20吋', '57-1-20', 'AAA0058-01_01.jpg', 'AAA0058-01_01.jpg'),
(77, 57, 2, 20, '20吋', '57-2-20', 'AAA0058-02_01.jpg', 'AAA0058-02_01.jpg'),
(78, 57, 6, 20, '20吋', '57-6-20', 'AAA0058-03_01.jpg', 'AAA0058-03_01.jpg'),
(79, 58, 1, 26, '26吋', '58-1-26', 'AAA0059-01_01.jpg', 'AAA0059-01_01.jpg'),
(80, 58, 2, 26, '26吋', '58-2-26', 'AAA0059-02_01.jpg', 'AAA0059-02_01.jpg'),
(81, 58, 6, 26, '26吋', '58-6-26', 'AAA0059-03_01.jpg', 'AAA0059-03_01.jpg'),
(82, 59, 1, 28, '28吋', '59-1-28', 'AAA0060-01_01.jpg', 'AAA0060-01_01.jpg'),
(83, 59, 2, 28, '28吋', '59-2-28', 'AAA0060-02_01.jpg', 'AAA0060-02_01.jpg'),
(84, 59, 6, 28, '28吋', '59-6-28', 'AAA0060-03_01.jpg', 'AAA0060-03_01.jpg'),
(85, 60, 1, 21, '21吋', '60-1-21', 'AAA0061-01_01.jpg', 'AAA0061-01_01.jpg;;AAA0061-01_02.jpg;;AAA0061-01_03.jpg;;AAA0061-01_04.jpg;;AAA0061-01_05.jpg;;AAA0061-01_06.jpg'),
(86, 60, 2, 21, '21吋', '60-2-21', 'AAA0061-02_01.jpg', 'AAA0061-02_01.jpg;;AAA0061-02_02.jpg;;AAA0061-02_03.jpg;;AAA0061-02_04.jpg;;AAA0061-02_05.jpg'),
(87, 60, 5, 21, '21吋', '60-5-21', 'AAA0061-03_01.jpg', 'AAA0061-03_01.jpg;;AAA0061-03_02.jpg;;AAA0061-03_03.jpg;;AAA0061-03_04.jpg;;AAA0061-03_05.jpg;;AAA0061-03_06.jpg'),
(88, 61, 4, 21, '22吋', '61-4-21', 'AAA0062-02_01.jpg', 'AAA0062-02_01.jpg;;AAA0062-02_02.jpg;;AAA0062-02_03.jpg;;AAA0062-02_04.jpg;;AAA0062-02_05.jpg'),
(89, 61, 1, 21, '22吋', '61-1-21', 'AAA0062-01_01.jpg', 'AAA0062-01_01.jpg;;AAA0062-01_02.jpg;;AAA0062-01_03.jpg;;AAA0062-01_04.jpg;;AAA0062-01_05.jpg'),
(90, 62, 1, 22, '22吋', '62-1-22', 'AAA0063-01_01.jpg', 'AAA0063-01_01.jpg;;AAA0063-01_02.jpg;;AAA0063-01_03.jpg;;AAA0063-01_04.jpg'),
(91, 62, 2, 22, '22吋', '62-2-22', 'AAA0063-02_01.jpg', 'AAA0063-02_01.jpg;;AAA0063-02_02.jpg;;AAA0063-02_03.jpg;;AAA0063-02_04.jpg'),
(92, 63, 1, 20, '20吋', '63-1-20', 'AAA0064-01_01.jpg', 'AAA0064-01_01.jpg'),
(93, 63, 2, 20, '20吋', '63-2-20', 'AAA0064-02_01.jpg', 'AAA0064-02_01.jpg'),
(94, 63, 5, 20, '20吋', '63-5-20', 'AAA0064-03_01.jpg', 'AAA0064-03_01.jpg'),
(95, 64, 1, 26, '26吋', '64-1-26', 'AAA0065-01_01.jpg', 'AAA0065-01_01.jpg'),
(96, 64, 2, 26, '26吋', '64-2-26', 'AAA0065-01_01.jpg', 'AAA0065-01_01.jpg'),
(97, 64, 5, 26, '26吋', '64-5-26', 'AAA0065-03_01.jpg', 'AAA0065-03_01.jpg'),
(98, 65, 1, 26, '26吋', '65-1-26', 'AAA0066-01_01.jpg', 'AAA0066-01_01.jpg'),
(99, 65, 2, 26, '26吋', '65-2-26', 'AAA0066-02_01.jpg', 'AAA0066-02_01.jpg'),
(100, 65, 5, 26, '26吋', '65-5-26', 'AAA0066-03_01.jpg', 'AAA0066-03_01.jpg'),
(101, 66, 1, 30, '30吋', '66-1-30', 'AAA0067-01_01.jpg', 'AAA0067-01_01.jpg'),
(102, 66, 2, 30, '30吋', '66-2-30', 'AAA0067-02_01.jpg', 'AAA0067-02_01.jpg'),
(103, 66, 5, 30, '30吋', '66-5-30', 'AAA0067-03_01.jpg', 'AAA0067-03_01.jpg'),
(104, 67, 1, 20, '20吋', '67-1-20', 'AAA0068-01_01.jpg', 'AAA0068-01_01.jpg'),
(105, 67, 2, 20, '20吋', '67-2-20', 'AAA0068-02_01.jpg', 'AAA0068-02_01.jpg'),
(106, 67, 14, 20, '20吋', '67-14-20', 'AAA0068-03_01.jpg', 'AAA0068-03_01.jpg'),
(107, 68, 1, 24, '24吋', '68-1-24', 'AAA0069-01_01.jpg', 'AAA0069-01_01.jpg'),
(108, 68, 2, 24, '24吋', '68-2-24', 'AAA0069-02_01.jpg', 'AAA0069-02_01.jpg'),
(109, 68, 14, 24, '24吋', '68-14-24', 'AAA0069-03_01.jpg', 'AAA0069-03_01.jpg'),
(110, 69, 1, 26, '26吋', '69-1-26', 'AAA0070-01_01.jpg', 'AAA0070-01_01.jpg'),
(111, 69, 2, 26, '26吋', '69-2-26', 'AAA0070-02_01.jpg', 'AAA0070-02_01.jpg'),
(112, 69, 14, 26, '26吋', '69-14-26', 'AAA0070-03_01.jpg', 'AAA0070-03_01.jpg'),
(113, 70, 1, 30, '30吋', '70-1-30', 'AAA0071-01_01.jpg', 'AAA0071-01_01.jpg'),
(114, 70, 2, 30, '30吋', '70-2-30', 'AAA0071-02_01.jpg', 'AAA0071-02_01.jpg'),
(115, 70, 14, 30, '30吋', '70-14-30', 'AAA0071-03_01.jpg', 'AAA0071-03_01.jpg'),
(116, 71, 1, 20, '20吋', '71-1-20', 'AAA0072-01_01.jpg', 'AAA0072-01_01.jpg'),
(117, 71, 3, 20, '20吋', '71-3-20', 'AAA0072-02_01.jpg', 'AAA0072-02_01.jpg'),
(118, 72, 1, 24, '24吋', '72-1-24', 'AAA0073-01_01.jpg', 'AAA0073-01_01.jpg'),
(119, 72, 3, 24, '24吋', '72-3-24', 'AAA0073-02_01.jpg', 'AAA0073-02_01.jpg'),
(120, 73, 1, 28, '28吋', '73-1-28', 'AAA0074-01_01.jpg', 'AAA0074-01_01.jpg'),
(121, 73, 3, 28, '28吋', '73-3-28', 'AAA0074-02_01.jpg', 'AAA0074-02_01.jpg'),
(122, 74, 1, 20, '20吋', '74-1-20', 'AAA0075-01_01.jpg', 'AAA0075-01_01.jpg'),
(123, 74, 2, 20, '20吋', '74-2-20', 'AAA0075-02_01.jpg', 'AAA0075-02_01.jpg'),
(124, 75, 1, 25, '25吋', '75-1-25', 'AAA0076-01_01.jpg', 'AAA0076-01_01.jpg;;AAA0076-01_02.jpg;;AAA0076-01_03.jpg;;AAA0076-01_04.jpg'),
(125, 75, 2, 25, '25吋', '75-2-25', 'AAA0076-02_01.jpg', 'AAA0076-02_01.jpg;;AAA0076-02_02.jpg;;AAA0076-02_03.jpg;;AAA0076-02_04.jpg'),
(126, 76, 4, 22, '22吋', '76-4-22', 'AAA0077-03_01.jpg', 'AAA0077-03_01.jpg;;AAA0077-03_02.jpg;;AAA0077-03_03.jpg;;AAA0077-03_04.jpg'),
(127, 76, 1, 22, '22吋', '76-1-22', 'AAA0077-01_01.jpg', 'AAA0077-01_01.jpg;;AAA0077-01_02.jpg;;AAA0077-01_03.jpg;;AAA0077-01_04.jpg'),
(128, 76, 2, 22, '22吋', '76-2-22', 'AAA0077-02_01.jpg', 'AAA0077-02_01.jpg;;AAA0077-02_02.jpg;;AAA0077-02_03.jpg;;AAA0077-02_04.jpg'),
(129, 77, 1, 30, '30吋', '77-1-30', 'AAA0078_01.jpg', 'AAA0078_01.jpg;;AAA0078_02.jpg'),
(130, 78, 1, 25, '25吋', '78-1-25', 'AAA0079_01.jpg', 'AAA0079_01.jpg;;AAA0079_02.jpg'),
(131, 79, 1, 22, '22吋', '79-1-22', 'AAA0080_01.jpg', 'AAA0080_01.jpg;;AAA0080_02.jpg'),
(132, 80, 5, 30, '30吋', '80-5-30', 'AAA0081_01.jpg', 'AAA0081_01.jpg;;AAA0081_02.jpg;;AAA0081_03.jpg;;AAA0081_04.jpg;;AAA0081_05.jpg'),
(133, 81, 5, 26, '26吋', '81-5-26', 'AAA0082_01.jpg', 'AAA0082_01.jpg;;AAA0082_02.jpg;;AAA0082_03.jpg;;AAA0082_04.jpg;;AAA0082_05.jpg;;AAA0082_06.jpg'),
(134, 82, 5, 20, '20吋', '82-5-20', 'AAA0083_01.jpg', 'AAA0083_01.jpg;;AAA0083_02.jpg;;AAA0083_03.jpg;;AAA0083_04.jpg;;AAA0083_05.jpg'),
(135, 83, 1, 28, '28吋', '83-1-28', 'AAA0084-01_01.jpg', 'AAA0084-01_01.jpg;;AAA0084-01_02.jpg;;AAA0084-01_03.jpg;;AAA0084-01_04.jpg'),
(136, 83, 7, 28, '28吋', '83-7-28', 'AAA0084-02_01.jpg', 'AAA0084-02_01.jpg;;AAA0084-02_02.jpg;;AAA0084-02_03.jpg;;AAA0084-02_04.jpg'),
(137, 84, 1, 26, '26吋', '84-1-26', 'AAA0086-01_01.jpg', 'AAA0086-01_01.jpg;;AAA0086-01_02.jpg;;AAA0086-01_03.jpg;;AAA0086-01_04.jpg'),
(138, 84, 7, 26, '26吋', '84-7-26', 'AAA0086-02_01.jpg', 'AAA0086-02_01.jpg;;AAA0086-02_02.jpg;;AAA0086-02_03.jpg;;AAA0086-02_04.jpg'),
(139, 85, 1, 20, '20吋', '85-1-20', 'AAA0086-01_01.jpg', 'AAA0086-01_01.jpg;;AAA0086-01_02.jpg;;AAA0086-01_03.jpg;;AAA0086-01_04.jpg'),
(140, 85, 7, 20, '20吋', '85-7-20', 'AAA0086-02_01.jpg', 'AAA0086-02_01.jpg;;AAA0086-02_02.jpg;;AAA0086-02_03.jpg;;AAA0086-02_04.jpg'),
(141, 86, 1, 22, '22吋', '86-1-22', 'AAA0087_01.jpg', 'AAA0087_01.jpg;;AAA0087_02.jpg;;AAA0087_03.jpg'),
(142, 87, 1, 20, '20吋', '87-1-20', 'AAA0088_01.jpg', 'AAA0088_01.jpg;;AAA0088_02.jpg;;AAA0088_03.jpg;;AAA0088_04.jpg;;AAA0088_05.jpg'),
(143, 88, 1, 30, '30吋', '88-1-30', 'AAA0089_01.jpg', 'AAA0089_01.jpg;;AAA0089_02.jpg;;AAA0089_03.jpg;;AAA0089_04.jpg;;AAA0089_05.jpg'),
(144, 97, 1, 27, '27吋', '97-1-27', 'AAA0098-01_01.jpg', 'AAA0098-01_01.jpg;;AAA0098-01_02.jpg;;AAA0098-01_03.jpg'),
(145, 97, 6, 27, '27吋', '97-6-27', 'AAA0098-02_01.jpg', 'AAA0098-02_01.jpg;;AAA0098-02_02.jpg;;AAA0098-02_03.jpg'),
(146, 99, 2, 25, '25吋', '99-2-25', 'AAA0100-01_01.jpg', 'AAA0100-01_01.jpg;;AAA0100-01_02.jpg;;AAA0100-01_03.jpg;;AAA0100-01_04.jpg'),
(147, 99, 5, 25, '25吋', '99-5-25', 'AAA0100-02_01.jpg', 'AAA0100-02_01.jpg;;AAA0100-02_02.jpg;;AAA0100-02_03.jpg;;AAA0100-02_04.jpg'),
(148, 100, 2, 25, '25吋', '100-2-25', 'AAA0101_01.jpg', 'AAA0101_01.jpg;;AAA0101_02.jpg;;AAA0101_03.jpg'),
(149, 101, 2, 26, '26吋', '101-2-26', 'AAA0103-02_01.jpg', 'AAA0103-02_01.jpg;;AAA0103-02_02.jpg;;AAA0103-02_03.jpg;;AAA0103-02_04.jpg'),
(150, 102, 2, 25, '25吋', '102-2-25', 'AAA0108-01_01.jpg', 'AAA0108-01_01.jpg;;AAA0108-01_02.jpg;;AAA0108-01_03.jpg;;AAA0108-01_04.jpg'),
(151, 102, 3, 25, '25吋', '102-3-25', 'AAA0108-02_01.jpg', 'AAA0108-02_01.jpg;;AAA0108-02_02.jpg;;AAA0108-02_03.jpg;;AAA0108-02_04.jpg'),
(152, 103, 5, 25, '25吋', '103-5-25', 'AAA0110-01_01.jpg', 'AAA0110-01_01.jpg;;AAA0110-01_02.jpg;;AAA0110-01_03.jpg'),
(153, 104, 13, 25, '25吋', '104-13-25', 'AAA0110-02_01.jpg', 'AAA0110-02_01.jpg;;AAA0110-02_02.jpg;;AAA0110-02_03.jpg;;AAA0110-02_04.jpg'),
(154, 106, 1, 25, '25吋', '106-1-25', '115-product-01.jpg', '115-product-01.jpg;;115-product-02.jpg;;115-product-03.jpg;;115-product-04.jpg;;115-product-05.jpg'),
(155, 106, 2, 25, '25吋', '106-2-25', '114-product-01.jpg', '114-product-01.jpg;;114-product-02.jpg;;114-product-03.jpg;;114-product-04.jpg;;114-product-05.jpg'),
(156, 107, 5, 26, '26吋', '107-5-26', '116-product-01.jpg', '116-product-01.jpg;;116-product-02.jpg;;116-product-03.jpg'),
(157, 108, 13, 25, '25吋', '108-13-25', '117-product-01.jpg', '117-product-01.jpg;;117-product-02.jpg;;117-product-03.jpg'),
(158, 109, 1, 24, '24吋', '109-1-24', '118-productl-01.jpg', '118-productl-01.jpg;;118-productl-02.jpg'),
(159, 109, 2, 24, '24吋', '109-2-24', '119-productl-01.jpg', '119-productl-01.jpg;;119-productl-02.jpg'),
(160, 110, 2, 24, '24吋', '110-2-24', '120-product-01.jpg', '120-product-01.jpg;;120-product-02.jpg;;120-product-03.jpg'),
(161, 110, 5, 24, '24吋', '110-5-24', '121-product-01.jpg', '121-product-01.jpg;;121-product-02.jpg;;121-product-03.jpg'),
(162, 111, 4, 24, '24吋', '111-4-24', '123-productl-01.jpg', '123-productl-01.jpg;;123-productl-02.jpg;;123-productl-03.jpg'),
(163, 111, 1, 24, '24吋', '111-1-24', '122-productl-01.jpg', '122-productl-01.jpg;;122-productl-02.jpg;;122-productl-03.jpg'),
(164, 112, 1, 29, '29吋', '112-1-29', '124-product-01.jpg', '124-product-01.jpg;;124-product-02.jpg;;124-product-03.jpg');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `color_mapping`
--
ALTER TABLE `color_mapping`
  ADD PRIMARY KEY (`color_sid`);

--
-- 資料表索引 `lunggage_data`
--
ALTER TABLE `lunggage_data`
  ADD PRIMARY KEY (`SID`);

--
-- 資料表索引 `product_list`
--
ALTER TABLE `product_list`
  ADD PRIMARY KEY (`sid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `color_mapping`
--
ALTER TABLE `color_mapping`
  MODIFY `color_sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- 使用資料表 AUTO_INCREMENT `product_list`
--
ALTER TABLE `product_list`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
