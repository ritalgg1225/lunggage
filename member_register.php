<?php
require __DIR__. '/__connect_db.php';
$page_name = 'register';
$page_title = '註冊';

//如果已經是登入狀態就跳轉首頁
if (isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}

?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="stylesheet" href="css/member-layout.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <title>會員中心 - 註冊</title>
    <style>
        /* -----------------------------------------------------Register RWD */
        @media screen and (max-width:1240px){
            .register-form{
                width: 70%;
            }
        }
        @media screen and (max-width:1024px){
            .member-wrap{
                max-width: 80vw;
            }
            .register-form{
                width: 80%;
            }

        }
        @media screen and (max-width:768px){
            .member-wrap{
                max-width: 90vw;
            }
            .register-sub{
                transform: rotate(0deg);
                top: -45px;
                left: 0;
                font-size: 45px;
                max-width: 90vw;
            }
            .member-scroll{
                width: 100%;
            }
            .member-row{
                height: auto;
            }

        }
        @media screen and (max-width:520px){
            .register-nav{
                margin-top: 130px;
            }
            .register-sub{
                font-size: 35px;
                top: -40px;
            }
            .register-form{
                width: 88%;
            }

        }
        @media screen and (max-width:400px){
            
            .register-sub{
                font-size: 30px;
                top: -35px;
            }

        }

        /*--------------------------info通知-----*/

        .info-wrap-bg{
            width: 100%;
            height: 100vh;
            background-color: #000000;
            position: fixed;
            z-index: 97;
            top: 0;
            left: 0;
            opacity: 0.7;
        }
        .info-alert{
            padding: 20px;
            color: #ffffff;
            background-color: #c67b8a;
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            z-index: 98;
            font-size: 1.2rem;
            letter-spacing: 2px;
            box-shadow: 1px 3px 10px #414449;
            transition: 0.5s;
            min-width: 450px;
            min-height: 180px;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width:630px){
            .info-alert{
                min-width: 80vw;
                min-height: 80vw;
            }

        }


    </style>
</head>

<body>
<?php include __DIR__ . '/__navbar.php'?>
<div id="info-wrap" class="d-none">
    <div class="info-wrap-bg"></div>
    <div id="info" class="info-alert d-flex"></div>
</div>
    <section>
        <div class="register-bg"></div>
        <div class="con-1440 member-wrap">
            <div class="ff-marko register-sub">
                MEMBER REGISTER
            </div>
            <div class="register-nav">
                <p class="register-nav-sub">請填寫會員註冊資料</p>
            </div>
            <div class="member-row">
                <h2 class="fw-300">會員註冊</h2>
<!--                <div class="member-scroll">-->
                    <div class="mb-1-2 register-form">
                        <form action="" method="post" name="registerform" onsubmit="return formCheckReg()">
                            <div class="p-b-40">
                                <label for="email" class="fw-500">會 員 信 箱<span style="color:#9e3c3c">*</span></label>
                                <input type="text" class="input-style" name="email" id="emailNew" autocomplete="off">
                                <small id="emailHelp"></small><br>
                                <span class="span-note"> <i> 會員信箱即是會員帳號，請仔細確認後填寫</i></span>
                            </div>
                            <div class="p-b-40">
                                <label for="password" class="fw-500">設 定 密 碼<span style="color:#9e3c3c">*</span></label>
                                <input type="password" class="input-style" name="password" id="passwordNew">
                                <small id="passwordHelp"></small><br>
                            </div>
                            <div class="p-b-40">
                                <label for="passwordCheck" class="fw-500">密 碼 確 認<span style="color:#9e3c3c">*</span></label>
                                <input type="password" class="input-style" name="passwordCheck" id="passwordCheckNew">
                                <small id="passwordCheckHelp"></small><br>
                            </div>
                            <div class="p-b-40">
                                <label for="name" class="fw-500">會 員 姓 名<span style="color:#9e3c3c">*</span></label>
                                <input type="text" class="input-style" name="name" id="nameNew">
                                <small id="nameHelp"></small><br>
                                <span class="span-note"> <i> 會員姓名即為預設的收件人姓名</i></span>
                            </div>
                            <div class="p-b-40">
                                <label for="mobile" class="fw-500">手 機 號 碼<span style="color:#9e3c3c">*</span></label>
                                <input type="text" class="input-style" name="mobile" id="mobileNew">
                                <small id="mobileHelp"></small><br>
                            </div>
                            <div class="p-b-40">
                                <label for="address" class="fw-500">收 件 地 址</label>
                                <input type="text" class="input-style" name="address" id="addressNew">
                                <small id="addressHelp"></small><br>
                            </div>
                            <div class="p-b-40">
                                <label for="birthday" class="fw-500">生 日</label><br>
                                <input type="date" class="input-style" name="birthday" id="birthdayNew" value="1900-01-01">
                            </div>
                            <div class="text-center m-b-50">
                                <button type="submit" class="btn-submit">註 冊</button>
                            </div>
                        </form>
<!--                    </div>-->
                </div>
            </div>
        </div>
        </div>
    </section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


    <script>
        var fields = ['email', 'password', 'passwordCheck', 'name', 'mobile', 'address'];
        var s;
        var info = $('#info');
        var infoWrap = $('#info-wrap');
        var emailHelp = $('#emailHelp');
        var email = $('#email');


        function formCheckReg() {
            var isPass = true;
            var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var mobile_pattern = /^09\d{2}\-?\d{3}\-?\d{3}$/;

            for (s in fields) {
                cancelAlert(fields[s]);
            }


            if (document.registerform.name.value.length < 2) {
                setAlert('name', '請輸入正確姓名');
                isPass = false;
            }

            if (!email_pattern.test(document.registerform.email.value)) {
                setAlert('email', 'email格式錯誤');
                isPass = false;
            }

            if (document.registerform.password.value.length < 6) {
                setAlert('password', '密碼欄位需6字元以上');
                isPass = false;
            }

            if (document.registerform.passwordCheck.value != document.registerform.password.value) {
                setAlert('passwordCheck', '密碼不相符');
                isPass = false;
            }
            if (!mobile_pattern.test(document.registerform.mobile.value)) {
                setAlert('mobile', '手機號碼格式或長度錯誤');
                isPass = false;
            }

            if(isPass){
                $.post('member_register_api.php', $(document.registerform).serialize(), function (data) {


                    if (data.success){
                        setTimeout(function(){
                            location.href = './';
                        }, 1000);
                    }else {
                        emailHelp.html(data.info);
                        email.css('border-bottom', '1px solid #9e3c3c');
                    }

                    if (data.info){
                        infoWrap.removeClass('d-none');
                        info.html(data.info);

                        setTimeout(function(){
                            infoWrap.addClass('d-none');
                        }, 1500);
                    }

                },'json');
            }

            return false;
        }


        // 輸入錯誤通知
        function setAlert(fieldName, message) {
            $('#' + fieldName).css('border-bottom', '1px solid #9e3c3c');
            $('#' + fieldName + 'Help').text(message);
        }

        // 取消輸入錯誤通知
        function cancelAlert(fieldName) {
            $('#' + fieldName).css('border-bottom', '1px solid #858a8f');
            $('#' + fieldName + 'Help').text('');
        }

    </script>

<?php include __DIR__ . '/__html__footer.php'?>