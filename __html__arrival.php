<?php 
//2019/02/15 14:50 Rita更新
//複製 Rita product_list 推薦用 
//右邊六個商品
$n_sql ="SELECT p.*, c.`size_text`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` WHERE c.`type_sid`>85 LIMIT 6";
$n_stmt = $pdo->query($n_sql);
$arrival = $n_stmt->fetchAll(PDO::FETCH_ASSOC);
//左邊一個大商品
$f_sql ="SELECT p.*, c.`size_text`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` WHERE c.`type_sid`=77 LIMIT 1";
$f_stmt = $pdo->query($f_sql);
//手機板四個商品
$m_sql ="SELECT p.*, c.`size_text`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` WHERE c.`type_sid`>85 LIMIT 2";
$m_stmt = $pdo->query($m_sql);
$mobile = $m_stmt->fetchAll(PDO::FETCH_ASSOC);
//暫時使用 html結構問題
$m2_sql ="SELECT p.*, c.`size_text`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` WHERE c.`type_sid`>87 LIMIT 2";
$m2_stmt = $pdo->query($m2_sql);
$mobile2 = $m2_stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<div class="arrival">
    <ul class="title flex">
        <li class="arrival_li ff-merri-wi p_50px_wii">NEW ARRIVAL</li>      
    </ul>

<!------ 2019/2/16 Rita更新 start------->
<!-- PC_RWD -->  
    <div class="img flex">
        <?php while( $f = $f_stmt->fetch(PDO::FETCH_ASSOC)  ):?>
            <div class="img_1">
                <a href="./product.php?sid=<?= $f['SID'] ?>">
                    <img src="./images/product/<?= $f['pic_nu']?>" alt="">
                    <p><?= $f['brand']?> <?= $f['type']?> <?= $f['size_text']?></p>
                </a>
            </div>
        <?php endwhile;?>
   
        <ul class="img_2 flex">
            <?php foreach($arrival as $v): ?>    
                <li>
                    <a href="./product.php?sid=<?= $v['SID'] ?>">
                        <img src="./images/product/<?= $v['pic_nu']?>" alt="">
                        <p><?= $v['brand']?> <?= $v['type']?> <?= $v['size_text']?></p><!-- 品牌 类型 尺寸 -->
                    </a>
                </li>   
            <?php endforeach; ?>
        </ul>
<!------ 2019/2/16 Rita更新 end------->

<!------ 2019/2/16 Rita更新 start------->
<!-- Mobile_RWD -->       
        <div>
            <ul class="img_rwd flex">
                <?php foreach($mobile as $m): ?>
                    <li>
                        <a href="./product.php?sid=<?= $m['SID'] ?>">
                            <img src="./images/product/<?= $m['pic_nu']?>" alt="">
                            <p><?= $m['brand']?> <?= $m['type']?> <?= $m['size_text']?></p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="img_rwd flex">
                <?php foreach($mobile2 as $m2): ?>
                    <li>
                        <a href="./product.php?sid=<?= $m2['SID'] ?>">
                            <img src="./images/product/<?= $m2['pic_nu']?>" alt="">
                            <p><?= $m2['brand']?> <?= $m2['type']?> <?= $m2['size_text']?></p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
<!------ 2019/2/16 Rita更新 end------->

    </div>
</div>
