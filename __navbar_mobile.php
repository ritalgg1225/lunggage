<nav class="nav_mobile">
    <div class="nav_container relative d-flex align-item-center mairgin-0auto">
        <div class="nav_left relative">
            <img src="./images/logo_190_black.svg" alt="">
        </div>
        <div class="flex-right d-flex">
            <div class="nav_mid relative">
                <ul class="nonstyle-ul d-flex justify-center align-item-center p-0 m-0">
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="">最新消息</a></li>
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="">挑選秘訣</a></li>
                    <div class="product_bar d-flex align-item-center">
                        <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color menu_product_R" href="">商品選購</a></li>
                    </div>
                    <li><a class="nonstyle-a nav_p-x-25 border-right maintext-color" href="">保固維修</a></li>
                    <div class="member_bar d-flex align-item-center">
                        <li><a class="nonstyle-a nav_p-x-25 menu_member_R maintext-color" href="">會員中心</a></li>
                    </div>
                </ul>
            </div>
            <div class="nav_mid_mobile d-none">

            </div>
            <div class="nav_right relative d-flex">
                <ul class="nonstyle-ul d-flex p-0 m-0">
                    <li class="nav_right_icon"><a class="nonstyle-a" href=""><img src="./images/icon_sm_search.svg"
                                                                                  alt=""></a></li>
                    <li class="nav_right_icon"><a class="nonstyle-a" href=""><img src="./images/icon_sm_login.svg"
                                                                                  alt=""></a></li>
                    <li class="nav_right_icon"><a class="nonstyle-a" href=""><img src="./images/icon_sm_shopbag.svg"
                                                                                  alt=""></a></li>
                    <li class="nav_right_icon"><a class="nonstyle-a" href=""><img src="./images/icon_sm_compare.svg"
                                                                                  alt=""></a></li>
                </ul>
            </div>
        </div>
        <div class="nav_menu d-none">
            <img src="./images/icon-menu-d.svg" alt="">
        </div>
    </div>
    <div class="product_menuframe_R d-flex absolute menu_bg justify-center d-none">
        <div class="product_menu_R d-flex relative">
            <div class="product_list_R relative">
                <ul class="nonstyle-ul product_new p-0 m-0">
                    <a class="nonstyle-a white-a" href="">
                        <li>最新商品</li>
                    </a>
                </ul>
            </div>
            <div class="product_list_R">
                <ul class="nonstyle-ul text-center p-0 m-0">
                    <li>品牌</li>
                    <li><a class="nonstyle-a white-a" href="">品牌1</a></li>
                    <li><a class="nonstyle-a white-a" href="">品牌2</a></li>
                    <li><a class="nonstyle-a white-a" href="">品牌3</a></li>
                    <li><a class="nonstyle-a white-a" href="">品牌4</a></li>
                    <li><a class="nonstyle-a white-a" href="">品牌5</a></li>
                    <li><a class="nonstyle-a white-a" href="">品牌6</a></li>
                </ul>
            </div>
            <div class="product_list_R">
                <ul class="nonstyle-ul text-center p-0 m-0">
                    <li>尺寸</li>
                    <li><a class="nonstyle-a white-a" href="">20吋</a></li>
                    <li><a class="nonstyle-a white-a" href="">22-24吋</a></li>
                    <li><a class="nonstyle-a white-a" href="">26-28吋</a></li>
                    <li><a class="nonstyle-a white-a" href="">30吋以上</a></li>
                </ul>
            </div>
            <div class="product_list_R">
                <ul class="nonstyle-ul text-center p-0 m-0">
                    <li>天數</li>
                    <li><a class="nonstyle-a white-a" href="">1-3日(約45L)</a></li>
                    <li><a class="nonstyle-a white-a" href="">2-4日(約55L)</a></li>
                    <li><a class="nonstyle-a white-a" href="">3-5日(約65L)</a></li>
                    <li><a class="nonstyle-a white-a" href="">5-7日(約90L)</a></li>
                    <li><a class="nonstyle-a white-a" href="">7日以上(約100L)</a></li>
                </ul>
            </div>
            <div class="product_list_R">
                <ul class="nonstyle-ul text-center p-0 m-0">
                    <li>類型</li>
                    <li><a class="nonstyle-a white-a" href="">硬箱</a></li>
                    <li><a class="nonstyle-a white-a" href="">軟箱</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="member_menuframe_R absolute menu_bg d-none justify-center">
        <div class="member_menu_R absolute transition">
            <div class="member_list_R relative">
                <ul class="nonstyle-ul d-flex p-0 m-0">
                    <li><a class="nonstyle-a white-a" href="">修改個人資料</a></li>
                    <li><a class="nonstyle-a white-a" href="">訂單資訊</a></li>
                    <li><a class="nonstyle-a white-a" href="">願望清單</a></li>
                    <li><a class="nonstyle-a white-a" href="">會員註冊</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
