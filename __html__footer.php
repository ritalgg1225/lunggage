<style>

    /* ----------------------------------footer*/

    footer {
        margin-top: 100px;
    }
    .mobile-menu{
        display: none;
    }

    /* ----------------------------------footer_1*/
    .footer_1 {
        width: 100%;
        background-color: rgba(215, 217, 219, 0.95);
    }

    .footer_1 ul {
        width: 75vw;
        margin: 0 auto;
        justify-content: center;
        list-style: none;
    }

    .footer_1 li {
        width: 15vw;
        text-align: center;
        padding: 20px 20px;
        list-style: none;
    }

    .footer_1 figure {
        width: 50%;
        margin: 0 auto;
    }

    .footer_1 img {
        width: 100%;
        object-fit: cover;
    }

    .footer_1 h6 {
        margin-bottom: 18px;
        font-size: 1.15rem;
        letter-spacing: 2px;
    }

    .footer_1 p {
        font-size: 1rem;
        letter-spacing: 2px;
        line-height: 1.5;
        font-weight: 300;
    }

    /* ----------------------------------footer_2*/
    .footer_bg {
        width: 100%;
        background: url("images/footer_bg_03.jpg") center center no-repeat;
        background-size: cover;
        overflow: hidden;
    }


    /*----footer左邊LOGO區域----*/
    .footer-left, .footer-center {
        margin-top: 65px;
    }

    .footer-left img {
        width: 100%;
    }

    .footer-left ul {
        width: 70%;
        margin: 0 auto;
    }

    .footer-left ul li {
        width: 100%;
        padding: 8px;
    }

    /*----footer中間選單區域----*/
    .footer-center {
        justify-content: space-evenly;
    }

    .footer-center ul {
        text-align: center;
    }

    .footer-center h6 {
        color: #ffffff;
        font-size: 1.25rem;
        font-weight: 400;
        margin-bottom: 10px;
        letter-spacing: 2px;
    }

    .footer-center a {
        text-decoration: none;
        display: block;
        color: #ffffff;
        padding: 10px 0;
        font-size: 1.1rem;
        font-weight: 200;
    }

    .footer-center a:hover {
        background: #3b668c;
        color: #ead46b;
        font-weight: 500;
        transition: 0.5s;
    }


    /*----footer右邊聯絡區域----*/
    .footer-right-bg {
        width: 200%;
        background: rgba(0, 0, 0, 0.687);
        padding: 15px 40px;
        color: #ffffff;
        margin: 40px 0;
    }

    .footer-right-bg hr {
        width: 40vw;
    }

    .footer-right-bg h4 {
        padding: 10px 0;
        font-weight: 500;
        font-size: 1.25rem;
        letter-spacing: 3px;
    }

    .footer-right-bg h3 {
        padding: 10px 0;
        font-weight: 500;
        font-size: 1.25rem;
        letter-spacing: 3px;
    }

    .footer-right-bg ul {
        margin-bottom: 8px;
    }

    .footer-right-bg li {
        padding: 5px 0;
        font-weight: 300;
        color: #c2cad8;
    }

    .footer-mail {
        width: 100%;
        padding: 10px 0;
    }

    .footer-mail > div {
        width: 100%;
        margin-top: 15px;
    }

    .footer-right-bg h4 label {
        color: #ffffff;
        font-size: 0.9rem;
        font-weight: 300;
        letter-spacing: 1px;
    }

    .footer-right-bg form input {
        color: #ffffff;
        font-family: 'Noto Sans TC', sans-serif;
        font-weight: 300;
        /*border: none;*/
        background: #5c5f5f;
        border-radius: 0;
        outline: none;
        padding: 8px 5px;
        width: 100%;
    }

    .footer-mail button {
        background: transparent;
        color: #cfb06d;
        font-family: 'Noto Sans TC', sans-serif;
        font-weight: 700;
        border: none;
        padding: 8px 15px;
        cursor: pointer;
    }
    .footer-mail button:hover {
        color: #ffdc8b;
    }


    /*----footer底部copy right----*/
    .footer_4 {
        text-align: center;
        padding-bottom: 20px;
    }

    .footer_4 p {
        color: #c2a883;
        font-size: 14px;
        margin: 0;
        padding: 20px 0 10px 0;
        border-top: #8d94a2 solid 1px;

    }

    /*-----------------------------------------RWD*/
    @media screen and (max-width: 1030px) {
        .footer_2 {
            max-width: 90vw;
        }
        .footer-right {
            display: none;
        }
        .footer-center {
            width: 70%;
        }

        .footer-left {
            width: 30%;
        }
    }
    @media screen and (max-width: 1000px) {

        .footer_1 p {
            display: none;
        }

        .footer_1 figure {
            width: 100%;
        }
    }

    @media screen and (max-width: 768px) {
        .footer_1 ul {
            width: 90vw;
            justify-content: space-between;
        }

        .footer_1 li {
            width: 20%;
        }
        .footer-center {
            width: 60%;
        }

        .footer-left {
            width: 40%;
        }

        .footer-center, .footer-left {
            margin: 30px 0;
        }
    }

    @media screen and (max-width: 670px) {


        .footer_2 {
            max-width: 100%;
            flex-direction: column;
        }
        .footer_1 li {
            width: 30%;
            padding: 20px 15px;
        }

        .footer-left {
            display: none;
        }

        .footer-center {
            width: 100%;
        }

        .footer-mobile-logo {
            width: 100%;
            background-color: rgba(255, 255, 255, 0.9);
            align-items: center;
            justify-content: space-between;
            padding: 10px 25px;
        }

        .footer-mobile-logo li {
            width: 80%;
            padding: 5px;
        }
        .mobile-hide {
            display: none;
        }
        .mobile-menu{
            display: block;
        }
        .footer-center {
            flex-direction: column;
            margin: 0;
        }
        .footer-center h6:after{
            content: ' + ';
        }
        .footer-center h6, .footer-center a {
            margin: 0;
            padding: 15px 0;
        }

        .footer-center li {
            border-bottom: 1px solid #dbdbdb;
            display: none;
        }

        .footer-center .footer-mobile-link-1 {
            background-color: #bbc5d1;
        }
        .footer-mobile-link-1 {
            overflow: hidden;

        }

        .footer-center .footer-mobile-link-2 {
            background-color: #96a1ad;
        }

        .footer-center .footer-mobile-link-3 {
            background-color: #5f6d79;
        }
    }


    @media screen and (max-width: 520px) {
        .footer-mobile-logo div, .footer-mobile-logo ul {
            width: 40%;
        }
        .footer_1 li{
            width: 33.3%;
        }
        .footer_1 h6{
            font-size: 1rem;
            font-weight: 500;
        }
        /*footer {*/
            /*margin-top: 50px;*/
        /*}*/
    }

    @media screen and (max-width: 400px) {

    }


</style>

<footer>
    <div class="footer_1">
        <ul class="d-flex">
            <li>
                <figure><img src="images/01.svg" alt=""></figure>
                <h6>物流配送</h6>
                <div><p>完成訂單流程將於3至5個工作日配送</p></div>
            </li>
            <li class="mobile-hide">
                <figure><img src="images/02.svg" alt=""></figure>
                <h6>全站免運</h6>
                <div><p>購買一件即免運條件，購物無額外負擔</p></div>
            </li>
            <li class="mobile-hide">
                <figure><img src="images/03.svg" alt=""></figure>
                <h6>官方代理</h6>
                <div><p>本站販賣之所有品牌皆取得官方授權，並非平行輸出</p></div>
            </li>
            <li>
                <figure><img src="images/04.svg" alt=""></figure>
                <h6>維修保固</h6>
                <div><p>經由本站購買並註冊的產品，除官方保固外，可另享一年延長保固期限</p></div>
            </li>
            <li>
                <figure><img src="images/05.svg" alt=""></figure>
                <h6>安全支付</h6>
                <div><p>每筆交易都使用SLL加密，以確保您的訂單安全，讓您能安心消費</p></div>
            </li>
        </ul>
    </div>

    <div class="footer_bg">
        <div class="con-1440 d-flex footer_2">
            <div class="footer-mobile-logo d-flex">
                <div class="mb-1-3">
                    <img src="images/logo_190_black.svg" alt="">
                </div>
                <ul class="d-flex mb-1-3">
                    <li><a href="">
                            <img src="images/icon_fb_2.svg" alt="">
                        </a></li>
                    <li><a href="">
                            <img src="images/icon_ig_2.svg" alt="">
                        </a></li>
                    <li><a href="">
                            <img src="images/icon_line_2.svg" alt="">
                        </a></li>
                </ul>
            </div>
            <div class="footer-left mb-1-4">
                <div>
                    <img src="images/logo_680x335_white.svg" alt="">
                </div>
                <ul class="d-flex">
                    <li><a href="">
                            <img src="images/icon_fb.svg" alt="">
                        </a></li>
                    <li><a href="">
                            <img src="images/icon_ig.svg" alt="">
                        </a></li>
                    <li><a href="">
                            <img src="images/icon_line.svg" alt="">
                        </a></li>
                </ul>
            </div>
            <div class="footer-center d-flex mb-1-2">
                <ul class="footer-mobile-link-1">
                    <h6 class="mobile-hide">會員系統</h6>
                    <h6 class="mobile-menu">會員系統</h6>
                    <li><a href="./member.php">我的帳戶</a></li>
                    <li><a href="./member_order.php">我的訂單</a></li>
                    <li><a href="./member_wishlist.php">願望清單</a></li>
                </ul>
                <ul class="footer-mobile-link-2">
                    <h6 class="mobile-hide">關於我們</h6>
                    <h6 class="mobile-menu">關於我們</h6>
                    <li><a href="./about.php">平台故事</a></li>
                    <!-- <li><a href="">網站使用條款</a></li> -->
                    <!-- <li><a href="">免責聲明</a></li> -->
                    <li><a href="./privacy.php">隱私權政策</a></li>
                    <li><a href="javascript:">網站地圖</a></li>
                </ul>
                <ul class="footer-mobile-link-3">
                    <h6 class="mobile-hide">Q&A</h6>
                    <h6 class="mobile-menu">Q&A</h6>
                    <li><a href="./pick.php">挑選秘訣</a></li>
                    <li><a href="./faq.php">常見問題</a></li>
                    <li><a href="./warranty.php">保固與維修</a></li>
                    <li><a href="./contact.php">退換貨</a></li>
                    <!-- <li><a href="">產品註冊</a></li> -->
                </ul>

            </div>
            <div class="footer-right mb-1-4">
                <div class="footer-right-bg">
                    <div class="mb-1-2">
                        <div>
                            <h3>聯絡資訊</h3>
                            <ul>
                                <li>Tel:+886-2-3313-6666</li>
                                <li>台北市信義區復興南路一段000巷</li>
                                <li>.container000@case-iii.com.tw</li>
                            </ul>
                        </div>
                        <hr>
                        <h4> 訂閱電子報 <label for="">享受最新優惠資訊</label> </h4>
                        <hr>
                        <form class="footer-mail">
                            <input type="text">
                            <button type="submit">送 出</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_4">
            <p>©2018 .Container company</p>
        </div>
</footer>


<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

<script>

    $('.mobile-menu').click(function () {
        $(this).nextAll().slideToggle(500);
    });


</script>

</body>

</html>