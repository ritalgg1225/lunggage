<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>挑選秘訣</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
    <!-- <script src="jq_scroll.js" type="text/javascript"></script> -->
    <!-- <script src="https://unpkg.com/scrollreveal"></script> -->
</head>
<?php require __DIR__. '/__connect_db.php'?>
<?php include __DIR__. '/__navbar.php' ?>
<style>
 @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
*{
    margin: 0;
    padding: 0;
}
body{
    color: #4d5258;
    font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif;
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
}
body{
    color: #4d5258;
    font-family: 'Marko One', serif;
    font-family: 'Mukta Malar', sans-serif;
    font-family: 'Noto Sans TC', sans-serif;
    font-size: 1rem;
    line-height: 1.7rem;
}
section{
    /* padding: 0; */
}
.object {
  opacity: 0;
    /* 加上後變成物件有往前的感覺 -1*/
  transform: translate(0, 20px); 
  transition: all 1.3s;
}
.visible { 
  opacity: 1;
 /* 加上後變成物件有往前的感覺 -2*/
  transform: translate(0, 0);
}
.t_bold{
    font-weight: 700;
}
.t_title{
    color: #fff;
    text-shadow: 4px 4px 15px#4d5258;
    z-index: 0;
}
.t_title:hover img{
    /* filter: saturate(150%); */
    filter: brightness(110%);
}
.t_title img{
    width: 100%;
    height: 20vh;
    /* filter: saturate(30%); */
    filter: grayscale(60%);
    object-fit: cover;
}
.t_title h2{
    font-size: 2rem;
    font-weight: 700;
    padding: 10px 0;
    letter-spacing: .5rem;
}
.t_title h4{
    font-size: 1.3rem;
    font-weight: 400;
    padding: 10px 0;
}
.t_content{
    width: 75vw;
    box-sizing: border-box;
    padding: 50px 20px;
    margin: 0 auto;
}
.flex{
    display:flex;
    flex-wrap: wrap;
}
.hide{
    display: none;
}
.relative{
    position: relative;
}
.absolute{
    position: absolute;
}
.t_box{
    /* border: 1px solid #4d5258; */
}
.circle{
    width: 17vh;
    height: 17vh;
    border-radius: 50%;
    /* background-color:#2c3e50; */
    margin: 0 auto;
}
.circle1{
    width: 10vh;
    height: 10vh;
    border-radius: 50%;
    /* background-color:#2c3e50; */
    margin: 0 auto;
    top: 50%;
    transform: translateY(-50%);
    opacity: 0;

}
.circle h2{
    color: #ffffff;
    font-weight: 600;
    letter-spacing: .2rem;
    line-height: 17vh;
}
.animateIn {
  animation-name: accordionIn;
  animation-duration: 0.65s;
  animation-iteration-count: 1;
  animation-direction: normal;
  animation-timing-function: ease-in-out;
  animation-fill-mode: both;
  animation-delay: 0s;
}
.animateOut {
  animation-name: accordionOut;
  animation-duration: 0.75s;
  animation-iteration-count: 1;
  animation-direction: alternate;
  animation-timing-function: ease-in-out;
  animation-fill-mode: both;
  animation-delay: 0s;
}
.option_yc{
    box-sizing: border-box;
    padding: 30px;
    width: 33%;
}
.option_yc dd{
    padding: 20px 0;
}
.option_yc img{
    width: 60%;
    /* margin: 0 auto; */
    top: 50%;
    transform: translateY(50%);
    transition: 1.3s;
    animation-timing-function: ease-in-out;
}
.option_img:hover img{
    transition: 1.3s;
    /* transform: scale(1.2); */
    transform: translateX(10%);
}
.option_img:hover .circle1{
    transition: 1s;
    animation-timing-function: ease-in-out;
    opacity: 1;
    transform: scale(3);
}
.color1{
    background-color: #aab9c0;
}
.color2{
    background-color: #687989;
}
.color3{
    background-color: #bea17f;
}
.t_center{
    text-align: center;
}
.t_left{
    text-align: left;
}
.d_left{
    z-index: 3;
    top: 5vh;
    left: 20vw;
}
.d_right{
    z-index: 3;
    top: 5vh;
    right: 20vw;
}
.t_title h4{

}
.topic5{
    margin-bottom: -100px;
}
/* .showme
{
    opacity:0;
} */
@media only screen and (min-width: 1201px) {
    .topic1 .option_img, .topic3 .option_img{
        display : none;
    }
}
@media only screen and (min-width : 768px) and (max-width : 1200px) {
    .option_yc{
        width: 50%;
    }
    .topic4 .option_img{
        display : none;
    }
}
@media only screen and (max-width : 815px){
    .t_content{
        width: 90%;
    }
    .circle{
        width: 15vw;
        height: 15vw;
    }
    .circle h2{
        font-size: 1rem;
        line-height: 15vw;
    }
}
@media only screen and (min-width : 481px) and (max-width : 767px) {
    .option_yc{
        width: 100%;
        padding: 30px 20px;
    }
    /* .t_content{
        width: 85%;
    } */
    .t_title .t_title_slogan{
    }
}
@media only screen and (max-width : 550px) {
    .d_left, .d_right{
        left: 0;
        /* margin: auto 0; */
        text-align: left;
        top: 50%;
        transform: translateY(-50%);
    }
    .t_title h2{
        padding-left: 8vw;
        font-size: 1.3rem;
    }
    .t_title h4{
        padding-left: 8vw;
        font-size: 1.1rem;
    }
    .t_content{
        width: 90%;
        /* padding: 8px; */
    }
    .topic .option_img{
        display: none;
    }
    .option_yc{
        width: 100%;
        padding: 20px 0 ;
    }
}

/* @media only screen and (max-width : 480px) {
    .t_box{
        flex-direction: column;
    }
    .option_yc{
        width: 100%;
        padding: 0;
    }
} */
</style>
<body>
        <section class="topic topic1 m-t-60-rwd">
            <div class="t_title relative">
                <div class="d_left absolute t_center t_title_slogan">
                    <h2>行李箱大小尺寸最重要</h2>
                    <hr>
                    <h4>先從天數,季節下手</h4>
                </div>  
                <!-- <img src="./images/cart_bg_01.jpg" alt="">  -->
                <img src="./images/scenario-p-01-yc.jpg" alt="">
            </div>
            <div class="t_box flex hide">
                <dl class="t_content flex">
                    <div class="option_yc t_center object">
                        <dt class="circle color1 object"><h2>短天數</h2></dt>
                        <dd class="object"><h3>1~3天<br>夏日衣物</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">登機箱的規格是<strong>約20吋以下，適合旅行期間約1~3天。</strong><br>
                        登機箱大部分都可以作為隨身行李帶上飛機，可以減少托運行李等待的時間，因此<strong>商務旅行</strong>時使用十分方便；在<strong>短期旅行</strong>或平時外出東西比較多時，也可使用登機箱；登機箱可以說是眾多場合皆可使用的萬用行李箱！
                        </dd>
                    </div>
                    <div class="option_yc t_center object">
                        <dt class="circle color2"><h2>中天數</h2></dt>
                        <dd class="object"><h3>4~7天<br>春秋出遊</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">中型行李箱大<strong>約22~27吋左右，適合旅行期間約4～7天。</strong><br>
                        在機場常見的行李箱尺寸就是中型行李箱，無論是短途旅行或是中長途旅行，皆可以使用中型行李箱；中型行李箱可以有較充裕的空間，因此在衣服很佔空間的<strong>冬季旅行</strong>、或去較為寒冷的地方，也能很安心的攜帶厚重的保暖衣物。
                        </dd>
                    </div>
                    <div class="option_yc t_center object">
                        <dt class="circle color3 object"><h2>長天數</h2></dt>
                        <dd class="object"><h3>7天以上<br>冬天賞雪 血拚採購</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">大型行李箱<strong>約28吋~32吋，適合旅行期間約在7天以上。</strong><br>
                        適用<strong>長途旅行</strong>或將<strong>兩人份的行</strong>李裝在一起托運，尤其適合需要帶大量衣物的人們。但是體積和重量相對來說較大且重；因此身材嬌小的女性或年長者，在拉行李箱時可能會比較辛苦。此外也需要避免因放過多行李，而造成<strong>行李箱過重</strong>的情況。

                        </dd>
                    </div>
                    <div class="option_yc t_center relative option_img">
                        <div class="object circle1 color1 relative"></div>
                        <img class="object relative" src="./images/scenario-p-05-yc.png" alt="">
                    </div>
                </dl>
            </div>
        </section>
        <section class="topic topic2">
                <div class="t_title relative">
                    <div class="d_right absolute t_center">
                        <h2>行李箱材質怎麼選</h2>
                        <hr>
                        <h4>有軟有硬,個人需求</h4>
                    </div> 
                    <!-- <img src="./images/cart_bg_01.jpg" alt="">  -->
                    <img src="./images/scenario-p-02-yc.jpg" alt="">    
                </div>
                <div class="t_box flex hide">
                    <dl class="t_content flex">
                        <div class="option_yc t_center">
                            <dt class="circle color1 object"><h2>ABS</h2></dt>
                            <dd class="object"><h3>防刮首選</h3></dd>
                            <hr class="object">
                            <dd class="t_left object">常見材質，有<strong>耐刮</strong>且容易修補的特性，但<strong>彈性差</strong>。<br>
                            熱真空成型，硬殼有內裡，其內部的質感比較精緻、箱體表面變化多，比布箱更耐衝擊。因為有箱框,相較之下<strong>重量較重</strong>,但可保護衣物不易變皺,易碎品也不易損壞。
                            </dd>
                            <dd class="t_left object">
                            如果在長時間-20度以下的境境會加速材質硬化，導致不當的人為搬運而破裂機率提高，去<strong>較寒冷國家</strong>的朋友，不建議攜帶ABS材質的行李箱。
                            </dd>
                        </div>
                        <div class="option_yc t_center">
                            <dt class="circle color2 object"><h2>PC</h2></dt>
                            <dd class="object"><h3>輕盈亮眼首選</h3></dd>
                            <hr class="object">
                            <dd class="t_left object"><strong>耐冷熱</strong>，抗衝擊性能好，折射率高加工性能好，<strong>韌性及吸震能力優</strong>，能有效的緩衝衝擊力道，並且不易破損。<br>  
                            純PC材質行李箱表面當然也是<strong>鏡面</strong>，整體視覺高，而且能印刷各式圖案，呈現多種色彩且不易脱落，在歐美國家盛行於行李箱上製各式花色圖案，顧客只需要將保護膜撕掉，就可以完整看到整體透亮度。
                            </dd>
                            <dd class="t_left object">
                            市場價格上，PC材質會比其他塑膠材料來的高，整體綜合性能來說目前算是<strong>行李箱材質中的佼佼者</strong>。
                            </dd>
                        </div>
                        <div class="option_yc t_center">
                            <dt class="circle color3 object"><h2>布箱</h2></dt>
                            <dd class="object"><h3>收納首選</h3></dd>
                            <hr class="object">
                            <dd class="t_left object">柔韌性，多層次外袋，耐摔耐撞耐壓。<br>
                            <strong>外部收納口袋</strong>是軟箱最大特色，且箱體深、收納較不受限，打開放置在旅館內不佔空間。缺點是若無保護套收納，纖維易卡髒污，不似硬箱易於清潔保養。另外如果購買布面沒有抗汙、抗潑水處理的軟箱，潑到飲料或下雨時表布淋濕，會比較令人頭痛。
                            </dd>
                            <dd class="t_left object">
                            由於方便收納體積大的物品，且打開不佔空間，是住宿旅館空間小或出國<strong>愛買族的首選</strong>。但是防盜與保護行李的功能相對差一點。
                            </dd>
                        </div>
                        <div class="option_yc t_center">
                            <dt class="circle color2 object"><h2>鋁鎂合金</h2></dt>
                            <dd class="object"><h3>保護首選</h3></dd>
                            <hr class="object">
                            <dd class="t_left object">是一種在鋁合金中加入鎂金屬的合金，擁有<strong>金屬強度和硬度</strong>，但<strong>重量比較輕</strong>，跟塑膠很接近。<br>
                            鋁鎂合金的行李箱<strong>保護性相當好</strong>，是講究內裝物不希望受到任何擠壓的旅人最佳選擇之一，只是重量要比其他材質重上許多，常常占掉很大托運重量，另外箱體經過撞擊容易凹損、刮傷，維修較難恢復原貌也是在選擇鋁鎂合金行李箱時需要考量的。
                            </dd>
                            <dd class="t_left object">
                            此材質行李箱如有受到強大撞擊凹陷時<strong>變形不易回復</strong>修理,在維修的層面上較難完整修復。
                            </dd>
                        </div>
                        <div class="option_yc t_center">
                            <dt class="circle color3 object"><h2>ABS+PC</h2></dt>
                            <dd class="object"><h3>耐用首選</h3></dd>
                            <hr class="object">
                            <dd class="t_left object"><strong>結合ABS高硬度高強度和PC耐冷熱及高韌性的特性</strong><br>
                            此材質在生活較常見於汽車內部零件與大型機具零件外殼等,市場上ABS+PC材質行李箱屬於較符合消費者需求的材質,因為有較佳的彈性、韌性及硬度、及吸震能力,因此能夠緩衝受撞擊的力道,使箱差不易破損,也能適當保護行李箱內部物品。  
                            </dd>
                            <dd class="t_left object">
                            如果是<strong>大尺寸行李箱</strong>可以考慮PC混ABS，同時擁有<strong>耐刮又耐摔</strong>的特點，推薦給<strong>追求耐用又念舊</strong>的人。    
                            </dd>
                        </div>
                        <div class="option_yc t_center relative option_img">
                            <div class="object circle1 color1 relative"></div>
                            <img class="object relative" src="./images/scenario-p-05-yc.png" alt="">
                        </div>
                    </dl>
                </div>
        </section>
        <section class="topic topic3">
            <div class="t_title relative">
                <div class="d_left absolute t_center">
                    <h2>好推好拉才是王道</h2>
                    <hr>
                    <h4>順手滑溜最輕鬆</h4>
                </div> 
                <img src="./images/scenario-p-03-yc.jpg" alt="">    
            </div>
            <div class="t_box flex hide">
                <dl class="t_content flex">
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"><h2>輪座</h2></dt>
                        <dd class="object"><h3>萬向|單向</h3></dd>
                        <hr class="t_left object">
                        <dd class="t_left object"><strong>單向：顧名思義就是輪座是固定的</strong>。只能直線向前或向後拉。<br>
                        ⼀般常⾒是使⽤在小旅⾏箱上 且⼤部分都是以兩輪為主。單向輪的優點是輪⼦直徑通常較⼤，拖拉起來會比較省⼒；缺點是只靠後兩輪拖⾏，容易造成輪皮磨損。
                        </dd>
                        <dd class="t_left object"><strong>萬向：就是俗稱的360度輪，各個⽅向都好拉。</strong><br>
                        萬向輪的優點在於可以四個輪⼦同時著地平推，有需要時也可以後兩輪著地斜拉 可依照載重及地形改變⾏⾛⽅向；缺點是若是輪⼦氧化或磨損，維修更換時需要相對較⾼的費    
                        </dd>
                        <dd class="t_left object">小提醒：測試輪子穩定度的方法為把箱子拿起來懸空或者橫放並撥動輪子，檢查輪子空轉時是否左右晃動。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color2 object"><h2>外觀</h2></dt>
                        <dd class="object"><h3>飛機輪|單輪</h3></dd>
                        <hr class="t_left object">
                        <dd class="t_left object"><strong>飛機輪：崎嶇地形也好推<br></strong>
                        優點是相同載重下，飛機輪更<strong>好推拉</strong>，尤其箱體斜拉時，輪子也不會成"內八"狀態，造成單一邊快速磨損。缺點則是一半的輪胎較薄易卡縫、迴轉半徑大需要用力。目前尺寸最大有直徑6cm的，較省力滑順。
                        </dd>
                        <dd class="t_left object"><strong>大四輪：最常見<br></strong>
                        優點是可在<strong>凹凸不平的路</strong>上躲過障礙、輕便、推拉靈活。附帶⼀提，其實單輪跟⾶機輪都有分等級，並不⼀定是⾶機輪就比較好，單輪就比較差，像是單輪中也有非常有名超級好的，⽇本<strong>HINOMOTO</strong>的輪⼦，滑順耐磨。 
                        </dd>
                        <dd class="t_left object">
                        附註：有軸承的輪⼦，載重時輪⼼比較耐磨，但是相對的輪⼦整體重量會較重。但是沒有軸承的話，耐重性會降。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"><h2>各式<br>種類</h2></dt>
                        <dd class="object"><h3>環保｜剎車｜輔助</h3></dd>
                        <hr class="t_left object">
                        <dd class="t_left object">PU環保輪<br>
                        此種胎皮其特性較有彈性而且避震效果更好也比較靜⾳。因各大廠牌為符合美國歐洲規範均使用PU環保輪，但是⻑時間靜置不⽤的話，一定會有<strong>氧化的情形胎皮會斷裂或脫落！</strong>。
                        </dd>
                        <dd class="t_left object"><strong>煞車輪<br></strong>
                        煞⾞輪的⽤意就是可以讓輪⼦鎖住，使⾏李箱達到穩定站立的效果，就不會隨意亂滑，非常適合拖著⾏李箱坐公⾞、捷運、火⾞、⾼鐵等⼤眾運輸⼯具的喜愛<strong>自由行者</strong>。
                        </dd>
                        <dd class="t_left object">輔助輪<br>
                        有了輔助輪的設計，不管是崎嶇路⾯或是斜坡，都能幫助減輕主輪負擔，也讓使⽤者斜拉時更省⼒， 還可以使箱體底部磨損機率⼤⼤降低。
                        </dd>
                    </div>
                    <div class="option_yc t_center relative option_img">
                        <div class="object circle1 color1 relative"></div>
                        <img class="object relative" src="./images/scenario-p-05-yc.png" alt="">
                    </div>
                </dl>
            </div>
        </section>
        <section class="topic topic4">
            <div class="t_title relative">
                <div class="d_right absolute t_center">
                    <h2>款式鎖頭知多少</h2>
                    <hr>
                    <h4>除了好開更要安全</h4>
                </div> 
                <img src="./images/scenario-p-04-yc.jpg" alt="">    
            </div>
            <div class="t_box flex hide">
                <dl class="t_content flex">
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"><h2>款式</h2></dt>
                        <dd class="object"><h3>拉鍊｜鋁框</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">拉鍊<br>
                        拉鍊箱除了輕量以外，因為沒有鋁框固定所以<strong>運用上較靈活</strong>， 且近年來許多拉鍊箱都推出可以<strong>前開與擴充</strong>的款式，整體來說CP值會比較高。不過，在保護性與<strong>防盜性</strong>方面拉鏈箱比框架箱<strong>差</strong>一些，如果是一般的單層拉鍊，用一支筆就可以戳開。
                        </dd>
                        <dd class="t_left object">鋁框<br>
                        箱體重量重很多,箱體中間為鋁框設計,整體較為堅硬,相對來說,擺放行李時空間較無彈性,<strong>不易被破壞</strong>,但是搬運過程中如鋁框遭受撞擊,有可能導致變形,會有上下蓋無法完全密合之可能性,<strong>不易修復</strong>,維修成本高。
                        </dd>
                        <dd class="t_left object">
                        小提醒：購買鋁框行李箱需額外注意箱扣設計（拉鍊式行李箱沒箱扣），L型箱扣（卡榫式）較會因為箱體遭受碰撞導致變形而無法扣住，而U型箱扣較不會受這問題影響。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"><h2>鎖頭</h2></dt>
                        <dd class="object"><h3>海關鎖｜密碼鎖｜鑰匙鎖</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                        密碼鎖開鎖麻煩，但不用攜帶鑰匙；鑰匙鎖開鎖容易，但鑰匙要保存好；以安全等級來說，<strong>都是防君子不防小人</strong>，所以看個人使用習慣挑選即可。
                        </dd>
                        <dd class="t_left object">TSA海關密碼鎖<br>
                        密碼不易被解，與一般密碼鎖不同地方，多一個鑰匙孔，鎖孔為標準海關鎖規格，海關有萬能鑰匙可開，如遇<strong>檢查不用擔心箱子被破壞</strong>。
                        </dd>
                        <dd class="t_left object">一般密碼鎖<br>
                        密碼不易破解，記得數字即可，無需搭配鑰匙，但如遇檢查可能被海關強力破壞。 
                        </dd>
                        <dd class="t_left object">鑰匙鎖<br>
                        簡單好開，<strong>也有搭配海關鎖的類型</strong>，過關比較不用擔心被破壞。但因鑰匙容易不見，所以不要放口袋（比較容易掉），可以放隨身小包包內。
                        </dd>
                    </div>
                    <div class="option_yc t_center relative option_img">
                        <div class="object circle1 color2 relative"></div>
                        <img class="object relative" src="./images/scenario-p-07-yc.png" alt="">
                    </div>
                </dl>
            </div>
        </section>
        <section class="topic topic5">
            <div class="t_title relative">
                <div class="d_left absolute t_center">
                    <h2>品牌特色不簡單</h2>
                    <hr>
                    <h4>挖掘個人定義，展現自我風格</h4>
                </div> 
                <img src="./images/scenario-p-05-yc.jpg" alt="">    
            </div>
            <div class="t_box flex hide">
                <dl class="t_content flex">
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"></dt>
                        <dd class="object"><h3>AWAY</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            它最大的特點在於只有必需，不帶累贅。首先設計簡潔，聚碳酸酯的外殼強度高、抗衝擊性強，最大的賣點是行李箱內置一塊可移除的 10000 mAh的電池，設有 2 個 USB 接口可供充電。創始人Jen Rubio和Stephanie Korey，兩個人對AWAY的定位是一個生活方式品牌而不是旅行箱品牌。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color2 object"></dt>
                        <dd class="object"><h3>DELSEY</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            始於1946年，法國的標誌性箱包品牌，在箱包領域中象徵著法國優質而卓越的工藝技術，以其可靠性和創新的設計著稱。自1946年年以來，DELSEY陪伴全世界的旅行者的個人和公務出行。七十年來，以其桀驁不馴的法式態度，融合專業技術，工藝，創新，優雅和膽魄，備受全球旅行者的青睞。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"></dt>
                        <dd class="object"><h3>HIDEO WAKAMATSU</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            若松秀夫於1989年發表品牌，其理想使命是「向世界廣泛介紹高水準的功能和精緻美麗的日本設計」。他運用自己留法習藝的豐富經驗，保留日本精緻工藝的精神，並融合歐美創新流行的時尚文化，致力創造為消費者帶來愉悅的產品。LOGO上的紅點表示太陽，也就是日本的象徵，也反映了品牌的精神。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color2 object"></dt>
                        <dd class="object"><h3>HARTMANN</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                                Hartmann的傳奇始自1877年。在人們乘輪船旅行的年代,Hartmann已深受各國高端旅行者的喜愛。Hartmann由多位當代極具影響力的設計師傾力打造,受到商界精英們的擁躉。緊跟旅行方式變革的腳步,不懈追求經典設計與卓越品質——這便是Hartmann引以為傲的傳奇故事。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"></dt>
                        <dd class="object"><h3>CROWN</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            皇冠集團從60年前的一個小小家庭式工廠，唯一的一台中古針車開始，一直發展到現在外銷市場遍及全球。皇冠集團致力於行李箱的開發與設計，一直以來吸收各界技術、應用於旅行箱的生產，現已發展出各種流行、極輕、多功能的旅行箱以滿足多樣變化的旅行需求。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"></dt>
                        <dd class="object"><h3>BRIC'S</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            於1952年創立於時尚之都米蘭至今已有六十多年歷史。並以創意(Creativity)、創新(Innovation)、工藝(Craftsmanship)、專利技術(Know-how)、傳承(Inheritance)為品牌中心思想，近年更將品牌精神延續到日常生活中，傳遞義式優雅及旅行生活的態度，透過時尚配色及最高級的物料，以實用兼具功能的設計表現時尚風格品味。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"></dt>
                        <dd class="object"><h3>PROTECA</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            ACE株式會社創業超過50年，至今依舊堅持在日本國內生產行李箱，取「Protect（守護）」、「Technology（技術）」與「ACE（ACE的經營理念）」的字首成立代表品牌「PROTECA」。PROTECA行李箱呈現出「日本製」獨有的「高品質」與「高設計性」，完美詮釋PROTECA特有的「高機能性」。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"></dt>
                        <dd class="object"><h3>CRUMPLER</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                                來自澳大利亞，生產各式各樣的包。它們的共性是具有超酷的造型，多變的色彩搭配以及良好的防護性，是潮流一族的必然之選。它有獨特的設計和醒目的顏色，用料及手工也是十分講究，特別可靠耐用。其標誌來自於澳洲的原駐民形象，靚麗的色彩和反傳統的時尚造型，代表著無憂無慮，反叛，有點輕狂和搞怪好玩的出位文化。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color2 object"></dt>
                        <dd class="object"><h3>CALPAK</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            這家總部位於加利福尼亞的品牌專注於手提箱，背包和行李袋，它的特點是豐富的獨特和時尚的設計，輕盈耐用及可以讓您輕鬆移動瞬間旋轉的360度輪。 從閃亮的粉紅色硬殼滾輪袋到大理石手提行李箱，俏皮的商品確保CALPAK愛好者始終以時尚的方式旅行。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color2 object"></dt>
                        <dd class="object"><h3>ANTLER</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            專門生產行李箱的英國品牌，其最大特色在擴展式行李箱，順滑滾輪及固定行李腳座等。如今是時尚達人的最愛，創立近百年以來，一直是歐洲尤其是英國上流社會人士的旅遊及商務首選箱包，其在款式設計及箱體材料的創新方向上一直致力於為全球的旅行者提供時尚流行，舒適安全的高品質創新產品。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"></dt>
                        <dd class="object"><h3>INCASE</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            堅持以設計為本，集生活風格和精良技術於一身，不斷為各種箱包提供創新的設計，深入研究各種箱包的特性，再與各個文化層次的創新者協力合作，我們致力於不斷打破常規創造最高品質的背包，滿足現今多樣化的旅行需求。我們的產品看似簡單，卻能將商品的功能性發揮至極，同時讓使用者感到非常方便。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color1 object"></dt>
                        <dd class="object"><h3>BRIGGS & RILEY</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            來自美國，長久以來以創新及超群卓越的產品設計著稱。隸屬於擁有百年歷史的專業旅行箱家族，總部設於美國紐約的Briggs & Riley，專注於提供給旅行者實用且奢華的旅行箱產品，Briggs & Riley柏萊立的全系列商品都擁有100%產品終身功能性保固，即使是因為航空公司所造成的人為損壞也不例外。
                        </dd>
                    </div>
                    <div class="option_yc t_center">
                        <dt class="circle color3 object"></dt>
                        <dd class="object"><h3>RICARDO</h3></dd>
                        <hr class="object">
                        <dd class="t_left object">
                            RICARDO BEVERLY HILLS是一個標誌性的美國品牌，通過充滿目的，誠實和和諧的設計來慶祝加州西海岸的靈感生活方式。從加利福尼亞汲取靈感並融入現代設計影響，我們生產出功能性，巧妙性和引人注目的高品質旅行解決方案。我們受到創造力的驅使，被西海岸所吸引，並痴迷於品質。
                        </dd>
                    </div>

                </dl>
            </div>
            
        </section> 

<script>
$(document).ready(function(){
  $(".t_title").on('click',function(){
  // $(this).find('.t_box').fadeToggle('slow'); //可反覆開合但效果普通
    var box = $(this).next();
    if(box.hasClass('hide')){
      
      box.removeClass('hide'); 
      box.fadeIn('slow');
    } else{
      box.addClass('hide');
    }
    // Animation complete.
  });
});

// $(document).ready(function() {   
//     /* Every time the window is scrolled ... */
//     $(window).on("scroll", function(){   
//         /* Check the location of each desired element */
//         $(document).each( function(){
//             var viewTop = $(document).scrollTop();//可視區頂
//             var viewBottom = viewTop + $(window).height();//可視區底
//             var tags = $('.t_box').find('.object');
//             var i;
//             for (var i = 0; i < tags.length; i++) {
//                 var eTop = tags.offset().top;//每個物件距離頂
//                 var eBottom = eTop + tags.height();//物件底    
//                 var tag = tags[i]; 
//                 if ((eBottom <= viewBottom) && (eTop >= viewTop)){
//                     $(tag).addClass("visible");//成立就變看見
//                 } 
//                 else {
//                     $(tag).removeClass("visible")//沒有就變成fadeOut
//                 }
//             if (eTop< viewTop || eBottom > viewBottom) {
//             // window.console&&console.log('hi no');
//             $(tag).removeClass("visible")//沒有就變成fadeOut

//             }
//             }
//         });     
//     });    
// });

// fadeIn+fadeOut(fadeout不適合文字區塊太長)
$(document).ready(function() {
  $(document).on("scroll", function () {
    var pageTop = $(document).scrollTop(); //用scrollTop去得到目前捲到哪裡
    var pageBottom = pageTop + $(window).height(); //視窗的底 (window is the browser, document is the page inside it.)
    // elems = document.querySelectorAll('.hidden');
    var tags = $('.t_box:not(".hide")').find('.object');
    window.console&&console.log('pageTop',pageTop);
    window.console&&console.log('pageBottom',pageBottom);
    window.console&&console.log('tags.length',tags.length);
    for (var i = 0; i < tags.length; i++) {
        var tag = tags[i];
        window.console&&console.log('$(tag).position()',$(tag).position());
        var eTop = $(tag).offset().top +200;
        if (eTop < pageBottom && eTop > pageTop) { //檢查每一個物件位置頂端有沒有比視窗高（可視區）
          $(tag).addClass("visible");//成立就變看見
          window.console&&console.log('hi yes');
        } 
        else {
            window.console&&console.log('hi no');
            $(tag).removeClass("visible")//沒有就變成fadeOut
        }
        if (eTop < pageTop || eTop > pageBottom) {
          window.console&&console.log('hi no');
          $(tag).removeClass("visible")//沒有就變成fadeOut
        }
    }   
  });
});
</script>   
<?php include __DIR__. '/__html__footer.php' ?>