
<?php require __DIR__. './__connect_db.php' ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
<link rel="stylesheet" href="./css/login.css">
<?php 
// session_start();
$login = isset($_SESSION['user']);

$page_name='detail';
$params = [];

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(isset($_GET['sid'])){
    $params['sid'] = $sid;
}

// $item = isset($_GET['item']) ? intval($_GET['item']) : 0;
// if(isset($_GET['item'])){
//     $params['item']=$item;
// }

$colorsid = isset($_GET['colorsid']) ? intval($_GET['colorsid']) : 0;
if(isset($_GET['colorsid'])){
    $params['colorsid']=$colorsid;
}
//要改成type_sid 先整理資料庫
$l_sql = "SELECT * FROM `lunggage_data` WHERE `SID`=". $sid;
$l_row = $pdo->query($l_sql)->fetch(PDO::FETCH_ASSOC);

// $keyItem = " AND pl.`size`= $item AND cm.`color_sid`= $colorsid ";
$p_sql = "SELECT *, pl.sid product_list_sid  FROM `product_list` pl JOIN `color_mapping` cm ON pl.color_sid=cm.color_sid WHERE pl.`type_sid`=". $sid;
$p_rows = $pdo->query($p_sql)->fetchAll(PDO::FETCH_ASSOC);

//推薦用
$r_sql ="SELECT p.*, c.`size`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` ORDER BY RAND() LIMIT 5";
$r_stmt = $pdo->query($r_sql);
$random = $r_stmt->fetchAll(PDO::FETCH_ASSOC);

// print_r($_SESSION['compare']);
// print_r($p_rows);
// exit;
?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品展示頁</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="./css/style-wawa.css">
    <link rel="stylesheet" href="./css/slick.css">
    <link rel="stylesheet" href="./css/slick-theme.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
<?php include __DIR__. './__navbar.php' ?>
<?php include __DIR__. './__html__label.php' ?>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
        /* * {
            box-sizing: border-box;
        } */
        html {
            font-size: 16px;
        }
        body {
            /*
            font-family: 'Marko One', serif;
            font-family: 'Mukta Malar', sans-serif;
            font-family: 'Merriweather', serif;
            */
            font-family: 'Noto Sans TC', sans-serif;
            font-size: 1rem;
            color: #4d5258;
        }

        /* ---------------------------Wawa tamp */
        .con-1440 {
            max-width: 75%;
            margin: 0 auto;
        }
        .ff-merri{
            font-family: 'Merriweather', serif;
        }
        /* -------------------Rita tamplate special for prdoduct */
        .con-1200 {
            width: 65.5%;
            max-width: 90%;
        }
        .fisrt-size:first-child{
            border-bottom : 3px solid #4d5258;
        }
        .size-click{
            border-bottom: 3px solid #4d5258;
        }
        .circle_out{
            width: 26px;
            height: 26px;
            border-radius: 50%;
            /* border: 1px solid #4d5258; */
        }
        .circle_first:first-child{
            border: 1px solid #4d5258;
        }
        .circle_click{
            border: 1px solid #4d5258;
        }
        .circle{
            width: 20px;
            height: 20px;
            border-radius: 50%;
            border: 1px solid rgb(255, 255, 255);
            /* background: #818E9B; */
        }
        .margin-right20{
            margin-right:20px; 
        }
        .gold-text{
            color: #cfb06d;
        }
        .full-darkbg{
            background: #E1E1E1;
        }
        .full-lightbg{
            background: #F9F6F1;
        }
        .padding-left30{
            padding-left:30px;
        }
        .padding-top10{
            padding-top:10px;
        }
        
        /*--------------------------------------product wrap*/
        .breadcrumbs{
            padding: 15px;
        }
        .breadcrumbs a{
            color: #4d5258;
            font-weight: 600;
        }
        .breadcrumbs a span{
            color: #243b55b3;
            font-weight: 400;
        }
        .product_picouter{
            width: 50%;
            flex-direction: column;
            margin-right: 50px;
        }
        .product_pic img{
            width: 100%;
        }
        
        .product_info {
            width: 50%;
            border: 1px solid #ccc;
            padding: 20px;
        }
        .product_name {
            border-bottom: 1px solid #ccc;
            justify-content: space-between;
            align-items: center;
        }
        .product_name p{
            margin: 10px 0;
            font-weight: 600;
        }
        .product_name a{
            width: 30px;
            height: 30px;
        }
        .product_name img{
            width: 100%;
            object-fit: cover;
        }
        /* slider */
        .product_pic{
            width:400px;
            height: 400px;
            overflow: hidden;
            margin: 0 auto;
        }
        .slider_wrap {
            height: 400px;
            left: 0;
            transition: .5s;
        }
        .slider_wrap li{
            padding-left:30px;
            background-color:#ffffff;
        }
        .slider_wrap li img {
            width: 350px;
            height: 100%;
            object-fit: cover;
            
        }
        .sliderbar {
            justify-content: center;
            align-items: center;
            z-index: 5;
            top: 0;
            bottom: 0;
            width: 35px;
            cursor: grab;
            background-color:#ffffff;
        }
        .arrow_r {
            right: 0;
        }
        .transition{
            transition:.5s;
        }
        /* ---------------此區slick 樣式 */
        .product_picouter>.slick-prev{
            left: 20px;
        }
        /* ------------------------尺寸5種img */
        .product_size ul{
            align-items: flex-end;
        }
        .size-xs{
            width: 25px;
            height: 25px;
        }
        .size-s{
            width: 30px;
            height: 30px;
        }
        .size-m{
            width: 35px;
            height: 35px;
        }
        .size-l{
            width: 40px;
            height: 40px;
        }
        .size-xl{
            width: 45px;
            height: 45px;
        }

        /* ------------------商品右側版 */
        .product_name{
            font-size: 2.2rem;
        }
        .product_size li{
            padding-bottom: 5px;
            /* border-bottom:3px solid #fff; */
        }

        .product_size a{
            color: #4d5258;
        }
        .product_intro{
            margin-top: 10px;
            line-height: 2.5rem;
        }
        .dt-type{
            font-size: 1.3rem;
        }
        .product_color{
            line-height:2.5rem;
        }
        .product_num{
            margin-top: 20px;
        }
        .product_num>.d-flex{
            margin-top: 10px;
        }
        .qty{
            border: 1px solid #ccc;
            border-radius: 3px;
            width: 22px;
            text-align: center;
            margin: 0 5px;
        }
        .product_num input{
            width: 50px;
        }
        .storage{
            margin-left: 20px;
        }

        .product_price{
            text-align: right;
            color: #9b2525;
            font-size: 2.1rem;
            font-weight: 500;
        }
        .product_price span{
            font-size: 1.5rem;
        }
        .product_btn .btn {
            width: 100%;
            /*height: 45px;*/
            border: none;
            background:#b0bdca;
            border-radius: 3px;
            padding: 14px 5px;
            margin: 10px auto;
            color: #fff;
            transition: 0.5s;
            
        }
        .product_btn .btn:hover{
            background:#99aabb;
            color: #395470;
            font-weight: 600;
        }

        .product_btn .btn1 {
            margin-top: 33px;
            background: #2c3e50;
            color: #fff;
        }
        .product_btn .btn1:hover{
            background:#395470;
            color: #cfb06d;
            font-weight: 600;
        }
        /* -----------------------information */
        .middle_border{
            padding-top: 80px;
            padding-bottom: 80px;
        }
        .for-line{
            border-bottom: 1px solid #cfb06d;
            font-size: 2.5rem;
            margin:10px 0 30px 0;
            /*font-weight: 500;*/
        }
        .middle_border p{
            margin:0 0 10px 0;
        }
        .article_infor>.inner_text{
            padding:0 80px 0 80px;
            font-size: 1rem;
            line-height: 1.8rem;
        }
        .detail_img{
            width: 45%;
            /* max-width: 500px; */
            /* height: 400px; */
            margin: 8px;
        }
        .product_detail img{
            width: 100% ;
            object-fit: cover;
        }
        .detail_group{
            /* padding-top: 30px; */
            flex-wrap: wrap;
            
        }
        /* -------------------specifications */
        .left_lung{
            width: 66%;
            justify-content: center;
        }
        .left_lung img{
            width: 75%;
        }
        .right_lung{
            width: 30%;
            background: #838F9B;
            border-radius: 10px;
            display:flex;
            align-items:center;
            justify-content:center;
        }
        .right_lung li:first-child{
            margin-right:50px;
            /* margin-left:20px; */
            font-weight:500;
            font-size:1.2rem;
        }
        .right_lung li{
            color: #F9F6F1;
            line-height: 3rem;
            padding-bottom:5px;
        }
        /* ----------------product_rule */
        .product_rule{
            color: #F9F6F1;
            /*padding-top: 80px;*/
            font-size: 0.9rem;
        }
        .full-imagebg{
            background: url("./images/product_rule.jpg") center bottom no-repeat;
            background-size: cover;
            /*height: 400px;*/
            z-index: -1;
        }
        .cover-bg{
            /*height: 400px;*/
            background: rgba(0, 0, 0, .6);
            z-index: 1;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 40px 0;
        }
        .product_rule ul li{
            padding: 5px 0;
            font-size: 0.9rem;
            color: #ffffffc7;
            line-height: 1.5;
        }
        .product_rule ul>.inner_text{
            font-size: 1.3rem;
            font-weight: 600;
            margin-top: 12px;
            color: #ffffff;
        }

        /* -------------slider bar */
        .recommend{
            margin-top:100px;
        }
        .slider_bar{
            padding:  50px 0;
            align-content: center;
            height:50%;
        }
        .like_pic{
            text-align: center;
        }
        .like{
            margin: 10px 0;
        }
        .slick-slide .like_pic img{
            width: 150px;
            height: 150px;
            margin: 0 auto;
        }
        /* ------------------------slick 樣式 */
        .slick-arrow::before{
            color: #4d5258;
            font-size: 1.5rem;
        }
        .slider_bar>.slick-prev, .slider_bar>.slick-next{
            top: 50%;
        }
        .recommend{
            margin-bottom:-200px;
        }

        /* -------------------------------------------------detail RWD */
        @media screen and (max-width:1120px) {
            .con-1200{
                width: 100%;
            }
            .product_rule ul {
                padding: 0 30px;
            }
        }
            @media screen and (max-width:830px) {
            .product_info{
                padding:10px 20px;
            }
            .middle_border{
                padding-top: 50px;
                padding-bottom: 50px;
            }
            .con-1200{
                    max-width: 95%;
                    padding: 10px;
            }
            .right_lung li{
                margin-right:20px;
            }
            .product_pic ul{
                    position:relative;
                    left:-20px;
                }

            
            @media screen and (max-width:767px){
                .product_wrap {
                    flex-direction: column;
                    align-items: center;
                }
                .product_picouter{
                    width: 100%;
                    margin-right: 0;
                    margin-bottom:30px;
                }
                .product_pic{
                    width:70%;
                }

                .product_info {
                    width: 95%;
                }
            }
            @media screen and (max-width:635px){
                /* -----細節 */
                .detail_img {
                    width: 90%;
                }
                .left_lung{
                    width: 100%;
                }
                .left_lung img{
                    width: 100%;
                }
                .right_lung{
                    width: 90%;
                    height:35%;
                }
                .right_lung li:first-child{
                    margin-right:50px;
                }
                .right_lung li{
                    line-height: 2rem;
                }
            }
            @media screen and (max-width:583px){
                .product_pic{
                    width:85%;
                }
                
            }
            @media screen and (max-width:519px){
                /* ----detail nav重疊 */
                .container{
                    position: relative;
                    top: 60px;
                    max-width: 100%;
                    padding: 0;
                }

                .product_btn .btn1{
                    margin-top: 40px;
                }
                .product_btn .btn{
                    width: 100%;
                    /*height: 40px;*/
                    /*line-height: 30px;*/
                    margin-bottom: 0;
                }
                /* ----商品上方文字區 */
                .product_info{
                    border: none;
                }
                /* ----特色區塊 */
                .spec_group {
                    flex-direction: column;
                    align-items: center;
                }
                .right_lung li:first-child{
                    margin-right:50px;
                }
                /* -----slider 樣式 */
                .slick-next{
                    right: 0;
                }
                .slick-prev{
                    left: -10px;
                    z-index: 5;
                }
                /*--- 0219 沒找到所以新增的地方 ---*/
                .article_infor{
                    padding-bottom: 30px;
                }
            }
            @media screen and (max-width:430px){
                .product_pic{
                    width:90%;
                }
                .slider_wrap li img{
                    left:-8%;
                }
            }
            
            @media screen and (max-width:395px){
            .product_rule{
                padding-top:40px;
            }
            .recommend{
            margin-bottom:-100px;
        }
        }
    }
}
    </style>
</head>

<body>
    <div class="container con-1200 mairgin-0auto">
        <div class="breadcrumbs">
            <a class="nonstyle-a" style="color:#243B55;" href="./index_commodity.php">商品選購 ＞ </a>
            <a class="nonstyle-a" ><span><?=$l_row['brand']?> ＞ </span></a>
            <a class="nonstyle-a" ><span> <?=$l_row['type']?></span></a>
        </div>
        <?php //$rowAll = $b_stmt->fetch(PDO::FETCH_ASSOC) ?>
        <div class="product_wrap d-flex" data-key="" data-type="<?=$l_row['SID']?>">
            <!-- 點選更換圖片區 start -->
            <div class="product_picouter d-flex justify-center">
            <div class="product_pic relative">
            <a id="goPrev" class="d-flex absolute sliderbar arrow_l transition"> <img src="images/prev.png" alt=""></a>
            <a id="goNext" class="d-flex absolute sliderbar arrow_r transition"> <img src="images/next.png" alt=""></a>
            <ul class="nonstyle-ul slider_wrap d-flex absolute" id="sliderWrap">
            <!-- <div><img id="product_image" src="" alt=""></div> -->
            </div>
        </div>
            <!-- 點選更換圖片區 end -->
            <!-- 商品名稱 尺寸 & 詳細說明 start -->
            <div class="product_info relative">
                <div class="product_name d-flex">
                    <p class="ff-mukta"><?= $l_row['brand'] ?></p>
                    <?php if($login==true): ?>
                        <a class="wish" href="javascript:void(0)"><img id="heartShape" src="./images/icon-love-1.svg" alt=""></a>
                    <?php else: ?>
                    <a class="wish open-popup-link" href="#test-popup3"><img src="./images/icon-love-1.svg" alt=""></a>
                    <?php endif; ?>
                </div>
            <!-- login popup start-->
            <div id="test-popup3" class="con-960-px d-flex login-con mfp-hide">
                <div class="login-row d-flex">
                    <div class="mb-3-5 login-picarea d-flex">
                        <div>
                            <h2>會員登入</h2>
                            <h3 class="ff-marko">Member Login</h3>
                            <p>還不是會員嗎?</p>
                            <a href="./member_register.php">立即註冊</span></a>
                        </div>
                    </div>
                    <div class="mb-2-5 login-formarea d-flex">
                        <form method="post" name="loginform3" class="d-flex loginform mb-4-5" onsubmit="return formCheck3()">
                            <div class="m-b-30 login-input-outline">
                                <label for="email" class="fw-300">會員帳號<span style="color:#9e3c3c">*</span></label><br>
                                <input type="text" class="login-input-style" name="email" id="email3"><br>
                                <small id="emailHelp3"></small>
                                <br>
                            </div>
                            <div class="m-b-30 login-input-outline">
                                <label for="password" class="fw-300">登入密碼<span style="color:#9e3c3c">*</span></label><br>
                                <input type="password" class="login-input-style" name="password" id="password3"><br>
                                <small id="passwordHelp3"></small>
                                <br>
                            </div>
                            <div class="d-flex login-link m-y-20">
                                <a href="javascript:">忘記密碼</a>
                            </div>
                            <div>
                                <button type="submit" class="btn-login">登 入</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- login popup end-->
                <div class="product_intro">
                    <ul class="nonstyle-ul p-0 m-0">
                        <li class="dt-type ff-mukta-noto"><?= $l_row['type'] ?></li>
                        <li><?= $p_rows[0]['size_text'] ?></li>
                        <li><?= $l_row['roll'] ?></li>
                    </ul>
                </div>
                <!-- 商品名稱 尺寸 & 詳細說明 end -->
                <!-- 尺寸區 start TODO::php某項商品的所有尺寸 -->
                <div class="product_size d-none">
                    <p>尺寸</p>
                    <ul class="nonstyle-ul d-flex p-0 m-0">
                    <?php $property_types = [];
                    foreach($p_rows as $size){ ?>
                    <?php if( in_array($size['size'],$property_types)){ 
                        continue;
                            } $property_types[]= $size['size'];
                        ?>
                        <li class="margin-right20 text-center click-size" data-key="<?= $size['size'] ?>"><a class="nonstyle-a" href="javascript:" data-key="<?= $size['size'] ?>"><img class="size-xs" src="./images/icon_luggage.svg" alt="">
                            <p class="p-0 m-0"><?= $size['size_text'] ?></p>
                            </a></li>                                             
                <?php  }?>
                    
                    </ul>
                </div>
                <!-- 尺寸區 end -->
                <!-- 顏色區 start TODO::php某項商品的所有顏色 -->
                <div class="product_color">
                    <p>顏色</p>
                    <ul class="nonstyle-ul d-flex p-0 m-0">

                    <?php foreach($p_rows as $k=>$color){ ?>
                    <?php if( in_array($color['color_sid'],$property_types)){ 
                        continue;
                            } $property_types[]= $color['color_sid'];
                        ?>
                        <div class="circle_out d-flex justify-center align-item-center margin-right20 <?= $k==0 ? 'circle_click' : '' ?>">
                        <a class="nonstyle-a click-color" href="javascript:sel_color(<?=$k?>)" data-key="<?= $color['color_sid'] ?>"><li class="circle" data-key="<?= $color['color_sid'] ?>" style="background:<?= $color['color_code']  ?>"></li></a>
                        </div>
                        <?php  } ?>
                    </ul>
                </div>
                <!-- 顏色區 end -->
                <!-- 數量區 start -->
                <div class="product_num">
                    數量
                    <div class="d-flex">
                    <button class="qty qty_decrese">-</button>
                    
                    <input name="productQty" class="productQty text-center" type="text" value="1">
                    <button class="qty qty_increse">+</button>
                    </div>
                </div>
                <!-- 數量區 end -->
                <div class="product_price ff-merri" data-price="<?= $l_row['price']?>"><span>NT$</span><?= $l_row['price']?></div>
                <div class="product_btn">
                    <button class="btn btn1 text-center add_to_cart_btn">加入購物車</button>
                        <?php if( count($_SESSION['compare']) >2 ): ?>
                            <button class="btn text-center" disabled=disabled >比較清單已達上限</button>
                        <?php else: ?>
                        <button class="btn text-center add_to_compare_btn">加入比較清單</button>
                        <?php endif; ?>
                </div>

            </div>
        </div>
        <div class="middle_border">
            <div class="article_infor">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">INFORMATION</p>
                </div>
                <p class="inner_text">
                    <?= $l_row['intro'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="full-darkbg">
        <div class="middle_border con-1200 mairgin-0auto">
            <div class="product_detail">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">PRODUCT DETAIL</p>
                </div>
                <div class="detail_group d-flex justify-center">
                        <?php $images =  $l_row['detail_img'] ;
                                $this_images = explode(";;", $images);
                            for($i=0; $i<count($this_images) ; $i++):
                        ?>
                    <div class="detail_img">
                    <?= '<img src="./images/product/' . $this_images[$i] . '" alt="">' ?>
                    </div>
                    <?php endfor; ?>
                    <!-- <div class="detail_img">
                            <img src="./images/2.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/3.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/4.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/5.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/6.png" alt="">
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="full-lightbg">
        <div class="middle_border con-1200 mairgin-0auto">
            <div class="spec">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">SPECIFICATIONS</p>
                </div>
                <div class="spec_group d-flex justify-center">
                    <div class="left_lung align-item-center d-flex">
                        <img src="./images/img-luggage-sketch.svg" alt="">
                    </div>
                    <div class="right_lung">
                        <ul class="nonstyle-ul">
                            <div class="d-flex">
                                <li class="margin-right20">款式</li>
                                <li><?= $l_row['border'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">顏色</li>
                                <li><?= $p_rows[0]['color'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">材質</li>
                                <li><?= $l_row['texture'] ?></li>
                            </div>
                            
                            <div class="d-flex">
                                <li class="margin-right20">尺寸</li>
                                <li><?= $p_rows[0]['size_text'] ?></li>
                            </div>

                            <div class="d-flex">
                                <li class="margin-right20">重量</li>
                                <li><?= $l_row['kg'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">容量</li>
                                <li><?= $l_row['insideL'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">保固</li>
                                <li><?= $l_row['warranty'] ?></li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="full-imagebg relative">
        <div class="cover-bg relative d-flex">
            <div class="product_rule">
                <ul class="nonstyle-ul">
                    <li class="inner_text">出貨與配送</li>
                    <li>1.在遵守本協議書的前提下，我方將為您供應訂單確認函所列的商品。我方將於5-7個工作日送達，但無法保證一定能在特定日期送達。</li>
                    <li>2.實際到貨時間取決於Samsonite所委託第三方物流供應商之運送時程，而第三方物流之運送時程不時會有變動，恕不另行通知。</li>
                    <li>3.我方委託第三方物流進行送貨。貨物送達時應有人負責簽收。若您無法簽收時，您同意由收件地址有辨別事理能力之代理人代為簽收。</li>
                </ul>
                <ul class="nonstyle-ul">
                    <li class="inner_text">退貨與退款處理</li>
                    <li>1.網購商品時，可在到貨七天內退貨。退貨及退款規範請參閱退款與退換貨。</li>
                    <li>2.如欲辦理退貨，請致電客服專線0800-088-349 轉電商客服中心，時間為週一至週五(例假日除外)，上午9點至下午5點，由專人為您服務或來信。</li>
                </ul>
                <ul class="nonstyle-ul">
                    <li class="inner_text">其他</li>
                    <li>1.6折(含)以下特價商品，恕不提供免費保修服務。</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="recommend_border con-1200 mairgin-0auto">
        <div class="recommend">
            <div class="for-line ">
                <p class="gold-text text-center ff-merri m-b-10">YOU MAY ALSO LIKE</p>
            </div>
            <div class="slider_bar d-flex">
            <?php foreach($random as $like): ?>
                    <?php if($like['brand']!=''): ?>
                <a class="nonstyle-a" href="./product.php?sid=<?= $like['SID'] ?>" style="color:#4d5258;">
                <div class="like_pic">
                <img src="./images/product/<?= $like['pic_nu'] ?>" alt="">
                        <div class="like"><?= $like['brand'] ?><?= "</br>". $like['type'] ?><?= " ". $like['size'] ?></div>
                </div>
                </a>
                <?php else: //除錯用 ?>
                
                    <p>沒有取到資料 請除錯</p>
                        <?php endif; ?>
                        <?php endforeach; ?>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="./js/__nav.js"> </script>
    <script src="./js/slick.min.js"></script>
    <script src="./js/detectmobilebrowser.js"></script>
    <script>
    //變數定義與function定義區
    //切換圖片 與圖片slider
    var $p_rows = <?= json_encode($p_rows); ?>;
    var current_item;
    var product_sid;
        //slider 圖片 --2019/2/16 Rita 更新
        function imgSelect(){
            var imgs_array = current_item['imgs'].split(";;") ;
            var slideNo = 0;
            var slideWidth = 380;
            var slideCount = imgs_array.length ;
            function goToSlide() {
                $("#sliderWrap").css("left", (0 - slideNo * slideWidth));
                };
                $("#sliderWrap").css("width", slideWidth * slideCount );
            for($i=0 ; $i<slideCount ; $i++){
                $("#sliderWrap").append(`<li><img src="images/product/${imgs_array[$i]}" alt=""></li>`) ;
            };
            if(imgs_array.length>2){
                $("#goNext").click(function () {
                    slideNo = slideNo + 1;
                    if (slideNo >= slideCount) slideNo = 0;
                    // console.log(slideNo);
                    goToSlide();
                });
                $("#goPrev").click(function () {
                    slideNo = slideNo - 1;
                    if (slideNo < 0) {
                        (slideNo = slideCount - 1);
                    }
                    goToSlide();
                });
            }else{
                $("#goNext").html('');
                $("#goPrev").html('');
            }
        };
        //切換圖片
        function sel_color(index){
            current_item = $p_rows[index];
            // $('#product_image').attr('src', './images/product/' + current_item['pic_nu'] );
            $(".product_wrap").attr('data-key',current_item['product_list_sid']);
            product_sid = current_item['product_list_sid'];
            checkWishList();
            $('#sliderWrap').html('');
            imgSelect();
        };

    //數量增減
    var amount = $('.productQty');
    var inputValue = parseInt($('.qty_increse').closest('.product_num').find('input').val());
    function countPrice (){
        var subprice = inputValue * parseInt($('.product_price').attr('data-price'));
        $('.product_price').html('NT$' + subprice);
    }
    
    //加入願望清單
    function changeWishlistIcon(inWishList){
            var image = inWishList ? "./images/icon-love-2.svg" : "./images/icon-love-1.svg"; 
            $("#heartShape").attr("src",image);
    };

    function checkWishList(){
        $.get('add_to_wishlist_api.php',{sid:product_sid}, function(data){
            if(data.success) {
                changeWishlistIcon(true); 
                
            } else {
                changeWishlistIcon(false); 
            }
        }, 'json');  
    };
    //2019/02/15 16:30 加入標籤
    function addClick(){
            // window.label.show();
                $('#label').css("border-radius", "25px 0 0 25px");//變成圓形
                $('#label').css("right", "0");

            setTimeout(function (){
                $('#label').attr("disabled",false);
                $('#label').css("border-radius", "5px 0 0 5px");
                $('#label').css("right", "-100px");
            },900);
        };
    //script 啟動區 or 匿名事件直接啟動
    //slider 加上手機版判斷
    if(jQuery.browser.mobile){
            $('.slider_bar').slick({
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 2});
            } else {
                $('.slider_bar').slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3});
    };
    //圖片slider
    sel_color(0);
    imgSelect();
    //顏色選擇
        $('.circle').click(function(){
            $(this).closest('.circle_out').siblings().css('border','1px solid #fff');
            $(this).closest('.circle_out').css('border','1px solid #4d5258');
        });

    //數量增減
    $('.qty_increse').click(function(){
        if(inputValue < 10 ){            
            inputValue = parseInt(inputValue + 1);
            amount.attr('value',inputValue);
        }
        countPrice();
    });
    $('.qty_decrese').click(function(){
        if(inputValue > 1){
            inputValue = parseInt(inputValue - 1);
            amount.attr('value',inputValue);
        }
        countPrice();
    });
    //比較按鈕 與ajax事件
    $('.add_to_compare_btn').click(function(){
        var colorsid = $('.circle_click>a').attr('data-key');
        
        $.get('./add_to_compare_api.php', {sid:product_sid, colorsid:colorsid}, function(data){
            // alert('您已加入比較清單');
            compare_count(data);
            addClick();
        }, 'json');
    });
    //加入購物車 比較 願望清單開始
        //點擊加入購物車
        $('.add_to_cart_btn').click(function(){
            var card = $('.product_wrap');
            var qty = card.find('input.productQty').val();
            $.get('add_to_cart_api.php', {sid:product_sid, qty:qty,add:true}, function(data){
                // alert('商品加入購物車');
                cart_count(data);//泡泡選單
                addClick();
            },'json');
        });
        //加入願望清單
        $('#heartShape').click(function(){
            var add = ($("#heartShape").attr("src") == "./images/icon-love-1.svg")? true: false;
            $.get('add_to_wishlist_api.php',{sid:product_sid,add:add}, function(data){
                if(data.success) {
                   changeWishlistIcon(add);
                }
            }, 'json');
        });
    //加入購物車 比較 願望清單結束
    </script>

<!-- js for login popup-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
    <!-- 登入視窗彈出 -->
<script>    
    // Example 1: From an element in DOM
    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
    // 表單
    var fields = ['email', 'password'];
    var s;
    var info = $('#info');
    var infoWrap = $('#info-wrap');
    var emailHelp = $('#emailHelp3');
    var passwordHelp = $('#passwordHelp3');
    var email = $('#email3');
    var password = $('#password3');


    function formCheck3() {
        var isPass = true;
        var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        for (s in fields) {
            cancelAlert3(fields[s]);
        }


        if (!email_pattern.test(document.loginform3.email.value)) {
            setAlert3('email', 'email格式錯誤');
            isPass = false;
        }

        if (document.loginform3.password.value.length < 6) {
            setAlert3('password', '密碼欄位需6字元以上');
            isPass = false;
        }

        if(isPass){
            $.post('member_login_api.php', $(document.loginform3).serialize(), function (data) {


                if (data.success){
                    location.reload();
                    // setTimeout(function(){
                    //     location.href = './';
                    // }, 1000);
                }else{
                    emailHelp.html(data.info);
                    passwordHelp.html(data.info);
                    email.css('border', '2px solid #9e3c3c');
                    password.css('border', '2px solid #9e3c3c');
                }

                if (data.info){
                    infoWrap.removeClass('d-none');
                    info.html(data.info);

                    setTimeout(function(){
                        infoWrap.addClass('d-none');
                    }, 1500);
                }

            },'json');
        }

        return false;
    }


    // 輸入錯誤通知
    function setAlert3(fieldName, message) {
        $('#' + fieldName).css('border', '1px solid #9e3c3c');
        $('#' + fieldName + 'Help').text(message);
    }

    // 取消輸入錯誤通知
    function cancelAlert3(fieldName) {
        $('#' + fieldName).css('border', '1px solid #858a8f');
        $('#' + fieldName + 'Help').text('');
    }
</script>
<!-- 0214:補上footer -->
<?php include __DIR__ . '/__html__footer.php' ?>