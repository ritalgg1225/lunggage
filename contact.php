<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>聯絡我們</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style-wawa.css">
    <link rel="shortcut icon" href="./images/favicon.png" type="image/x-icon">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

</head>
<?php require __DIR__. '/__connect_db.php'?>
<?php include __DIR__. '/__navbar.php' ?>

<style>
/* -------------------------------------- body---------------------------------------------------*/
@import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
*{
    padding: 0;
    margin: 0;
}
/* html{
    height: 100vh;
    overflow: hidden;
} */
body{
    /* color: rgba(225, 225, 225, .7); */
    background-color: #ffffff;
}
/* -------------group  style-------------------- */
.t_center{
    text-align: center;
    margin:  0 auto;
}
.t_left{
    text-align: left;
}
.t_right{
    text-align: right;
}
.container{
    width: 75vw;
    margin:  0 auto;
}
.relative{
    position: relative;
}
.flex{
    display: flex;
}
/* -----------------------contact------------------------- */
.contact_card{
    color: #243b55;
    /* color: rgba(225, 225, 225, .7); */
    /* top: 20vh; */
    /* background: linear-gradient(166deg, #687989 50%, #243B55 100%); */
    /* background-color: #687989; */
    box-sizing: border-box;
    padding: 20vh 0;
    height: 80vh;
    margin-bottom: -100px;/* /拿掉footer的padding-top */
    
}
.contact_content{
    /* background: linear-gradient(166deg, #687989 50%, #243B55 100%); */
    box-sizing: border-box;
    padding: 10vh 0;
}
.line{
    /* top: 50%; */
    font-size: 1.5rem;
    width: 22rem;
    margin: 0 auto;
    text-align: center;
    border-right: 3px solid #243b55 ;
    /* border-right: 3px solid #bea17f ; */
    /* rgba(225, 225, 225, .7)  */
    transform: rotateY(-50%);
    white-space: nowrap;
    overflow: hidden;
    letter-spacing: .5rem;
}
.contact_info{
    list-style: none;
    padding:  20px 0;
    /* flex-direction: ; */
    flex-wrap: wrap;
}
.contact_title, .contact_number{
    width: 45%;
    padding: 10px;
    line-height: 1.7rem;
}
/* .contact_number{
    width: 65%;
} */
/* animation */
.anim_typewriter{
    animation: typewriter 3s steps(22) 1s 1 normal both,
    blinkTextCursor 700ms steps(22) infinite normal;
    /* animation: typewriter 2.5s steps(11, end),
    blinkTextCursor 3s steps(1) infinite; */
}
@keyframes typewriter{
    from{width: 0;}
    to{width: 22rem;}
}
/* ---------------------social media---------------------- */
.social-media{
    /* padding-top: 20px; */
    flex-direction: row;
    bottom: 15%;
}
.social-media .icon{
    padding: 10px;
    width: 49px;
}
.social-media .icon img{
    width: 100%;
}
@keyframes blinkTextCursor{
    from{border-right-color: rgba(225, 225, 225, .7);}
    to {border-right-color: #243b55;}
}
@media only screen and (max-width : 480px) {
    .container{
        width: 85%;
    }
    .contact_title{
        width: 22%;
        padding: 6px;
    }
    .contact_number{
        width: 55%;
        padding: 6px;
    }
    .line{
    font-size: 1rem;
    width: 16rem;
    /* margin: 0 auto; */
    /* text-align: center; */
    /* border-right: 3px solid #bea17f ; */
    /* white-space: nowrap; */
    /* overflow: hidden; */
    letter-spacing: .3rem;
    font-weight: bold;
    }
    .anim_typewriter{
    animation: typewriter 3s steps(16) 1s 1 normal both,
    blinkTextCursor 700ms steps(16) infinite normal;
    /* animation: typewriter 2.5s steps(11, end),
    blinkTextCursor 3s steps(1) infinite; */
    }
    @keyframes typewriter{
        from{width: 0;}
        to{width: 16rem;}
    }
}
</style>
<body>
<!-- <div class="banner relative">
    <h2 class="absolute">聯絡我們</h2>
</div> -->
<div class=" relative contact_card">
    <div class="contact_content container">
        <p class="line anim_typewriter relative">
            <strong>如有任何問題請聯繫客服</strong>
        </p>
        <ul class="contact_info t_center flex">
            <div class="contact_title">
                <li class="t_right">電話 :</li>
                <li class="t_right">Email :</li>
                <li class="t_right">地址 :</li>
            </div>
            <div class="contact_number">
                <li class="t_left">02-3313-6666</li>
                <li class="t_left">container000@iii.com.tw</li>
                <li class="t_left">台北市大安區復興南路一段390號2樓</li>
            </div>
        </ul>
    </div>    
    <div class="social-media flex relative">
        <div class="t_center flex">
            <div class="icon"><a href=""><img src="./images/icon_line.svg" alt=""></a></div>
            <div class="icon"><a href=""><img src="./images/icon_fb.svg" alt=""></a></div>
            <div class="icon"><a href=""><img src="./images/icon_ig.svg" alt=""></a></div>
        </div>
    </div>
</div>

<?php include __DIR__. '/__html__footer.php' ?>