<!----------------------------------- contain_body php 商品圖片 名字 加入比較 願望清單 一共幾頁-->
<?php 
require __DIR__. '/__connect_db.php';
$page_name = 'contianer_list_data';
$per_page = 16;//一页16笔

//$page -> 用户要看第几页
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
if (isset($page)){
    $perams['page'] = $page;
}

// $cate -> 第几个品牌
// $cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;
// if (isset($cate)){
//     $perams['cate'] = $cate;
// }
// $perams['cate'] = 1;
// $cate=1;

$where = 'WHERE 1';
// if (! empty($cate)){
//     $where.= " AND `brand`='%s' ";
// }

 
// 取得總筆數
$t_sql = " SELECT COUNT(1) FROM lunggage_data $where";
$total_rows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];
$total_page = ceil($total_rows/$per_page); //算整数

// echo $total_page ;

//取得商品资料
$p_sql = sprintf("SELECT * FROM lunggage_data $where LIMIT %s, %s ", ($page-1)*$per_page, $per_page);;
// sprintf("SELECT * FROM lunggage_data $where LIMIT %s, %s ", ($page-1)*$per_page, $per_page);
//($page-1):因为第一页是 0， 所以page-1   $per_page:一个有几笔
$p_stmt = $pdo->query($p_sql);
// $row=$p_stmt->fetch(PDO::FETCH_ASSOC);

?>
<!----------------------------------- contain_body php end-->
<!----------------------------------- contain_body html----->
<div class="contain_body">

<!----------------------------------- 商品列表----->

    <ul class="flex" id="product_list">
    <?php /*
        <?php 
            while(  $row =$p_stmt->fetch(PDO::FETCH_ASSOC)  ):
                     //print_r($row);

                    $pic_order="SELECT * FROM product_list WHERE type_sid ={$row['type_sid']} GROUP BY `type_sid`";
                    $pic_query = $pdo->query($pic_order);
                    $pic_fetch = $pic_query->fetch(PDO::FETCH_ASSOC);
                    $src="./images/product/".$pic_fetch['pic_nu'];
                    
                    // print_r($src);
        ?>
 
                    <li class="animatable fadeInDown">

                        <!-- 商品點擊區塊 -->
                        <div class="list_ani  absolute">
                                <ul class="list_ani_ul ">
                                    <a href="./product.php?sid=<?= $row['SID'] ?>">
                                    <li>
                                            <div class="flex list_ani_search">
                                                <p>商品詳細介紹</p>
                                                <img class="wish" src="./images/icon_sm_search-wi.png" alt="搜尋">
                                            </div>
                                            <p>NT. <?php echo $row['price']; ?> </p>
                                    </li>
                                    </a>
                                    <!-- <a href=""> -->
                                    <li  class="flex">
                                            <p>加入願望清單</p>
                                            <img class="wish" src="./images/icon-love-wi.png" alt="願望">
                                    </li>
                                    <!-- </a> -->
                                    <!-- <a href=""> -->
                                    <li  class="flex add_click">
                                            <p>加入比較列表</p>
                                            <img class="add" src="./images/icon_sm_compare-wi.png" alt="比較"> 
                                    </li>
                                    <!-- </a> -->
                                </ul>
                        </div>
                        <!-- 商品图片 -->
                        <img class="relative" src=<?php echo $src; ?> alt=""> 
                        <!-- <img class="relative" src="./images_shop/AAA0001_02.jpg" alt=""> -->
                    
                        <!-- 商品名字 -->
                        <h6><?php echo $row['brand']; ?></h6>

                        <!-- 商品系列 -->
                        <p class="p_12px_wil"><?php echo $row['type']; ?></p>

                        <!-- 商品颜色 -->
                        <div class="ul-color flex">
                            <div class="li-color"></div>
                            <div class="li-color"></div>
                            <div class="li-color"></div>
                        </div>
                        
                        <!-- 商品价钱 -->
                        <div class="price" style="color:#961900"><p>NT. <?php echo $row['price']; ?> </p></div>

                        <!-- 商品最爱和比较 -->
                        <div class="buy">
                            <div class="buy_box">
                                <img class="wish" src="./images/icon-love-1.svg" alt="">
                                <img class="add" src="./images/icon_sm_compare.svg" alt="">
                            </div>
                        </div>

                        <!--底線-->
                        <div class="buy_border"></div>
                    </li>
                    
        <?php endwhile; ?>
*/ ?>
    </ul> 
</div>

<!---------------------------------------------------- 一共几页 -->
<div class="nav_wi">
    <ul class="page-ul-wi flex" id="pagination_ul">
        
<?php /*

        <?php for( $i=1; $i<=$total_page; $i++ ): 
            $params['page'] = $i;
            ?>
            
        <li class="page-item-wi page_template" >
        <a class="page-link page_num" href="?<?= http_build_query($params) ?>"><?= $i ?></a>
        </li>
        
        <?php endfor; ?>

*/ ?>
    </ul>
    <!---------------------------------------------------- 一共几页  end-->
</div>

