<?php require __DIR__. '/__connect_db.php';
$page_name='cart';
if(! empty($_SESSION['cart'])){
    // 購物車裡要有商品

    // 取得關聯式陣列的 key
    $keys = array_keys($_SESSION['cart']);

    //$sql = "SELECT * FROM `products` WHERE `sid` IN ('18', '20', '7')";
    // $sql = sprintf("SELECT * FROM `lunggage_data` WHERE `SID` IN ('%s')",
    //     implode("','", $keys)
    // );

    // $sql = sprintf(" SELECT * FROM `lunggage_data` JOIN `product_color` ON `lunggage_data`.`SID` = `product_color`.`cosi_id`  WHERE `lunggage_data`.`SID` = 1",
    // implode("','", $keys)

    $sql = sprintf(" SELECT * FROM `product_list`
JOIN `lunggage_data` ON product_list.type_sid=lunggage_data.SID
JOIN color_mapping ON color_mapping.color_sid=product_list.color_sid
WHERE product_list.sid IN ('%s') ",
        implode("','", $keys)
    );

    $stmt = $pdo->query($sql);

//     var_dump($stmt);
//     exit;
    $cart_data = []; // 存放商品資料和數量
    // $cart_data = 1; // 存放商品資料和數量

    while($r = $stmt->fetch(PDO::FETCH_ASSOC)){

        // 把商品的數量, 設定給該項目的 qty 屬性
        // $r['qty'] = $_SESSION['cart'][$r['SID']];

        // $cart_data[$r['SID']] = $r;
        // 2019/2/16 Rita更新
        $r['qty'] = $_SESSION['cart'][$r['sid']];

        $cart_data[$r['sid']] = $r;
    }

//    // 依照用戶加入的順序去取得商品資料
//    foreach($keys as $k){
//        print_r( $cart_data[$k] );
//    }


}
?>
<?php include __DIR__. '/cart_head.php' ?>

    <div class="bgImgDC">
        <!-- 小0特區HEADER -->
        <?php include __DIR__. '/__navbar.php' ?>
        <!-- 流程進度圖 -->
        <?php include __DIR__. '/cart_step2.php' ?>
        <!-- wrapper -->
        <?php include __DIR__. '/cart_content2db.php' ?>
            <!-- WA特區FOOTER -->
            <?php include __DIR__. '/__html__footer_DC.php' ?>
    </div>
